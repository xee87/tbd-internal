<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//Auth::routes(['register' => false]);

Route::get('/' ,'LandingController@index')->name('landing');
Route::get('/home' ,'LandingController@index2')->name('landing2');
Route::get('/n-subscription' ,'LandingController@newsubscrption')->name('newsubs');


Route::post('/', 'ContractorController@store')->name('contract_store');

Route::get('/whyus', 'LandingController@aboutus')->name('about_us');
Route::get('/quotes', 'LandingController@quotes')->name('quotes');
Route::any('/save_quotes', 'LandingController@save_quotes')->name('save_quotes');


Route::get('/sample-project', 'LandingController@sampleproject')->name('sample_project');

Route::post('/contactus', 'LandingController@contactmail')->name('mail_contact');

Route::get('/coveragearea', 'LandingController@coveragearea')->name('coverage_area');

Route::get('/contactus', 'LandingController@contactus')->name('contact_us');
/*Route::prefix('/project')->middleware('auth','role:Admin|DEO')->group(function(){

});*/
Route::get('/user-register', 'RegistrationController@registerUser')/*->middleware('auth', 'role:User|Admin')*/->name('r_user');
Route::post('/user-register', 'RegistrationController@store')/*->middleware('auth', 'role:User|Admin')*/->name('r_user_store');

Route::get('/user_login', 'RegistrationController@user_login')->name('user_l');
Route::post('/user_login', 'RegistrationController@user_store')->name('user_l_store');

Route::get('logout', 'Auth\LoginController@logout')->name('logout');

//Route::get('/search' ,'LandingController@search')/*->middleware('auth', 'role:User|Admin')*/->name('search');
Route::any('/search' ,'LandingController@search_filter')->name('s_filter');
Route::post('/ajax_search' ,'LandingController@ajax_search')->name('ajax_search');
Route::post('/s_save_filter' ,'LandingController@s_save_filter')->middleware('auth', 'role:User|Admin')->name('s_save_filter');

Route::get('/otp/{id}' ,'LandingController@otp_set')->name('otp_set');

Route::get('/otp_single/{id}/{key}/{qtoprice}' ,'LandingController@otp_set_single')->name('otp_set_single');
Route::get('/grab_all_files/{id}/{docs}' ,'LandingController@grab_all_files')->name('grab_all_files');

Route::get('/subplan' ,'LandingController@subplan')->name('subplan');
Route::get('/productdetail/{id}' ,'LandingController@project_detail')->name('pdetail');
Route::get('/productdownload/file/{id}/id/{key}/{proamount}' ,'LandingController@project_download')->name('p_download')->middleware('role:User|Admin','auth');

Route::get('/user_record' ,'LandingController@user_record')->name('user_record')->middleware('role:User|Admin','auth');

Route::get('/plans', 'PlanController@index')/*->middleware('auth', 'role:User|Admin')*/->name('plans');
//Route::get('/plans', 'PlanController@index')->middleware('auth', ['except' => ['User','Admin']])->name('plans');
// Route::get('/plan/{plan}', 'PlanController@planshow')->name('show')->middleware('role:User|Admin','auth');
Route::get('/braintree/token', 'BraintreeTokenController@index')->name('token');

Route::post('/subscription', 'SubscriptionController@create')->name('subscription.create')->middleware('role:User|Admin','auth');
Route::post('/rp', 'SettingsController@rp')->name('rp')->middleware('role:User|Admin','auth');

Route::post('/otp_set', 'SettingsController@otp_sett')->name('otp_sett')->middleware('role:User|Admin','auth');
Route::get('test-email', 'JobController@processQueue');
Route::any('/saveplan', 'ProjectController@saveplan')->name('save_plan');

Auth::routes(['verify' => true]);
// Route::get('/dashboard', 'HomeController@index')->name('home');
Route::prefix('/dashboard')->middleware('is_user')->group(function(){

    Route::get('/', 'HomeController@index')->name('home');
    Route::post('/', 'HomeController@index')->name('hometwo');


    // UserManagement
    Route::get('/customer', 'UserController@clients')->middleware('role:Admin','auth')->name('client_view');
    Route::prefix('/user')->middleware('role:Admin','auth')->group(function(){
        Route::get('/', 'UserController@index');

        Route::get('/', 'UserController@users')->name('user_view');
        Route::get('/add', 'UserController@create')->name('user_add');
        Route::post('/add', 'UserController@store')->name('user_create');
        Route::get('/delete/{id}', 'UserController@destroy')->name('user_delete');
        Route::get('/edit/{id}', 'UserController@edit')->name('user_edit');
        Route::post('/edit/{id}', 'UserController@update')->name('user_update');
        Route::get('/edit_status_active/{id}', 'UserController@status_active')->name('user_status_active');
        Route::get('/edit_status_inactive/{id}', 'UserController@status_inactive')->name('user_status_inactive');
        Route::get('/edit_status_delete/{id}', 'UserController@status_delete')->name('user_status_delete');
    });
    // Category Management
    Route::prefix('/category')->middleware('auth','role:Admin')->group(function(){
        Route::get('/', 'CategoryController@index')->name('cat_view');
        Route::get('/add', 'CategoryController@create')->name('cat_add');
        Route::post('/add', 'CategoryController@store')->name('cat_create');
        Route::get('/delete/{id}', 'CategoryController@destroy')->name('cat_delete');
        Route::get('/edit/{id}', 'CategoryController@edit')->name('cat_edit');
        Route::post('/edit/{id}', 'CategoryController@update')->name('cat_update');
    });

    Route::prefix('/quotes')->middleware('auth','role:Admin')->group(function(){
        Route::any('/', 'LandingController@all_quotes')->name('all_quotes');
        Route::any('/view/{id}', 'LandingController@view_quotes')->name('view_quotes');
    });

    Route::get('/contractor/{id}', 'ContractorController@view')->name('contract_view');

    // Category Management
    // Route::prefix('/subscription')->middleware('auth','role:Admin')->group(function(){
    //     Route::get('/', 'CategoryController@index')->name('cat_view');
    // });
    // Addendum Management
    Route::get('/addendum', 'ProjectController@addendum')->name('addendum_view');
    Route::get('/addendum/add', 'ProjectController@add_addendum')->name('addendum_add');
    Route::post('/addendum/save', 'ProjectController@save_addendum')->name('addendum_save');
    Route::get('/addendum/edit/{id}', 'ProjectController@edit_addendum')->name('addendum_edit');
    Route::any('/addendum/delete/{id}', 'ProjectController@delete_addendum')->name('addendum_delete');
    Route::post('/addendum/edit/{id}', 'ProjectController@update_addendum')->name('addendum_update');

    // Project Management
    Route::prefix('/project')->middleware('auth','role:Admin|DEO')->group(function(){
        Route::get('/', 'ProjectController@index')->name('pro_view');

        Route::get('/add', 'ProjectController@create')->name('pro_add');


        Route::post('/add', 'ProjectController@store')->name('pro_create');
        Route::post('/solicitation/', 'ProjectController@sol_add')->name('solicitation_add');
        Route::post('/solicitation-close/', 'ProjectController@sol_award')->name('solicitation_award');
        Route::get('/delete/{name}/{id}/{type}', 'ProjectController@destroy')->name('pro_delete');
        Route::post('/delete-solicitation', 'ProjectController@delete_solicitation')->name('solicitation_delete');
        Route::get('/edit/{id}', 'ProjectController@edit')->name('pro_edit');
        Route::post('/edit/{id}', 'ProjectController@update')->name('pro_update');
        Route::get('/edit_status_active/{id}', 'ProjectController@status_active')->name('pro_status_active');
        Route::get('/edit_status_inactive/{id}', 'ProjectController@status_inactive')->name('pro_status_inactive');
        Route::get('/edit_status_delete/{id}', 'ProjectController@status_delete')->name('pro_status_delete');
    });

    Route::prefix('/ownerlist')->middleware('auth','role:Admin|DEO')->group(function(){
        Route::get('/', 'ProjectController@owner_list')->name('owner_list');
        Route::get('/ownerlistadd', 'ProjectController@owner_list_add')->name('owner_list_add');
        Route::post('/ownercreate', 'ProjectController@owner_create')->name('owner_create');
        Route::get('/owneredit/{id}', 'ProjectController@owner_edit')->name('owner_edit');
        Route::post('/ownerupdate/{id}', 'ProjectController@owner_update')->name('owner_update');
        Route::get('/ownerdel/{id}', 'ProjectController@owner_del')->name('owner_del');
    });

    Route::prefix('/division')->middleware('auth','role:Admin|DEO')->group(function(){
        Route::get('/', 'DivisionController@index')->name('division_list');
        Route::post('/', 'DivisionController@create_division')->name('division_create');
        Route::get('/edit_status_active/{id}', 'DivisionController@status_active')->name('div_status_active');
        Route::get('/edit_status_inactive/{id}', 'DivisionController@status_inactive')->name('div_status_inactive');
        Route::get('/edit_status_delete/{id}', 'DivisionController@status_delete')->name('div_status_delete');
    });

    Route::prefix('/planholderlist')->middleware('auth','role:Admin|DEO')->group(function(){

        Route::get('/', 'ProjectController@planholder_list')->name('planholder_list');
        Route::get('/planholderadd', 'ProjectController@planholder_add')->name('planholder_add');
        Route::post('/planholdercreate', 'ProjectController@planholder_create')->name('planholder_create');
        Route::get('/planholderedit/{id}', 'ProjectController@planholder_edit')->name('planholder_edit');
        Route::post('/planholderupdate/{id}', 'ProjectController@planholder_update')->name('planholder_update');
        // Route::get('/ownerdel/{id}', 'ProjectController@owner_del')->name('planholder_del');
    });

    // Subscription Management
    Route::prefix('/sub_downloads')->middleware('role:Admin','auth')->group(function(){
        Route::get('/', 'SubscriptionController@sub_downloads')->name('sub_downloads');
        Route::get('/sub_add', 'SubscriptionController@sub_add')->name('sub_add');
        Route::post('/sub_add', 'SubscriptionController@sub_store')->name('sub_create');
        Route::get('/{id}', 'SubscriptionController@sub_downloads_edit')->name('sub_downloads_edit');
        Route::post('/{id}', 'SubscriptionController@sub_downloads_update')->name('sub_downloads_update');
    });

    // Settings Management
    Route::prefix('/settings')->middleware('role:Admin','auth')->group(function(){
        Route::get('/', 'SettingsController@setting_downloads')->name('setting_downloads');
    });

    // QTO Project Management
    Route::prefix('/qtoproject')->middleware('auth','role:QTO')->group(function(){
        Route::get('/', 'ProjectController@qtoview')->name('qto_view');
        Route::get('/qtoview_detail/{id}', 'ProjectController@qtoview_detail')->name('qto_detail_view');
        Route::post('image/upload/{id}','ProjectController@fileStore')->name('qto_upload');
        Route::post('image/delete','ProjectController@fileDestroy');
        // QTO Project Management
        Route::get('/file/{id}/id/{key}', 'ProjectController@downloadqto')->name('qto_download');
        Route::get('/delete/{id}/id/{key}', 'ProjectController@deleteqto')->name('qto_delete');
        Route::post('/qtoview_detail/qtovideo_upload', 'ProjectController@upload_qto_video')->name('qto_video_url');
    });
});

<?php

//namespace App\Http\Middleware;
use Illuminate\Http\Request;


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::post('/user-login/', 'RegistrationController@user_store')->name('user_l_store');
Route::post('/user-register', 'RegistrationController@store')/*->middleware('auth', 'role:User|Admin')*/->name('r_user_store');


//Route::middleware('APIToken')->group(function () {
//    // Logout
//    Route::get('/logout/', 'Auth\LoginController@logout')->name('logout');
//
//});




//Route::apiResourse('/', 'ProjectController');

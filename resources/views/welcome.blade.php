@extends('layout')

@section('content')


    <style type="text/css">

        .newcf {
            position: relative;
            background-color: black;
            /*height: 350px;*/
            min-height: 450px;
            width: 100%;
            overflow: hidden;
            background-color: #4b4b4b;
        }

        .newcf video {
            position: absolute;
            top: 50%;
            left: 50%;
            min-width: 100%;
            min-height: 100%;
            width: auto;
            height: auto;
            z-index: 0;
            -ms-transform: translateX(-50%) translateY(-50%);
            -moz-transform: translateX(-50%) translateY(-50%);
            -webkit-transform: translateX(-50%) translateY(-50%);
            transform: translateX(-50%) translateY(-50%);
        }

        @media (pointer: coarse) and (hover: none) {
            header {
                /*background: url('http://sandbox.thewikies.com/vfe-generator/images/big-buck-bunny_poster.jpg') black no-repeat center center scroll;*/
            }

            header video {
                display: none;
            }
        }


    </style>

    <div class="intro-video ameoba-right-top ameoba-right-pattern" style="">

        <div class="container ">

            <div class="row bottom-top-padding">
                <div class="col-md-6">

                    <div class="my-image-firsrt-landing">
                        {{--<img class="img-responsive" src="images/Banner-Document-graphic.png">--}}
                        <div style="display:inline-block;  height:auto;">
                            <img style="cursor: pointer;" src="images/welcome-tbd.png" data-toggle="modal" data-target="#intro-video-modal" class="zoom img-responsive">
                        </div>
                    </div>
                </div>

                <div class="col-md-6">
                    <h2 class="margin top-heading"> Fastest and Easiest <span class="my-size">way to Bid</span></h2>
                    <p class="sub-con-head hide">Sub-Contractors</p>

                    <div class="heading-points">
                        <h4 class=""><i class="fa fa-check" aria-hidden="true"></i>
                            Takeoff by Division <span style="color: #084887;">(Subcontractors)</span></h4>

                        <h4><i class="fa fa-check" aria-hidden="true"></i>
                            Full Project Takeoff</h4>

                        <h4><i class="fa fa-check" aria-hidden="true"></i>
                            Takeoff on Demand</h4>

                        <h4><i class="fa fa-check" aria-hidden="true"></i>
                            Takeoff Subscription Plans</h4>

                        <h4><i class="fa fa-check" aria-hidden="true"></i>
                            Project Leads, Plans and Specs</h4>
                        <div class="parent-free">
                            <div class="rotate-free">
                                <span class=""><img style="width:85%;" src="images/Free-Tag.png"></span>
                            </div>
                        </div>
                    </div>



                    <div class="my-button-firsrt">
                        <form class="navbar-form" action="{{ route('s_filter') }}" method="post" role="form"
                              enctype="multipart/form-data">
                            @csrf
                            <div class="white-text">
                                <button class=" button home-explore-btn btn orange-button-bg">Explore More
                                    <i class="fa fa-arrow-right button-arrow"></i>
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

            <div class="row hide">
                <div class="col-md-6 video-embed-tbd">
                    <div style="display:inline-block; width:100%; height:auto;">
                        <img src="images/embed-video-tbd.png" data-toggle="modal" data-target="#intro-video-modal" class="img-responsive">
                    </div>
                </div>
                <div class="col-md-6 header-content">
                    <div class="heading">The #1 Takeoff Website</div>
                    <div class="intro">True Bid Data is the fastest and easiet way to bid.</div>
                    <ul class="specs">
                        <li> <i class="fa fa-check-square-o fa-1x"></i>
                            Project Divisions Takeoff
                        </li>
                        <li><i class="fa fa-check-square-o fa-1x"></i>
                            Full Project Takeoff
                        </li>
                        <li><i class="fa fa-check-square-o fa-1x"></i>
                            Takeoff on Demand
                        </li>
                        <li><i class="fa fa-check-square-o fa-1x"></i>
                            Takeoff Subscription Plans
                        </li>

                        {{--<li> <i class="fa fa-check-square-o fa-1x"></i>--}}
                            {{--Improves Accuracy--}}
                        {{--</li>--}}
                        {{--<li><i class="fa fa-check-square-o fa-1x"></i>--}}
                            {{--Saves Time--}}
                        {{--</li>--}}
                        {{--<li><i class="fa fa-check-square-o fa-1x"></i>--}}
                            {{--Increases Profit--}}
                        {{--</li>--}}
                    </ul>
                    <div class="text-left">
                        <div class="btn btn-3d btn-reveal btn-orange large-btn">
                            <i class="fa fa-download"></i>

                            <span><a class="white-text" href="/user-register"> Explore More</a></span>

                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>

    <div id="why-us">

        <div class="bg-blue">

            <div class="container about-us-content">

                <div class="">


                    <div class="row">
                        <div class="col-md-2 col-xs-4"><img class="progress-icons" src="images/win.png">

                            <p class="bg-color-white">Project Leads</p>
                        </div>
                        <div class="col-md-2 col-xs-4"><img class="progress-icons" src="images/home.png">

                            <p class="bg-color-white">Plans & Specs</p>
                        </div>

                        <div class="col-md-2 col-xs-4"><img class="progress-icons" src="images/clipboard.png">

                            <p class="bg-color-white">Quantities</p>
                        </div>
                        <div class="col-md-2 col-xs-4"><img class="progress-icons" src="images/result.png">

                            <p class="bg-color-white">Bid Results</p>
                        </div>
                        <div class="col-md-2 col-xs-4"><img class="progress-icons" src=" images/analysis (1).png">

                            <p class="bg-color-white">Rate Analysis</p>
                        </div>
                        <div class="col-md-2 col-xs-4"><img class="progress-icons" src=" images/bar-chart.png">

                            <p class="bg-color-white">Primavera P6</p>
                        </div>


                    </div>

                </div>
            </div>
        </div>
    </div>

    <div class="for-color hide">
        <div class="container">

            <div class="row">

                <div class="reward-tag">
                    <img src="images/stamp.png">
                </div>


                <div class="line-down ">
                    <span class="sec-heading">What do </span><span class="head-bold">We Offer</span>
                    <div class="green-border">&nbsp;</div>
                </div>

                <div class="offers">

                    <div class="col-md-4 col-xs-6 col-sm-6">
                        <div class="line-content">
                            <img src="/images/icons/win.png" class="offer-sec-img">
                            <div class="break"><h4>Project Leads </h4></div>
                        </div>
                    </div>

                    <div class="col-md-4 col-xs-6 col-sm-6">
                        <div class="line-content">
                            <img src="images/icons/home.png"  class="offer-sec-img">

                            <div class="break"><h4>Plan and Specs </h4></div>
                        </div>
                    </div>

                    <div class="col-md-4 col-xs-6 col-sm-6">
                        <div class="line-content">
                            <img src="images/icons/clipboard.png" class="offer-sec-img">
                            <div class="break"><h4>Quantities </h4></div>
                        </div>
                    </div>

                    <div class="col-md-4 col-xs-6 col-sm-6">
                        <div class="line-content">
                            <img src="images/icons/result.png" class="offer-sec-img">
                            <div class="break"><h4>Bid Results </h4></div>
                        </div>
                    </div>

                    <div class="col-md-4 col-xs-6 col-sm-6">
                        <div class="line-content">
                            <img src="images/icons/analysis.png" class="offer-sec-img">

                            <div class="break"><h4>Rate Analysis </h4></div>
                        </div>
                    </div>

                    <div class="col-md-4 col-xs-6 col-sm-6">
                        <div class="line-content">
                            <img src="images/icons/bar-chart.png" class="offer-sec-img">
                            <div class="break"><h4>Primavera P6 </h4></div>
                        </div>
                    </div>

                </div>

            </div>

        </div>

    </div>

    <div class="quantitytakeoffs ameoba-left-pattern">
        <div class="container">

            <div class="tw0-box">
                <div class="line-down ">
                    <span class="sec-heading">Get </span><span class="head-bold">Quantity Takeoffs</span>
                    <div class="green-border">&nbsp;</div>
                </div>
                <div class="">
                    <div class="row">

                        <div class="col-md-4">
                            <div class="border-first project-takeOff">
                                <div class="search-book"><img style="width: 60%;" src="images/search-graphic.png"></div>
                                <div class="qto-home"><h2>Buy Project TakeOff</h2></div>
                                <div class="no-sub"><h4>No Subscription Required</h4></div>
                                <form class="navbar-form" action="{{ route('s_filter') }}" method="post" role="form"
                                      enctype="multipart/form-data">
                                    @csrf
                                    <div class="button-search">
                                        <button class="button home-explore-btn btn orange-button-bg">
                                            Buy TakeOff
                                            <i class="fa fa-arrow-right button-arrow" aria-hidden="true"></i>
                                        </button>
                                    </div>
                                </form>

                            </div>
                        </div>

                        <div class="col-md-4 ">
                            <div class="border-gray">
                                <div class="after-searching"><h3>Recommended</h3></div>
                                <div class="book-image"><img src="images/decide-graphic.png" style="width: 55%;"></div>
                                <div class="search-book">
                                    <h4 style="padding-bottom: 5px;">Check Out Our <span class="head-bold">Convenient </span></h4>
                                    <h4>Subscription Plans</h4>
                                </div>
                                <div class="button-searc">
                                    <a href="/plans">
                                        <button class="button button1 btn golden-button-bg">Subscription Plans
                                            <i class="fa fa-arrow-right button-arrow" aria-hidden="true"></i>
                                        </button>
                                    </a>
                                </div>


                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="border-first border-last">
                                <div class="">
                                    <p>
                                        <b>
                                            TakeOff On Demand
                                            <span class="blue-text">(TOD)</span>

                                        </b>
                                    </p>

                                    <div class="" ><img style="width: 60%;" src="images/take-off-demand.png"></div>


                                    <div style="padding-bottom: 19px;" class="no-sub"><h4 style="padding-top: 25px;">Submit Your Plans & Specs to Get</h4><h4><b> Free</b> Quote Today</h4></div>



                                </div>

                                <div class="button-search">
                                    <a href="/quotes">
                                        <button style="background-size: cover;" class="button home-explore-btn btn blue-button-bg">Get Quote
                                            <i class="fa fa-arrow-right button-arrow" aria-hidden="true"></i>
                                        </button>
                                    </a>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

            <div class="row features-ribon hide" id="mobilediv" style=" margin-top:2% !important;">


                <div class="col-lg-2 col-sm-4 col-xs-6">

                    <center><img src="images/search icon 2100.png">

                        <h3><b>Project Leads</b></h3>
                        <h4 style="color:#ff7829">We Provide</h4></center>

                </div>

                <div class="col-lg-2 col-sm-4 col-xs-6">
                    <center><img src="images/search icon 3100.png">

                        <h3><b>Plans & Specs</b></h3>
                        <h4 style="color:#ff7829">We Have</h4></center>
                </div>

                <div class="col-lg-2 col-sm-4 col-xs-6">
                    <center><img src="images/search icon 4100.png">

                        <h3><b>Quantities</b></h3>
                        <h4 style="color:#ff7829">We Provide</h4></center>
                </div>

                <div class="col-lg-2 col-sm-4 col-xs-6">
                    <center><img src="images/search-icon-1100.png">

                        <h3><b>Bid Results</b></h3>
                        <h4 style="color:#ff7829">We Provide</h4></center>
                </div>

                <div class="col-lg-2 col-sm-4 col-xs-6">
                    <center><img src="images/search-icon_3100.png">

                        <h3><b>Rate Analysis</b></h3>
                        <h4 style="color:#ff7829">We Provide</h4></center>
                </div>


                <div class="col-lg-2 col-sm-4 col-xs-6">
                    <center>
                        <img src="images/project-sceduling.png">
                        <h3><b>Primavera P6</b></h3>
                        <h4 style="color:#ff7829">We Provide</h4>
                    </center>
                </div>

            </div>
            {{--<div class="ameoba-right-pattern">&nbsp;</div>--}}
        </div>
    </div>

    <div class="ameoba-right-pattern">
        <div class="container">
            <div class="">

                <div class="three-box">
                    <div class="line-down ptb-60">
                        <span class="head-bold">List Yourself </span><span class="">With Us</span>
                        <div class="green-border">&nbsp;</div>
                    </div>
                    <div class="row">

                        <div class="col-md-4 col-sm-4 list-r-p">
                            <div class="blue-background listing">
                                <div class=""><h2>General <br> <span>Contractor</span> <br><span style="font-weight: normal">Listing</span></h2></div>
                                <div class="button-cool">
                                    <a id="general_list" data-toggle="modal" data-target="#contractList">
                                        <button style="" class="button button3 btn orange-button-bg">Enlist
                                            <i class="fa fa-arrow-right button-arrow" aria-hidden="true"></i>
                                        </button>
                                    </a>

                                </div>
                            </div>
                        </div>

                        <div class="col-md-4 col-sm-4 list-lr-p">
                            <div class="blue-one listing">
                                <div class=""><h2>SubContractor <br><span style="font-weight: normal">Listing</span></h2></div>
                                <div class="button-mid">
                                    <a href="" id="sub_list" data-toggle="modal" data-target="#contractList">
                                        <button style="" class="button button3 btn orange-button-bg">Enlist
                                            <i class="fa fa-arrow-right button-arrow" aria-hidden="true"></i>
                                        </button>
                                    </a>

                                </div>
                            </div>
                        </div>

                        <div class="col-md-4 col-sm-4 listing list-l-p">
                            <div class="blue">
                                <div class=""><h2>Trade <br> <span>Contractor</span> <br><span style="font-weight: normal">Listing</span></h2></div>
                                <div class="button-cool">
                                    <a href="" id="trade_list" data-toggle="modal" data-target="#contractList">
                                        <button style="" class="button button3 btn orange-button-bg">Enlist
                                            <i class="fa fa-arrow-right button-arrow" aria-hidden="true"></i>
                                        </button>
                                    </a>

                                </div>
                            </div>
                        </div>


                    </div>

                </div>

                {{--<hr>--}}
                <div class="row hide" style="text-align:center">

                    <div class="col-lg-4 col-sm-6 list-r-p">
                        <h3 >
                            <span style="border:2px solid #ff7829; color:#ff7829; padding: 8px 14px 8px 14px; "> G </span>
                            eneral Contractor Listing
                        </h3>
                        <br>

                        <p><i>Click here to list yourself with us</i></p>
                        <a href="" id="general_list" data-toggle="modal" data-target="#contractList" style="text-decoration: none;color:black">
                            <i class="fa fa-check-circle" style="color:#ff7829"></i>
                            Click Here
                        </a>

                    </div>

                    <div class="col-lg-4 col-sm-6 list-lr-p">

                        <h3><span style="border:2px solid #ff7829; color:#ff7829; padding: 8px 14px 8px 14px; ">S</span>ubcontractor Listing</h3>
                        <br>

                        <p><i>Click here to list yourself with us</i></p>
                        <a href="" id="sub_list" data-toggle="modal" data-target="#contractList" style="text-decoration: none;color:black">
                            <i class="fa fa-check-circle" style="color:#ff7829"></i>
                            Click Here
                        </a>
                        {{--<input placeholder="Address" type="text"--}}
                        {{--class="form-control address" id="autocomplete" onFocus="geolocate()"--}}
                        {{--name="address" value=""  minlength="4" maxlength="255" autofocus required />--}}

                    </div>

                    <div class="col-lg-4 col-sm-6 list-l-p">

                        <h3 ><span style="border:2px solid #ff7829; color:#ff7829; padding: 8px 14px 8px 14px; ">T</span>rade Contractor Listing</h3>
                        <br>

                        <p><i>Click here to list yourself with us</i></p>
                        <a href="" id="trade_list" data-toggle="modal" data-target="#contractList" style="text-decoration: none;color:black"><i class="fa fa-check-circle"
                                                                                                                                                style="color:#ff7829"></i> Click Here</a>
                    </div>

                </div>
            </div>
            {{--<hr>--}}
        </div>
    </div>

    <div class="container">
        <div class="row" style="margin: 58px">
            <!--  <center><h2>CLIENTS WE HAVE WORKED WITH</h2></center> -->

            <div class="owl-carousel owl-theme m-t-lg">
                <?php for($i = 1; $i <= 38; $i++):?>
                <div>
                    <img src="images/brands/<?php echo $i?>.png" alt="">
                </div>
                <?php endfor;?>
            </div>

        </div>
    </div>

    <div class="modal fade" id="contractList" tabindex="-1" role="dialog" aria-labelledby="closedLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content card card-info">
                <div class="modal-header card-header">
                    <h4 style="display:inline;" class="modal-title title-list">Contractor Listing</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>
                <form class="contract-form" action="{{ route('contract_store') }}" method="post" role="form" >
                    <div class="modal-body">
                        @csrf
                        <div class="row">

                            <div class="col-lg-6 mb-10">
                                <label>Company Name</label>
                                <input placeholder="Title" type="text"
                                       class="form-control"
                                       name="company_name" value=""  minlength="4" maxlength="255" autofocus required>
                            </div>
                            <div class="col-lg-6 mb-10">
                                <label>Address</label>
                                <input placeholder="Address" type="text"
                                       class="form-control address" id="autocomplete" onFocus="geolocate()"
                                       name="address" value=""  minlength="4" maxlength="255" autofocus required />
                            </div>

                            <div class="col-lg-6 mb-10">
                                <label>Contact Name</label>
                                <input placeholder="Contact Name" type="text"
                                       class="form-control"
                                       name="contact_name" value="" minlength="4" maxlength="255" autofocus required>
                            </div>
                            <div class="col-lg-6 mb-10">
                                <label>Email</label>
                                <input placeholder="Email" type="email"
                                       class="form-control"
                                       name="email" value="" required minlength="4" maxlength="255" autofocus required>
                                <input id="con_type" type="hidden" value="" name="type">
                            </div>

                            <div class="col-lg-6 mb-10">
                                <label>Contact #</label>
                                <input placeholder="Contact" type="text"
                                       class="form-control"
                                       name="contact" value="" required minlength="4" maxlength="255" autofocus required>
                            </div>
                            <div class="col-lg-6 mb-10">
                                <label>Fax #</label>
                                <input placeholder="Fax" type="text"
                                       class="form-control"
                                       name="fax" value=""  minlength="4" maxlength="255" autofocus>
                            </div>

                            <div class="col-lg-6 mb-10">
                                <label>Website</label>
                                <input placeholder="Website" type="url"
                                       class="form-control"
                                       name="website" value=""  minlength="4" maxlength="255" autofocus>
                            </div>
                            <div class="col-lg-6 mb-10">
                                <label>Divisions of Interest</label>
                                <select class="form-control select2" name="csi_division[]" multiple="multiple"
                                        placeholder="Select CSI Division" style="width: 100%;" autofocus>
                                    @if(isset($divisions))
                                        <?php foreach($divisions as $division){ ?>
                                        <option value="{{$division->division_name}}">{{$division->division_name}}</option>

                                        <?php } ?>
                                    @endif
                                </select>
                            </div>

                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn subcontactor-listing-btn orange-button-bg" data-dismiss="modal">Close</button>
                        <button type="submit" style="background-color: #f0542d; border: none;" class="btn subcontactor-listing-btn blue-button-bg">List</button>
                    </div>
                </form>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div>

    @if(isset($successMsg) && $successMsg == 'added')

        <script>
            jQuery(document).ready(function(){

                swal({
                    title: "Listed!",
                    text: "Successfully!",
                    type: "success"

                }).then(function(isConfirm) {
                    if(isConfirm){
                        window.location.href = '/';
                    }
                })
            });
        </script>

    @endif

    <div id="intro-video-modal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content" style="margin-top:20%;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <iframe width="100%" height="315" src="https://www.youtube.com/embed/-phmMgFyGu0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>

    <script type="text/javascript">

        var placeSearch, autocomplete;

        function initAutocomplete() {
            autocomplete = new google.maps.places.Autocomplete(
                    document.getElementById('autocomplete'), {types: ['geocode']});
        }

        function geolocate() {
            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(function (position) {
                    var geolocation = {
                        lat: position.coords.latitude,
                        lng: position.coords.longitude
                    };
                    var circle = new google.maps.Circle({
                        center: geolocation,
                        radius: position.coords.accuracy
                    });
                    autocomplete.setBounds(circle.getBounds());
                });
            }
        }

    </script>

    <script src="http://maps.googleapis.com/maps/api/js?key=AIzaSyD5KPKoO7ZP-grfU1aOx2GD1ra1pQMBdAQ&libraries=places&callback=initAutocomplete"
            async defer>
    </script>

@endsection

@extends('layouts.app')

@section('content')



<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
            </div>
            <div class="col-sm-6">
                <div class="float-sm-right">

                </div>
            </div>
        </div>
    </div>
</section>

<!-- Main content -->
<section class="content">
    <div class="card card-info card-outline">
           <div class="container-fluid mt-2">
            <div class="info-box bg-info">
                <span class="info-box-icon"><i class="fa fa-clipboard"></i></span>
                <div class="info-box-content">
                    <div class="col-md-5 col-sm-6 col-xs-6">
                        <h1>Add Subscription</h1>
                    </div>
                </div>
                <!--<span class="info-box-icon float-right">-->
                <!--    <a class="add-resource" href="{{route('pro_view')}}" title="Add New Group"> <i class="fa fa-list"></i></a>-->
                <!--</span>-->
            </div>
        </div>
         
          
            <form action="{{ route('sub_create') }}" method="post" role="form" enctype="multipart/form-data" style="border:0px;padding:0px 0px;" >
                @csrf
                @if ($errors->any())
                            @foreach ($errors->all() as $error)
                                <div class="alert alert-danger mb-2" role="alert">
                                    {{ $error }}
                                </div>
                                @endforeach
                            @endif
                          
                <div class="row " style="padding: 40px;">
                <div class="col-lg-4 mb-3">
                    <label>Package Name</label>
                    <input placeholder="Package Name" type="text" class="form-control{{ $errors->has('pname') ? ' is-invalid' : '' }}"
                        name="pname" required>
                </div>
                 <div class="col-lg-4 mb-3">
                    <label>Package Slug Name</label>
                    <input placeholder="Package Slug Name" type="text" class="form-control{{ $errors->has('psname') ? ' is-invalid' : '' }}"
                        name="psname" required>
                </div>
                 <div class="col-lg-4 mb-3">
                    <label>Braintree id</label>
                    <input placeholder="Braintree id" type="text" class="form-control{{ $errors->has('braintree_id') ? ' is-invalid' : '' }}"
                        name="braintree_id" required>
                </div>
                 <div class="col-lg-4 mb-3">
                    <label>Description</label>
                    <input placeholder="Description" type="text" class="form-control{{ $errors->has('description') ? ' is-invalid' : '' }}"
                        name="description" >
                </div>
                <div class="col-lg-4 mb-3">
                    <label>Cost</label>
                    <input placeholder="Cost" type="text" class="form-control{{ $errors->has('cost') ? ' is-invalid' : '' }}"
                        name="cost" required>
                </div>
                <div class="col-lg-4 mb-3">
                    <label>Downloads</label>
                    <input placeholder="Downloads" type="number" class="form-control{{ $errors->has('download') ? ' is-invalid' : '' }}"
                        name="download" required>
                </div>

                <div class="col-lg-4 mb-3">
                    <label>Max Price</label>
                    <input placeholder="Max Price" type="number" class="form-control{{ $errors->has('max_price') ? ' is-invalid' : '' }}"
                        name="max_price" required>
                </div>

                </div>
                
                <div class="row " >
                
                
                 <div class="col-lg-12 " style="background-color: rgba(0,0,0,.03); padding: 2%">
                              <center>
                                  <button type="submit" class="btn btn-info  " style="padding-left:6% ;padding-right:6% ">Create</button>
                                    
                                </center>
                </div>
                
                 </div>
            </form>
           
       
</div>
</section>



@endsection

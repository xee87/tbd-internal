@extends('layouts.app')

@section('content')
<!-- Content Header (Page header) -->
<div id="MyElement" class="alert alert-success alert-dismissible fade hide " role="alert">
  <h3 style="color:#fff"><strong><i class="fa fa-check" aria-hidden="true"></i></strong> Alert!</h3> 
  <p style="color:#fff">{{Session::get('alertone')}}</p>
  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
  </button>
</div>

<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
            </div>
            <div class="col-sm-6">
                <div class="float-sm-right">

                </div>
            </div>
        </div>
    </div>
</section>

<!-- Main content -->
<section class="content">

   <div class="card card-info card-outline">
        <div class="container-fluid mt-2">
            <div class="info-box bg-info">
                <span class="info-box-icon"><i class="fa fa-clipboard"></i></span>
                <div class="info-box-content">
                    <div class="col-md-5 col-sm-6 col-xs-6">
                        <h1>Register Price Management</h1>
                    </div>
                </div>
                
            </div>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered table-hover">
                    <thead>
                    <tr>
                        <th>Sr#</th>
                        <th>Register Price</th>
                        <th>Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php $countit = 1; ?>

                    @foreach($strprices as $substrprices)


                        <tr>
                            <td>{{$countit}}</td>

                            <td>{{$substrprices->cost}}</td>



                            <td>
                                <div class="btn-group">
                                    <button type="button" class="btn btn-default">Action</button>
                                    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown"
                                            aria-expanded="false">
                                        <span class="caret"></span>
                                        <span class="sr-only">Toggle Dropdown</span>
                                    </button>
                                    <div class="dropdown-menu" role="menu" x-placement="top-start" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(67px, -165px, 0px);">
                                        <a class="dropdown-item" data-toggle="modal" data-target="#myModal" onclick=" document.getElementById('reg_price').value = '{{$substrprices->cost}}' " >Edit</a>

                                    </div>
                                </div>
                            </td>
                        </tr>
                        <?php $countit++; ?>

                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <!-- /.card-body -->
    </div>

<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content" style="margin-top:20%;">
      <div class="modal-header">
         <h4 class="modal-title" >Edit Price:</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
       
      </div>
      <div class="modal-body">
        <form action="{{ route('rp') }}" method="post">

        @csrf
        
        <input id="reg_price" name ="reg_price"  type="text" class="form-control" placeholder="Reg Price">
        <br>
        <center><button type="submit" class="btn btn-info  " style="padding-left:6% ;padding-right:6% ">Update</button></center>

       </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>








   <div class="card card-info card-outline">
        <div class="container-fluid mt-2">
            <div class="info-box bg-info">
                <span class="info-box-icon"><i class="fa fa-clipboard"></i></span>
                <div class="info-box-content">
                    <div class="col-md-5 col-sm-6 col-xs-6">
                        <h1>One Time Payment Management</h1>
                    </div>
                </div>
                
            </div>
        </div>
        <div class="card-body">
            <div class="table-responsive">

                <table  class="table table-bordered table-hover">
                    <thead>
                    <tr>
                        <th>Sr#</th>
                        <th>Project Price Range</th>
                        <th>Otp Complete Project</th>
                        <!-- <th>Otp Single File</th> -->
                        <th>Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php $countit = 1; ?>

                    @foreach($otp_rate as $strotp_rate)


                        <tr>

                            <td>{{$countit}}</td>

                            <td>{{$strotp_rate->price_range}}</td>

                            <td>{{$strotp_rate->otp_project}}</td>



                            <td>
                                <div class="btn-group">
                                    <button type="button" class="btn btn-default">Action</button>
                                    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown"
                                            aria-expanded="false">
                                        <span class="caret"></span>
                                        <span class="sr-only">Toggle Dropdown</span>
                                    </button>
                                    <div class="dropdown-menu" role="menu" x-placement="top-start" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(67px, -165px, 0px);">
                                        <a class="dropdown-item" data-toggle="modal" data-target="#myModaltwo" onclick="change_otp('{{$strotp_rate->id}}','{{$strotp_rate->price_range}}','{{$strotp_rate->otp_project}}','{{$strotp_rate->otp_file}}')" >Edit</a>

                                    </div>
                                </div>
                            </td>
                        </tr>
                        <?php $countit++; ?>

                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <!-- /.card-body -->
    </div>


<script type="text/javascript">

    function change_otp(otpidd , pr , otpp , otpf){


        document.getElementById('otpid').value= otpidd;

        document.getElementById('pricerange').value= pr;

        document.getElementById('otpproject').value= otpp;

        document.getElementById('otpfile').value= otpf;

    }
    

</script>



<div id="myModaltwo" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content" style="margin-top:20%;">
      <div class="modal-header">
         <h4 class="modal-title" >Edit Price:</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
       
      </div>
      <div class="modal-body">
        <form action="{{ route('otp_sett') }}" method="post">

        @csrf
        
        <input id="otpid" name ="otpid"  type="hidden">
        <label>Project price range</label>
        <input id="pricerange" name ="pricerange"  type="number" class="form-control" placeholder="Price range" required><br>

        <label>Otp project price</label>
        <input id="otpproject" name ="otpproject"  type="number" class="form-control" placeholder="Otp project" required><br>

        
        <input id="otpfile" name ="otpfile"  type="hidden" class="form-control" placeholder="Otp file" required>

        <br>
        <center><button type="submit" class="btn btn-info  " style="padding-left:6% ;padding-right:6% ">Update</button></center>

       </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>

    <!-- /.card -->
<script>
    
    var existtwo = '{{Session::has('alertone')}}';
    
    if(existtwo){

document.getElementById('MyElement').classList.add('show');
document.getElementById('MyElement').classList.remove('hide');

    }
  </script>
</section>
<!-- /.content -->
@endsection

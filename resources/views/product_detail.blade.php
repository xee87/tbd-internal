
<?php

use App\Helper\TBD;
//use Session;

$hasSubscription = is_object(auth()->user()) ? auth()->user()->subscribed('main') : false;
$isDEO = is_object(auth()->user()) ? auth()->user()->hasRole('DEO') : false;
$isQTO = is_object(auth()->user()) ? auth()->user()->hasRole('QTO') : false;
$showContent = ($isQTO || $isDEO) ?  false : true;

$QTOFiles = json_decode($project->qto_upload);
$QTOFilePrice = json_decode($project->qto_price);

$SubMessage = '';
$alertView = 'hide';
$needUpgrade = 0;
if(auth()->user()){
    if(!$hasSubscription){
        $SubMessage = 'Buy Quantity Takoffs for this project ';
        $alertView = '';
    }
}else{
    $alertView = 'hide';
}

$bidPhase = TBD::GetSolicitationLabel($project->bid_phase);
//var_dump($arrayphinfo); exit;

$pro = $project;
//var_dump(Session::has('file_download'));
//if(Session::has('file_download')){
//    $file =  Session::get('file_download');
//    var_dump($file);
//    echo Storage::download($file);
//}


?>
@extends('layout')



@section('content')

    <style type="text/css">

        .card {
            box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
            transition: 0.3s;
            width: 40%;
        }

        .card:hover {
            box-shadow: 0 8px 16px 0 rgba(0, 0, 0, 0.2);
        }
    </style>

    <div class="container-fluid productDetail">

        <div id="MyElement" class="alert alert-success alert-dismissible hide" style="background-color:#28a745">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>

            <h3 style="color:#fff"><strong><i class="fa fa-check" aria-hidden="true"></i></strong> Alert!</h3>

            <p style="color:#fff">{{Session::get('alertone')}}</p>

        </div>

        <div class="row projectTitle p-y-md">

            <div class="col-md-8 p-t-sm">
                <h2 style="margin: 0 0 5px;">
                    <b style="color:#084887">{{$project->title}}</b>
                </h2>
                <?php if(!empty($powner->owner_address)){?>
                <h5 class="detail-loc">
                    <i class="fa fa-map-marker loc-icon" aria-hidden="true"></i> &nbsp; {{$powner->owner_address }}
                </h5>
                <?php } ?>
                <?php if(!empty($powner->owner_name)){?>
                <h5 class="detail-name" >
                    <i class="fa fa-user" aria-hidden="true"></i> &nbsp; {{$powner->owner_name}}
                </h5>
                <?php } ?>
            </div>

            <div class="col-md-4">

                <?php /* if($showContent): */?>

                <div class="row fixBtns" style="margin-top: 4%;">

                    <?php if(empty($userprojectrecord) && !is_null($QTOFiles) && !is_null($QTOFilePrice)){?>


                            <!---div class="col-md-4"--->
                    <?php //GRab ALL files removed by client on 5-18-19 word document.?>
                    <?php /*if(!$isZipDownloaded){?>

                            <!---<button class="btn btn-3d btn-reveal btn-orange" data-toggle="modal" data-target="#myModal">
                            <i class="fa fa-download"></i>
                            <span>Grab All Files</span>
                        </button>--->

                    <?php }else{ ?>
                            <!---<a class="btn btn-3d btn-reveal btn-orange" href="{{route('otp_set', $project->id)}}">
                            <i class="fa fa-download"></i>
                            <span>Download ZIP</span>
                        </a>---->
                    <?php } */?>
                            <!---/div--->



                    <?php }else if(!empty($userprojectrecord) && $userprojectrecord->downloads != $userprojectrecord->downloads_used && $project->bid_amount > $userprojectrecord->max_amount && !is_null($QTOFiles) && !is_null($QTOFilePrice)){ ?>


                            <!---div class="col-md-4"--->
                    <?php /* if(!$isZipDownloaded){?>
                            <!----<button class="btn btn-3d btn-reveal btn-orange" data-toggle="modal" data-target="#myModal">
                            <i class="fa fa-download"></i>
                            <span>Grab All Files</span>
                        </button>---->
                    <?php }else{ ?>
                            <!----<a class="btn btn-3d btn-reveal btn-orange" href="{{route('otp_set', $project->id)}}">
                            <i class="fa fa-download"></i>
                            <span>Download ZIP</span>
                        </a>---->
                    <?php } */ ?>
                            <!----/div--->

                    <?php }else { ?>

                    <?php } ?>

                            <!----div class="">

                    <div class="col-md-2"></div>

                    <div class="col-md-10"---->

                    {{--<div class="col-md-2 hidden-xs hidden-sm"></div>--}}

                    <div class="col-md-2 hidden-xs hidden-sm"></div>

                    <div class="{{ (auth()->user())?'col-md-5':'col-md-10'  }} no-padding">
                        <div class="printBtn right-align">
                            <a style="background-color: #555555;" class="downloads-button btn black-button-bg" onclick="printDiv('printableArea')">
                                Print
                                <i class="fa fa-print button-arrow" aria-hidden="true"></i>
                            </a>
                        </div>
                    </div>

                    @if(auth()->user())

                    <div class="col-md-5 no-padding">

                        <div class="downloadHistoryUser right-align">

                            <a style="" class="downloads-button btn blue-button-bg" href="/user_record">History
                                <i class="fa fa-download  button-arrow" aria-hidden="true"></i>
                            </a>

                        </div>

                    </div>

                    @endif

                    <?php //if(!empty($pro->grab_docs)){ ?>
                    <div class="col-md-4 hide">

                        <div class="downloadHistoryUser grabDoc right-align ">

                            <?php
                            $fileUrl = '';
                            $url = "/public/documents/";
                            if(auth()->user()){
                                if(is_object($userprojectrecord) && (int)$pro->bid_amount > $userprojectrecord->max_amount){
                                    $needUpgrade = 1;
                                }
                            }
                            //if(!empty($pro->grab_docs)){

                               // foreach(explode(',', $pro->grab_docs) as $key => $abcd){

                                    if(auth()->user()){
                                        if($hasSubscription || $isQTOFileDownloaded){
                                            if($needUpgrade == 1 && !$isQTOFileDownloaded){
                                                $fileUrl =  route('grab_all_files',['id' => $pro->id,'key' => 0 , 'proamount' => $pro->bid_amount ]);
                                            }else{
                                             //   $fileUrl = config('app.url') . $url . $abcd;
                                                 $fileUrl =   route('grab_all_files',[ 'id' => $pro->id, 'docs'=> 'bid_doc_url' ]);
                                            }
                                        }else{
                                            $fileUrl = '/plans';
                                        }
                                    }else{
                                        $fileUrl = '/user_login';
                                    }
                               // }
                          //  }
                            ?>

                            <a class="btn btn-3d btn-reveal btn-orange btn-width" href="{{ $fileUrl }}">
                                <i class="fa fa-download" aria-hidden="true"></i>
                                <span> Grab All Docs </span>
                            </a>
                        </div>
                    </div>
                    <?php //}
                    ?>
                            <!---/div--->
                    <!---/div--->
                    <?php /* endif; */?>
                </div>
            </div>

        </div>

        <div class="row">

            <div class="col-md-12">

                <div class="tab">

                    <?php

                    $qtoffCount = 0;
                    if(!empty($pro->qto_upload && !empty($pro->qto_price))){
                    $qtoffCount = count(json_decode($pro->qto_upload));
                    }

                    $planCount = 0;
                    if(!empty($pro->plan_doc)){
                    $planCount = count(explode(',', $pro->plan_doc));
                    }

                    $subCount = 0;
                    if(!empty($pro->bid_doc_url)){
                    $subCount = count(explode(',', $pro->bid_doc_url));
                    }

                    $addCount = 0;
                    if(!empty($pro->addendum)){
                    $addCount = count(explode(',', $pro->addendum));
                    }

                    if($showContent): ?>

                    <a class="tablinks active" href="#prodetail" onclick="openCity(event, 'ProD')" id="defaultOpen">Project Details</a>
                    <?php if(auth()->user()) : ?>
                    <a class="tablinks" href="#owner"  onclick="openCity(event, 'owners')">Owner/Authority</a>
                    <?php endif; ?>
                    <a class="tablinks" href="#specs" onclick="openCity(event, 'PD')">Specs  ( {{ $subCount }} )</a>
                    <a class="tablinks" href="#plan" onclick="openCity(event, 'plans')">Plans  ( {{ $planCount }} )</a>
                    <?php if(!empty($pro->addendum)): ?>
                    <a class="tablinks" href="#add" onclick="openCity(event, 'addendum')">Addendum  ( {{ $addCount }} ) </a>
                    <?php endif; ?>
                    <a class="tablinks" href="#qtytoff" onclick="openCity(event, 'qtyTakeOff')">Quantity Takeoffs  ( {{ $qtoffCount }} )</a>
                    <?php if($pro->qto_video): ?>
                    <a class="tablinks" href="#qtyvid" onclick="openCity(event, 'qtyTakeOff-video')">Quantity Takeoff Video </a>
                    <?php endif; ?>
                    <a class="tablinks hide" href="#cat" onclick="openCity(event, 'categories')" id="defaultOpen1">Categories
                    </a>
                    <?php if(auth()->user()) : ?>
                    <a class="tablinks" href="#planholders" onclick="openCity(event, 'planholder')">Plan Holders</a>
                    <?php endif; ?>
                    <?php if($bidPhase == 'Award'): ?>
                    {{--<a class="tablinks" href="#bidphase" onclick="openCity(event, 'award')">Awarded</a>--}}
                    <?php endif; ?>

                    <?php endif;?>

                </div>

                <div id="ProD" style="" class="tabcontent <?php if(!$showContent) echo " guest-tab m-y-xl "?>">
                    <div class="row">
                        <div class="col-md-12 hide <?php if(!$showContent) echo " hide "?>">
                            <h4 class="orange-color p-y-lg"> Project Details</h4>
                        </div>

                    </div>



                    <div id="printableArea" class="well print-area" >
                        <div class="row">
                            {{--@auth--}}
                            <div class="col-md-12">
                                {{--@guest
                                @endauth
                                    <div class="col-md-9">
                                @endguest--}}
                                <div class="col-md-12">
                                    <div class="print-logo" style="display: none; text-align: center;">
                                        <img width="150" src="{{  URL::to('/images/tbd-logo.png') }}">
                                    </div>
                                </div>
                                <div class="row">

                                    <div class="col-md-6 print-reduce">


                                        <div class="row">
                                            <div class="col-md-5">
                                                <h4>Solicitation</h4>
                                            </div>
                                            <div class="col-md-7">

                                                <p>
                                                    <?php //$bidPhase = TBD::GetSolLabelView($pro->solicitation); ?>
                                                    {{ $pro->solicitation  }}
                                                </p>

                                            </div>
                                        </div> {{-- Solicitation --}}
                                        <hr>

                                        <div class="row">
                                            <div class="col-md-5">
                                                <h4>Bidding Method</h4>
                                            </div>
                                            <div class="col-md-7">

                                                <p>{{$pro->bid_method}}</p>

                                            </div>
                                        </div> {{-- Bid Method--}}

                                        <hr>

                                        <div class="row">
                                            <div class="col-md-5">
                                                <h4>Bid Phase</h4>
                                            </div>
                                            <div class="col-md-7">
                                                <p>{{  $bidPhase }}</p>
                                            </div>
                                        </div> {{-- Bid Phase--}}
                                        <hr>

                                        <div class="row">
                                            <div class="col-md-5">
                                                <h4>Completion Time</h4>
                                            </div>
                                            <div class="col-md-7">

                                                <p>{{$pro->completion_time}}</p>

                                            </div>
                                        </div> {{-- CSI Division--}}
                                        <hr>

                                        <div class="row">
                                            <div class="col-md-5">
                                                <h4>Liquidated Damages</h4>
                                            </div>
                                            <div class="col-md-7">


                                                <?php $money = '$ ' . number_format($pro->liquidated_damages); ?>

                                                <p>{{  $money }} per CCD's</p>

                                            </div>
                                        </div> {{-- Liquidated Damages--}}
                                        <hr>

                                        <div class="row">
                                            <div class="col-md-5">
                                                <h4>Pre-bid Meeting</h4>
                                            </div>
                                            <div class="col-md-7">
                                                <?php
                                                $originalDate = $pro->pre_bid_date;
                                                $newDate = TBD::formatDate($originalDate);
                                                ?>

                                                <p>{{$newDate}} &nbsp;<b> @ &nbsp; </b> {{$pro->pre_bid_location}} </p>


                                            </div>
                                        </div> {{-- Pre-bid Meeting--}}
                                        <hr>

                                        <div class="row">
                                            <div class="col-md-5">
                                                <h4>Bonds</h4>
                                            </div>
                                            <div class="col-md-7">

                                                <p>
                                                    Bid Bond :&nbsp; {{$pro->b_bond}}%
                                                </p>

                                                <p>
                                                    Perfomance Bond :&nbsp; {{$pro->p_bond}}%
                                                </p>

                                                <p>
                                                    Payment Bond :&nbsp; {{$pro->pay_bond}}%
                                                </p>

                                            </div>
                                        </div> {{-- Bonds --}}
                                        <hr>

                                    </div>

                                    <div class="col-md-6 print-reduce1">

                                        <div class="row">
                                            <div class="col-md-4">
                                                <h4>Bid Range</h4>
                                            </div>
                                            <?php
                                            $rawAmount = $newbidamout = number_format_short(strlen($pro->bidrange) > 0 && is_numeric($pro->bidrange) ? $pro->bidrange : $pro->bid_amount);
                                            function number_format_short($n, $precision = 1)
                                            {
                                            if ($n < 900) {
                                            // 0 - 900
                                            $n_format = number_format($n, $precision);
                                            $suffix = '';
                                            } else if ($n < 900000) {
                                            // 0.9k-850k
                                            $n_format = number_format($n / 1000, $precision);
                                            $suffix = 'K';
                                            } else if ($n < 900000000) {
                                            // 0.9m-850m
                                            $n_format = number_format($n / 1000000, $precision);
                                            $suffix = 'M';
                                            } else if ($n < 900000000000) {
                                            // 0.9b-850b
                                            $n_format = number_format($n / 1000000000, $precision);
                                            $suffix = 'B';
                                            } else {
                                            // 0.9t+
                                            $n_format = number_format($n / 1000000000000, $precision);
                                            $suffix = 'T';
                                            }
                                            // Remove unecessary zeroes after decimal. "1.0" -> "1"; "1.00" -> "1"
                                            // Intentionally does not affect partials, eg "1.50" -> "1.50"
                                            if ($precision > 0) {
                                            $dotzero = '.' . str_repeat('0', $precision);
                                            $n_format = str_replace($dotzero, '', $n_format);
                                            }
                                            return $n_format . $suffix;
                                            }
                                            ?>

                                            <div class="col-md-8">

                                                <p> {{ (strlen($pro->bidrange) > 0) ? $pro->bidrange : '--'}}</p>

                                                {{--<span data-toggle="tooltip" data-placement="top"
                                                      title="{{$rawAmount}}"> $ {{$newbidamout}}</span>--}}
                                            </div>

                                        </div> {{-- Bid Amount--}}
                                        <hr>

                                        <div class="row">
                                            <div class="col-md-4">
                                                <h4>Location:</h4>
                                            </div>
                                            <div class="col-md-8">

                                                <p>
                                                    {{$pro->location}}
                                                </p>

                                            </div>
                                        </div> {{-- Location--}}
                                        <hr>

                                        <div class="row">
                                            <div class="col-md-4">
                                                <h4>Bid Date</h4>
                                            </div>
                                            <div class="col-md-8">
                                                <?php
                                                $originalDate = $pro->bid_date;
                                                $newDate = TBD::formatDate($originalDate);
                                                ?>
                                                <p>{{$newDate}}</p>
                                            </div>
                                        </div> {{-- Bid Date--}}
                                        <hr>

                                        <div class="row">
                                            <div class="col-md-4">
                                                <h4>Bid Location</h4>
                                            </div>
                                            <div class="col-md-8">
                                                <p>{{$pro->bid_location}}</p>
                                            </div>
                                        </div> {{-- Bid Location--}}
                                        <hr>


                                        <div class="row">
                                            <div class="col-md-4">
                                                <h4>Notes</h4>
                                            </div>
                                            <div class="col-md-8">
                                                <p>{{$pro->notes}}</p>
                                            </div>
                                        </div> {{-- Notes --}}

                                        <hr class="hide hidden">

                                        <div class="row hide hidden">
                                            <div class="col-md-4">
                                                <h4>Bid Range</h4>
                                            </div>
                                            <div class="col-md-8">

                                                <p> {{ (strlen($pro->bidrange) > 0) ? $pro->bidrange : '--'}}</p>

                                            </div>
                                        </div> {{-- Bid Range --}}
                                        <hr>

                                        <div class="row">
                                            <div class="col-md-4">
                                                <h4>CSI Division</h4>
                                            </div>
                                            <div class="col-md-8">

                                                <?php

                                                $divisions = explode(',', $pro->csi_division);

                                                $division_names = [];
                                                foreach($divisions as $k => $division){
                                                $division_names[$division] = TBD::LoadDataFromDB('csi_division', ['*'], null, ['id' => $division]);
                                                }

                                                foreach($division_names as $k => $name){ ?>

                                                <p>{{$name[0]->division_name}}</p>

                                                <?php    }

                                                ?>


                                            </div>
                                        </div> {{-- CSI Division --}}
                                        <hr>


                                    </div>

                                </div>

                                <div class="row">
                                    <?php
                                    if(count($solicitations) > 0){ ?>

                                    <div class="col-md-4">
                                        <h4>Bid Results</h4>
                                    </div>

                                    <div class="col-md-12">


                                        <table class="table table-striped sash">
                                            <thead>
                                            <tr class="newth">
                                                <th class="awarded-star" style="width:5%"></th>
                                                <th style="width:20%">#</th>
                                                <th style="width:30%">Name</th>
                                                <th style="width:20%">Price</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?php

                                            foreach($solicitations as $k => $detail){
                                            $isAwarded = '';
                                            if($detail->awarded == 1) $isAwarded = 'prova';

                                            $details = '';
                                            foreach($planHolderDetails as $key => $planHolder){
                                            if($planHolder->id == $detail->planholder_id){
                                            $details = $planHolder->company;
                                            }
                                            }
                                            ?>

                                            <tr class="">
                                                <td class="{{ $isAwarded }}" data-ribbon="★"></td>
                                                <td>{{ $k+1 }}</td>
                                                <td>{{ $details }}</td>
                                                <td> USD {{ number_format($detail->price) }}</td>
                                            </tr><?php
                                            } ?>

                                            </tbody>
                                        </table>

                                    </div>
                                    <?php  } ?>
                                </div>

                            </div>

                        </div>
                    </div>
                </div>
                <?php if($showContent): ?>
                        <!---<div id="categories" class="tabcontent ">

                    <h4 class="orange-color p-y-lg">Project Categories</h4>

                    <?php if(!empty($categoryproject)){?>


                        <h4>{{$categoryproject->name}}</h4>

                    <p>{{$categoryproject->cat_description}}</p>


                    <?php }else{?>

                        <p> No Categories!</p>


                        <?php }    ?>


                        </div>---->

                <div id="qtyTakeOff" class="tabcontent">

                    <h4 class="orange-color p-y-lg">Quantity Takeoffs</h4>

                    <?php if(!empty($pro->qto_upload) && !empty($pro->qto_price) ){
                    $hide = 'hide';
                    if(auth()->user()){
                    if(is_object($userprojectrecord) && (int)$pro->bid_amount > $userprojectrecord->max_amount){
                    $hide = '';
                    $SubMessage = 'Buy Quantity Takoffs for this project';
                    //$perProject = ($plan->downloads != -1)?$plan->max_rate/$plan->downloads:;
                    $alertView = '';
                    $needUpgrade = 1;
                    }
                    }
                    ?>

                    <div id="MyElement" class="alert alert-danger alert-dismissible {{  $hide }}"
                         style="background-color:#dc3545; border-color: #dc3545;">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>

                        <h3 style="color:#fff;margin-top:0px;"><strong><i class="fa fa-exclamation-triangle" aria-hidden="true"></i></strong>
                            {{ $SubMessage }}</h3>

                        {{--<p style="color:#fff">{{Session::get('alertone')}}</p>--}}

                    </div>
                    <div class="">
                        <table class="table">
                            <thead>
                            <tr style="background-color:#f0542d " class="newth">
                                {{--<th style="width:20%">#</th>--}}
                                <th style="width:30%">File Name</th>
                                <th style="width:15%">Size</th>
                                <th style="width:15%">Date</th>
                                <th style="width:10%">Doc</th>


                                <?php if(empty($userprojectrecord)){?>

                                <th style="width:10%">One Off</th>

                                <?php  }else if(!empty($userprojectrecord) &&
                                $userprojectrecord->downloads != $userprojectrecord->downloads_used &&
                                (is_object($userprojectrecord) && (int)$pro->bid_amount > $userprojectrecord->max_amount)
                                ){?>

                                <th style="width:10%">One Off</th>

                                <?php }else {
                                } ?>

                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            $downloadqto = json_decode($pro->qto_upload);
                            $strqtoprice = json_decode($pro->qto_price);




                            foreach($downloadqto as $key => $abcd){
                           // if (!isset(explode('.',$abcd)[1])) continue;
                            ?>


                            <tr>
                                {{--<td>File {{$key+1}}</td>--}}
                                <td class="file-name" style="word-wrap:break-word;font-weight: 700;">{{ explode('.',$abcd)[1] }}</td>
                                <td>
                                    <?php
                                    $pdffilesize = Storage::size($abcd);
                                    echo bytesToHuman($pdffilesize);
                                    ?>
                                </td>
                                <?php
                                $originalDate = $pro->updated_at;
                                $newDate = TBD::formatOnlyDate($originalDate);
                                $authcheck = (auth()->user())?'true':'false';
                                ?>
                                <td> {{$newDate}}</td>
                                <td>

                                    {{--<a href="{{route('p_download',['id' => $pro->id,'key' => $key , 'proamount'=> $pro->bid_amount ])}}">--}}
                                        {{--<img src='{{asset('images/excel.png')}}' class="specs-docs-image">--}}
                                    {{--</a>--}}
                                    <a class="first-download {{(auth()->user())?'':'hide'}}"
                                       data-toggle="modal" data-target="#myModaltwo"
                                       data-href="{{route('otp_set_single', ['id' => $pro->id,'key' => $key , 'qtoprice'=> $strqtoprice[$key] ])}}" data-auth="{{ $authcheck  }}"
                                       data-price="<?php echo $strqtoprice[$key]; ?>"
                                       data-project="<?php echo $project->title; ?>"

                                       style="color: #fff;min-width:80%;">

                                        <img src='{{asset('images/excel.png')}}' class="specs-docs-image">

                                    </a>

                                    <a href="/user_login" class="{{(auth()->user())?'hide':''}}">
                                        <img src='{{asset('images/excel.png')}}' class="specs-docs-image">
                                    </a>

                                </td>
                                <?php if(empty($userprojectrecord)){?>
                                <td>

                                    <?php
                                    $singleFileDownloaded = TBD::isSingleFileDownloaded($project->id, $key);
                                    ?>


                                        <?php
                                        if (empty($strqtoprice[$key])) {
                                        $strqtoprice[$key] = '1';
                                        }
                                        if($singleFileDownloaded == false && $isZipDownloaded == false){

                                        ?>
                                        <span>

                                            <a class="first-download btn btn-3d btn-reveal {{ ($key==0)?'btn-blue blue-button-bg':'btn-orange orange-button-bg' }}  {{(auth()->user())?'':'hide'}}"
                                               data-toggle="modal" data-target="#myModaltwo"
                                               data-href="{{route('otp_set_single', ['id' => $pro->id,'key' => $key , 'qtoprice'=> $strqtoprice[$key] ])}}"
                                               data-auth="{{ $authcheck  }}"
                                               data-price="<?php echo $strqtoprice[$key]; ?>"
                                               data-project="<?php echo $project->title; ?>"
                                               style="min-width:80%;color: #fff">
                                                    Buy for ${{$strqtoprice[$key]}}
                                            </a>

                                            <a href="/user_login" class="btn btn-3d btn-reveal {{ ($key==0)?'btn-blue blue-button-bg':'btn-orange orange-button-bg' }} {{(auth()->user())?'hide':''}}">
                                                Buy for ${{$strqtoprice[$key]}}
                                            </a>

                                        </span>

                                        <?php }else{ ?>

                                        <span>
                                            <a class="btn btn-3d btn-reveal {{ ($key==0)?'btn-blue blue-button-bg':'btn-orange orange-button-bg' }}"
                                               href="{{route('otp_set_single', ['id' => $pro->id,'key' => $key , 'qtoprice'=> $strqtoprice[$key] ])}}"
                                               data-price="<?php echo $strqtoprice[$key] ?>" style="color: #fff;min-width:80%;">
                                                Download File
                                            </a>
                                        </span>

                                        <?php } ?>


                                </td>

                                <?php  }else if(!empty($userprojectrecord) && $userprojectrecord->downloads != $userprojectrecord->downloads_used && $pro->bid_amount > $userprojectrecord->max_amount ){?>
                                <td>


                                        <?php
                                        if (empty($strqtoprice[$key])) {
                                        $strqtoprice[$key] = '1';
                                        }

                                        $singleFileDownloaded = TBD::isSingleFileDownloaded($project->id, $key);
                                        if($singleFileDownloaded == false && $isZipDownloaded == false){
                                        ?>
                                    {{--<button data-toggle="modal" data-target="#myModaltwo"></button>--}}

                                        <span>

                                            <a class="first-download btn btn-3d btn-reveal {{ ($key==0)?'btn-blue blue-button-bg':'btn-orange orange-button-bg' }} {{(auth()->user())?'':''}}"
                                                 data-toggle="modal" data-target="#myModaltwo"
                                                 data-href="{{route('otp_set_single', ['id' => $pro->id,'key' => $key , 'qtoprice'=> $strqtoprice[$key] ])}}" data-auth="{{ $authcheck  }}"
                                                 data-price="<?php echo $strqtoprice[$key]; ?>"
                                                 data-project="<?php echo $project->title; ?>"

                                               style="color: #fff;min-width:80%;">

                                                Buy for ${{$strqtoprice[$key]}}

                                            </a>

                                            {{--<a href="/user_login" class="btn btn-3d btn-reveal {{ ($key==0)?'btn-blue blue-button-bg':'btn-orange orange-button-bg' }} {{(auth()->user())?'hide':''}}">--}}
                                                {{--Buy for ${{$strqtoprice[$key]}}--}}
                                            {{--</a>--}}
                                        </span>
                                        <?php }else{ ?>
                                        <span>
                                            <a class="btn btn-3d btn-reveal {{ ($key==0)?'btn-blue blue-button-bg':'btn-orange orange-button-bg' }}"
                                               href="{{route('otp_set_single', ['id' => $pro->id,'key' => $key , 'qtoprice'=> $strqtoprice[$key] ])}}"  data-price="<?php echo $strqtoprice[$key] ?>" style="color: #fff;min-width:80%;">
                                                Download File
                                            </a>
                                        </span>
                                        <?php } ?>



                                </td>
                                <?php }else {
                                } ?>
                            </tr>

                            <?php
                            } ?>
                            </tbody>
                        </table>
                    </div>

                    <?php }else{?>

                    <p class="no-results">Please check back later, we'll update <b>Quantity Takeoff Documents</b> soon!</p>

                    <?php } ?>

                </div>


                <div id="PD" class="tabcontent">

                    <h4 class="orange-color p-y-lg">Specs Documents</h4>

                    <?php if(!empty($pro->bid_doc_url)){?>

                        <div id="MyElement" class="alert alert-danger alert-dismissible  {{ $alertView }}"
                         style="background-color:#dc3545; border-color: #dc3545; ">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>

                        <h3 style="color:#fff;margin-top:0px;"><strong><i class="fa fa-exclamation-triangle" aria-hidden="true"></i></strong>
                            {{ $SubMessage }}</h3>

                        <p style="color:#fff">{{Session::get('alertone')}}</p>

                    </div>

                        <div class="">
                            <table class="table">
                                <thead>
                                    <tr class="newth">
                                        <th style="width:30%">File Name</th>
                                        <th style="width:15%">Size</th>
                                        <th style="width:15%">Date</th>
                                        <th style="width:15%; background-color: #084887;">
                                            <div class="downloadHistoryUser grabDoc left-align">

                                                <?php
                                                $fileUrl = '';
                                                $modalShow = false;
                                                $url = "/public/documents/";
                                                if(auth()->user()){
                                                    if(is_object($userprojectrecord) && (int)$pro->bid_amount > $userprojectrecord->max_amount){
                                                        $needUpgrade = 1;
                                                    }
                                                }

                                                if(auth()->user()){
                                                    if($hasSubscription || $isQTOFileDownloaded){
                                                        if($needUpgrade == 1 && !$isQTOFileDownloaded){
                                                            $fileUrl =  route('p_download',['id' => $pro->id,'key' =>  0, 'proamount' => $pro->bid_amount ]);
                                                        }else{
                                                //   $fileUrl = config('app.url') . $url . $abcd;
                                                            $fileUrl = route('grab_all_files',[ 'id' => $pro->id, 'docs'=> 'bid_doc_url' ]);
                                                            $modalShow = false;
                                                        }
                                                    }else{
                                                        $fileUrl = '/plans';
                                                    }
                                                }else{
                                                    $fileUrl = '/user_login';
                                                    $modalShow = false;
                                                }

                                                $fileUrl = route('grab_all_files',[ 'id' => $pro->id, 'docs'=> 'bid_doc_url' ]); ?>

                                                <a style="color:white;" href="{{ $fileUrl  }}">
                                                    <i class="fa fa-download" aria-hidden="true"></i>
                                                    <span> Grab All Docs </span>
                                                </a>

                                            </div>
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php

                                    $url = "/public/bid_uploads/";
                                    $downloadqto = explode(',',$pro->bid_doc_url);

                                    foreach($downloadqto as $key => $abcd){

                                        $fileName = explode('_', $abcd);

                                        $fileName = (isset($fileName[1])) ? explode('.', $fileName[1])[0] : $fileName[0];
                                ?>

                                <tr>
                                    <td style="word-wrap:break-word; font-weight: bold;">
                                        {{ $fileName  }}
                                    </td>

                                    <td>
                                        <?php

                                            $pdffilesize = is_file(public_path('bid_uploads/' . $abcd)) ? File::size(public_path('bid_uploads/' . $abcd)) : '333';
                                            echo bytesToHuman($pdffilesize);

                                        ?>
                                    </td>

                                    <?php
                                        $originalDate = $pro->updated_at;
                                        $newDate = TBD::formatOnlyDate($originalDate);
                                    ?>

                                    <td> {{$newDate}}</td>

                                    <td>


                                        <?php
                                            $fileUrl = '';
                                            $modalShow = true;
                                            if(auth()->user()){
                                                if($hasSubscription || $isQTOFileDownloaded){
                                                    if($needUpgrade == 1 && !$isQTOFileDownloaded){
                                                        $fileUrl =  route('p_download',['id' => $pro->id,'key' => $key , 'proamount' => $pro->bid_amount ]);
                                                }else{

                                                    $fileUrl = config('app.url') . $url . $abcd;
                                                    $modalShow =false;
                                                }
                                            }else{
                                                $fileUrl = '/plans';}
                                            }else{
                                                $fileUrl = '/user_login';
                                                $modalShow =false;
                                            }

                                        ?>


                                        <?php  $fileUrl = config('app.url') . 'bid_uploads/' . $abcd; ?>
                                        <a href="{{ url('bid_uploads/' . $abcd)  }}">
                                            <img class="specs-docs-image" src="{{asset('images/pdf.png')}}">
                                        </a>

                                    </td>
                                </tr> <?php
                                }  ?>

                                </tbody>
                            </table>
                        </div>

                    <?php }else{?>

                        <p class="no-results">Please check back later, we will update Specs documents soon.</p>

                    <?php }?>

                </div>

                <div id="addendum" class="tabcontent">

                    <h4 class="orange-color p-y-lg">Addendum Documents</h4>

                    <?php if(!empty($pro->addendum)){?>

                    <div id="MyElement" class="alert alert-danger alert-dismissible  {{ $alertView }}"
                         style="background-color:#dc3545; border-color: #dc3545; ">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>

                        <h3 style="color:#fff;margin-top:0px;"><strong><i class="fa fa-exclamation-triangle" aria-hidden="true"></i></strong>
                            {{ $SubMessage }}</h3>

                        <p style="color:#fff">{{Session::get('alertone')}}</p>

                    </div>

                    <div class="">
                        <table class="table ">
                            <thead>
                            <tr class="newth">
                                <th style="width:30%">File Name</th>
                                <th style="width:15%">Size</th>
                                <th style="width:15%">Date</th>
                                <th style="width:15%; background-color: #084887;">
                                    <div class="downloadHistoryUser grabDoc left-align">

                                        <?php
                                            $fileUrl = '';
                                            $url = "/public/documents/";

                                            if(auth()->user()){
                                                if(is_object($userprojectrecord) && (int)$pro->bid_amount > $userprojectrecord->max_amount){
                                                    $needUpgrade = 1;
                                                }
                                            }

                                            if(auth()->user()){
                                                if($hasSubscription || $isQTOFileDownloaded){
                                                    if($needUpgrade == 1 && !$isQTOFileDownloaded){
                                                        $fileUrl =  route('p_download',['id' => $pro->id,'key' => 0, 'proamount' => $pro->addendum ]);
                                                    }else{

                                                        $fileUrl = route('grab_all_files',[ 'id' => $pro->id, 'docs'=> 'bid_doc_url' ]);
                                                    }
                                                }else{
                                                    $fileUrl = '/plans';
                                                }
                                            }else{
                                                $fileUrl = '/user_login';
                                            }

                                            $fileUrl = route('grab_all_files',[ 'id' => $pro->id, 'docs'=> 'bid_doc_url' ]);
                                        ?>

                                        <a style="color:white;" href="{{ $fileUrl  }}">
                                            <i class="fa fa-download" aria-hidden="true"></i>
                                            <span> Grab All Docs </span>
                                        </a>


                                    </div>

                                </th>
                            </tr>
                            </thead>
                            <tbody>
                                <?php
                                    $url = "/public/bid_uploads/";
                                    $downloadqto = explode(',',$pro->addendum);

                                    foreach($downloadqto as $key => $abcd){
                                        $fileName = explode('_', $abcd);
                                        $fileName = (isset($fileName[1])) ? explode('.', $fileName[1])[0] : $fileName[0];
                                ?>
                                <tr>
                                    <td  style="word-wrap:break-word;font-weight: 700;">
                                    <?php //var_dump($abcd)?>
                                    {{ $fileName  }}
                                </td>
                                    <td>
                                        <?php

                                            $pdffilesize = is_file(public_path('bid_uploads/' . $abcd)) ?
                                                File::size(public_path('bid_uploads/' . $abcd)) : '333';

                                            echo bytesToHuman($pdffilesize);

                                        ?>
                                    </td>
                                        <?php
                                            $originalDate = $pro->updated_at;
                                            $newDate = TBD::formatOnlyDate($originalDate);
                                        ?>
                                    <td> {{$newDate}}</td>

                                    <td>

                                        <?php

                                            $fileUrl = '';

                                            if(auth()->user()){
                                                if($hasSubscription || $isQTOFileDownloaded){
                                                    if($needUpgrade == 1 && !$isQTOFileDownloaded){
                                                        $fileUrl =  route('p_download',['id' => $pro->id,'key' => $key , 'proamount' => $pro->bid_amount ]);
                                                    }else{
                                                        $fileUrl = config('app.url') . $url . $abcd;
                                                    }
                                                }else{
                                                    $fileUrl = '/plans';
                                                }
                                            }else{
                                                $fileUrl = '/user_login';
                                            }
                                            $fileUrl = config('app.url') . $url . $abcd;
                                        ?>
                                        <a href="{{ url('bid_uploads/' . $abcd)  }}">
                                            <img class="specs-docs-image" src="{{asset('images/pdf.png')}}">
                                        </a>
                                    </td>

                                </tr> <?php
                            }  ?>

                            </tbody>
                        </table>
                    </div>

                    <?php }else{?>
                        <p class="no-results">Addendum documents are not available. </p>
                    <?php }?>

                </div>

                <div id="plans" class="tabcontent">

                    <h4 class="orange-color p-y-lg">Plan Documents</h4>

                    <?php
                    if(!is_null($pro->plan_doc)){?>
                    <div id="MyElement" class="alert alert-danger alert-dismissible  {{ $alertView }}"
                         style="background-color:#dc3545; border-color: #dc3545;">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>

                        <h3 style="color:#fff;margin-top:0px;"><strong><i class="fa fa-exclamation-triangle" aria-hidden="true"></i></strong>
                            {{ $SubMessage }}</h3>

                        <p style="color:#fff">{{Session::get('alertone')}}</p>

                    </div>
                    <div class="" >
                        <table class="table " style="">
                            <thead>
                            <tr class="newth">
                                <th style="width:30%">File Name</th>
                                <th style="width:15%">Size</th>
                                <th style="width:15%">Date</th>
                                <th style="width:15%; background-color: #084887;">
                                    <div class="downloadHistoryUser grabDoc left-align">

                                        <?php
                                        $fileUrl = '';
                                        $url = "/public/documents/";
                                        if(auth()->user()){
                                        if(is_object($userprojectrecord) && (int)$pro->bid_amount > $userprojectrecord->max_amount){
                                        $needUpgrade = 1;
                                        }
                                        }

                                        if(auth()->user()){
                                        if($hasSubscription || $isQTOFileDownloaded){
                                        if($needUpgrade == 1 && !$isQTOFileDownloaded){
                                        $fileUrl =  route('p_download',['id' => $pro->id,'key' => 0 , 'proamount' => $pro->plan_doc ]);
                                        }else{
                                        //   $fileUrl = config('app.url') . $url . $abcd;
                                        $fileUrl = route('grab_all_files',[ 'id' => $pro->id, 'docs'=> 'bid_doc_url' ]);
                                        }
                                        }else{
                                        $fileUrl = '/plans';
                                        }
                                        }else{
                                        $fileUrl = '/user_login';
                                        }
                                        // }
                                        //  }
                                        ?>

                                        <?php $fileUrl = route('grab_all_files',[ 'id' => $pro->id, 'docs'=> 'bid_doc_url' ]); ?>
                                        <a style="color:white;" href="{{ $fileUrl  }}">
                                            <i class="fa fa-download" aria-hidden="true"></i>
                                            <span> Grab All Docs </span>
                                        </a>

                                            {{--@if($modalShow)--}}
                                                {{--<a style="color:white;" data-toggle="modal" data-target="#plans-modal"  href="">--}}
                                                    {{--<i class="fa fa-download" aria-hidden="true"></i>--}}
                                                    {{--<span> Grab All Docs </span>--}}
                                                {{--</a>--}}
                                            {{--@else--}}
                                                {{--<a style="color:white;" href="{{ $fileUrl  }}">--}}
                                                    {{--<i class="fa fa-download" aria-hidden="true"></i>--}}
                                                    {{--<span> Grab All Docs </span>--}}
                                                {{--</a>--}}
                                            {{--@endif--}}
                                    </div>
                                </th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                                $url = "/public/bid_uploads/";
                                $downloadqto = explode(',', $pro->plan_doc);
                            foreach($downloadqto as $key => $abcd){
                                $fileName = explode('_', $abcd);
                                if (!isset($fileName[1])) continue;
                            ?>
                            <tr>
                                <td style="word-wrap:break-word;font-weight: 700;"> {{  explode('.', explode('_',$abcd)[1])[0]  }}</td>
                                <td>
                                    <?php  $pdffilesize = (is_file(public_path('bid_uploads/' . $abcd))) ? File::size(public_path('bid_uploads/' . $abcd)) : '333';
                                    echo bytesToHuman($pdffilesize);
                                    ?>
                                </td>
                                <?php
                                    $originalDate = $pro->updated_at;
                                    $newDate = TBD::formatOnlyDate($originalDate);
                                ?>

                                <td> {{$newDate}}</td>

                                <td>


                                    <?php
                                        $fileUrl = '';

                                        if(auth()->user()){

                                            if($hasSubscription || $isQTOFileDownloaded){
                                                if($needUpgrade == 1 && !$isQTOFileDownloaded){
                                                    $fileUrl =  route(
                                                        'p_download',
                                                        ['id' => $pro->id,'key' => $key , 'proamount' => $pro->bid_amount ]
                                                    );

                                                }else{
                                                   // $fileUrl = config('app.url') . $url . $abcd;
                                                    $fileUrl = config('app.url') . 'bid_uploads/' . $abcd;
                                                    var_dump($fileUrl);
                                                    var_dump(config('app.url'));
                                                }

                                            }else{
                                                $fileUrl = '/plans';
                                            }

                                        }else{
                                            $fileUrl = '/user_login';
                                        }

                                        //$fileUrl = config('app.url') . $url . $abcd; // Broken URL
                                    ?>
                                    <a href="{{ url('bid_uploads/' . $abcd)  }}">
                                        <img class="specs-docs-image" src="{{asset('images/pdf.png')}}">
                                    </a>
                                </td>
                            </tr><?php
                            }
                            ?>

                            </tbody>
                        </table>
                    </div>
                    <?php }else{?>
                    <p class="no-results">Please check back later, we will update Plans documents soon.</p>
                    <?php }?>

                </div>


                <?php if($pro->qto_video) :?>

                <div id="qtyTakeOff-video" class="tabcontent">

                    <h4 class="orange-color p-y-lg">Quantity Takeoff Video</h4>
                    <div class="video-container">
                        <?php echo \App\Helper\TBD::GetQTOVideoIframe($pro->qto_video);?>
                    </div>

                </div>

                <?php endif;?>

                <?php endif;?>

                <?php

                    function bytesToHuman($bytes){
                        $units = ['B', 'KB', 'MB', 'GB', 'TB', 'PB'];

                        for ($i = 0; $bytes > 1024; $i++) {
                            $bytes /= 1024;
                        }

                        return round($bytes, 2) . ' ' . $units[$i];
                    }

                    if($showContent):
                ?>



                <div style="padding-left:12px;" class="tabcontent" id="owners">

                    <h4 class="orange-color p-y-lg">Project Owner</h4>

                    <?php if(!empty($powner->owner_name)){?>

                    <img src="{{asset('images/user-512.png')}}" alt="Avatar" style="width:100px; display:none;">

                    <div class="row">

                        <div class="col-md-4 col-xs-5" style="padding-right: 0px; padding-top: 10px">
                            <h4>Owner/Authority</h4>
                            <hr/>
                            <h4>Address</h4>
                            <hr/>
                            <h4>Contact Name</h4>
                            <hr/>
                            <h4>Contact Number</h4>
                            <hr/>
                            <h4>Email</h4>
                            <hr/>
                            <h4>Fax</h4>
                            <hr/>
                        </div>

                        <div class="col-md-8 col-xs-7 " style="padding-top: 8px">
                            <p class="">{{$powner->owner_name}}</p>
                            <hr/>
                            <p class="">{{$powner->owner_address}}</p>
                            <hr>
                            <p class="">{{strlen($powner->contact_name) > 0 ? $powner->contact_name : '--' }}</p>
                            <hr>
                            <p class="">{{strlen($powner->owner_phone) > 0 ? $powner->owner_phone : '--'}}</p>
                            <hr>
                            <p class="">{{(strlen($powner->owner_email) > 0) ? $powner->owner_email: '--'}}</p>
                            <hr>
                            <p class="">{{(strlen($powner->owner_fax)>0) ? $powner->owner_fax: '--'}}</p>
                            <hr>
                        </div>

                    </div>

                    <?php }else{ ?>

                    <p> Owner details not updated, please check again later! </p>
                    <?php } ?>

                </div>



                <div style="padding-left: 0" class="tabcontent" id="planholder">

                    <h4 class="orange-color p-y-lg">Project Plan Holders</h4>

                    {{--<img src="{{asset('images/user-512.png')}}" alt="Avatar" style="width:100px">--}}

                    <?php if(!empty($arrayphinfo)){ $count = 1; ?>

                    <div class="table-responsive">

                        <table class="table ">
                            <thead>
                            <tr class="newth">
                                <th style="width:5%">#</th>
                                <th style="width:20%">Company</th>
                                <th style="width:35%">Address</th>
                                <th style="width: 20%">Contact Name</th>
                                <th style="width: 20%">Contact #</th>
                            </tr>
                            </thead>
                            <tbody>


                            @foreach($arrayphinfo as $apinfo)


                                <tr>
                                    <td>{{ $count++ }}</td>
                                    <td>{{$apinfo->company}}</td>
                                    <td>{{$apinfo->planholder_address}}</td>
                                    <td>{{$apinfo->planholder_contact}}</td>
                                    <td>{{$apinfo->planholder_phone}}</td>
                                </tr>

                                {{--<div class="row">--}}

                                {{--<div class="col-md-4">--}}
                                {{--<div class="row">--}}
                                {{--<div class="col-md-4 col-xs-4">--}}
                                {{--<h4>Name</h4>--}}
                                {{--</div>--}}
                                {{--<div class="col-md-8 col-xs-8">--}}
                                {{--<p class="m-y-md">{{$apinfo->company}}</p>--}}
                                {{--</div>--}}
                                {{--</div>--}}
                                {{--</div>--}}

                                {{--<div class="col-md-4">--}}
                                {{--<div class="row">--}}
                                {{--<div class="col-md-4 col-xs-4">--}}
                                {{--<h4>Address</h4>--}}
                                {{--</div>--}}
                                {{--<div class="col-md-8 col-xs-8">--}}
                                {{--<p class="m-y-md">{{$apinfo->planholder_address}}</p>--}}
                                {{--</div>--}}
                                {{--</div>--}}
                                {{--</div>--}}

                                {{--<div class="col-md-4">--}}
                                {{--<div class="row">--}}
                                {{--<div class="col-md-4 col-xs-4">--}}
                                {{--<h4>Contact</h4>--}}
                                {{--</div>--}}
                                {{--<div class="col-md-8 col-xs-8">--}}
                                {{--<p class="m-y-md">{{$apinfo->planholder_contact}}</p>--}}
                                {{--</div>--}}
                                {{--</div>--}}
                                {{--</div>--}}
                                {{--</div>--}}
                                {{--<hr>--}}


                                {{--<h4><b>{{$apinfo->planholder_name}}</b></h4>
                                <p>Address : {{$apinfo->planholder_address}}</p>
                                <p>Contact : {{$apinfo->planholder_contact}}</p>

                                <hr>--}}

                            @endforeach

                            </tbody>
                        </table>
                    </div>



                    <?php }else {
                    echo "No Data Available";
                    } ?>
                </div>

                <?php if($bidPhase <> 'Open Solicitation'): ?>

                <div class="tabcontent" id="bidResult">

                    <?php
                    if(count($solicitations) > 0){ ?>
                    <table class="table ">
                        <thead>
                        <tr class="newth">
                            <th style="width:20%">#</th>
                            <th style="width:30%">Name</th>
                            <th style="width:20%">Price</th>
                            <th style="width: 30%">Date</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php

                        foreach($solicitations as $k => $detail){
                        $details = '';
                        foreach($planHolderDetails as $key => $planHolder){
                        if($planHolder->id == $detail->planholder_id){
                        $details = $planHolder->company;
                        }
                        }
                        ?>

                        <tr>
                            <td>{{ $k+1 }}</td>
                            <td>{{ $details }}</td>
                            <td>${{ $detail->price }}</td>
                            <td>{{ TBD::formatDate($detail->created_at) }}</td>
                        </tr><?php
                        } ?>

                        </tbody>
                    </table>
                    <?php   }else { ?>

                    <p class="no-results"> Data not available</p>

                    <?php } ?>
                </div>



                <?php /*if($bidPhase == 'Award'): ?>

                {{--<div class="tabcontent @guest hide @endguest" id="award">
                    <?php
                    if(count($solicitations) > 0){ ?>
                    <table class="table table-striped">
                        <thead>
                        <tr class="newth">
                            <th style="width:20%">#</th>
                            <th style="width:30%">Name</th>
                            <th style="width:20%">Price</th>
                            <th style="width:30%">Date</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php

                        foreach($solicitations as $k => $detail){
                        $isAwarded = '';
                        if($detail->awarded == 1) $isAwarded = 'project-awarded';

                        $details = '';
                        foreach($planHolderDetails as $key => $planHolder){
                            if($planHolder->id == $detail->planholder_id){
                                $details = $planHolder->company;
                            }
                        }
                        ?>

                        <tr class="{{ $isAwarded }}">
                            <td>{{ $k+1 }}</td>
                            <td>{{ $details }}</td>
                            <td>${{ $detail->price }}</td>
                            <td>{{ TBD::formatDate($detail->created_at) }}</td>
                        </tr><?php
                        } ?>

                        </tbody>
                    </table>
                    <?php   }else { ?>

                    <p class="no-results"> Data not available</p>

                    <?php } ?>

                </div>--}}

                <?php endif; */?>


                <?php endif; ?>

                <?php endif; ?>


            </div>


        </div>


    </div>


    <div id="myModal" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content" style="margin-top:20%;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    Are you sure you want to buy all files of this project? Payment will be detected from
                    your card!
                    <br><br>

                    <center><a href="{{route('otp_set', $project->id)}}" class="btn btn-default"
                               style="background-color: #084887; color:#fff">Yes, Proceed</a></center>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>


    <div id="myModaltwo" class="modal myModalPlan fade singlePrice" role="dialog">
        <div class="modal-dialog" style="min-width: 70%;">

            <!-- Modal content-->
            <div class="modal-content" style="margin-top:5%;">
                {{--<div class="modal-header">--}}
                    <button type="button" class="close" data-dismiss="modal">&times;</button>

                {{--</div>--}}

                <div class="row">

                    <div  class="col-md-6 padding-r-0">
                        <div class="border-gray">
                            <div style="visibility: hidden;" class="after-searching"><p>Recommended</p></div>
                            <div class="book-image"><img src="{{asset('images/decide-graphic.png')}}" style="width: 49%;"></div>
                            <div style="visibility: hidden;" class="search-book"><h4>Check Out Our <span class="head-bold">Convenient </span>Subscription Plans</h4></div>
                            <div style="visibility: hidden;" class="button-searc">
                                <a href="/plans">
                                    <button class="button button1 btn golden-button-bg">Subscription Plans
                                        <i class="fa fa-arrow-right button-arrow" aria-hidden="true"></i>
                                    </button>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 padding-l-0">
                        <div class="border-blue">


                            <h2 class="pro-title">Bill Avenue Road Constructions</h2>
                            <p style="padding:0 60px; font-weight: bold;">File will be sent to you via your provided email once the payment has been made over PayPal. Please use the link below to pay</p>
                            <h4>TakeOff: <span class="file-name">Expert 459M</span></h4>
                            <div class="btn-cont">
                                <a href="https://paypal.me/tbdata?locale.x=en_US" target="_blank">
                                    <button class="button btn buy-btn button-orange">
                                        <span style="font-size: 22px;font-weight: 600;" ><i class="fa fa-paypal"></i> Continue to Paypal</span>
                                        <i class="fa fa-arrow-right button-arrow" aria-hidden="true"></i>
                                        <p style="font-size: 18px;">Buy for $<span class="price"></span></p>
                                    </button>
                                </a>
                             </div>


                        </div>
                    </div>

                </div>

            </div>

        </div>
    </div>


    <div id="plans-modal" class="modal myModalPlan fade singlePrice" role="dialog">
        <div class="modal-dialog" style="min-width: 70%;">

            <!-- Modal content-->
            <div class="modal-content" style="margin-top:5%;">
                {{--<div class="modal-header">--}}
                <button type="button" class="close" data-dismiss="modal">&times;</button>

                {{--</div>--}}

                <div class="row">

                    <div  class="col-md-6 padding-r-0">
                        <div class="border-gray">
                            <div class="after-searching"><p></p></div>
                            <div class="book-image"><img src="{{asset('images/decide-graphic.png')}}" style="width: 49%;"></div>
                            <div class="search-book"><h4>To Download Plans and Specs Kindly Purchase </h4>
                                <h4><span class="head-bold">Quantity Takeoff </span>File for this Project
                                </h4>
                            </div>
                            {{--<div class="button-searc">--}}
                                {{--<a href="/plans">--}}
                                    {{--<button class="button button1 btn golden-button-bg">Subscription Plans--}}
                                        {{--<i class="fa fa-arrow-right button-arrow" aria-hidden="true"></i>--}}
                                    {{--</button>--}}
                                {{--</a>--}}
                            {{--</div>--}}
                        </div>
                    </div>
                    <div class="col-md-6 padding-l-0">
                        <div class="border-blue">
                            <div class="after-searching"><p style="font-weight: bold;"><bold>Recommended</bold></p></div>

                            <h3 class="sub-title">For Subscribers of Subscription Plans all Plans & Specs are Available Free of Charge.
                                </h3>
                            {{--<h4>Subscribe Here</h4>--}}
                            <div class="btn-cont">
                                <a href="{{ $fileUrl }}">
                                    <button class="button btn buy-btn button-orange">
                                        <span style="font-size: 22px;font-weight: 600;" >Subscribe Here</span>
                                        <i class="fa fa-arrow-right button-arrow" aria-hidden="true"></i>
                                    </button>
                                </a>
                            </div>


                        </div>
                    </div>

                </div>

            </div>

        </div>
    </div>




    <script>
        function openCity(evt, cityName) {
            var i, tabcontent, tablinks;
            tabcontent = document.getElementsByClassName("tabcontent");
            for (i = 0; i < tabcontent.length; i++) {
                tabcontent[i].style.display = "none";
            }
            tablinks = document.getElementsByClassName("tablinks");
            for (i = 0; i < tablinks.length; i++) {
                tablinks[i].className = tablinks[i].className.replace(" active", "");
            }
            document.getElementById(cityName).style.display = "block";
            evt.currentTarget.className += " active";
        }

        // Get the element with id="defaultOpen" and click on it


        //document.getElementById("defaultOpen").click();


        var existtwo = '{{Session::has('alertone')}}';

        if (existtwo) {

            document.getElementById('MyElement').classList.add('show');
            document.getElementById('MyElement').classList.remove('hide');

        }

        function printDiv(divName) {
            var printContents = document.getElementById(divName).innerHTML;
            var originalContents = document.body.innerHTML;

            document.body.innerHTML = printContents;

            window.print();

            document.body.innerHTML = originalContents;
        }
    </script>
@endsection
@extends('layout')

@section('content')

    <style type="text/css">


    </style>


    <section>


        <div class="container" style="margin-top: 2%;">
            <div class="row">
                <div class="col-md-12">
                    <div class="cover-area">
                        <h3 class="area-two">
                            <span>
                                <i class="fa fa-signal signal-legnth"></i>&nbsp;&nbsp;&nbsp;
                            </span>
                            Coverage Area
                        </h3>
                    </div>
                </div>
            </div>


            <div class="row">

                <div class="col-lg-8 col-md-8">

                    <h3 class="ny-cities">
                        New York Counties
                    </h3>

                    <ul class="list top-list">

                        <li>
                            <i class="material-icons" style="font-size:20px;">place</i>
                            Albany County
                        </li>
                        <li>
                            <i class="material-icons">place</i>
                            Allegany County

                        </li>
                        <li>
                            <i class="material-icons">place</i>
                            Bronx County
                        </li>
                        <li>
                            <i class="material-icons">place</i>
                            Cattaraugus County
                        </li>
                        <li>
                            <i class="material-icons">place</i>
                            Cayuga County
                        </li>
                        <li>
                            <i class="material-icons">place</i>
                            Chautauqua County
                        </li>
                        <li>
                            <i class="material-icons">place</i>
                            Chemung County
                        </li>
                        <li>
                            <i class="material-icons">place</i>
                            Chenango County
                        </li>
                        <li>
                            <i class="material-icons">place</i>
                            Clinton County
                        </li>
                        <li>
                            <i class="material-icons">place</i>
                            Columbia County
                        </li>
                        <li>
                            <i class="material-icons">place</i>
                            Cortland County
                        </li>
                        <li>
                            <i class="material-icons">place</i>
                            Delaware County
                        </li>
                        <li>
                            <i class="material-icons">place</i>
                            Dutchess County
                        </li>
                        <li>
                            <i class="material-icons">place</i>
                            Erie County
                        </li>
                        <li>
                            <i class="material-icons">place</i>
                            Essex County
                        </li>
                        <li>
                            <i class="material-icons">place</i>
                            Franklin County
                        </li>
                        <li>
                            <i class="material-icons">place</i>
                            Fulton County
                        </li>
                        <li>
                            <i class="material-icons">place</i>
                            Genesee County
                        </li>
                        <li>
                            <i class="material-icons">place</i>
                            Greene County
                        </li>
                        <li>
                            <i class="material-icons">place</i>
                            Hamilton County
                        </li>
                        <li>
                            <i class="material-icons">place</i>
                            Herkimer County
                        </li>
                        <li>
                            <i class="material-icons">place</i>
                            Jefferson County
                        </li>
                        <li>
                            <i class="material-icons">place</i>
                            Kings County

                        </li>
                        <li>
                            <i class="material-icons">place</i>
                            Lewis County
                        </li>
                        <li>
                            <i class="material-icons">place</i>
                            Livingston County
                        </li>
                        <li>
                            <i class="material-icons">place</i>
                            Madison County
                        </li>
                        <li>
                            <i class="material-icons">place</i>
                            Monroe County
                        </li>
                        <li>
                            <i class="material-icons">place</i>
                            Montgomery County
                        </li>
                        </li>
                        <li>
                            <i class="material-icons">place</i>
                            Nassau County
                        </li>
                        </li>
                        <li>
                            <i class="material-icons">place</i>
                            New York County

                        </li>
                        </li>
                        <li>
                            <i class="material-icons">place</i>
                            Niagara County
                        </li>
                        </li>
                        <li>
                            <i class="material-icons">place</i>
                            Oneida County
                        </li>
                        </li>
                        <li>
                            <i class="material-icons">place</i>
                            Onondaga County
                        </li>
                        </li>
                        <li>
                            <i class="material-icons">place</i>
                            Ontario County
                        </li>
                        </li>
                        <li>
                            <i class="material-icons">place</i>
                            Ontario County
                        </li>
                        </li>
                        <li>
                            <i class="material-icons">place</i>
                            Orleans County
                        </li>
                        </li>
                        <li>
                            <i class="material-icons">place</i>
                            Oswego County
                        </li>
                        </li>
                        <li>
                            <i class="material-icons">place</i>
                            Otsego County
                        </li>
                        </li>
                        <li>
                            <i class="material-icons">place</i>
                            Putnam County
                        </li>
                        </li>
                        <li>
                            <i class="material-icons">place</i>
                            Queens County
                        </li>
                        </li>
                        <li>
                            <i class="material-icons">place</i>
                            Rensselaer County
                        </li>
                        </li>
                        <li>
                            <i class="material-icons">place</i>
                            Yates County

                        </li>
                        </li>
                        <li>
                            <i class="material-icons">place</i>
                            Rockland County
                        </li>
                        </li>
                        <li>
                            <i class="material-icons">place</i>
                            Saint Lawrence County

                        </li>
                        </li>
                        <li>
                            <i class="material-icons">place</i>
                            Saratoga County
                        </li>
                        </li>
                        <li>
                            <i class="material-icons">place</i>
                            Schenectady County
                        </li>
                        </li>
                        <li>
                            <i class="material-icons">place</i>
                            Schoharie County
                        </li>
                        <li>
                            <i class="material-icons">place</i>
                            Seneca County
                        </li>
                        </li>
                        <li>
                            <i class="material-icons">place</i>
                            Steuben County
                        </li>
                        <li>
                            <i class="material-icons">place</i>
                            Suffolk County
                        </li>
                        <li>
                            <i class="material-icons">place</i>
                            Sullivan County
                        </li>
                        <li>
                            <i class="material-icons">place</i>
                            Tioga County
                        </li>
                        <li>
                            <i class="material-icons">place</i>
                            Tompkins County
                        </li>
                        <li>
                            <i class="material-icons">place</i>
                            Ulster County
                        </li>
                        <li>
                            <i class="material-icons">place</i>
                            Warren County
                        </li>
                        <li>
                            <i class="material-icons">place</i>
                            Washington County
                        </li>
                        <li>
                            <i class="material-icons">place</i>
                            Wayne County
                        </li>
                        <li>
                            <i class="material-icons">place</i>
                            Westchester County
                        </li>
                        <li>
                            <i class="material-icons">place</i>
                            Richmond County
                        </li>
                        <li>
                            <i class="material-icons">place</i>

                            Wyoming County
                        </li>


                    </ul>
                </div>

                <div class="col-lg-4 col-centered col-md-4">
                    <img class="map-image" src="images/map1.png" style="width: 100%;padding-top: 75px;">

                </div>


            </div>

            <br>

            <div class="Connecticut-Countries">
                <div class="row">

                    <div class="col-lg-7 col-md-7 hidden-xs hidden-sm col-sm-12">
                        <img src="images/map2.png" style="width: 50%;">
                        <br>

                        <p style="color: black; font-size: 16px;" class="paragraph"><a style="color: black;" href="/quotes"><b>Take-Off on Demand (TOD)</b></a> available for all states of <b>America</b>.
                        </p>
                    </div>


                    <div class="col-lg-5 col-md-5 order-md-2 col-sm-12">
                        <h3 style="font-weight:bold; margin-bottom: 20px; color:black;">Connecticut Counties</h3>

                        <ul class="list top-list">
                            <li>
                                <i class="material-icons">place</i>
                                Fairfield County
                            </li>
                            <li>
                                <i class="material-icons">place</i>
                                Hartford County
                            </li>
                            <li>
                                <i class="material-icons">place</i>
                                Litchfield County
                            </li>
                            <li>
                                <i class="material-icons">place</i>
                                Middlesex County
                            </li>
                            <li>
                                <i class="material-icons">place</i>
                                New Haven County
                            </li>
                            <li>
                                <i class="material-icons">place</i>
                                London County
                            </li>
                            <li>
                                <i class="material-icons">place</i>
                                Tolland County
                            </li>
                            <li>
                                <i class="material-icons">place</i>
                                Windham County
                            </li>


                        </ul>

                    </div>

                    <div class="col-centered hidden-md hidden-lg col-sm-12">
                        <img class="map-img-2" src="images/map2.png" style="width: 70%;">
                        <br>

                        <p style="color: black; font-size: 16px;" class="paragraph"><a style="color: black;" href="/quotes"><b>Take-Off on Demand (TOD)</b></a> available for all states of <b>America</b>.
                        </p>
                    </div>


                </div>
            </div>
        </div>

    </section>


@endsection
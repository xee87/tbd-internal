<?php //setlocale(LC_MONETARY,"en_US"); ?>
@extends('layouts.app')

@section('content')
        <!-- Content Header (Page header) -->

<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">

            <div class="col-sm-12">
                <div id="MyElement" class="alert alert-success alert-dismissible fade hide " role="alert">
                    <h3 style="color:#fff"><strong><i class="fa fa-check" aria-hidden="true"></i></strong> Alert!</h3>
                    <p style="color:#fff">Project <?php if(!empty($_GET['msg'])){echo $_GET['msg']; $gomsg= 1;}?> Successfully!</p>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div id="MyElement2" class="alert alert-success alert-dismissible fade hide " role="alert">
                    <h3 style="color:#fff"><strong><i class="fa fa-check" aria-hidden="true"></i></strong> Alert!</h3>
                    <p style="color:#fff">{{Session::get('alertone')}}</p>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="float-sm-right">

                </div>
            </div>
        </div>
    </div>
</section>

<!-- Main content -->
<section class="content">
    <div class="card card-info card-outline">
        <div class="container-fluid mt-2">
            <div class="info-box bg-info">
                <span class="info-box-icon"><i class="fa fa-clipboard"></i></span>
                <div class="info-box-content">
                    <div class="col-md-12 col-sm-12 col-xs-6">
                        <h1>Quotes</h1>
                    </div>
                </div>
                <span class="info-box-icon float-right">
                    <a class="add-resource" href="" title="Add New Group"> <i class="fa fa-plus"></i></a>
                </span>
            </div>
        </div>
        <div class="card-body">

            <div class="table-responsive">
                <table id="user_table" class="table table-bordered table-hover">
                    <thead>
                    <tr>
                        <th>Name</th>
                        <th>Company</th>
                        <th>Contact No.</th>
                        <th style=" width: 35px;">Email</th>
                        <th>No. of Drawings</th>
                        <th>Target No.</th>
                        <th>Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($quotes as $q)

                        <tr>
                            <td>{{$q->name}}</td>
                            <td>{{ $q->company }}</td>
                            <td>{{$q->contact}}</td>
                            <td>
                                {{ $q->email  }}
                            </td>
                            <td>
                                {{ $q->no_drawings }}
                            </td>
                            <td>
                                {{ $q->targets }}
                            </td>
                            <td>
                                <div class="btn-group">
                                    <a href="{{ route('view_quotes', ['id' => $q->id]) }}"  type="button" class="btn btn-default">View</a>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <!-- /.card-body -->
    </div>
    <!-- /.card -->
    <script>

        var existtwo =  '{{ !empty($gomsg) }}' ;

        if(existtwo){

            document.getElementById('MyElement').classList.add('show');
            document.getElementById('MyElement').classList.remove('hide');

        }
    </script>

    <script>

        var existtwo2 = '{{Session::has('alertone')}}';

        if(existtwo2){

            document.getElementById('MyElement2').classList.add('show');
            document.getElementById('MyElement2').classList.remove('hide');

        }
    </script>
</section>
<!-- /.content -->
@endsection

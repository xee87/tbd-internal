@extends('layouts.app')

@section('content')
<?php use App\Helper\TBD; ?>

    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">

                <div class="col-sm-12">
                    <div id="MyElement2" class="alert alert-success alert-dismissible fade hide " role="alert">
                        <h3 style="color:#fff"><strong><i class="fa fa-check" aria-hidden="true"></i></strong> Alert!</h3>
                        <p style="color:#fff">{{Session::get('alertone')}}</p>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="float-sm-right">

                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="card card-info card-outline">
            <div class="container-fluid mt-2">
                <div class="info-box bg-info">
                    <span class="info-box-icon"><i class="fa fa-clipboard"></i></span>
                    <div class="info-box-content">
                        <div class="col-md-5 col-sm-6 col-xs-6">
                            <h1>View Quotes</h1>
                        </div>
                    </div>

                </div>
            </div>



            <!-- /.card-body -->
        </div>

        <div class="card card-info card-outline">

            <div class="card-body">

                <div class="container-fluid" >
                    <div class="row">
                        <div class="col-md-12">
                            <div class="table-resposive">
                                <h4>Quotes Details</h4>
                                <table class="table table-striped">
                                    <thead>
                                    <tr>
                                        {{--<th colspan="2">Label</th>--}}
                                        {{--<th>Value</th>--}}
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td>Name</td>
                                        <td>{{$q->name}} </td>
                                    </tr>
                                    <tr>
                                        <td>Company</td>
                                        <td>{{$q->company}}</td>
                                    </tr>
                                    <tr>
                                        <td>Contact No.</td>
                                        <td> {{$q->contact}} </td>
                                    </tr>
                                    <tr>
                                        <td>Email</td>
                                        <td>{{$q->email}}</td>
                                    </tr>
                                    <tr>
                                        <td>Address</td>
                                        <td>{{$q->address}}</td>
                                    </tr>
                                    <tr>
                                        <td>No. of Drawings</td>
                                        <td>{{ $q->no_drawings  }}</td>
                                    </tr>
                                    <tr>
                                        <td>Target No. of Days</td>
                                        <td>{{$q->targets}}</td>
                                    </tr>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


        </div>

        <?php if(!empty($q->specification_docs)){ ?>
        <div class="card card-info card-outline">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-12">
                        <h4>Specification Docs</h4>
                        <div class="table-resposive">
                            <table class="table table-striped">
                                <thead>
                                <tr>
                                    <th style="width:30%">File Name</th>
                                    <th style="width:15%">Size</th>
                                    <th style="width:15%">Date</th>
                                    <th style="width:15%">Download</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                $downloadqto = explode(',', $q->specification_docs);
                                $url = "/public/bid_uploads/";
                                foreach($downloadqto as $key => $abcd){
                                    $fileName = explode('_', $abcd);
                                    $fileName = (isset($fileName[1])) ? explode('.', $fileName[1])[0] : $fileName[0];
                                ?>

                                <tr>
                                    <td  style="word-wrap:break-word;font-weight: 700;">
                                        <?php //var_dump($abcd)?>
                                        {{ $fileName  }}
                                    </td>

                                    <td>
                                        <?php  $pdffilesize = is_file(public_path('bid_uploads/' . $abcd)) ? File::size(public_path('bid_uploads/' . $abcd)) : '333';

                                        //\Faker\Provider\File::size
                                        echo TBD::bytesToHuman($pdffilesize);

                                        ?>
                                    </td>


                                    <?php
                                    $originalDate = $q->updated_at;
                                    $newDate = TBD::formatOnlyDate($originalDate);
                                    ?>
                                    <td> {{$newDate}}</td>
                                    <td>

                                        <a href="{{ config('app.url') . $url . $abcd }}">
                                            <img class="specs-docs-image" style="width: 25%;" src="{{asset('images/pdf.png')}}">
                                        </a>

                                    </td>
                                </tr>


                                <?php
                                }
                                ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php } ?>


        <?php if(!empty($q->plan_doc)){ ?>

            <div class="card card-info card-outline">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12">
                            <h4>Plan Docs</h4>
                            <div class="table-resposive">
                                <table class="table table-striped">
                                    <thead>
                                    <tr>
                                        <th style="width:30%">File Name</th>
                                        <th style="width:15%">Size</th>
                                        <th style="width:15%">Date</th>
                                        <th style="width:15%">Download</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    $downloadqto = explode(',', $q->plan_doc);
                                    $url = "/public/bid_uploads/";
                                    foreach($downloadqto as $key => $abcd){
                                    $fileName = explode('_', $abcd);
                                    $fileName = (isset($fileName[1])) ? explode('.', $fileName[1])[0] : $fileName[0];
                                    ?>

                                    <tr>
                                        <td  style="word-wrap:break-word;font-weight: 700;">
                                            <?php //var_dump($abcd)?>
                                            {{ $fileName  }}
                                        </td>

                                        <td>
                                            <?php  $pdffilesize = is_file(public_path('bid_uploads/' . $abcd)) ? File::size(public_path('bid_uploads/' . $abcd)) : '333';

                                            //\Faker\Provider\File::size
                                             echo TBD::bytesToHuman($pdffilesize);

                                            ?>
                                        </td>


                                        <?php
                                        $originalDate = $q->updated_at;
                                        $newDate = TBD::formatOnlyDate($originalDate);
                                        ?>
                                        <td> {{$newDate}}</td>
                                        <td>

                                            <a href="{{ config('app.url') . $url . $abcd }}">
                                                <img class="specs-docs-image" style="width: 25%;" src="{{asset('images/pdf.png')}}">
                                            </a>

                                        </td>
                                    </tr>


                                    <?php
                                    }
                                    ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        <?php } ?>

















        <!-- /.card -->
    </section>
    <!-- /.content -->
    <script>

        var existtwo2 = '{{Session::has('alertone')}}';

        if(existtwo2){

            document.getElementById('MyElement2').classList.add('show');
            document.getElementById('MyElement2').classList.remove('hide');

        }
    </script>
@endsection
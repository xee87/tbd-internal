@extends('layout')

@section('content')

<div class="container-fluid" >

	<div class="row" style =" background-image:url('images/blur.png') ; background-size: 100%; background-repeat: no-repeat;  height:400px;   padding: 4%" >
		 <center> <div style="width: 50%;    margin-top: 8%;">
		
		<h1 style="color:#f0542d;">Subscription Plans</h1>
		
  
      </div></center>
  </div>


 </div>


   <div class="container" style=" margin-top: 2%; height: 350px;">
    <div class="row">
      <div class="col-lg-4">
        <div class="thumbnail">
          <div class="caption">
            <div style="background-color: #cd7f32; padding: 10%;border-radius: 5px;">
            <center><h4>Bronze</h4></center>
            </div>
            <br>	
            <p style="padding: 2%;"><i class="fa fa-download" aria-hidden="true"></i> Number of Projects Downloads <strong style="float:right;">20</strong></p>
            <p style="padding: 2%;"><i class="fa fa-clock-o" aria-hidden="true"></i> Time Period <strong  style="float:right;">20 days</strong></p>
              <br>	
            	<center><a href="#" class="btn btn-default" role="button" data-toggle="modal" data-target="#productOrder" style="background-color:#f0542d; color:#fff">Buy</a> </center>
          </div>
        </div>
      </div>

      <div class="col-lg-4">
        <div class="thumbnail">
          <div class="caption">
          	 <div style="background-color: #C0C0C0; padding: 10%;border-radius: 5px;">
            <center><h4>Silver</h4></center>
        </div>
         <br>
            <p style="padding: 2%;"><i class="fa fa-download" aria-hidden="true"></i> Number of Projects Downloads <strong style="float:right;">40</strong></p>
            <p style="padding: 2%;"><i class="fa fa-clock-o" aria-hidden="true"></i> Time Period <strong style="float:right;">40 days</strong></p>
              <br>	
            	<center><a href="#" class="btn btn-default" role="button" data-toggle="modal" data-target="#productOrder" style="background-color:#f0542d; color:#fff">Buy</a> </center>
          </div>
        </div>
      </div>

      <div class="col-lg-4">
        <div class="thumbnail">
          <div class="caption">
          	 <div style="background-color:#D4AF37; padding: 10%;border-radius: 5px;">
            <center><h4>Gold</h4></center>
        </div>
         <br>
            <p style="padding: 2%;"><i class="fa fa-download" aria-hidden="true"></i> Number of Projects Downloads <strong style="float:right;">60</strong></p>
            <p style="padding: 2%;"><i class="fa fa-clock-o" aria-hidden="true"></i> Time Period <strong style="float:right;">60 days</strong></p>
              <br>	
            	<center><a href="#" class="btn btn-default" role="button" data-toggle="modal" data-target="#productOrder" style="background-color:#f0542d; color:#fff">Buy</a> </center>
          </div>
        </div>
      </div>


    </div>
  </div>

    @endsection
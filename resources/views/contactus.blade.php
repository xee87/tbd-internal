@extends('layout')

@section('content')

    <style>



    </style>

    <section class="contact-page p-y-xl">
        <div class="container">

            <div class="row">
                <div class="col-md-6">
                    <form action="{{ route('mail_contact')  }}"  method="post" role="form" >
                        @csrf
                        <div class="contact-form">
                            <div class="col-md-6 no-padding">
                                <div style="margin-bottom: 20px; text-align: left" class="line-down ">
                                    <span class="sec-heading">Contact </span><span class="head-bold">Us</span>
                                    <div style="margin: 5px 30px;width: 30%" class="green-border">&nbsp;</div>
                                </div>
                            </div>
                            <ul class="cform">
                                <li class="half pr-15">
                                    <input type="text" name="c_name" class="form-control" placeholder="Full Name" required>
                                </li>
                                <li class="half pl-15">
                                    <input type="email" name="c_email" class="form-control" placeholder="Email" required>
                                </li>
                                <li class="half pr-15">
                                    <input type="tel" name="c_contact" class="form-control" placeholder="Contact" required>
                                </li>
                                <li class="half pl-15">
                                    <input type="text" name="c_subject" class="form-control" placeholder="Subject" required>
                                </li>
                                <li class="full">
                                    <textarea class="textarea-control" name="c_message" placeholder="Message" required></textarea>
                                </li>
                                <li class="full">
                                    <input type="submit" value="Submit" class="fsubmit btn btn-3d">
                                </li>
                            </ul>
                        </div>
                    </form>
                </div>
                <div class="col-md-6">
                    <div class="google-map">
                        {{--<iframe src="https://maps.google.com/maps?q=lahore%20pakistan&t=&z=13&ie=UTF8&iwloc=&output=embed"--}}
                                {{--width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>--}}
                        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3024.6000085557503!2d-74.00946988415929!3d40.704806179332614!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x89c25a161aaa68c5%3A0xf28c0256bfab5cd3!2s99%20Wall%20St%20%23556%2C%20New%20York%2C%20NY%2010005%2C%20USA!5e0!3m2!1sen!2s!4v1571744797979!5m2!1sen!2s" width="600" height="600" frameborder="0" style="border:0;" allowfullscreen=""></iframe>

                    </div>
                </div>
            </div>
        </div>
        <div class="container contact-info">
            <div class="row">
                <div class="col-md-12">
                    <h3>Contact Information</h3>
                </div>
                <!--Contact Info Start-->
                <div class="col-md-4">
                    <div class="c-info">
                        <h6>Address:</h6>
                        <p>99 Wall Street, Suite 556</p>
                        <p>New York, NY, 10005</p>
                    </div>
                </div>
                <!--Contact Info End-->
                <!--Contact Info Start-->
                <div class="col-md-4">
                    <div class="c-info">
                        <h6>Contact:</h6>

                        <p>
                            <strong>Phone:</strong>
                            <a href="tel:1 718 717 2601">1-718-717-2601</a>
                        </p>

                        <p>
                            <strong>&nbsp;</strong>
                            <a href="">&nbsp;</a>
                        </p>

                       {{-- <p><strong>Fax:</strong> +1 321 2345 876-7</p>--}}
                    </div>
                </div>
                <!--Contact Info End-->
                <!--Contact Info Start-->
                <div class="col-md-4">
                    <div class="c-info">
                        <h6>For More Information:</h6>

                        <p>
                            <strong>Support:</strong>
                            <a href="mailto:support@truebiddata.com">support@truebiddata.com</a>
                        </p>
                        <p>
                            <strong>Sales:</strong>
                            <a href="mailto:sales@truebiddata.com">sales@truebiddata.com</a>
                        </p>
                    </div>
                </div>
                <!--Contact Info End-->
            </div>
        </div>
    </section>

    @if(isset($successMsg) && $successMsg == 'sent')
        <script>
            jQuery(document).ready(function(){
                swal({
                    title: "Sent!",
                    text: "Thanks to contact us, we will contact you soon!",
                    type: "success"

                });
            });
        </script>
    @endif


@endsection



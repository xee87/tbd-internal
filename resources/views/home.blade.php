@extends('layouts.app')

@section('content')


        <!-- Main content -->
<section class="content">

    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Dashboard </h1>
                </div><!-- /.col -->

            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>


    <div class="container-fluid chart-container">
        <div class="row">

            <!-- /.col-md-6 -->
            <div class="col-lg-6">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Sales Report</h3>

                        <div class="card-tools">
                            <button type="button" class="btn btn-tool" data-widget="collapse" data-toggle="tooltip"
                                    title="Collapse">
                                <i class="fa fa-minus"></i></button>
                            <!-- <button type="button" class="btn btn-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                                <i class="fa fa-times"></i></button> -->
                        </div>
                    </div>
                    <div class="card-body">

                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-lg-6">
                                    <center style="margin-top: 16%;">
                                        <h2 class="text-bold text-lg">${{$totalsale}}</h2>

                                        <p>Total Sale</p>
                                    </center>
                                </div>

                                <div class="col-lg-6 ">
                                    <div class="callout callout-info" style="    background-color: #f4f4f4;">
                                        <center>

                                            <h4>Monthly Sale</h4>
                                            <br>

                                            <form action="{{ route('hometwo') }}" method="post" role="form"
                                                  enctype="multipart/form-data">
                                                @csrf
                                                <div class="input-group mb-3">
                                                    <input type="month" name="monthcheck" class="form-control">
                  <span class="input-group-append">
                    <button type="submit" class="btn btn-info btn-flat"><i class="fa fa-arrow-right"
                                                                           aria-hidden="true"></i></button>
                  </span>
                                                </div>
                                            </form>


                                            <br>

                                            <h4 class="text-bold text-lg"> ${{$totalmonthlysale}}</h4>

                                            <center>
                                    </div>
                                </div>


                            </div>
                        </div>


                    </div>
                </div>
            </div>


            <!-- /.col-md-6 -->
            <div class="col-lg-6">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Visitor Report </h3>

                        <div class="card-tools">
                            <button type="button" class="btn btn-tool" data-widget="collapse" data-toggle="tooltip"
                                    title="Collapse">
                                <i class="fa fa-minus"></i></button>
                            <!-- <button type="button" class="btn btn-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                                <i class="fa fa-times"></i></button> -->
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="d-flex">
                            <p class="d-flex flex-column">
                                <span class="text-bold text-lg">{{$totalvisitors}}</span>
                                <span>Visitors Over Time</span>
                            </p>
                            <!-- <p class="ml-auto d-flex flex-column text-right">
                              <span class="text-success">
                                <i class="fa fa-arrow-up"></i> 12.5%
                              </span>
                              <span class="text-muted">Since last week</span>
                            </p> -->
                        </div>
                        <!-- /.d-flex -->

                        <div class="position-relative mb-4">
                            <canvas id="visitors-chart" height="200"></canvas>
                        </div>

                        <div class="d-flex flex-row justify-content-end">
                  <span class="mr-2">
                    <i class="fa fa-square text-primary"></i> This Week
                  </span>

                            <!-- <span>
                              <i class="fa fa-square text-gray"></i> Last Week
                            </span> -->
                        </div>
                    </div>
                </div>
            </div>


            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Download Count</h3>

                        <div class="card-tools">
                            <button type="button" class="btn btn-tool" data-widget="collapse" data-toggle="tooltip"
                                    title="Collapse">
                                <i class="fa fa-minus"></i></button>
                            <!-- <button type="button" class="btn btn-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                                <i class="fa fa-times"></i></button> -->
                        </div>
                    </div>
                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif


                        <p>Total Downloads: {{ $strdc->tpd }}</p>


                        <table class="table table-striped">
                            <thead>
                            <tr class="newth">
                                <th style="width:10%">Sr#</th>
                                <th style="width:70%">Project Title</th>
                                <th>No. of Downloads</th>

                            </tr>
                            </thead>
                            <tbody>

                            <?php $count = 1; ?>
                            @foreach($downcount as $dc)
                                <tr>
                                    <td>{{$count}}</td>

                                    <td>
                                        <a href="{{route('pdetail', $dc->project_id)}}">{{ $dc->project_title }}</a>
                                    </td>

                                    <td>{{$dc->pd }}</td>

                                </tr>
                                <?php $count++; ?>
                            @endforeach


                            </tbody>

                        </table>


                    </div>
                    <!-- /.card-body -->
                    <!--  <div class="card-footer">
                         Footer
                     </div> -->
                    <!-- /.card-footer-->
                </div>
                <!-- /.card -->
            </div>


        </div>
        <!-- /.row -->
    </div>


</section>


<!-- /.content
<script src="{{asset('plugins/jquery/jquery.min.js')}}"></script>-->
<!-- Bootstrap
<script src="{{asset('plugins/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
<!-- AdminLTE -
<script src="{{asset('dist/js/adminlte.min.js')}}"></script>

<script src="dist/js/demo.js"></script>-->

<script type="text/javascript">

    var one = <?php echo $visiternow[6]; ?> ;

    var two = <?php echo $visiternow[5]; ?> ;

    var three = <?php echo $visiternow[4]; ?> ;

    var four = <?php echo $visiternow[3]; ?> ;

    var five = <?php echo $visiternow[2]; ?> ;

    var six = <?php echo $visiternow[1]; ?> ;

    var seven = <?php echo $visiternow[0]; ?> ;


    var oned = '<?php echo $visiterdate[6]; ?>';

    var twod = '<?php echo $visiterdate[5]; ?>';

    var threed = '<?php echo $visiterdate[4]; ?>';

    var fourd = '<?php echo $visiterdate[3]; ?>';

    var fived = '<?php echo $visiterdate[2]; ?>';

    var sixd = '<?php echo $visiterdate[1]; ?>';

    var sevend = ' <?php echo $visiterdate[0]; ?> ';

</script>


@endsection

@extends('layout')

@section('content')


    <div class="container-fluid productDetail">

        <div id="MyElement" class="alert alert-success alert-dismissible hide" style="background-color:#28a745">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>

            <h3 style="color:#fff"><strong><i class="fa fa-check" aria-hidden="true"></i></strong> Alert!</h3>

            <p style="color:#fff"></p>

        </div>

        <div class="row projectTitle p-y-md">
            <div class="col-md-8 p-t-sm">
                <h2 style="margin: 0 0 5px;">
                    <b style="color:#084887">PS 500 (Bronx)</b>
                </h2>
                <h5 class="detail-loc">
                    <i class="fa fa-map-marker loc-icon" aria-hidden="true"></i> &nbsp; 30-30 Thomson Avenue, Long
                    Island City, NY 11101-3045
                </h5>
                <h5 class="detail-name">
                    <i class="fa fa-user" aria-hidden="true"></i> &nbsp; New York City School Construction Authority
                </h5>
            </div>
            <div class="col-md-4">


                <div class="row fixBtns" style="margin-top: 4%;">


                    <!----div class="">

            <div class="col-md-2"></div>

            <div class="col-md-10"---->

                    <div class="col-md-2 hidden-xs hidden-sm"></div>

                    <div class="col-md-10 no-padding">
                        <div class="printBtn right-align">
                            <a style="background-color: #555555;" class="downloads-button btn black-button-bg"
                               onclick="printDiv('printableArea')">
                                Print
                                <i class="fa fa-print button-arrow" aria-hidden="true"></i>
                            </a>
                        </div>
                    </div>

                    {{--<div class="col-md-5">--}}

                        {{--<div class="downloadHistoryUser right-align">--}}

                            {{--<a style="" class="downloads-button btn blue-button-bg" href="/user_record">Downlaods--}}
                                {{--<i class="fa fa-download  button-arrow" aria-hidden="true"></i>--}}
                            {{--</a>--}}

                        {{--</div>--}}


                    {{--</div>--}}

                    <div class="col-md-4 hide">

                        <div class="downloadHistoryUser grabDoc right-align ">


                            <a class="btn btn-3d btn-reveal btn-orange btn-width"
                               href="http://tbd/grab_all_files/8/bid_doc_url">
                                <i class="fa fa-download" aria-hidden="true"></i>
                                <span> Grab All Docs </span>
                            </a>
                        </div>
                    </div>
                    <!---/div--->
                    <!---/div--->
                </div>
            </div>

        </div>

        <div class="row">


            <div class="col-md-12">

                <div class="tab">


                    <a class="tablinks active" href="#prodetail" onclick="openCity(event, 'ProD')" id="defaultOpen">Project
                        Details</a>
                    <a class="tablinks" href="#owner" onclick="openCity(event, 'owners')">Owner/Authority</a>
                    <a class="tablinks" href="#specs" onclick="openCity(event, 'PD')">Specifications ( 15 )</a>
                    <a class="tablinks" href="#plan" onclick="openCity(event, 'plans')">Plans ( 9 )</a>
                    <a class="tablinks" href="#add" onclick="openCity(event, 'addendum')">Addendum ( 1 ) </a>
                    <a class="tablinks" href="#qtytoff" onclick="openCity(event, 'qtyTakeOff')">Quantity Takeoffs ( 1
                        )</a>
                    <a class="tablinks" href="#qtyvid" onclick="openCity(event, 'qtyTakeOff-video')">Quantity Takeoff
                        Video </a>
                    <a class="tablinks hide" href="#cat" onclick="openCity(event, 'categories')" id="defaultOpen1">Categories
                    </a>
                    <a class="tablinks" href="#planholders" onclick="openCity(event, 'planholder')">Plan Holders</a>


                </div>

                <div id="ProD" style="display: block;" class="tabcontent ">
                    <div class="row">
                        <div class="col-md-12 hide ">
                            <h4 class="orange-color p-y-lg"> Project Details</h4>
                        </div>

                    </div>


                    <div id="printableArea" class="well print-area">
                        <div class="row">

                            <div class="col-md-12">

                                <div class="col-md-12">
                                    <div class="print-logo" style="display: none; text-align: center;">
                                        <img src="http://tbd/images/tbd-logo.png" width="150">
                                    </div>
                                </div>
                                <div class="row">

                                    <div class="col-md-6 print-reduce">


                                        <div class="row">
                                            <div class="col-md-5">
                                                <h4>Solicitation</h4>
                                            </div>
                                            <div class="col-md-7">

                                                <p>
                                                    Awarded Solicitation
                                                </p>

                                            </div>
                                        </div>
                                        <hr>

                                        <div class="row">
                                            <div class="col-md-5">
                                                <h4>Bidding Method</h4>
                                            </div>
                                            <div class="col-md-7">

                                                <p>Competitive Public Bids</p>

                                            </div>
                                        </div>

                                        <hr>

                                        <div class="row">
                                            <div class="col-md-5">
                                                <h4>Bid Phase</h4>
                                            </div>
                                            <div class="col-md-7">
                                                <p>Award</p>
                                            </div>
                                        </div>
                                        <hr>

                                        <div class="row">
                                            <div class="col-md-5">
                                                <h4>Completion Time</h4>
                                            </div>
                                            <div class="col-md-7">

                                                <p>360 CCD's</p>

                                            </div>
                                        </div>
                                        <hr>

                                        <div class="row">
                                            <div class="col-md-5">
                                                <h4>Liquidated Damages</h4>
                                            </div>
                                            <div class="col-md-7">


                                                <p>$ 5,000 per CCD's</p>

                                            </div>
                                        </div>
                                        <hr>

                                        <div class="row">
                                            <div class="col-md-5">
                                                <h4>Pre-bid Meeting</h4>
                                            </div>
                                            <div class="col-md-7">

                                                <p>Aug 04, 2019 10:45 pm &nbsp;<b> @ &nbsp; </b> 199 Lafayette Street,
                                                    New York, NY, USA </p>


                                            </div>
                                        </div>
                                        <hr>

                                        <div class="row">
                                            <div class="col-md-5">
                                                <h4>Bonds</h4>
                                            </div>
                                            <div class="col-md-7">

                                                <p>
                                                    Bid Bond :&nbsp; 5%
                                                </p>

                                                <p>
                                                    Perfomance Bond :&nbsp; 100%
                                                </p>

                                                <p>
                                                    Payment Bond :&nbsp; 100%
                                                </p>

                                            </div>
                                        </div>
                                        <hr>

                                    </div>

                                    <div class="col-md-6 print-reduce1">

                                        <div class="row">
                                            <div class="col-md-4">
                                                <h4>Bid Range</h4>
                                            </div>

                                            <div class="col-md-8">

                                                <p> $4,000,001 - $5,000,000</p>


                                            </div>

                                        </div>
                                        <hr>

                                        <div class="row">
                                            <div class="col-md-4">
                                                <h4>Location:</h4>
                                            </div>
                                            <div class="col-md-8">

                                                <p>
                                                    199 Lafayette Street, New York, NY, USA
                                                </p>

                                            </div>
                                        </div>
                                        <hr>

                                        <div class="row">
                                            <div class="col-md-4">
                                                <h4>Bid Date</h4>
                                            </div>
                                            <div class="col-md-8">
                                                <p>Aug 04, 2019 10:45 pm</p>
                                            </div>
                                        </div>
                                        <hr>

                                        <div class="row">
                                            <div class="col-md-4">
                                                <h4>Bid Location</h4>
                                            </div>
                                            <div class="col-md-8">
                                                <p>1998 Broadway, New York, NY, USA</p>
                                            </div>
                                        </div>
                                        <hr>


                                        <div class="row">
                                            <div class="col-md-4">
                                                <h4>Notes</h4>
                                            </div>
                                            <div class="col-md-8">
                                                <p>N/A</p>
                                            </div>
                                        </div>

                                        <hr class="hide hidden">

                                        <div class="row hide hidden">
                                            <div class="col-md-4">
                                                <h4>Bid Range</h4>
                                            </div>
                                            <div class="col-md-8">

                                                <p> $4,000,001 - $5,000,000</p>

                                            </div>
                                        </div>
                                        <hr>

                                        <div class="row">
                                            <div class="col-md-4">
                                                <h4>CSI Division</h4>
                                            </div>
                                            <div class="col-md-8">


                                                <p>Div 01 - General Requirements</p>


                                                <p>Div 02 - Existing Conditions</p>


                                                <p>Div 03 - Concrete</p>


                                                <p>Div 04 - Masonry</p>


                                                <p>Div 05 - Metals</p>


                                                <p>Div 06 - Wood, Plastics and Composites</p>


                                                <p>Div 07 - Thermal and Moisture Protection</p>


                                                <p>Div 08 - Openings</p>


                                                <p>Div 09 - Finishes</p>


                                                <p>Div 10 - Specialities</p>


                                                <p>Div 11 - Equipment</p>


                                                <p>Div 12 - Furnishings</p>


                                                <p>Div 13 - Special Construction</p>


                                                <p>Div 14 - Conveying Systems</p>


                                                <p>Div 15 - Mechanical</p>


                                                <p>Div 16 - Electrical</p>


                                            </div>
                                        </div>
                                        <hr>


                                    </div>

                                </div>

                                <div class="row">

                                    <div class="col-md-4">
                                        <h4>Bid Results</h4>
                                    </div>

                                    <div class="col-md-12">


                                        <table class="table table-striped sash">
                                            <thead>
                                            <tr class="newth">
                                                <th class="awarded-star" style="width:5%"></th>
                                                <th style="width:20%">#</th>
                                                <th style="width:30%">Name</th>
                                                <th style="width:20%">Price</th>
                                            </tr>
                                            </thead>
                                            <tbody>

                                            <tr class="">
                                                <td class="" data-ribbon="★"></td>
                                                <td>1</td>
                                                <td>Plan Holder Company</td>
                                                <td> USD 80,000,000</td>
                                            </tr>
                                            <tr class="">
                                                <td class="prova" data-ribbon="★"></td>
                                                <td>2</td>
                                                <td>ABC Consulting Private Limited</td>
                                                <td> USD 10,000,000</td>
                                            </tr>
                                            <tr class="">
                                                <td class="" data-ribbon="★"></td>
                                                <td>3</td>
                                                <td>chaudry malik</td>
                                                <td> USD 100,000</td>
                                            </tr>
                                            </tbody>
                                        </table>

                                    </div>
                                </div>

                            </div>

                        </div>
                    </div>
                </div>
                <!---<div id="categories" class="tabcontent ">

<h4 class="orange-color p-y-lg">Project Categories</h4>



<h4>Testcat</h4>

<p>Testcat123 abc abc abababababasd</p>




</div>---->

                <div id="qtyTakeOff" class="tabcontent" style="display: none;">

                    <h4 class="orange-color p-y-lg">Quantity Takeoffs</h4>


                    <div id="MyElement" class="alert alert-danger alert-dismissible hide"
                         style="background-color:#dc3545; border-color: #dc3545;">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>

                        <h3 style="color:#fff;margin-top:0px;"><strong><i class="fa fa-close"
                                                                          aria-hidden="true"></i></strong>
                        </h3>

                        <p style="color:#fff"></p>

                    </div>
                    <div class="">
                        <table class="table">
                            <thead>
                            <tr class="newth" style="background-color:#f0542d ">

                                <th style="width:30%">File Name</th>
                                <th style="width:15%">Size</th>
                                <th style="width:15%">Date</th>
                                <th style="width:10%">Doc</th>


                            </tr>
                            </thead>
                            <tbody>

                            <tr>

                                <td class="file-name" style="word-wrap:break-word;font-weight: 700;">AMEER Khan
                                    (Excavation) Bill No
                                </td>
                                <td>
                                    29.42 KB
                                </td>
                                <td> Aug 07, 2019</td>
                                <td>

                                    <a href="{{ asset('productdownload/file/8/id/0/10000') }}">
                                        <img src="http://tbd/images/excel.png" class="specs-docs-image">
                                    </a>

                                </td>
                            </tr>

                            </tbody>
                        </table>
                    </div>


                </div>


                <div id="PD" class="tabcontent" style="display: none;">

                    <h4 class="orange-color p-y-lg">Specification Documents</h4>


                    <div id="MyElement" class="alert alert-danger alert-dismissible  hide"
                         style="background-color:#dc3545; border-color: #dc3545; ">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>

                        <h3 style="color:#fff;margin-top:0px;"><strong><i class="fa fa-close"
                                                                          aria-hidden="true"></i></strong>
                        </h3>

                        <p style="color:#fff"></p>

                    </div>


                    <div class="">
                        <table class="table">
                            <thead>
                            <tr class="newth">
                                <th style="width:30%">File Name</th>
                                <th style="width:15%">Size</th>
                                <th style="width:15%">Date</th>
                                <th style="width:10%; background-color: #084887;">
                                    <div class="downloadHistoryUser grabDoc left-align">


                                        <a style="color:white;"
                                           href="http://tbd.omairusaf.com/grab_all_files/8/bid_doc_url">
                                            <i class="fa fa-download" aria-hidden="true"></i>
                                            <span> Grab All Docs </span>
                                        </a>
                                    </div>
                                </th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td style="word-wrap:break-word; font-weight: bold;">
                                    A-02
                                </td>

                                <td>
                                    88.19 KB
                                </td>


                                <td> Aug 26, 2019</td>
                                <td>


                                    <a href=" http://tbd.omairusaf.com/public/bid_uploads/20190804015447_A-02.pdf ">
                                        <img class="specs-docs-image" src="http://tbd.omairusaf.com/images/pdf.png">
                                    </a>


                                </td>
                            </tr>
                            <tr>
                                <td style="word-wrap:break-word; font-weight: bold;">
                                    A-03
                                </td>

                                <td>
                                    108.4 KB
                                </td>


                                <td> Aug 26, 2019</td>
                                <td>


                                    <a href=" http://tbd.omairusaf.com/public/bid_uploads/20190804015448_A-03.pdf ">
                                        <img class="specs-docs-image" src="http://tbd.omairusaf.com/images/pdf.png">
                                    </a>


                                </td>
                            </tr>
                            <tr>
                                <td style="word-wrap:break-word; font-weight: bold;">
                                    A-04
                                </td>

                                <td>
                                    130.95 KB
                                </td>


                                <td> Aug 26, 2019</td>
                                <td>


                                    <a href=" http://tbd.omairusaf.com/public/bid_uploads/20190804015454_A-04.pdf ">
                                        <img class="specs-docs-image" src="http://tbd.omairusaf.com/images/pdf.png">
                                    </a>


                                </td>
                            </tr>
                            <tr>
                                <td style="word-wrap:break-word; font-weight: bold;">
                                    A-05
                                </td>

                                <td>
                                    261.53 KB
                                </td>


                                <td> Aug 26, 2019</td>
                                <td>


                                    <a href=" http://tbd.omairusaf.com/public/bid_uploads/20190804015459_A-05.pdf ">
                                        <img class="specs-docs-image" src="http://tbd.omairusaf.com/images/pdf.png">
                                    </a>


                                </td>
                            </tr>
                            <tr>
                                <td style="word-wrap:break-word; font-weight: bold;">
                                    A-06
                                </td>

                                <td>
                                    277.23 KB
                                </td>


                                <td> Aug 26, 2019</td>
                                <td>


                                    <a href=" http://tbd.omairusaf.com/public/bid_uploads/20190804015513_A-06.pdf ">
                                        <img class="specs-docs-image" src="http://tbd.omairusaf.com/images/pdf.png">
                                    </a>


                                </td>
                            </tr>
                            <tr>
                                <td style="word-wrap:break-word; font-weight: bold;">
                                    S-02
                                </td>

                                <td>
                                    247.31 KB
                                </td>


                                <td> Aug 26, 2019</td>
                                <td>


                                    <a href=" http://tbd.omairusaf.com/public/bid_uploads/20190804015528_S-02.pdf ">
                                        <img class="specs-docs-image" src="http://tbd.omairusaf.com/images/pdf.png">
                                    </a>


                                </td>
                            </tr>
                            <tr>
                                <td style="word-wrap:break-word; font-weight: bold;">
                                    S-03
                                </td>

                                <td>
                                    209.11 KB
                                </td>


                                <td> Aug 26, 2019</td>
                                <td>


                                    <a href=" http://tbd.omairusaf.com/public/bid_uploads/20190804015537_S-03.pdf ">
                                        <img class="specs-docs-image" src="http://tbd.omairusaf.com/images/pdf.png">
                                    </a>


                                </td>
                            </tr>
                            <tr>
                                <td style="word-wrap:break-word; font-weight: bold;">
                                    S-04
                                </td>

                                <td>
                                    397.56 KB
                                </td>


                                <td> Aug 26, 2019</td>
                                <td>


                                    <a href=" http://tbd.omairusaf.com/public/bid_uploads/20190804015546_S-04.pdf ">
                                        <img class="specs-docs-image" src="http://tbd.omairusaf.com/images/pdf.png">
                                    </a>


                                </td>
                            </tr>
                            <tr>
                                <td style="word-wrap:break-word; font-weight: bold;">
                                    S-05
                                </td>

                                <td>
                                    1.37 MB
                                </td>


                                <td> Aug 26, 2019</td>
                                <td>


                                    <a href=" http://tbd.omairusaf.com/public/bid_uploads/20190804015607_S-05.pdf ">
                                        <img class="specs-docs-image" src="http://tbd.omairusaf.com/images/pdf.png">
                                    </a>


                                </td>
                            </tr>
                            <tr>
                                <td style="word-wrap:break-word; font-weight: bold;">
                                    S-06
                                </td>

                                <td>
                                    195.27 KB
                                </td>


                                <td> Aug 26, 2019</td>
                                <td>


                                    <a href=" http://tbd.omairusaf.com/public/bid_uploads/20190804015616_S-06.pdf ">
                                        <img class="specs-docs-image" src="http://tbd.omairusaf.com/images/pdf.png">
                                    </a>


                                </td>
                            </tr>
                            <tr>
                                <td style="word-wrap:break-word; font-weight: bold;">
                                    S-07
                                </td>

                                <td>
                                    466.01 KB
                                </td>


                                <td> Aug 26, 2019</td>
                                <td>


                                    <a href=" http://tbd.omairusaf.com/public/bid_uploads/20190804015621_S-07.pdf ">
                                        <img class="specs-docs-image" src="http://tbd.omairusaf.com/images/pdf.png">
                                    </a>


                                </td>
                            </tr>
                            <tr>
                                <td style="word-wrap:break-word; font-weight: bold;">
                                    S-08
                                </td>

                                <td>
                                    233.52 KB
                                </td>


                                <td> Aug 26, 2019</td>
                                <td>


                                    <a href=" http://tbd.omairusaf.com/public/bid_uploads/20190804015624_S-08.pdf ">
                                        <img class="specs-docs-image" src="http://tbd.omairusaf.com/images/pdf.png">
                                    </a>


                                </td>
                            </tr>
                            <tr>
                                <td style="word-wrap:break-word; font-weight: bold;">
                                    S-09
                                </td>

                                <td>
                                    490.71 KB
                                </td>


                                <td> Aug 26, 2019</td>
                                <td>


                                    <a href=" http://tbd.omairusaf.com/public/bid_uploads/20190804015638_S-09.pdf ">
                                        <img class="specs-docs-image" src="http://tbd.omairusaf.com/images/pdf.png">
                                    </a>


                                </td>
                            </tr>
                            <tr>
                                <td style="word-wrap:break-word; font-weight: bold;">
                                    S-10
                                </td>

                                <td>
                                    210.52 KB
                                </td>


                                <td> Aug 26, 2019</td>
                                <td>


                                    <a href=" http://tbd.omairusaf.com/public/bid_uploads/20190804015635_S-10.pdf ">
                                        <img class="specs-docs-image" src="http://tbd.omairusaf.com/images/pdf.png">
                                    </a>


                                </td>
                            </tr>
                            <tr>
                                <td style="word-wrap:break-word; font-weight: bold;">
                                    S-11
                                </td>

                                <td>
                                    211.85 KB
                                </td>


                                <td> Aug 26, 2019</td>
                                <td>


                                    <a href=" http://tbd.omairusaf.com/public/bid_uploads/20190804015646_S-11.pdf ">
                                        <img class="specs-docs-image" src="http://tbd.omairusaf.com/images/pdf.png">
                                    </a>


                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>


                </div>

                <div id="addendum" class="tabcontent" style="display: none;">

                    <h4 class="orange-color p-y-lg">Addendum Documents</h4>


                    <div id="MyElement" class="alert alert-danger alert-dismissible  hide"
                         style="background-color:#dc3545; border-color: #dc3545; ">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>

                        <h3 style="color:#fff;margin-top:0px;"><strong><i class="fa fa-close"
                                                                          aria-hidden="true"></i></strong>
                        </h3>

                        <p style="color:#fff"></p>

                    </div>

                    <div class="">
                        <table class="table ">
                            <thead>
                            <tr class="newth">
                                <th style="width:30%">File Name</th>
                                <th style="width:15%">Size</th>
                                <th style="width:15%">Date</th>
                                <th style="width:10%; background-color: #084887;">
                                    <div class="downloadHistoryUser grabDoc left-align">


                                        <a style="color:white;" href="{{ asset('grab_all_files/8/bid_doc_url') }}">
                                            <i class="fa fa-download" aria-hidden="true"></i>
                                            <span> Grab All Docs </span>
                                        </a>
                                    </div>

                                </th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td style="word-wrap:break-word;font-weight: 700;">
                                    S-09
                                </td>

                                <td>
                                    490.71 KB
                                </td>


                                <td> Aug 07, 2019</td>
                                <td>


                                    <a href="{{ asset('bid_uploads/20190804021601_S-09.pdf') }}">
                                        <img class="specs-docs-image" src="http://tbd/images/pdf.png">
                                    </a>


                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>


                </div>

                <div id="plans" class="tabcontent" style="display: none;">

                    <h4 class="orange-color p-y-lg">Plan Documents</h4>

                    <div id="MyElement" class="alert alert-danger alert-dismissible  hide"
                         style="background-color:#dc3545; border-color: #dc3545;">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>

                        <h3 style="color:#fff;margin-top:0px;"><strong><i class="fa fa-close"
                                                                          aria-hidden="true"></i></strong>
                        </h3>

                        <p style="color:#fff"></p>

                    </div>
                    <div class="">
                        <table class="table " style="">
                            <thead>
                            <tr class="newth">
                                <th style="width:30%">File Name</th>
                                <th style="width:15%">Size</th>
                                <th style="width:15%">Date</th>
                                <th style="width:10%; background-color: #084887;">
                                    <div class="downloadHistoryUser grabDoc left-align">


                                        <a style="color:white;" href="http://tbd/grab_all_files/8/bid_doc_url">
                                            <i class="fa fa-download" aria-hidden="true"></i>
                                            <span> Grab All Docs </span>
                                        </a>
                                    </div>
                                </th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td style="word-wrap:break-word;font-weight: 700;"> A-02</td>
                                <td>
                                    88.19 KB
                                </td>
                                <td> Aug 07, 2019</td>
                                <td>


                                    <a href="{{ asset('public/bid_uploads/20190804021623_A-02.pdf') }}">
                                        <img class="specs-docs-image" src="http://tbd/images/pdf.png">
                                    </a>


                                </td>
                            </tr>
                            <tr>
                                <td style="word-wrap:break-word;font-weight: 700;"> A-03</td>
                                <td>
                                    108.4 KB
                                </td>
                                <td> Aug 07, 2019</td>
                                <td>


                                    <a href="{{ asset('public/bid_uploads/20190804021624_A-03.pdf') }}">
                                        <img class="specs-docs-image" src="http://tbd/images/pdf.png">
                                    </a>


                                </td>
                            </tr>
                            <tr>
                                <td style="word-wrap:break-word;font-weight: 700;"> A-04</td>
                                <td>
                                    130.95 KB
                                </td>
                                <td> Aug 07, 2019</td>
                                <td>


                                    <a href="{{ asset('public/bid_uploads/20190804021626_A-04.pdf') }}">
                                        <img class="specs-docs-image" src="http://tbd/images/pdf.png">
                                    </a>


                                </td>
                            </tr>
                            <tr>
                                <td style="word-wrap:break-word;font-weight: 700;"> S-02</td>
                                <td>
                                    247.31 KB
                                </td>
                                <td> Aug 07, 2019</td>
                                <td>


                                    <a href="{{ asset('public/bid_uploads/20190804021633_S-02.pdf') }}">
                                        <img class="specs-docs-image" src="http://tbd/images/pdf.png">
                                    </a>


                                </td>
                            </tr>
                            <tr>
                                <td style="word-wrap:break-word;font-weight: 700;"> S-03</td>
                                <td>
                                    209.11 KB
                                </td>
                                <td> Aug 07, 2019</td>
                                <td>


                                    <a href="{{ asset('public/bid_uploads/20190804021638_S-03.pdf') }}">
                                        <img class="specs-docs-image" src="http://tbd/images/pdf.png">
                                    </a>


                                </td>
                            </tr>
                            <tr>
                                <td style="word-wrap:break-word;font-weight: 700;"> S-04</td>
                                <td>
                                    397.56 KB
                                </td>
                                <td> Aug 07, 2019</td>
                                <td>


                                    <a href="{{ asset('public/bid_uploads/20190804021638_S-04.pdf') }}">
                                        <img class="specs-docs-image" src="http://tbd/images/pdf.png">
                                    </a>


                                </td>
                            </tr>
                            <tr>
                                <td style="word-wrap:break-word;font-weight: 700;"> S-06</td>
                                <td>
                                    195.27 KB
                                </td>
                                <td> Aug 07, 2019</td>
                                <td>


                                    <a href="{{ asset('public/bid_uploads/20190804021648_S-06.pdf') }}">
                                        <img class="specs-docs-image" src="http://tbd/images/pdf.png">
                                    </a>


                                </td>
                            </tr>
                            <tr>
                                <td style="word-wrap:break-word;font-weight: 700;"> S-07</td>
                                <td>
                                    466.01 KB
                                </td>
                                <td> Aug 07, 2019</td>
                                <td>


                                    <a href="{{ asset('public/bid_uploads/20190804021701_S-07.pdf') }}">
                                        <img class="specs-docs-image" src="http://tbd/images/pdf.png">
                                    </a>


                                </td>
                            </tr>
                            <tr>
                                <td style="word-wrap:break-word;font-weight: 700;"> S-08</td>
                                <td>
                                    233.52 KB
                                </td>
                                <td> Aug 07, 2019</td>
                                <td>


                                    <a href="{{ asset('public/bid_uploads/20190804021700_S-08.pdf') }}">
                                        <img class="specs-docs-image" src="http://tbd/images/pdf.png">
                                    </a>


                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>

                </div>


                <div id="qtyTakeOff-video" class="tabcontent" style="display: none;">

                    <h4 class="orange-color p-y-lg">Quantity Takeoff Video</h4>

                    <div class="video-container">
                        <div class="youtube-article">
                            <iframe class="dt-youtube" src="//www.youtube.com/embed/O0OXfN0JQ5s" allowfullscreen=""
                                    width="100%" height="360" frameborder="0"></iframe>
                        </div>
                    </div>

                </div>


                <div style="padding-left: 12px; display: none;" class="tabcontent" id="owners">

                    <h4 class="orange-color p-y-lg">Project Owner</h4>


                    <img src="http://tbd/images/user-512.png" alt="Avatar" style="width:100px; display:none;">

                    <div class="row">

                        <div class="col-md-4 col-xs-5" style="padding-right: 0px; padding-top: 10px">
                            <h4>Owner/Authority</h4>
                            <hr>
                            <h4>Address</h4>
                            <hr>
                            <h4>Contact Name</h4>
                            <hr>
                            <h4>Contact Number</h4>
                            <hr>
                            <h4>Email</h4>
                            <hr>
                            <h4>Fax</h4>
                            <hr>
                        </div>

                        <div class="col-md-8 col-xs-7 " style="padding-top: 8px">
                            <p class="">New York City School Construction Authority</p>
                            <hr>
                            <p class="">30-30 Thomson Avenue, Long Island City, NY 11101-3045</p>
                            <hr>
                            <p class="">--</p>
                            <hr>
                            <p class="">--</p>
                            <hr>
                            <p class="">lpersaud@nycsca.org</p>
                            <hr>
                            <p class="">7184720477</p>
                            <hr>
                        </div>

                    </div>


                </div>


                <div style="padding-left: 0px; display: none;" class="tabcontent" id="planholder">

                    <h4 class="orange-color p-y-lg">Project Plan Holders</h4>


                    <div class="table-responsive">

                        <table class="table ">
                            <thead>
                            <tr class="newth">
                                <th style="width:5%">#</th>
                                <th style="width:20%">Company</th>
                                <th style="width:35%">Address</th>
                                <th style="width: 20%">Contact Name</th>
                                <th style="width: 20%">Contact #</th>
                            </tr>
                            </thead>
                            <tbody>


                            <tr>
                                <td>1</td>
                                <td>Plan Holder</td>
                                <td>Main market street</td>
                                <td>askjdhakjshal</td>
                                <td></td>
                            </tr>


                            <tr>
                                <td>2</td>
                                <td>chaudry malik</td>
                                <td>house 43 street 67</td>
                                <td>087363333333</td>
                                <td></td>
                            </tr>


                            <tr>
                                <td>3</td>
                                <td>Buildcroft Construction Private Limited</td>
                                <td>30-30 Thomson Avenue, Long Island City, NY 11101-3045</td>
                                <td>Mr Jawad</td>
                                <td>(718) 472-8230</td>
                            </tr>


                            <tr>
                                <td>4</td>
                                <td>Plan Holder Company</td>
                                <td>Islamabad, Pakistan</td>
                                <td>Contact Name</td>
                                <td>(963) 113-2132</td>
                            </tr>

                            <tr>
                                <td>5</td>
                                <td>ABC Consulting Private Limited</td>
                                <td>Thomasville, NC, USA</td>
                                <td>Mr John Mason</td>
                                <td>(718) 111-1234</td>
                            </tr>


                            </tbody>
                        </table>
                    </div>


                </div>


                <div class="tabcontent" id="bidResult" style="display: none;">

                    <table class="table ">
                        <thead>
                        <tr class="newth">
                            <th style="width:20%">#</th>
                            <th style="width:30%">Name</th>
                            <th style="width:20%">Price</th>
                            <th style="width: 30%">Date</th>
                        </tr>
                        </thead>
                        <tbody>

                        <tr>
                            <td>1</td>
                            <td>Plan Holder Company</td>
                            <td>$80000000</td>
                            <td>Aug 04, 2019 02:00 pm</td>
                        </tr>
                        <tr>
                            <td>2</td>
                            <td>ABC Consulting Private Limited</td>
                            <td>$10000000</td>
                            <td>Aug 04, 2019 02:00 pm</td>
                        </tr>
                        <tr>
                            <td>3</td>
                            <td>chaudry malik</td>
                            <td>$100000</td>
                            <td>Aug 05, 2019 06:20 pm</td>
                        </tr>
                        </tbody>
                    </table>
                </div>


            </div>


        </div>


    </div>

    <script>
        function openCity(evt, cityName) {
            var i, tabcontent, tablinks;
            tabcontent = document.getElementsByClassName("tabcontent");
            for (i = 0; i < tabcontent.length; i++) {
                tabcontent[i].style.display = "none";
            }
            tablinks = document.getElementsByClassName("tablinks");
            for (i = 0; i < tablinks.length; i++) {
                tablinks[i].className = tablinks[i].className.replace(" active", "");
            }
            document.getElementById(cityName).style.display = "block";
            evt.currentTarget.className += " active";
        }

        // Get the element with id="defaultOpen" and click on it


        //document.getElementById("defaultOpen").click();


        var existtwo = '{{Session::has('alertone')}}';

        if (existtwo) {

            document.getElementById('MyElement').classList.add('show');
            document.getElementById('MyElement').classList.remove('hide');

        }

        function printDiv(divName) {
            var printContents = document.getElementById(divName).innerHTML;
            var originalContents = document.body.innerHTML;

            document.body.innerHTML = printContents;

            window.print();

            document.body.innerHTML = originalContents;
        }
    </script>


@endsection
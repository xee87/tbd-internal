@extends('layout')

@section('content')



    <section class="about-section">

        <div id="why-us">
            <div class="we-are-master-one">
            <div class="container">

                <div class="we-are-master">

                    <div class="row">

                        <div class="col-md-6">

                            <div class="ready-for-quality">
                                <div class=""><h2 class="about-us">Why Us</h2></div>
                                <div class=""><h3 class="master">Yes, We are master!</h3></div>
                                <div class=""><h3 class="master">and We are ready to help you.</h3></div>
                                <div class=""><p class="master-two">it's the quality of our estimators that sets us apart.</p>
                                </div>
                            </div>

                        </div>


                        <div class="col-md-6 align-right">
                            <img class="about-us-banner" src="images/aboutus-banner-image.png">
                        </div>


                    </div>
                </div>


            </div>
            </div>

            <div class="bg-gray">
                <div class="container">
                        <div class="row">
                            <div class="col-md-2 col-xs-4"><img class="progress-icons" src="images/win.png">

                                <p class="bg-color-white">Project Leads</p>
                            </div>
                            <div class="col-md-2 col-xs-4"><img class="progress-icons" src="images/home.png">

                                <p class="bg-color-white">Plans & Specs</p>
                            </div>

                            <div class="col-md-2 col-xs-4"><img class="progress-icons" src="images/clipboard.png">

                                <p class="bg-color-white">Quantities</p>
                            </div>
                            <div class="col-md-2 col-xs-4"><img class="progress-icons" src="images/result.png">

                                <p class="bg-color-white">Bid Results</p>
                            </div>
                            <div class="col-md-2 col-xs-4"><img class="progress-icons" src=" images/analysis (1).png">

                                <p class="bg-color-white">Rate Analysis</p>
                            </div>
                            <div class="col-md-2 col-xs-4"><img class="progress-icons" src=" images/bar-chart.png">

                                <p class="bg-color-white">Primavera P6</p>
                            </div>


                        </div>
                </div>
            </div>


            <div class="like-bid">

                <div class="dark-buildings">

                    <div class="container">

                        <div class="row">

                            <div class="col-md-12 col-sm-12">
                                <h1 class="like-a-winner">Bid Like a Winner with True Bid Data</h1>
                            </div>

                            <div class="col-md-7 align-left">

                                <p style="padding-top: 10px;">We believe that there is no better way to Bid, other than True Bid Data. As we create articulate Bid Data out of scattered information. </p>
                                <br>

                                <p>A more valuable, less invasive way, where customers are earned rather than bought. It’s our mission to make every important aspect of our customer experience better with unmatched absoluteness, courteousness and consideration.</p>

                            </div>

                            <div class="col-md-5 align-right">
                                <img class="buildings" src="images/buildings-dark.png">
                            </div>

                        </div>

                    </div>
                </div>
                <div class="bg-green-splash">
                <div class="container">
                    <div class="black-color">


                        <p>Our centre of interest is to make sure that our client gets all the Bid Data in just one
                            click without any ambiguity and errors. We ensure that our Quantity Take-offs are precise
                            and immaculate. </p>
                        <br>

                        <p>Agility, reliability and ability are our main strengths, which are utilized in a balanced way
                            to fulfil our customers’ requirements, taking it to a whole new level of certainty within
                            the time frame. </p>
                        <br>

                        <p>We have technically sound, highly experienced, motivated and professional staff, using their
                            expertise to get the work done in time, with high standard of accuracy. Our team is capable
                            to examine the bids with delicacy, keeping in mind the interest of client. </p>
                        <br>

                        <p>We are the only ones that provides bid details and bid documents free of charge. However, for Quantity Take-off, there are many options available. One new feature is added, if any customer
                            wants Quantity Take-off of any particular division or project, they can buy it alone rather than buying
                            the subscription plans. (One-Off Feature)</p>
                        <br>

                        <p> Take-off on Demand (TOD) provides Quantity Take-off of the projects which are not listed on
                            our website. This allows customer to gain every angle coverage of the project
                            quantities.</p>
                        <br>

                        <p> Surely, we are pioneers in bidding solutions. That’s why, we are True Bid Data. </p>
                    </div>


                </div>
                </div>
            </div>


        </div>


    </section>


@endsection
@extends('layout')

@section('content')

    <link rel="stylesheet" href="{{asset('plugins/datatables/dataTables.bootstrap4.css')}}">
    <script src="{{asset('plugins/datatables/jquery.dataTables.js')}}"></script>
    <script src="{{asset('plugins/datatables/dataTables.bootstrap4.js')}}"></script>



    <div class="container" style="margin-top:4%; margin-bottom: 6%">

        <div id="MyElement" class="alert alert-success alert-dismissible hide" style="background-color:#28a745">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <h3 style="color:#fff"><strong><i class="fa fa-check" aria-hidden="true"></i></strong> Alert!</h3>
            <p style="color:#fff">{{Session::get('alertone')}}</p>

        </div>


        <div class="row">


            <div class="col-lg-12">

                <h2 style="color:#084887;">Download History</h2>

            </div>
        </div>

        <?php if(!empty($d->downloads) || !empty($singleDownloaded)){ ?>


        @if(!empty($d->downloads))
            <div class="row">
                <div class="col-lg-12">

                        {{--<h4><span style="color:#084887">{{$d->downloads_used}}</span> downloads used out of <b>Unlimited</b> </h4>--}}

                        <h4><span style="color:#084887">{{$d->downloads_used}}</span> downloads used out of <?php echo ($d->downloads != -1)?$d->downloads:'<b>Unlimited</b>'; ?> </h4>

                    <br>

                    @if($d->downloads != -1)

                        <?php
                        $percentage = ($d->downloads_used / $d->downloads) * 100;
                        $newper = (int)$percentage;
                        ?>
                        <div class="progress">
                            <div class="progress-bar" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100" style="width:{{$newper}}%; background-color:#084887; ">
                                {{$newper}}%
                            </div>
                        </div>
                    @endif
                </div>
            </div>
        @else
            <?php
                $user_project = $singleDownloaded;
            ?>
        @endif

        <div class="row">
            <div class="col-lg-12">
                <div class="table-responsive">
                    <table id="example" class="table table-bordered table-hover ">
                        <thead>
                            <tr>
                            <th>Sr #</th>
                            <th>Project Title</th>
                            <th>File Name</th>
                            <th>Download Date</th>

                        </tr>
                        </thead>
                        <tbody>
                        <?php $countit=1;?>
                            @foreach($user_project as $up)
                                <tr>
                                    <td>{{$countit}}</td>
                                    <td>{{$up->project_title}}</td>
                                    <td>{{$up->file_name}}</td>
                                    <td>{{$up->created_at}}</td>

                                </tr>
                                <?php $countit++;?>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>

        </div>

        <?php } else{ ?>

            <center><h4>No Record Found</h4></center>


        <?php } ?>

    </div>

    <script>

        var existtwo = '{{Session::has('alertone')}}';

        if(existtwo){

            document.getElementById('MyElement').classList.add('show');
            document.getElementById('MyElement').classList.remove('hide');

        }
    </script>

    </body>

    <script type="text/javascript">
        $(document).ready(function() {
            $('#example').dataTable( {
                "pageLength": 30
            } );
        } );
    </script>

@endsection
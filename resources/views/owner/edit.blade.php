@extends('layouts.app')
@section('content')
<!-- Select2 -->
  <link rel="stylesheet" href="{{asset('/plugins/select2/select2.min.css')}}">
  

<style type="text/css">
    .dropzone {
    
    width: 100%;
}

.select2-container--default .select2-selection--multiple .select2-selection__choice{

  color:black;
}
</style>
<!-- Content Header (Page header) -->

<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
            </div>
            <div class="col-sm-6">
                <div class="float-sm-right">

                </div>
            </div>
        </div>
    </div>
</section>

<!-- Main content -->
<section class="content">
    <div class="card card-info card-outline">
        <div class="container-fluid mt-2">
            <div class="info-box bg-info">
                <span class="info-box-icon"><a class="add-resource" href="{{route('owner_list')}}" title="Add New Group"> <i class="fa fa-arrow-left"></i></a></span>
                <div class="info-box-content">
                    <div class="col-md-5 col-sm-6 col-xs-6">
                        <h1>Update Owner</h1>
                    </div>
                </div>
                <span class="info-box-icon float-right">
                    <i class="fa fa-list"></i>
                </span>
            </div>
        </div>
       
            <form action="{{ route('owner_update', $ownerinfo->id) }}" method="post" role="form" enctype="multipart/form-data"  style="border:0px;padding:0px 0px;" >
                @csrf
                @if ($errors->any())
                            @foreach ($errors->all() as $error)
                                <div class="alert alert-danger mb-2" role="alert">
                                    {{ $error }}
                                </div>
                                @endforeach
                            @endif
                <div class="row " style="padding: 40px;">


                    <div class="col-lg-4 mb-3">
                        <label>Authority</label>
                        <input placeholder="Owner" type="text" class="form-control{{ $errors->has('ownername') ? ' is-invalid' : '' }}" name="ownername" value="{{ $ownerinfo->owner_name }}" required autofocus>
                    </div>

                    <div class="col-lg-4 mb-3">
                        <label>Contact Name</label>
                        <input placeholder="Contact Name" type="text" class="form-control{{ $errors->has('contact_name') ? ' is-invalid' : '' }}"
                               name="contact_name" value="{{ $ownerinfo->contact_name }}" required autofocus>
                    </div>

                    <div class="col-lg-4 mb-3">
                        <label>Address</label>
                        <input id="autocomplete" onFocus="geolocate()" placeholder="Owner Address" type="text" class="form-control{{ $errors->has('owneraddress') ? ' is-invalid' : '' }}"
                               name="owneraddress" value="{{ $ownerinfo->owner_address }}" autofocus>
                    </div>

                    <div class="col-lg-4 mb-3">
                        <label>Phone</label>
                        <input placeholder="Phone" type="tel" class="usPhoneFax form-control{{ $errors->has('owner_phone') ? ' is-invalid' : '' }}"
                               name="owner_phone" value="{{ $ownerinfo->owner_phone }}" required autofocus>
                    </div>

                    <div class="col-lg-4 mb-3">
                        <label>Email</label>
                        <input placeholder="Email" type="email" class="form-control{{ $errors->has('owner_email') ? ' is-invalid' : '' }}"
                               name="owner_email" value="{{ $ownerinfo->owner_email }}" autofocus>
                    </div>

                    <div class="col-lg-4 mb-3">
                        <label>Fax</label>
                        <input placeholder="Fax" type="text" class="usPhoneFax form-control{{ $errors->has('owner_fax') ? ' is-invalid' : '' }}"
                               name="owner_fax" value="{{ $ownerinfo->owner_fax }}" autofocus>
                    </div>

                <div class="col-lg-4 mb-3">
                  
                    <label>Project</label>

                    <select class="form-control" name="owner_proj">

                        <option value="">--Select Project--</option>
                        @foreach($user_pro as $up)

                            <option value="{{$up->id}}">{{$up->title}}</option>


                        @endforeach
                    </select>
                
                </div>
                 

                  <div class="col-lg-12 mb-3">
                  
                    <label>Project Owned</label>

                      <div class="container-fluid">
                        
                            <div class="row">
                                @foreach($user_pro_owner as $upo)
                                  <div class="col-lg-2">
                                     <span onclick = " delpro('{{$upo->id}}') " class="list-group-item active">

                                        {{$upo->title}}
                                        <span class="pull-right" id="slide-submenu">
                                            <i class="fa fa-times"></i>
                                        </span>

                                    </span>
                                  </div> 
                                @endforeach


                             </div>     
                      </div>
                </div>
                 
                </div>
               
            
               <div class="col-lg-12 " style="background-color: rgba(0,0,0,.03); padding: 2%">
                              <center>
                                  <button type="submit" class="btn btn-info  " style="padding-left:6% ;padding-right:6% ">Update</button>
                                    
                                </center>
                </div>
                
                </form>
       
        <!-- /.card-body -->
  
    <!-- /.card -->
  </div>
</section>

<script>


var placeSearch, autocomplete;

function initAutocomplete() {
  autocomplete = new google.maps.places.Autocomplete(
      document.getElementById('autocomplete'), {types: ['geocode']});
      autocomplete = new google.maps.places.Autocomplete(
      document.getElementById('autocomplete1'), {types: ['geocode']});
//   autocomplete.setFields('address_components');
//   autocomplete.addListener('place_changed', fillInAddress);
}

function geolocate() {
  if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(function(position) {
      var geolocation = {
        lat: position.coords.latitude,
        lng: position.coords.longitude
      };
      var circle = new google.maps.Circle(
          {center: geolocation, radius: position.coords.accuracy});
      autocomplete.setBounds(circle.getBounds());
    });
  }
}
</script>

    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD5KPKoO7ZP-grfU1aOx2GD1ra1pQMBdAQ&libraries=places&callback=initAutocomplete"
        async defer></script>

@endsection

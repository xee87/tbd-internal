<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
	<head>
		<!--[if gte mso 9]><xml><o:OfficeDocumentSettings><o:AllowPNG/><o:PixelsPerInch>96</o:PixelsPerInch></o:OfficeDocumentSettings></xml><![endif]-->
		<title>TBD - Email Template</title>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<style type="text/css">
			a{
				outline:none;
				color:#76C5B5;
				text-decoration:none;
			}
			a:hover{text-decoration:underline !important;}
			.h-n a{text-decoration:underline;}
			.h-n a:hover{text-decoration:none !important;}
			a[x-apple-data-detectors]{color:inherit !important; text-decoration:none !important;}
			a[href^="tel"]:hover{text-decoration:none !important;}
			.active-i a:hover,
			.active-t:hover{opacity:0.8;}
			.active-i a,
			.active-t{transition:all 0.2s ease;}
			a img{border:none;}
			b, strong{font-weight:700;}
			p{margin:0;}
			th{padding:0;}
			table td{mso-line-height-rule:exactly;}
			.ns span{color:inherit !important; text-decoration:none !important; border:none !important;}
			.ns a{color:inherit !important; text-decoration:none !important; border:none !important;}
			.l-white a{color:#fff;}
			[style*="Open Sans"]{font-family:Open Sans, Arial, Helvetica, sans-serif !important;}
			[style*="Open Sans"] font-weight:700;{font-family:Open Sans, font-weight:700; Arial, Helvetica, sans-serif !important;}
			[style*="Open Sans"]{font-family:Open Sans, Times, Times New Roman !important;}
			* { -webkit-font-smoothing: antialiased; -moz-osx-font-smoothing: grayscale; }
			.ls{letter-spacing:.2px;}
			@media only screen and (max-width:375px) and (min-width:374px) {
				.gmail-fix{min-width:374px !important;}
			}
			@media only screen and (max-width:414px) and (min-width:413px) {
				.gmail-fix{min-width:413px !important;}
			}
			@media only screen and (max-width:623px) {
				.w-100p{width:100%!important;}
			}
			@media only screen and (max-width:500px) {
				.tflex{display:block!important;width:100%!important;box-sizing:border-box!important;}
				.d-b{display:block!important;}
				.ta-c{text-align:center!important;}
				.ta-r{text-align:right!important;}
				.thold{display:table!important;width:100%!important;}
				.thead{display:table-header-group!important;width:100%!important;}
				.trow{display:table-row!important;width:100%!important;}
				.tfoot{display:table-footer-group!important;width:100%!important;}
				.hm{display:none!important;width:0!important;height:0!important;padding:0!important;font-size:0!important;line-height:0!important;}
				.w-70{width:70px!important;}
				.w-155{width:155px!important;}
				.lh-19{line-height:19px!important;}
				.plr-0{padding-left:0!important;padding-right:0!important;}
				.plr-15{padding-left:15px!important;padding-right:15px!important;}
				.ptb-24{padding-top:24px!important;padding-bottom:24px!important;}
				.ptb-26{padding-top:26px!important;padding-bottom:26px!important;}
				.pt-32{padding-top:32px!important;}
				.pb-17{padding-bottom:17px!important;}
				.pb-35{padding-bottom:35px!important;}
				.pb-61{padding-bottom:61px!important;}
			}
		</style>

		<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,700" rel="stylesheet">

		<!--[if mso]>
		<style type=”text/css”>
			.ls-mso{letter-spacing:0;}
		</style>
		<![endif]-->
	</head>
	<body style="background:#fff; margin:0; padding:0; -webkit-text-size-adjust:100%; -ms-text-size-adjust:100%;">
		<table class="gmail-fix" width="100%" style="background:#f5f5f5; min-width:320px;" cellspacing="0" cellpadding="0">
			<!-- header -->
			<tr>
				<td class="ptb-26" style="background:#f7f7f7; border-bottom:1px solid #d6d6d5; padding:20px 15px 15px;">
					<table class="w-100p" width="576" align="center" style="max-width:576px; margin:0 auto;" cellpadding="0" cellspacing="0">
						<tr>
							<td>
								<table width="100%" cellpadding="0" cellspacing="0">
									<tr>
										<td style="padding:0 0 5px;">
											<center>	<h3>
												True Bid Date
											</h3></center>
										</td>
									</tr>
								</table>
							</td>
							<td width="20"></td>
						</tr>
					</table>
				</td>
			</tr>
			<!-- content -->
			<tr>
				<td class="pt-32 pb-61" style="border-bottom:1px solid #d6d6d5; padding:32px 15px 0px;">
					<table class="w-100p" width="576" align="center" style="max-width:576px; margin:0 auto;" cellpadding="0" cellspacing="0">
						<tr>
							<td style="padding:0 0 14px; font:700 28px/32px 'Open Sans', sans-serif; color:#222222;">
								PROJECT ASSIGNED!
							</td>
						</tr>
						<tr>
							<td class="pb-35" style="padding:0 0 43px; font:16px/24px 'Open Sans', Arial, Helvetica, sans-serif, ; color:#222222; letter-spacing:.2px;">

								
						

								<p style="margin:16px 0 8px 0; font-weight:700;">\
								<h1>Hi, {{ $name }}</h1>
								You have been assigned a project. Please Login to your account. 
								</p>
								<center>
								<a href="{{ url('/user_login') }}">Log In</a>
								</center>
							</td>
						</tr>
					</table>
				</td>
			</tr>
			<!-- footer -->
			<tr>
				<td class="ptb-24" style="background:#222222; padding:50px 15px;">
					<table class="w-100p" width="576" align="center" style="max-width:576px; margin:0 auto;" cellpadding="0" cellspacing="0">
						<tr>
						
							<th class="tflex" width="27" height="16"></th>
							<th class="tflex" align="left">
								<table width="100%" cellpadding="0" cellspacing="0">
									<tr>
										<td class="ls-mso ls ta-c" style="font:12px/19px Arial, Helvetica, sans-serif, 'Open Sans'; color:#ffffff;">
											<center>
											<span class="ns d-b">© TBD</span><span class="hm"> &nbsp;&bull;&nbsp; </span>
											<a style="color:#ffffff; text-decoration:none;" href="mailto:info@cipp2023.com">info@tbd.com</a>
											</center>
										</td>
									</tr>
								</table>
							</th>
						</tr>
					</table>
				</td>
			</tr>
		</table>
	</body>
</html>

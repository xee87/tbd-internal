@extends('layouts.app')

@section('content')
<!-- Select2 -->
  <link rel="stylesheet" href="{{asset('/plugins/select2/select2.min.css')}}">
  
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.4.0/min/dropzone.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.4.0/dropzone.js"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<style type="text/css">
    .dropzone {
    
    width: 100%;
}

.select2-container--default .select2-selection--multiple .select2-selection__choice{

	color:black;
}
</style>
<!-- Content Header (Page header) -->

<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
            </div>
            <div class="col-sm-6">
                <div class="float-sm-right">

                </div>
            </div>
        </div>
    </div>
</section>

<!-- Main content -->
<section class="content">
    <div class="card card-info card-outline">
        <div class="container-fluid mt-2">
            <div class="info-box bg-info">
                <span class="info-box-icon"><a class="add-resource" href="{{route('planholder_list')}}" title="Add New Group"> <i class="fa fa-arrow-left"></i></a></span>
                <div class="info-box-content">
                    <div class="col-md-5 col-sm-6 col-xs-6">
                        <h1>Add Plan Holder</h1>
                    </div>
                </div>
                <span class="info-box-icon float-right">
                    <i class="fa fa-list"></i>
                </span>
            </div>
        </div>
       
            <form action="{{ route('planholder_create') }}" method="post" role="form" enctype="multipart/form-data"  style="border:0px;padding:0px 0px;" >
                @csrf
                @if ($errors->any())
                            @foreach ($errors->all() as $error)
                                <div class="alert alert-danger mb-2" role="alert">
                                    {{ $error }}
                                </div>
                                @endforeach
                            @endif
                <div class="row " style="padding: 40px;">

                <div class="col-lg-4 mb-3">
                    <label>Company</label>
                    <input placeholder="Company" type="text" class="form-control{{ $errors->has('company') ? ' is-invalid' : '' }}"
                        name="company" value="{{ old('company') }}" required autofocus>
                </div>
               
               <div class="col-lg-4 mb-3">
                    <label>Address</label>
                    <input id="autocomplete" onFocus="geolocate()" placeholder="Address" type="text" class="form-control{{ $errors->has('planholderaddress') ? ' is-invalid' : '' }}"
                        name="planholderaddress" value="{{ old('planholderaddress') }}" required autofocus>
               </div>

                <div class="col-lg-4 mb-3">
                    <label>Contact Name</label>
                    <input placeholder="Plan Holder Contact" type="text" class="form-control{{ $errors->has('planholdercontact') ? ' is-invalid' : '' }}"
                        name="planholdercontact" value="{{ old('planholdercontact') }}" required autofocus>
                </div>

                <div class="col-lg-4 mb-3">
                    <label>Phone</label>
                    <input placeholder="Phone" type="tel" class="usPhoneFax form-control{{ $errors->has('ph_phone') ? ' is-invalid' : '' }}"
                           name="ph_phone" value="{{ old('ph_phone') }}"  autofocus>
                </div>

                <div class="col-lg-4 mb-3">
                    <label>Fax</label>
                    <input placeholder="Fax" type="text" class="usPhoneFax form-control{{ $errors->has('ph_fax') ? ' is-invalid' : '' }}"
                           name="ph_fax" value="{{ old('planholdercontact') }}" autofocus>
                </div>

                <div class="col-lg-4 mb-3">
                    <label>Email</label>
                    <input placeholder="Email" type="email" class="form-control{{ $errors->has('ph_email') ? ' is-invalid' : '' }}"
                           name="ph_email" value="{{ old('ph_email') }}"  autofocus >
                </div>





                <div class="col-lg-4 mb-3 hide">
                  
                    <label>Project</label>

                    <select class="form-control" name="owner_proj">

                        <option value="">--Select Project--</option>
                        @foreach($user_pro as $up)

                            <option value="{{$up->id}}">{{$up->title}}</option>


                        @endforeach
                    </select>
                
                </div>
                 
                 
                </div>
               
            
               <div class="col-lg-12 " style="background-color: rgba(0,0,0,.03); padding: 2%">
                              <center>
                                  <button type="submit" class="btn btn-info  " style="padding-left:6% ;padding-right:6% ">Create</button>
                                    
                                </center>
                </div>
                
                </form>
       
        <!-- /.card-body -->
  
    <!-- /.card -->

</section>

<script>

  var placeSearch, autocomplete;

function initAutocomplete() {
  autocomplete = new google.maps.places.Autocomplete(
      document.getElementById('autocomplete'), {types: ['geocode']});
      autocomplete = new google.maps.places.Autocomplete(
      document.getElementById('autocomplete1'), {types: ['geocode']});
//   autocomplete.setFields('address_components');
//   autocomplete.addListener('place_changed', fillInAddress);
}

function geolocate() {
  if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(function(position) {
      var geolocation = {
        lat: position.coords.latitude,
        lng: position.coords.longitude
      };
      var circle = new google.maps.Circle(
          {center: geolocation, radius: position.coords.accuracy});
      autocomplete.setBounds(circle.getBounds());
    });
  }
}
</script>

    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD5KPKoO7ZP-grfU1aOx2GD1ra1pQMBdAQ&libraries=places&callback=initAutocomplete"
        async defer></script>

@endsection

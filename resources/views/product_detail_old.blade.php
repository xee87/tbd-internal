
<?php

use App\Helper\TBD;

$hasSubscription = is_object(auth()->user()) ? auth()->user()->subscribed('main') : false;
$isDEO = is_object(auth()->user()) ? auth()->user()->hasRole('DEO') : false;
$isQTO = is_object(auth()->user()) ? auth()->user()->hasRole('QTO') : false;
$showContent = ($isQTO || $isDEO) ?  false : true;

$QTOFiles = json_decode($project->qto_upload);
$QTOFilePrice = json_decode($project->qto_price);

$bidPhase = TBD::GetSolicitationLabel($project->bid_phase);

$pro = $project;
?>
@extends('layout')



@section('content')

    <style type="text/css">

        .card {
            box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
            transition: 0.3s;
            width: 40%;
        }

        .card:hover {
            box-shadow: 0 8px 16px 0 rgba(0, 0, 0, 0.2);
        }
    </style>

    <div class="container-fluid productDetail">

        <div id="MyElement" class="alert alert-success alert-dismissible hide" style="background-color:#28a745">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>

            <h3 style="color:#fff"><strong><i class="fa fa-check" aria-hidden="true"></i></strong> Alert!</h3>

            <p style="color:#fff">{{Session::get('alertone')}}</p>

        </div>

        <div class="row projectTitle p-y-md">
            <div class="col-md-7 p-t-sm">
                <p style="   margin: 0 0 0px;">
                    <b style="font-size:25px; color:#fff">{{$project->title}}</b>
                </p>
                <?php if(!empty($powner->owner_name)){?>
                <p style="color:#fff;">
                    <i class="fa fa-user" aria-hidden="true"></i> &nbsp; {{$powner->owner_name}}
                </p>
                <?php } ?>
            </div>
            <div class="col-md-5">

                <?php if($showContent): ?>

                <div class="row text-center" style="margin-top: 2%;">

                    <?php if(empty($userprojectrecord) && !is_null($QTOFiles) && !is_null($QTOFilePrice)){?>

                    @auth
                    <div class="col-md-4">
                        <?php //GRab ALL files removed by client on 5-18-19 word document.?>
                        <?php /*if(!$isZipDownloaded){?>

                                <!---<button class="btn btn-3d btn-reveal btn-orange" data-toggle="modal" data-target="#myModal">
                            <i class="fa fa-download"></i>
                            <span>Grab All Files</span>
                        </button>--->

                        <?php }else{ ?>
                                <!---<a class="btn btn-3d btn-reveal btn-orange" href="{{route('otp_set', $project->id)}}">
                            <i class="fa fa-download"></i>
                            <span>Download ZIP</span>
                        </a>---->
                        <?php } */?>
                    </div>
                    @endauth


                    <?php }else if(!empty($userprojectrecord) && $userprojectrecord->downloads != $userprojectrecord->downloads_used && $project->bid_amount > $userprojectrecord->max_amount && !is_null($QTOFiles) && !is_null($QTOFilePrice)){ ?>

                    @auth
                    <div class="col-md-4">

                        <?php /* if(!$isZipDownloaded){?>
                                <!----<button class="btn btn-3d btn-reveal btn-orange" data-toggle="modal" data-target="#myModal">
                            <i class="fa fa-download"></i>
                            <span>Grab All Files</span>
                        </button>---->
                        <?php }else{ ?>
                                <!----<a class="btn btn-3d btn-reveal btn-orange" href="{{route('otp_set', $project->id)}}">
                            <i class="fa fa-download"></i>
                            <span>Download ZIP</span>
                        </a>---->
                        <?php } */ ?>
                    </div>
                    @endauth
                    <?php }else { ?>
                    <div class="col-md-4"></div>
                    <?php } ?>

                    <div class="col-md-4">
                        @auth
                        <div class="downloadHistoryUser">

                            <a class="btn btn-3d btn-reveal btn-orange" href="/user_record" style="">
                                <i class="fa fa-download" aria-hidden="true"></i>
                                <span> My Downloads </span>
                            </a>
                        </div>
                        @endauth
                    </div>

                    <div class="col-md-3">
                        @auth
                        <div class="printBtn">
                            <button class="btn btn-3d btn-reveal btn-orange" onclick="printDiv('printableArea')">
                                <i class="fa fa-print"></i>
                                <span> Print </span>
                            </button>
                        </div>
                        @endauth
                    </div>
                </div>
                <?php endif;?>
            </div>

        </div>

        <div class="row">



            <div class="col-md-12">

                <div class="tab">

                    <?php if($showContent): ?>
                    @auth
                    <button class="tablinks" onclick="openCity(event, 'ProD')" id="defaultOpen">Project Details</button>
                    <?php if($hasSubscription) : ?>
                    <button class="tablinks" onclick="openCity(event, 'owners')">Owner/Authority</button>
                    <?php endif; ?>
                    <button class="tablinks" onclick="openCity(event, 'PD')">Specifications</button>
                    <button class="tablinks" onclick="openCity(event, 'plans')">Plans</button>
                    <?php if(!empty($pro->addendum)): ?>
                    <button class="tablinks" onclick="openCity(event, 'addendum')">Addendum</button>
                    <?php endif; ?>
                    <button class="tablinks" onclick="openCity(event, 'qtyTakeOff')">Quantity Takeoffs</button>
                    <?php if($pro->qto_video): ?>
                    <button class="tablinks" onclick="openCity(event, 'qtyTakeOff-video')">Quantity Takeoff Video</button>
                    <?php endif; ?>
                    <button class="tablinks hide" onclick="openCity(event, 'categories')" id="defaultOpen">Categories
                    </button>
                    <?php if($hasSubscription) : ?>
                    <button class="tablinks" onclick="openCity(event, 'planholder')">Plan Holders</button>
                    <?php endif; ?>
                    <?php if($bidPhase == 'Award'): ?>
                    {{--<button class="tablinks" onclick="openCity(event, 'award')">Awarded</button>--}}
                    <?php endif; ?>

                    @endauth
                    <?php endif;?>

                </div>

                <div id="ProD" class="tabcontent @guest guest-tab @endguest <?php if(!$showContent) echo " guest-tab m-y-xl "?>">
                    <div class="row">
                        <div class="col-md-12 @guest hide @endguest <?php if(!$showContent) echo " hide "?>">
                            <h4 class="orange-color p-y-lg"> Project Details</h4>
                        </div>

                    </div>



                    <div id="printableArea" class="well @auth print-area @endauth" >
                        <div class="row">
                            {{--@auth--}}
                            <div @auth class="col-md-12" @endauth @guest class="col-md-9" @endguest>
                                {{--@guest
                                @endauth
                                    <div class="col-md-9">
                                @endguest--}}
                                <div class="col-md-12">
                                    <div class="print-logo" style="display: none; text-align: center;">
                                        <img width="150" src="{{  URL::to('/images/tbd-logo.png') }}">
                                    </div>
                                </div>
                                <div class="row">

                                    <div class="col-md-6 print-reduce">


                                        <div class="row">
                                            <div class="col-md-5">
                                                <h4>Solicitation</h4>
                                            </div>
                                            <div class="col-md-7">
                                                @auth
                                                <p>
                                                    <?php //$bidPhase = TBD::GetSolLabelView($pro->solicitation); ?>
                                                    {{ $pro->solicitation  }}
                                                </p>
                                                @endauth
                                                @guest
                                                <p class="lock">
                                                    <i class="fa fa-lock" aria-hidden="true"></i>
                                                    Registered member only
                                                </p>
                                                @endguest
                                            </div>
                                        </div> {{-- Solicitation --}}
                                        <hr>

                                        <div class="row">
                                            <div class="col-md-5">
                                                <h4>Bidding Method</h4>
                                            </div>
                                            <div class="col-md-7">
                                                <?php if($showContent): ?>
                                                @auth
                                                <p>{{$pro->bid_method}}</p>
                                                @endauth

                                                @guest
                                                <p class="lock">
                                                    <i class="fa fa-lock" aria-hidden="true"></i>
                                                    Registered member only
                                                </p>
                                                @endguest
                                                <?php else: ?>
                                                <p class="lock">
                                                    <i class="fa fa-lock" aria-hidden="true"></i>
                                                    Registered member only
                                                </p>
                                                <?php endif; ?>


                                            </div>
                                        </div> {{-- Bid Method--}}

                                        <hr>

                                        <div class="row">
                                            <div class="col-md-5">
                                                <h4>Bid Phase</h4>
                                            </div>
                                            <div class="col-md-7">
                                                <?php if($showContent): ?>
                                                @auth
                                                <p>{{  $bidPhase }}</p>
                                                @endauth

                                                @guest
                                                <p class="lock">
                                                    <i class="fa fa-lock" aria-hidden="true"></i>
                                                    Registered member only
                                                </p>
                                                @endguest
                                                <?php else: ?>
                                                <p class="lock">
                                                    <i class="fa fa-lock" aria-hidden="true"></i>
                                                    Registered member only
                                                </p>
                                                <?php endif; ?>


                                            </div>
                                        </div> {{-- Bid Phase--}}
                                        <hr>

                                        <div class="row">
                                            <div class="col-md-5">
                                                <h4>Completion Time</h4>
                                            </div>
                                            <div class="col-md-7">
                                                <?php if($showContent): ?>
                                                @auth
                                                <p>{{$pro->completion_time}}</p>
                                                @endauth

                                                @guest
                                                <p class="lock">
                                                    <i class="fa fa-lock" aria-hidden="true"></i>
                                                    Registered member only
                                                </p>
                                                @endguest
                                                <?php else: ?>
                                                <p class="lock">
                                                    <i class="fa fa-lock" aria-hidden="true"></i>
                                                    Registered member only
                                                </p>
                                                <?php endif; ?>

                                            </div>
                                        </div> {{-- CSI Division--}}
                                        <hr>

                                        <div class="row">
                                            <div class="col-md-5">
                                                <h4>Liquidated Damages</h4>
                                            </div>
                                            <div class="col-md-7">
                                                <?php if($showContent):

                                                $money = '$ ' . number_format($pro->liquidated_damages);
                                                ?>
                                                @auth
                                                <p>{{  $money }} per CCD's</p>
                                                @endauth

                                                @guest
                                                <p class="lock">
                                                    <i class="fa fa-lock" aria-hidden="true"></i>
                                                    Registered member only
                                                </p>
                                                @endguest
                                                <?php else: ?>
                                                <p class="lock">
                                                    <i class="fa fa-lock" aria-hidden="true"></i>
                                                    Registered member only
                                                </p>
                                                <?php endif; ?>
                                            </div>
                                        </div> {{-- Liquidated Damages--}}
                                        <hr>

                                        <div class="row">
                                            <div class="col-md-5">
                                                <h4>Pre-bid Meeting</h4>
                                            </div>
                                            <div class="col-md-7">
                                                <?php
                                                $originalDate = $pro->pre_bid_date;
                                                $newDate = TBD::formatDate($originalDate);
                                                if($showContent): ?>

                                                @auth
                                                <p>{{$newDate}} &nbsp;<b> @ &nbsp; </b> {{$pro->pre_bid_location}} </p>
                                                @endauth

                                                @guest
                                                <p class="lock">
                                                    <i class="fa fa-lock" aria-hidden="true"></i>
                                                    Registered member only
                                                </p>
                                                @endguest
                                                <?php else: ?>
                                                <p class="lock">
                                                    <i class="fa fa-lock" aria-hidden="true"></i>
                                                    Registered member only
                                                </p>
                                                <?php
                                                endif; ?>


                                            </div>
                                        </div> {{-- Pre-bid Meeting--}}
                                        <hr>

                                        <div class="row">
                                            <div class="col-md-5">
                                                <h4>Bonds</h4>
                                            </div>
                                            <div class="col-md-7">
                                                <?php

                                                if($showContent): ?>

                                                @auth
                                                <p>
                                                    Bid Bond :&nbsp; {{$pro->b_bond}}%
                                                </p>

                                                <p>
                                                    Perfomance Bond :&nbsp; {{$pro->p_bond}}%
                                                </p>

                                                <p>
                                                    Payment Bond :&nbsp; {{$pro->pay_bond}}%
                                                </p>
                                                @endauth

                                                @guest
                                                <p class="lock">
                                                    <i class="fa fa-lock" aria-hidden="true"></i>
                                                    Registered member only
                                                </p>
                                                @endguest
                                                <?php else: ?>
                                                <p class="lock">
                                                    <i class="fa fa-lock" aria-hidden="true"></i>
                                                    Registered member only
                                                </p>
                                                <?php
                                                endif; ?>
                                            </div>
                                        </div> {{-- Bonds --}}
                                        <hr>

                                    </div>

                                    <div class="col-md-6 print-reduce1">

                                        <div class="row">
                                            <div class="col-md-4">
                                                <h4>Bid Range</h4>
                                            </div>
                                            <?php
                                            $rawAmount = $newbidamout = number_format_short(strlen($pro->bidrange) > 0 && is_numeric($pro->bidrange) ? $pro->bidrange : $pro->bid_amount);
                                            function number_format_short($n, $precision = 1)
                                            {
                                            if ($n < 900) {
                                            // 0 - 900
                                            $n_format = number_format($n, $precision);
                                            $suffix = '';
                                            } else if ($n < 900000) {
                                            // 0.9k-850k
                                            $n_format = number_format($n / 1000, $precision);
                                            $suffix = 'K';
                                            } else if ($n < 900000000) {
                                            // 0.9m-850m
                                            $n_format = number_format($n / 1000000, $precision);
                                            $suffix = 'M';
                                            } else if ($n < 900000000000) {
                                            // 0.9b-850b
                                            $n_format = number_format($n / 1000000000, $precision);
                                            $suffix = 'B';
                                            } else {
                                            // 0.9t+
                                            $n_format = number_format($n / 1000000000000, $precision);
                                            $suffix = 'T';
                                            }
                                            // Remove unecessary zeroes after decimal. "1.0" -> "1"; "1.00" -> "1"
                                            // Intentionally does not affect partials, eg "1.50" -> "1.50"
                                            if ($precision > 0) {
                                            $dotzero = '.' . str_repeat('0', $precision);
                                            $n_format = str_replace($dotzero, '', $n_format);
                                            }
                                            return $n_format . $suffix;
                                            }
                                            ?>

                                            <div class="col-md-8">
                                                <?php if($showContent): ?>
                                                @auth
                                                <p> {{ (strlen($pro->bidrange) > 0) ? $pro->bidrange : '--'}}</p>
                                                @endauth

                                                @guest
                                                <p class="lock">
                                                    <i class="fa fa-lock" aria-hidden="true"></i>
                                                    Registered member only
                                                </p>
                                                @endguest
                                                <?php else: ?>
                                                <p class="lock">
                                                    <i class="fa fa-lock" aria-hidden="true"></i> Registered member only
                                                </p>
                                                <?php endif; ?>
                                                {{--<span data-toggle="tooltip" data-placement="top"
                                                      title="{{$rawAmount}}"> $ {{$newbidamout}}</span>--}}
                                            </div>

                                        </div> {{-- Bid Amount--}}
                                        <hr>

                                        <div class="row">
                                            <div class="col-md-4">
                                                <h4>Location:</h4>
                                            </div>
                                            <div class="col-md-8">
                                                @auth
                                                <p>
                                                    {{$pro->location}}
                                                </p>
                                                @endauth
                                                @guest
                                                <p class="lock">
                                                    <i class="fa fa-lock" aria-hidden="true"></i> Registered member only
                                                </p>
                                                @endguest
                                            </div>
                                        </div> {{-- Location--}}
                                        <hr>

                                        <div class="row">
                                            <div class="col-md-4">
                                                <h4>Bid Date</h4>
                                            </div>
                                            <div class="col-md-8">
                                                <?php
                                                $originalDate = $pro->pre_bid_date;
                                                $newDate = TBD::formatDate($originalDate);
                                                ?>

                                                @auth
                                                <p>{{$newDate}}</p>@endauth
                                                @guest
                                                <p class="lock">
                                                    <i class="fa fa-lock" aria-hidden="true"></i>
                                                    Registered member only
                                                </p>
                                                @endguest
                                            </div>
                                        </div> {{-- Bid Date--}}
                                        <hr>

                                        <div class="row">
                                            <div class="col-md-4">
                                                <h4>Bid Location</h4>
                                            </div>
                                            <div class="col-md-8">
                                                @auth
                                                <p>{{$pro->bid_location}}</p>@endauth
                                                @guest
                                                <p class="lock">
                                                    <i class="fa fa-lock" aria-hidden="true"></i> Registered member only
                                                </p>
                                                @endguest
                                            </div>
                                        </div> {{-- Bid Location--}}
                                        <hr>


                                        <div class="row">
                                            <div class="col-md-4">
                                                <h4>Notes</h4>
                                            </div>
                                            <div class="col-md-8">
                                                @auth
                                                <p>{{$pro->notes}}</p>@endauth
                                                @guest
                                                <p class="lock">
                                                    <i class="fa fa-lock" aria-hidden="true"></i> Registered member only
                                                </p>
                                                @endguest
                                            </div>
                                        </div> {{-- Notes --}}

                                        <hr class="hide hidden">

                                        <div class="row hide hidden">
                                            <div class="col-md-4">
                                                <h4>Bid Range</h4>
                                            </div>
                                            <div class="col-md-8">
                                                <?php if($showContent): ?>
                                                @auth
                                                <p> {{ (strlen($pro->bidrange) > 0) ? $pro->bidrange : '--'}}</p>
                                                @endauth

                                                @guest
                                                <p class="lock">
                                                    <i class="fa fa-lock" aria-hidden="true"></i>
                                                    Registered member only
                                                </p>
                                                @endguest
                                                <?php else: ?>
                                                <p class="lock">
                                                    <i class="fa fa-lock" aria-hidden="true"></i> Registered member only
                                                </p>
                                                <?php endif; ?>
                                            </div>
                                        </div> {{-- Bid Range --}}
                                        <hr>

                                        <div class="row">
                                            <div class="col-md-4">
                                                <h4>CSI Division</h4>
                                            </div>
                                            <div class="col-md-8">
                                                @auth

                                                <?php

                                                $divisions = explode(',', $pro->csi_division);

                                                $division_names = [];
                                                foreach($divisions as $k => $division){
                                                $division_names[$division] = TBD::LoadDataFromDB('csi_division', ['*'], null, ['id' => $division]);
                                                }

                                                foreach($division_names as $k => $name){ ?>

                                                <p>{{$name[0]->division_name}}</p>

                                                <?php    }

                                                ?>

                                                @endauth
                                                @guest
                                                <p class="lock">
                                                    <i class="fa fa-lock" aria-hidden="true"></i> Registered member only
                                                </p>
                                                @endguest
                                            </div>
                                        </div> {{-- CSI Division --}}
                                        <hr>
                                        @guest
                                        <div class="row">
                                            <div class="col-md-4">
                                                <h4>Bid Range</h4>
                                            </div>
                                            <div class="col-md-8">
                                                <p class="lock">
                                                    <i class="fa fa-lock" aria-hidden="true"></i> Registered member only
                                                </p>
                                            </div>
                                        </div>
                                        <hr>
                                        @endguest


                                    </div>

                                </div>
                                @auth
                                <div class="row">

                                    <div class="col-md-4">
                                        <h4>Bid Results</h4>
                                    </div>

                                    <div class="col-md-12">

                                        <?php
                                        if(count($solicitations) > 0){ ?>
                                        <table class="table table-striped sash">
                                            <thead>
                                            <tr class="newth">
                                                <th style="width:5%"></th>
                                                <th style="width:20%">#</th>
                                                <th style="width:30%">Name</th>
                                                <th style="width:20%">Price</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?php

                                            foreach($solicitations as $k => $detail){
                                            $isAwarded = '';
                                            if($detail->awarded == 1) $isAwarded = 'prova';

                                            $details = '';
                                            foreach($planHolderDetails as $key => $planHolder){
                                            if($planHolder->id == $detail->planholder_id){
                                            $details = $planHolder->company;
                                            }
                                            }
                                            ?>

                                            <tr class="">
                                                <td class="{{ $isAwarded }}" data-ribbon="★"></td>
                                                <td>{{ $k+1 }}</td>
                                                <td>{{ $details }}</td>
                                                <td> USD {{ number_format($detail->price) }}</td>
                                            </tr><?php
                                            } ?>

                                            </tbody>
                                        </table>
                                        <?php   }else { ?>
                                        <p class=""> No Results Available</p>
                                        <?php } ?>
                                    </div>
                                </div>
                                @endauth

                            </div>
                            @guest
                            <div class="col-md-3">
                                <div class="instantAccessForm m-t-xl">
                                    <div class="row">
                                        <div class="col-md-4 hide">

                                        </div>
                                        <div class="col-md-12">
                                            <h4>
                                                <i class="fa fa-key fa-2x orange-color hide"></i>
                                                Get instant access
                                            </h4>
                                            <p>
                                                Registration is required to access bid details and documents.
                                            </p>
                                            <hr>
                                            <p>
                                                Quantity take-off is available for <b> registered members </b> only.
                                            </p>
                                            <hr>
                                        </div>
                                        <div class="col-md-12">

                                            <div class="text-center">
                                                <a class="btn btn-3d btn-reveal btn-orange" href="/register">
                                                    <i class="fa fa-key"></i>
                                                    <span>Get Access</span>
                                                </a>
                                            </div>

                                        </div>
                                    </div>

                                </div>

                                <div class="instantAccessForm m-t-lg hide">
                                    <h4> Get instant access </h4>

                                    <hr>

                                </div>
                            </div>
                            @endguest

                        </div>
                    </div>
                </div>
                <?php if($showContent): ?>
                <div id="categories" class="tabcontent @guest hide @endguest">

                    <h4 class="orange-color p-y-lg">Project Categories</h4>

                    <?php if(!empty($categoryproject)){?>


                    <h4>{{$categoryproject->name}}</h4>

                    <p>{{$categoryproject->cat_description}}</p>


                    <?php }else{?>

                    <p> No Categories!</p>


                    <?php }    ?>


                </div>

                <div id="PD" class="tabcontent @guest hide @endguest">

                    <h4 class="orange-color p-y-lg">Specification Documents</h4>

                    <?php if(!empty($pro->bid_doc_url)){?>

                    <div class="table-responsive">
                        <table class="table table-striped">
                            <thead>
                            <tr class="newth">
                                <th style="width:50%">File Name</th>
                                <th style="width:15%">Size</th>
                                <th style="width:15%">Upload Date</th>
                                <th style="width:10%">Grab Document</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php

                            $url = "/public/bid_uploads/";
                            $downloadqto = explode(',',$pro->bid_doc_url);

                            foreach($downloadqto as $abcd){
                            $fileName = explode('_', $abcd);
                            $fileName = (isset($fileName[1])) ? $fileName[1] : $fileName[0];


                            ?>
                            <tr>
                                <td>
                                    <?php //var_dump($abcd)?>
                                    {{ $fileName  }}
                                </td>

                                <td>
                                    <?php  $pdffilesize = is_file(public_path('bid_uploads/' . $abcd)) ? File::size(public_path('bid_uploads/' . $abcd)) : '333';

                                    //\Faker\Provider\File::size
                                    echo bytesToHuman($pdffilesize);

                                    ?>
                                </td>


                                <?php
                                $originalDate = $pro->updated_at;
                                $newDate = TBD::formatDate($originalDate);
                                ?>
                                <td> {{$newDate}}</td>
                                <td>
                                    @auth
                                    <center>
                                        <a href="{{config('app.url')}}{{$url }}{{$abcd}}">
                                            <img class="specs-docs-image" src="{{asset('images/pdf.png')}}">
                                        </a>
                                    </center>
                                    @endauth

                                    @guest
                                    <center>
                                        <a href="{{url('/user_login')}}">
                                            <img class="specs-docs-image" src="{{asset('images/pdf17.png')}}">
                                        </a>
                                    </center>
                                    @endguest
                                </td>
                            </tr> <?php
                            }  ?>

                            </tbody>
                        </table>
                    </div>

                    <?php }else{?>
                    <p class="no-results">Specification documents are not available</p>
                    <?php }?>

                </div>

                <div id="addendum" class="tabcontent @guest hide @endguest">

                    <h4 class="orange-color p-y-lg">Addendum Documents</h4>

                    <?php if(!empty($pro->addendum)){?>

                    <div class="table-responsive">
                        <table class="table table-striped">
                            <thead>
                            <tr class="newth">
                                <th style="width:50%">File Name</th>
                                <th style="width:15%">Size</th>
                                <th style="width:15%">Upload Date</th>
                                <th style="width:10%">Grab Document</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php

                            $url = "/public/bid_uploads/";
                            $downloadqto = explode(',',$pro->addendum);


                            foreach($downloadqto as $abcd){
                            $fileName = explode('_', $abcd);
                            $fileName = (isset($fileName[1])) ? $fileName[1] : $fileName[0];


                            ?>
                            <tr>
                                <td>
                                    <?php //var_dump($abcd)?>
                                    {{ $fileName  }}
                                </td>

                                <td>
                                    <?php  $pdffilesize = is_file(public_path('bid_uploads/' . $abcd)) ? File::size(public_path('bid_uploads/' . $abcd)) : '333';

                                    //\Faker\Provider\File::size
                                    echo bytesToHuman($pdffilesize);

                                    ?>
                                </td>


                                <?php
                                $originalDate = $pro->updated_at;
                                $newDate = TBD::formatDate($originalDate);
                                ?>
                                <td> {{$newDate}}</td>
                                <td>
                                    @auth
                                    <center>
                                        <a href="{{config('app.url')}}{{$url }}{{$abcd}}">
                                            <img class="specs-docs-image" src="{{asset('images/pdf.png')}}">
                                        </a>
                                    </center>
                                    @endauth

                                    @guest
                                    <center>
                                        <a href="{{url('/user_login')}}">
                                            <img class="specs-docs-image" src="{{asset('images/pdf17.png')}}">
                                        </a>
                                    </center>
                                    @endguest
                                </td>
                            </tr> <?php
                            }  ?>

                            </tbody>
                        </table>
                    </div>

                    <?php }else{?>
                    <p class="no-results">Addendum documents are not available. </p>
                    <?php }?>

                </div>

                <div id="plans" class="tabcontent @guest hide @endguest">

                    <h4 class="orange-color p-y-lg">Plan Documents</h4>

                    <?php
                    if(!is_null($pro->plan_doc)){?>
                    <div class="table-responsive">
                        <table class="table table-striped">
                            <thead>
                            <tr class="newth">
                                <th style="width:50%">File Name</th>
                                <th style="width:15%">Size</th>
                                <th style="width:15%">Upload Date</th>
                                <th style="width:10%">Grab Document</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            $url = "/public/bid_uploads/";
                            $downloadqto = explode(',', $pro->plan_doc);
                            foreach($downloadqto as $abcd){
                            ?>
                            <tr>
                                <td> {{explode('_',$abcd)[1]}}</td>
                                <td>
                                    <?php  $pdffilesize = (is_file(public_path('bid_uploads/' . $abcd))) ? File::size(public_path('bid_uploads/' . $abcd)) : '333';
                                    echo bytesToHuman($pdffilesize);
                                    ?>
                                </td>
                                <?php
                                $originalDate = $pro->updated_at;
                                $newDate = TBD::formatDate($originalDate);
                                ?>
                                <td> {{$newDate}}</td>
                                <td>
                                    @auth
                                    <center>
                                        <a href="{{config('app.url')}}{{$url }}{{$abcd}}">
                                            <img class="specs-docs-image" src="{{asset('images/pdf.png')}}">
                                        </a>
                                    </center>
                                    @endauth

                                    @guest
                                    <center>
                                        <a href="{{url('/user_login')}}">
                                            <img class="specs-docs-image" src="{{asset('images/pdf17.png')}}">
                                        </a>
                                    </center>
                                    @endguest
                                </td>
                            </tr><?php
                            }
                            ?>

                            </tbody>
                        </table>
                    </div>
                    <?php }else{?>
                    <p class="no-results">Plan documents are not available</p>
                    <?php }?>

                </div>

                <div id="qtyTakeOff" class="tabcontent @guest hide @endguest">

                    <h4 class="orange-color p-y-lg">Quantity Takeoffs</h4>

                    <?php if(!empty($pro->qto_upload) && !empty($pro->qto_price) ){   ?>
                    <table class="table table-striped">
                        <thead>
                        <tr class="newth">
                            {{--<th style="width:20%">#</th>--}}
                            <th style="width:30%">File Name</th>
                            <th style="width:15%">Size</th>
                            <th style="width:15%">Upload Date</th>
                            <th style="width:10%">Document</th>
                            <?php if(empty($userprojectrecord)){?>

                            <th style="width:10%">One Off</th>

                            <?php  }else if(!empty($userprojectrecord) && $userprojectrecord->downloads != $userprojectrecord->downloads_used && $pro->bid_amount > $userprojectrecord->max_amount){?>

                            <th style="width:10%">One Off</th>

                            <?php }else {
                            } ?>

                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        $downloadqto = json_decode($pro->qto_upload);
                        $strqtoprice = json_decode($pro->qto_price);

                        foreach($downloadqto as $key => $abcd){  ?>

                        <tr>
                            {{--<td>File {{$key+1}}</td>--}}
                            <td>{{ explode('.',$abcd)[1] }}</td>
                            <td>
                                <?php
                                $pdffilesize = Storage::size($abcd);
                                echo bytesToHuman($pdffilesize);
                                ?>
                            </td>
                            <?php
                            $originalDate = $pro->updated_at;
                            $newDate = TBD::formatDate($originalDate);
                            ?>
                            <td> {{$newDate}}</td>
                            <td>
                                @auth
                                <center>
                                    <a href="{{route('p_download',['id' => $pro->id,'key' => $key , 'proamount'=> $pro->bid_amount ])}}">
                                        <img src='{{asset('images/excel.png')}}' class="specs-docs-image">
                                    </a>
                                </center>
                                @endauth

                                @guest
                                <center>
                                    <a href="{{url('/user_login')}}">
                                        <img src='{{asset('images/blockexcel.png')}}' class="specs-docs-image">
                                    </a>
                                </center>
                                @endguest
                            </td>
                            <?php if(empty($userprojectrecord)){?>
                            <td>
                                @auth
                                <?php
                                $singleFileDownloaded = TBD::isSingleFileDownloaded($project->id, $key);
                                ?>
                                <button class="btn btn-3d btn-reveal btn-orange">
                                    <i class="fa fa-download"></i>
                                    <?php
                                    if (empty($strqtoprice[$key])) {
                                    $strqtoprice[$key] = '1';
                                    }
                                    if($singleFileDownloaded == false && $isZipDownloaded == false){
                                    ?>
                                    <span>
                                            <a href="{{route('otp_set_single', ['id' => $pro->id,'key' => $key , 'qtoprice'=> $strqtoprice[$key] ])}}"  data-price="<?php echo $strqtoprice[$key] ?>" onclick="return confirm('Are you sure you want to buy only one file of this project? Payment will be detected from your card!')" style="color: #fff">Get this File for ${{$strqtoprice[$key]}}</a>
                                        </span>

                                    <?php }else{ ?>

                                    <span>
                                            <a href="{{route('otp_set_single', ['id' => $pro->id,'key' => $key , 'qtoprice'=> $strqtoprice[$key] ])}}"  data-price="<?php echo $strqtoprice[$key] ?>" style="color: #fff">Download File </a>
                                        </span>

                                    <?php } ?>
                                </button>

                                @endauth
                            </td>

                            <?php  }else if(!empty($userprojectrecord) && $userprojectrecord->downloads != $userprojectrecord->downloads_used && $pro->bid_amount > $userprojectrecord->max_amount ){?>
                            <td>
                                @auth
                                <button class="btn btn-3d btn-reveal btn-orange">
                                    <i class="fa fa-download"></i>
                                    <?php
                                    if (empty($strqtoprice[$key])) {
                                    $strqtoprice[$key] = '1';
                                    }

                                    $singleFileDownloaded = TBD::isSingleFileDownloaded($project->id, $key);
                                    if($singleFileDownloaded == false && $isZipDownloaded == false){
                                    ?>

                                    <span><a href="{{route('otp_set_single', ['id' => $pro->id,'key' => $key , 'qtoprice'=> $strqtoprice[$key] ])}}" data-price="<?php echo $strqtoprice[$key]; ?>" onclick="return confirm('Are you sure you want to buy only one file of this project? Payment will be detected from your card!')" style="color: #fff">Get this File for ${{$strqtoprice[$key]}}</a></span>
                                    <?php }else{ ?>
                                    <span>
                                            <a href="{{route('otp_set_single', ['id' => $pro->id,'key' => $key , 'qtoprice'=> $strqtoprice[$key] ])}}"  data-price="<?php echo $strqtoprice[$key] ?>" style="color: #fff">Download File </a>
                                        </span>
                                    <?php } ?>

                                </button>
                                @endauth
                            </td>
                            <?php }else {
                            } ?>
                        </tr>

                        <?php
                        } ?>
                        </tbody>
                    </table>

                    <?php }else{?>

                    <p class="no-results">Please check back later, we'll update <b>Quantity Takeoff Documents</b> soon!</p>

                    <?php } ?>

                </div>

                <?php if($pro->qto_video) :?>

                <div id="qtyTakeOff-video" class="tabcontent @guest hide @endguest">

                    <h4 class="orange-color p-y-lg">Quantity Takeoff Video</h4>
                    <div class="video-container">
                        <?php //echo ($pro->qto_video) ? $pro->qto_video : ' No video available'?>
                        <?php echo \App\Helper\TBD::GetQTOVideoIframe($pro->qto_video);?>
                    </div>

                </div>
                <?php endif;?>

                <?php endif;?>

                <?php
                function bytesToHuman($bytes){
                $units = ['B', 'KB', 'MB', 'GB', 'TB', 'PB'];

                for ($i = 0; $bytes > 1024; $i++) {
                $bytes /= 1024;
                }

                return round($bytes, 2) . ' ' . $units[$i];
                }
                ?>

                <?php if($showContent): ?>

                <?php if($hasSubscription): ?>

                <div class="tabcontent @guest hide @endguest" id="owners">

                    <h4 class="orange-color p-y-lg">Project Owner</h4>

                    <?php if(!empty($powner->owner_name)){?>

                    <img src="{{asset('images/user-512.png')}}" alt="Avatar" style="width:100px; display:none;">

                    <div class="row">

                        <div class="col-md-4 col-xs-5" style="padding-right: 0px; padding-top: 10px">
                            <h4>Owner/Authority</h4>
                            <hr/>
                            <h4>Address</h4>
                            <hr/>
                            <h4>Contact Name</h4>
                            <hr/>
                            <h4>Contact Number</h4>
                            <hr/>
                            <h4>Email</h4>
                            <hr/>
                            <h4>Fax</h4>
                            <hr/>
                        </div>

                        <div class="col-md-8 col-xs-7 " style="padding-top: 8px">
                            <p class="">{{$powner->owner_name}}</p>
                            <hr/>
                            <p class="">{{$powner->owner_address}}</p>
                            <hr>
                            <p class="">{{strlen($powner->contact_name) > 0 ? $powner->contact_name : '--' }}</p>
                            <hr>
                            <p class="">{{strlen($powner->owner_phone) > 0 ? $powner->owner_phone : '--'}}</p>
                            <hr>
                            <p class="">{{(strlen($powner->owner_email) > 0) ? $powner->owner_email: '--'}}</p>
                            <hr>
                            <p class="">{{(strlen($powner->owner_fax)>0) ? $powner->owner_fax: '--'}}</p>
                            <hr>
                        </div>

                    </div>

                    <?php }else{ ?>

                    <p> Owner details not updated, please check again later! </p>
                    <?php } ?>

                </div>

                <div style="padding-left: 0" class="tabcontent @guest hide @endguest" id="planholder">

                    <h4 class="orange-color p-y-lg">Project Plan Holders</h4>

                    {{--<img src="{{asset('images/user-512.png')}}" alt="Avatar" style="width:100px">--}}

                    <?php if(!empty($arrayphinfo)){ $count = 1; ?>

                    <table class="table table-striped">
                        <thead>
                        <tr class="newth">
                            <th style="width:5%">#</th>
                            <th style="width:20%">Company</th>
                            <th style="width:35%">Address</th>
                            <th style="width: 20%">Contact Name</th>
                            <th style="width: 20%">Contact #</th>
                        </tr>
                        </thead>
                        <tbody>


                        @foreach($arrayphinfo as $apinfo)


                            <tr>
                                <td>{{ $count++ }}</td>
                                <td>{{$apinfo->company}}</td>
                                <td>{{$apinfo->planholder_address}}</td>
                                <td>{{$apinfo->planholder_contact}}</td>
                                <td>{{$apinfo->planholder_phone}}</td>
                            </tr>

                            {{--<div class="row">--}}

                            {{--<div class="col-md-4">--}}
                            {{--<div class="row">--}}
                            {{--<div class="col-md-4 col-xs-4">--}}
                            {{--<h4>Name</h4>--}}
                            {{--</div>--}}
                            {{--<div class="col-md-8 col-xs-8">--}}
                            {{--<p class="m-y-md">{{$apinfo->company}}</p>--}}
                            {{--</div>--}}
                            {{--</div>--}}
                            {{--</div>--}}

                            {{--<div class="col-md-4">--}}
                            {{--<div class="row">--}}
                            {{--<div class="col-md-4 col-xs-4">--}}
                            {{--<h4>Address</h4>--}}
                            {{--</div>--}}
                            {{--<div class="col-md-8 col-xs-8">--}}
                            {{--<p class="m-y-md">{{$apinfo->planholder_address}}</p>--}}
                            {{--</div>--}}
                            {{--</div>--}}
                            {{--</div>--}}

                            {{--<div class="col-md-4">--}}
                            {{--<div class="row">--}}
                            {{--<div class="col-md-4 col-xs-4">--}}
                            {{--<h4>Contact</h4>--}}
                            {{--</div>--}}
                            {{--<div class="col-md-8 col-xs-8">--}}
                            {{--<p class="m-y-md">{{$apinfo->planholder_contact}}</p>--}}
                            {{--</div>--}}
                            {{--</div>--}}
                            {{--</div>--}}
                            {{--</div>--}}
                            {{--<hr>--}}


                            {{--<h4><b>{{$apinfo->planholder_name}}</b></h4>
                            <p>Address : {{$apinfo->planholder_address}}</p>
                            <p>Contact : {{$apinfo->planholder_contact}}</p>

                            <hr>--}}

                        @endforeach

                        </tbody>
                    </table>

                    <?php }else {
                    echo "No Data Available";
                    } ?>
                </div>

                <?php if($bidPhase <> 'Open Solicitation'): ?>

                <div class="tabcontent @guest hide @endguest" id="bidResult">

                    <?php
                    if(count($solicitations) > 0){ ?>
                    <table class="table table-striped">
                        <thead>
                        <tr class="newth">
                            <th style="width:20%">#</th>
                            <th style="width:30%">Name</th>
                            <th style="width:20%">Price</th>
                            <th style="width: 30%">Date</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php

                        foreach($solicitations as $k => $detail){
                        $details = '';
                        foreach($planHolderDetails as $key => $planHolder){
                        if($planHolder->id == $detail->planholder_id){
                        $details = $planHolder->company;
                        }
                        }
                        ?>

                        <tr>
                            <td>{{ $k+1 }}</td>
                            <td>{{ $details }}</td>
                            <td>${{ $detail->price }}</td>
                            <td>{{ TBD::formatDate($detail->created_at) }}</td>
                        </tr><?php
                        } ?>

                        </tbody>
                    </table>
                    <?php   }else { ?>

                    <p class="no-results"> Data not available</p>

                    <?php } ?>
                </div>

                <?php endif; ?>

                <?php /*if($bidPhase == 'Award'): ?>

                {{--<div class="tabcontent @guest hide @endguest" id="award">
                    <?php
                    if(count($solicitations) > 0){ ?>
                    <table class="table table-striped">
                        <thead>
                        <tr class="newth">
                            <th style="width:20%">#</th>
                            <th style="width:30%">Name</th>
                            <th style="width:20%">Price</th>
                            <th style="width:30%">Date</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php

                        foreach($solicitations as $k => $detail){
                        $isAwarded = '';
                        if($detail->awarded == 1) $isAwarded = 'project-awarded';

                        $details = '';
                        foreach($planHolderDetails as $key => $planHolder){
                            if($planHolder->id == $detail->planholder_id){
                                $details = $planHolder->company;
                            }
                        }
                        ?>

                        <tr class="{{ $isAwarded }}">
                            <td>{{ $k+1 }}</td>
                            <td>{{ $details }}</td>
                            <td>${{ $detail->price }}</td>
                            <td>{{ TBD::formatDate($detail->created_at) }}</td>
                        </tr><?php
                        } ?>

                        </tbody>
                    </table>
                    <?php   }else { ?>

                    <p class="no-results"> Data not available</p>

                    <?php } ?>

                </div>--}}

                <?php endif; */?>

                <?php endif; ?>

                <?php endif; ?>


            </div>


        </div>


    </div>

    @auth
    <div id="myModal" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content" style="margin-top:20%;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    Are you sure you want to buy all files of this project? Payment will be detected from
                    your card!
                    <br><br>

                    <center><a href="{{route('otp_set', $project->id)}}" class="btn btn-default"
                               style="background-color: #f0542d; color:#fff">Yes, Proceed</a></center>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>
    <div id="myModaltwo" class="modal fade singlePrice" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content" style="margin-top:20%;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>

                </div>
                <div class="modal-body">
                    Are you sure you want to buy only one file of this project? Payment will be detected
                    from your card!
                    <br><br>

                    <center><a href="" class="btn btn-default"
                               style="background-color: #f0542d; color:#fff">Yes, Proceed</a></center>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>

    @endauth

    <script>
        function openCity(evt, cityName) {
            var i, tabcontent, tablinks;
            tabcontent = document.getElementsByClassName("tabcontent");
            for (i = 0; i < tabcontent.length; i++) {
                tabcontent[i].style.display = "none";
            }
            tablinks = document.getElementsByClassName("tablinks");
            for (i = 0; i < tablinks.length; i++) {
                tablinks[i].className = tablinks[i].className.replace(" active", "");
            }
            document.getElementById(cityName).style.display = "block";
            evt.currentTarget.className += " active";
        }

        // Get the element with id="defaultOpen" and click on it
        document.getElementById("defaultOpen").click();


        var existtwo = '{{Session::has('alertone')}}';

        if (existtwo) {

            document.getElementById('MyElement').classList.add('show');
            document.getElementById('MyElement').classList.remove('hide');

        }

        function printDiv(divName) {
            var printContents = document.getElementById(divName).innerHTML;
            var originalContents = document.body.innerHTML;

            document.body.innerHTML = printContents;

            window.print();

            document.body.innerHTML = originalContents;
        }
    </script>
@endsection
@extends('layout')

@section('content')


    <div class="container quotes con-form">

        <div class="row">
        <div class="col-md-12">
            <form action="{{ route('save_quotes')  }}" method="post" role="form">
                @csrf
                <div class="contact-form">
                    <div class="col-md-4">
                        <div style="margin-bottom: 50px; text-align: left" class="line-down ">
                            <span class="sec-heading">Get </span><span class="head-bold">Quote</span>
                            <div style="margin: 5px 30px;width: 30%" class="green-border">&nbsp;</div>
                        </div>
                    </div>
                    <ul class="cform">
                        <li class="col-md-6">
                            <label>Name <span style="color: #f0542d;">*</span></label>
                            <input type="text" name="name" class="form-control" placeholder="Name" required="">
                        </li>
                        <li class="col-md-6">
                            <label>Company <span style="color: #f0542d;">*</span></label>
                            <input type="text" name="company" class="form-control" placeholder="Company" required="">
                        </li>
                        <li class="col-md-6 pr-15">
                            <label>Contact No. <span style="color: #f0542d;">*</span></label>
                            <input type="tel" name="contact" class="form-control" placeholder="Contact No." required="">
                        </li>
                        <li class="col-md-6 pr-15">
                            <label>Email <span style="color: #f0542d;">*</span></label>
                            <input type="email" name="email" class="form-control" placeholder="Email" required="">
                        </li>
                        <li class="col-md-6 pr-15">
                            <label>Address <span style="color: #f0542d;">*</span></label>
                            <input type="text" id="autocomplete1" onFocus="geolocate()" name="address" class="form-control" placeholder="Address" required="" autofocus>
                        </li>
                        <li class="col-md-6 pr-15">
                            <label>No. of Drawings <span style="color: #f0542d;">*</span></label>
                            <input type="text" name="no_drawings" class="form-control" placeholder="No. of Drawings" required="">
                        </li>
                        <li class="col-md-6 pr-15">
                            <label>Target No. of Days <span style="color: #f0542d;">*</span></label>
                            <input class="form-control" name="targets" placeholder="Target No. of Days" required="">
                        </li>
                    </ul>
                </div>


                <div class="">
                    <div class="col-lg-6 mb-3" style="margin-top:20px; ">
                        <input type="hidden" name="specification_docs" value="" class="specification_docs_hidden"/>
                        <label>Specification Documents</label>

                        <div class="dzStyling well dz-clickable clearfix" id="dzSpecsDocs">
                            {{--<input name="file" type="file" multiple />--}}
                            <div class="dz-default dz-message">
                                <span>
                                    <i style="color: #084887;" class="upload-icon ace-icon fa fa-cloud-upload fa-3x"></i>
                                    <br>
                                    <span class="bigger-150 bolder">
                                        <i class="ace-icon fa fa-caret-right red"></i>
                                            Drop files
                                    </span>
                                        to upload
                                    <span class="smaller-80 grey">(or click)</span>
                                    <br>
                                    Only "PDF" files allowed.
                                </span>
                            </div>
                        </div>


                        <div class="dz-default dz-message">


                        </div>

                    </div>

                    <div class="col-lg-6 mb-3" style="margin-top:20px; ">
                        <input type="hidden" name="plan_doc" value="" class="design_upload_hidden"/>
                        <label>Plan Documents</label>
                        <div class="dzStyling well dz-clickable clearfix" id="dropzonePlanDocs">
                            {{--<input name="file" type="file" multiple />--}}
                            <div class="dz-default dz-message">
                                <span>
                                    <i style="color: #084887;" class="upload-icon ace-icon fa fa-cloud-upload fa-3x"></i>
                                    <br>
                                    <span class="bigger-150 bolder">
                                        <i class="ace-icon fa fa-caret-right red"></i>
                                            Drop files
                                    </span>
                                        to upload
                                    <span class="smaller-80 grey">(or click)</span>
                                    <br>
                                    Only "PDF" files allowed.
                                </span>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-12" style="margin-top: 25px;">
                    <button type="submit"  class="fsubmit btn blue-button-bg">
                        Get Quotes
                        <i class="fa fa-arrow-right button-arrow" aria-hidden="true"></i>
                    </button>
                </div>


            </form>
        </div>

        </div>

    </div>



        <div id="preview-template" class="hide">
            <div class="dz-preview dz-file-preview">
                <div class="dz-image">
                    <img data-dz-thumbnail="">
                </div>

                <div class="dz-details">
                    <div class="dz-size">
                        <span data-dz-size=""></span>
                    </div>

                    <div class="dz-filename">
                        <span data-dz-name=""></span>
                    </div>
                </div>

                <div class="dz-progress">
                    <span class="dz-upload" data-dz-uploadprogress=""></span>
                </div>

                <div class="dz-error-message">
                    <span data-dz-errormessage=""></span>
                </div>

                <div class="dz-success-mark">
                <span class="fa-stack fa-lg bigger-150">
                    <i class="fa fa-circle fa-stack-2x white"></i>

                    <i class="fa fa-check fa-stack-1x fa-inverse green"></i>
                </span>
                </div>

                <div class="dz-error-mark">
                <span class="fa-stack fa-lg bigger-150">
                    <i class="fa fa-circle fa-stack-2x white"></i>

                    <i class="fa fa-remove fa-stack-1x fa-inverse red"></i>
                </span>
                </div>
            </div>
        </div>
        <div class="plan_documents" data-url="{{route('save_plan')}}"></div>

        <script>
            var placeSearch, autocomplete;

            function initAutocomplete() {

                autocomplete = new google.maps.places.Autocomplete(
                        document.getElementById('autocomplete1'), {types: ['geocode']});

//   autocomplete.setFields('address_components');
//   autocomplete.addListener('place_changed', fillInAddress);
            }

            function geolocate() {
                if (navigator.geolocation) {
                    navigator.geolocation.getCurrentPosition(function (position) {
                        var geolocation = {
                            lat: position.coords.latitude,
                            lng: position.coords.longitude
                        };
                        var circle = new google.maps.Circle({
                            center: geolocation,
                            radius: position.coords.accuracy
                        });
                        autocomplete.setBounds(circle.getBounds());
                    });
                }
            }
        </script>
        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD5KPKoO7ZP-grfU1aOx2GD1ra1pQMBdAQ&libraries=places&callback=initAutocomplete"
                async defer>
        </script>

        @if(session()->has('msg'))
            <script>
                jQuery(document).ready(function(){
                    swal({
                        title: "Sent!",
                        text: "Thank you! We have received your enquiry and will respond to you within 24 hours.",
                        type: "success"

                    });
                });
            </script>
        @endif

@endsection
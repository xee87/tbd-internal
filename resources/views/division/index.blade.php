<?php setlocale(LC_MONETARY,"en_US");   ?>
@extends('layouts.app')

@section('content')
<!-- Content Header (Page header) -->

<section class="content-header">

    <div class="container-fluid">
        <div class="row mb-2">

            <div class="col-sm-12">
                <div id="MyElement" class="alert alert-success alert-dismissible fade hide " role="alert">
                    <h3 style="color:#fff"><strong><i class="fa fa-check" aria-hidden="true"></i></strong> Alert!</h3>
                    <p style="color:#fff">Division <?php if(!empty($_GET['msg'])){echo $_GET['msg']; $gomsg= 1;}?> Successfully!</p>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div id="MyElement2" class="alert alert-success alert-dismissible fade hide " role="alert">
                    <h3 style="color:#fff"><strong><i class="fa fa-check" aria-hidden="true"></i></strong> Alert!</h3>
                    <p style="color:#fff">{{Session::get('alertone')}}</p>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            </div>

        </div>
    </div>
</section>

<!-- Main content -->
<section class="content">
    <div class="card card-info card-outline">
        <div class="container-fluid mt-2">
            <div class="info-box bg-info">
                <span class="info-box-icon"><i class="fa fa-external-link"></i></span>
                <div class="info-box-content">
                    <div class="col-md-5 col-sm-6 col-xs-6">
                        <h1>Divisions</h1>
                    </div>
                </div>
                <span class="info-box-icon float-right">
                    <a class="add-resource" href="javascript:void(0)" title="Add New Division" data-toggle="modal" data-target="#myModal">
                        <i class="fa fa-plus"></i>
                    </a>
                </span>
            </div>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table id="user_table" class="table table-bordered table-hover">
                    <thead>
                    <tr>
                        <th>Division Name</th>
                        <th>Date</th>
                        <th>Status</th>
                        <th>Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($divisions as $pro)
                        <tr>
                            <td>{{$pro->division_name}}</td>
                            <td>{{$pro->updated_at}}</td>
                            <td>
                                <div class="btn-group">

                                    <button type="button" class="btn btn-default">{{($pro->status == 1) ? 'Active': 'Inactive'}}</button>

                                    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown"
                                            aria-expanded="false">
                                        <span class="caret"></span>
                                        <span class="sr-only">Toggle Dropdown</span>
                                    </button>
                                    <div class="dropdown-menu" role="menu" x-placement="top-start" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(67px, -165px, 0px);">
                                        <a class="dropdown-item" href="{{route('div_status_active', $pro->id)}}">Active</a>
                                        <a class="dropdown-item" href="{{route('div_status_inactive', $pro->id)}}">Inactive</a>
                                        <a class="dropdown-item" href="{{route('div_status_delete', $pro->id)}}">Delete</a>
                                    </div>
                                </div>
                            </td>
                            <td>
                                <div class="btn-group">
                                    <button type="button" class="btn btn-default">Action</button>
                                    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown"
                                            aria-expanded="false">
                                        <span class="caret"></span>
                                        <span class="sr-only">Toggle Dropdown</span>
                                    </button>
                                    <div class="dropdown-menu" role="menu" x-placement="top-start" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(67px, -165px, 0px);">
                                        <a class="dropdown-item editDivision" href="javascript:void(0)" data-name="{{$pro->division_name}}" data-id="{{$pro->id}}" title="Update Division" data-toggle="modal" data-target="#myModal">Edit</a>

                                    </div>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <!-- /.card-body -->
    </div>
</section>
    <!-- /.card -->
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Add Division</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>
                <form class="division-form" action="{{ route('division_create') }}" method="post" role="form" enctype="multipart/form-data" id="" style="border:0px;padding:0px 0px;">
                    @csrf
                    <div class="modal-body">
                        <label >Division Name</label>
                        <input placeholder="Division Name" type="text"
                               class="form-control divisionName {{ $errors->has('division_name') ? ' is-invalid' : '' }}"
                               name="division_name" value="" required autofocus>
                        <input type="hidden" value="" name="id" class="divisionId">

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-info">Create</button>
                    </div>
                </form>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div>
    <script>

    var existtwo =  '{{ !empty($gomsg) }}' ;

    if(existtwo){
        //$('#')
        document.getElementById('MyElement').classList.add('show');
        document.getElementById('MyElement').classList.remove('hide');

    }
    var existtwo2 = '{{Session::has('alertone')}}';

    if(existtwo2){
        document.getElementById('MyElement2').classList.add('show');
        document.getElementById('MyElement2').classList.remove('hide');
    }

  </script>

<!-- /.content--->
@endsection

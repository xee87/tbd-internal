@extends('layouts.app')

@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">

            </div>
            <div class="col-sm-6">
                <div class="float-sm-right">

                </div>
            </div>
        </div>
    </div>
</section>

<!-- Main content -->
<section class="content">
    <div class="card card-info card-outline">
        <div class="container-fluid mt-2">
            <div class="info-box bg-info">
             <span class="info-box-icon"> <a class="add-resource" href="{{route('cat_view')}}" title="Add New Group"> <i class="fa fa-arrow-left"></i></a></span>
                <div class="info-box-content">
                    <div class="col-md-5 col-sm-6 col-xs-6">
                        <h1>Add Category</h1>
                    </div>
                </div>
                <span class="info-box-icon float-right">
                     <i class="fa fa-list"></i>
                </span>
            </div>
        </div>
  
            <form action="{{ route('cat_create') }}" method="post">
                @csrf
                @if ($errors->any())
                            @foreach ($errors->all() as $error)
                                <div class="alert alert-danger mb-2" role="alert">
                                    {{ $error }}
                                </div>
                                @endforeach
                            @endif
                            <div class="row " style="padding: 40px;">
                <div class="col-lg-6 mb-3">
                    <label for="name">Name</label>
                    <input id="name" placeholder="Name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}"
                        name="name" value="{{ old('name') }}" required autofocus>
                </div>
                <div class="col-lg-6 mb-3">
                    <label>Parent</label>
                    <select class="form-control" name="parent" value="{{ old('parent') }}">
                    <option value="0" >No Parent</option>
                    @foreach($cats as $cat)
                    <option value="{{$cat->id}}">{{$cat->name}}</option>
                    @endforeach
                    </select>
                </div>

                <div class="col-lg-12 mb-3">
                    <label>Description</label>
                    <textarea rows="4" cols="50" class="form-control" name="cat_description" >{{ old('cat_description') }}</textarea>
                    
                </div>
               </div>
            
               <div class="col-lg-12 " style="background-color: rgba(0,0,0,.03); padding: 2%">
                              <center>
                                  <button type="submit" class="btn btn-info  " style="padding-left:6% ;padding-right:6% ">Create</button>
                                    
                                </center>
                </div>
                
            </form>
   
    <!-- /.card -->

</section>
<!-- /.content -->
@endsection

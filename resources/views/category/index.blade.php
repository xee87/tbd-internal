@extends('layouts.app')

@section('content')
<!-- Content Header (Page header) -->


<div id="MyElement" class="alert alert-success alert-dismissible fade hide " role="alert">
  <h3 style="color:#fff"><strong><i class="fa fa-check" aria-hidden="true"></i></strong> Alert!</h3> 
  <p style="color:#fff">{{Session::get('alertone')}}</p>
  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
  </button>
</div>

<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
            </div>
            <div class="col-sm-6">
                <div class="float-sm-right">

                </div>
            </div>
        </div>
    </div>
</section>

<!-- Main content -->
<section class="content">
    <div class="card card-info card-outline">
        <div class="container-fluid mt-2">
            <div class="info-box bg-info">
                <span class="info-box-icon"><i class="fa fa-folder-open"></i></span>
                <div class="info-box-content">
                    <div class="col-md-12">
                        <h1>Categories</h1>
                    </div>
                </div>
                <span class="info-box-icon float-right">
                    <a class="add-resource" href="{{route('cat_add')}}" title="Add New Group"> <i class="fa fa-plus"></i></a>
                </span>
            </div>
        </div>
        <div class="card-body">
            <table id="user_table" class="table table-bordered table-hover">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Parent</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($cats as $cat)
                    <tr>
                        <td>{{$cat->name}}</td>
                        @if($cat->name === $cat->parent->name)
                        <td>No Parent</td>
                        @else
                        <td>{{$cat->parent->name}}</td>
                        @endif
                        <td>
                            <div class="btn-group">
                                <button type="button" class="btn btn-default">Action</button>
                                <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown"
                                    aria-expanded="false">
                                    <span class="caret"></span>
                                    <span class="sr-only">Toggle Dropdown</span>
                                </button>
                                <div class="dropdown-menu" role="menu" x-placement="top-start" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(67px, -165px, 0px);">
                                    <a class="dropdown-item" href="{{route('cat_edit', $cat->id)}}">Edit</a>
                                    <!-- <a class="dropdown-item" href="{{route('cat_delete', $cat->id)}}">Delete</a> -->
                                </div>
                            </div>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        <!-- /.card-body -->
    </div>
    <!-- /.card -->
<script>
    
    var existtwo = '{{Session::has('alertone')}}';
    
    if(existtwo){

document.getElementById('MyElement').classList.add('show');
document.getElementById('MyElement').classList.remove('hide');

    }
  </script>
</section>
<!-- /.content -->
@endsection

@extends('layouts.app')

@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">

            </div>
            <div class="col-sm-6">
                <div class="float-sm-right">

                </div>
            </div>
        </div>
    </div>
</section>

<!-- Main content -->
<section class="content">
    <div class="card card-info card-outline">
        <div class="container-fluid mt-2">
            <div class="info-box bg-info">
                 <span class="info-box-icon"> <a class="add-resource" href="{{route('cat_view')}}" title="Add New Group"> <i class="fa fa-arrow-left"></i></a></span>
                <div class="info-box-content">
                    <div class="col-md-5 col-sm-6 col-xs-6">
                        <h1>Edit Category</h1>
                    </div>
                </div>
                <span class="info-box-icon float-right">
                     <i class="fa fa-list"></i>
                </span>
            </div>
        </div>
     
            <form action="{{ route('cat_update',$id->id) }}" method="post">
                @csrf
                @if ($errors->any())
                            @foreach ($errors->all() as $error)
                                <div class="alert alert-danger mb-2" role="alert">
                                    {{ $error }}
                                </div>
                                @endforeach
                            @endif

                <div class="row " style="padding: 40px;">
                     <div class="col-lg-6 mb-3">
              
                    <label for="name">Name</label>
                    <input id="name" placeholder="Name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}"
                        name="name" required autofocus value="{{$id->name}}">
                </div>
                <div class="col-lg-6 mb-3">
                    <label>Parent</label>
                    <select class="form-control" name="parent" value="{{ old('parent') }}" >
                    @foreach($cats as $cat)
                    <?php if( $cat->id == $id->parent_id){?>
                    
                    <option value="{{$cat->id}}">{{$cat->name}}</option>

                    <?php } ?>
                    
                   
                    @endforeach
                     @foreach($cats as $cat)
                    <?php if( $cat->id != $id->parent_id){?>
                    
                    <option value="{{$cat->id}}">{{$cat->name}}</option>

                    <?php } ?>
                    
                   
                    @endforeach
                    </select>
                </div>

                <div class="col-lg-12 mb-3">
                    <label>Description</label>
                    <textarea rows="4" cols="50" class="form-control" name="cat_description"> {{$id->cat_description}}</textarea>
                    
                </div>







                </div>
                 <div class="col-lg-12 " style="background-color: rgba(0,0,0,.03); padding: 2%">
                              <center>
                                  <button type="submit" class="btn btn-info  " style="padding-left:6% ;padding-right:6% ">Update</button>
                                    
                                </center>
                </div>
               


            </form>
        
        <!-- /.card-body -->
   
    <!-- /.card -->

</section>
<!-- /.content -->
@endsection

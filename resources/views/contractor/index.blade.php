
@extends('layouts.app')

@section('content')
        <!-- Content Header (Page header) -->

<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-md-12">
                <div id="MyElement" class="alert alert-success alert-dismissible fade hide " role="alert">
                    <h3 style="color:#fff"><strong><i class="fa fa-check" aria-hidden="true"></i></strong> Alert!</h3>
                    <p style="color:#fff">{{Session::get('alertone')}}</p>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            </div>
        </div>
    </div>
</section>


<!-- Main content -->
<section class="content">
    <div class="card card-info card-outline">
        <div class="container-fluid mt-2">
            <div class="info-box bg-info">
                <span class="info-box-icon"><i class="fa fa-suitcase"></i></span>
                <div class="info-box-content">
                    {{-- <div class="col-md-5 col-sm-6 col-xs-6">--}}

                    <?php
                       // var_dump($contractors); exit;
                        $type = 0;
                        if(isset($contractors[0]->type)){
                            $type = (int)$contractors[0]->type;
                        }else{
                            $type = (int)$contractors;
                        }


                        if($type == 1){
                            echo '<h1>General Contractors</h1>';
                        }elseif($type == 2){
                            echo '<h1>Sub Contractors</h1>';
                        }elseif($type == 3){
                            echo '<h1>Trade Contractors</h1>';
                        }

                    ?>

                    {{--</div>--}}
                </div>
                <span class="info-box-icon float-right hide">
                    <a class="add-resource" href="{{route('owner_list_add')}}" title="Add New Group"> <i class="fa fa-plus"></i></a>
                </span>
            </div>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table id="user_table" class="table table-bordered table-hover">
                    <thead>
                    <tr>
                        <th>Sr#</th>
                        <th>Company Name</th>
                        <th>Address</th>
                        <th>Contact Name</th>
                        <th>Email</th>
                        <th>Contact #</th>
                        <th>Fax #</th>
                        <th>Website</th>
                        <th>Divisions of Interest</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php $countit =1 ;?>
                    @if(is_object($contractors))
                        @foreach($contractors as $contractor)
                            <tr>
                                <td>{{$countit}}</td>
                                <td>{{$contractor->company_name}}</td>
                                <td>{{$contractor->address}}</td>
                                <td>{{$contractor->contact_name}}</td>
                                <td>{{$contractor->email}}</td>
                                <td>{{$contractor->contact}}</td>
                                <td>{{$contractor->fax}}</td>
                                <td>{{$contractor->website}}</td>
                                <td>

                                    @foreach(explode(',', $contractor->divisions) as $k => $divs)
                                        @if($divs) <span class="badge badge-success">{{ $divs  }}</span> @endif
                                        @if(($k+1) % 3 == 0) <br> @endif
                                    @endforeach


                                </td>

                                {{--<td>--}}
                                {{--<div class="btn-group">--}}
                                {{--<button type="button" class="btn btn-default">Action</button>--}}
                                {{--<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown"--}}
                                {{--aria-expanded="false">--}}
                                {{--<span class="caret"></span>--}}
                                {{--<span class="sr-only">Toggle Dropdown</span>--}}
                                {{--</button>--}}
                                {{--<div class="dropdown-menu" role="menu" x-placement="top-start" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(67px, -165px, 0px);">--}}
                                {{--<a class="dropdown-item" href="{{route('owner_edit',$proowners->id)}}">Edit</a>--}}

                                {{--</div>--}}
                                {{--</div>--}}
                                {{--</td>--}}
                            </tr>
                            <?php $countit++; ?>
                        @endforeach
                    @endif
                    </tbody>
                </table>
            </div>
        </div>
        <!-- /.card-body -->
    </div>
    <!-- /.card -->
    <script>

        var existtwo = '{{Session::has('alertone')}}';

        if(existtwo){

            document.getElementById('MyElement').classList.add('show');
            document.getElementById('MyElement').classList.remove('hide');

        }
    </script>
</section>
<!-- /.content -->
@endsection

@extends('layout')

@section('content')



    <style>

        body {

            background-image: url('/images/O7KlZIGA.png');
            background-position-y: bottom;
            background-size: cover;
            background-repeat: no-repeat;
        }

        @media (max-width: 1024px) {

            body {

                background-image: url('/images/O7KlZIGA.png');
                background-position-y: center;
                background-size: cover;
                background-repeat: no-repeat;
            }

            #mr {
               margin-bottom: 25% !important;
               margin-top: 25% !important;
                min-height: 350px !important;
            }

        }


    </style>


    <div class="container" id="mr" style="margin-top:8%; min-height: 537px;">

        <div class="row">

            <div class="center-block no-padding col-md-5 user-notices">
                <div id="MyElement" class="alert alert-success alert-dismissible hide" style="background-color:#28a745">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>

                    <h3 style="color:#fff"><strong><i class="fa fa-check" aria-hidden="true"></i></strong> Alert!</h3>

                    <p style="color:#fff">{{Session::get('alertone')}}</p>

                </div>
                <div id="MyElementtwo" class="alert alert-danger alert-dismissible hide"
                     style="background-color:#dc3545; border-color: #dc3545">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>

                    <h3 style="color:#fff"><strong><i class="fa fa-check" aria-hidden="true"></i></strong> Alert!</h3>

                    <p style="color:#fff">{{Session::get('alerttwo')}}</p>

                </div>
            </div>

            <div class="center-block col-md-5 " style="background-color: #eee;padding: 3%;">
                <center><h3 style="color: #084887;">Welcome</h3></center>
                <br>

                <form action="{{ route('user_l_store') }}" method="post">
                    @csrf
                    @if ($errors->any())
                        @foreach ($errors->all() as $error)
                            <div class="alert alert-danger mb-2" role="alert">
                                {{ $error }}
                            </div>
                        @endforeach
                    @endif
                    <input type="email" name="email" class="form-control" placeholder="Email" required/>
                    {{--<input type="hidden" name="pre_url" class="form-control" value="{{ URL::previous()  }}" />--}}
                    </br>
                    <input type="password" name="password" class="form-control " placeholder="password" required/>
                    </br>
                    <div class="wrapper">
            <span class="group-btn">
                <input type="hidden" name="status" class="form-control " value="1"/>
                <center>
                    <button type="submit" style="background-color: #084887;" class="btn btn-3d btn-orange blue-button-bg">
                        {{--<i class="fa fa-key"></i>--}}
                        <span> Login</span>
                    </button>
                </center>
            </span>
                    </div>

                </form>
                <br>

                <div id="register-link">
                    <a href="/user-register" class="text-info" style="/*float:left*/">Create an account</a>
                    <a href="/password/reset" class="text-info" style="float:right">Forgotten your Password?</a>
                </div>
            </div>


        </div>
        <script>

            var existtwo = '{{Session::has('alertone')}}';

            if (existtwo) {

                document.getElementById('MyElement').classList.add('show');
                document.getElementById('MyElement').classList.remove('hide');

            }


            var existtwoo = '{{Session::has('alerttwo')}}';

            if (existtwoo) {

                document.getElementById('MyElementtwo').classList.add('show');
                document.getElementById('MyElementtwo').classList.remove('hide');

            }
        </script>


    </div>


@endsection

@extends('layout')

@section('content')

    <div class="container-fluid">

        <div class="row"
             style=" background-image:url('{{asset('images/blur.png')}}') ; background-size: 100%; background-repeat: no-repeat;  height:400px;   padding: 4%">
            <center>
                <div style="width: 50%;    margin-top: 8%;">

                    <h1 style="color:#f0542d;">Subscription Plans</h1>


                </div>
            </center>
        </div>


    </div>

    <div class="container" style=" margin-top: 2%; margin-bottom: 4%;">

        <div class="row justify-content-center">
            <center><h3>{{ $plan->name }}</h3></center>

            <br>
            <h4>Payment Method:</h4>

            <form method="post" action="{{ route('subscription.create') }}">
                @csrf
                <div id="dropin-container"></div>
                <hr/>
                <input type="hidden" name="plan" value="{{ $plan->id }}"/>
                <center>
                    <button type="submit" class="btn btn-success btn-lg " id="payment-button" disabled>Pay</button>
                </center>
            </form>


        </div>
    </div>
    <br>
@endsection

@section('scripts')
    <script src="https://js.braintreegateway.com/js/braintree-2.32.1.min.js"></script>
    <script>
        jQuery.ajax({
                    url: "{{ route('token') }}",
                })
                .done(function (res) {
                    braintree.setup(res.data.token, 'dropin', {
                        container: 'dropin-container',
                        onReady: function () {
                            jQuery('#payment-button').removeAttr('disabled');
                        }
                    });
                });
    </script>
@endsection
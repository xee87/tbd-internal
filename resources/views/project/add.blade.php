<?php
    use \Illuminate\Support\Facades\View;
?>
@extends('layouts.app')

@section('content')
        <!-- Select2 -->
<link rel="stylesheet" href="{{asset('/plugins/select2/select2.min.css')}}">
<link rel="stylesheet" href="{{asset('/plugins/dropzone/css/dropzone.min.css')}}">
<script src="{{asset('/plugins/dropzone/js/dropzone.js')}}"></script>
<script src="{{asset('/plugins/sweetalert/sweetalert.min.js')}}"></script>

<style type="text/css">
    .select2-container--default .select2-selection--multiple .select2-selection__choice {

        color: black;
    }
</style>
<!-- Content Header (Page header) -->

<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
            </div>
            <div class="col-sm-6">
                <div class="float-sm-right">

                </div>
            </div>
        </div>
    </div>
</section>

<!-- Main content -->
<section class="content">
    <div class="card card-info card-outline">
        <div class="container-fluid mt-2">
            <div class="info-box bg-info">
                <span class="info-box-icon"><a class="add-resource" href="{{route('pro_view')}}" title="Add New Group">
                        <i class="fa fa-arrow-left"></i></a></span>

                <div class="info-box-content">
                    <div class="col-md-5 col-sm-6 col-xs-6">
                        <h1>Add Project</h1>
                    </div>
                </div>
                <span class="info-box-icon float-right">
                    <i class="fa fa-list"></i>
                </span>
            </div>
        </div>

        <form action="{{ route('pro_create') }}" method="post" role="form" enctype="multipart/form-data"
              class="" id="" style="border:0px;padding:0px 0px;">
            @csrf
            @if ($errors->any())
                <div class="alert alert-danger mb-2" role="alert">
                    @foreach ($errors->all() as $error)
                        <p>{{ $error }}</p>
                    @endforeach
                </div>
            @endif
            <div style="padding: 40px">
                <div class="row " style="">

                    <div class="col-lg-4 mb-3">
                        <label>Title</label>
                        <input placeholder="Title" type="text"
                               class="form-control{{ $errors->has('title') ? ' is-invalid' : '' }}"
                               name="title" value="{{ old('title') }}" required minlength="4" maxlength="255" autofocus>
                    </div>
                    <div class="col-lg-4 mb-3">
                        <label>Solicitation</label>
                        <input placeholder="Solicitation" type="text"
                               class="form-control{{ $errors->has('solicitation') ? ' is-invalid' : '' }}"
                               name="solicitation" value="{{ old('solicitation') }}" required minlength="4" maxlength="255" autofocus>
                    </div>
                    <div class="col-lg-4 mb-3">
                        <label>Bid Amount</label>
                        <input placeholder="Bid Amount" type="number"
                               class="form-control{{ $errors->has('bid_amount') ? ' is-invalid' : '' }}"
                               name="bid_amount" value="{{ old('bid_amount') }}" required autofocus>
                    </div>
                    <div class="col-lg-4 mb-3" id="locationField">
                        <label>Project Location</label>
                        <input id="autocomplete" onFocus="geolocate()" placeholder="Location" type="text"
                               class="form-control{{ $errors->has('location') ? ' is-invalid' : '' }}"
                               name="location" value="{{ old('location') }}" required minlength="4" maxlength="255" autofocus>
                    </div>
                    <div class="col-lg-4 mb-3">
                        <label>Bidding Method</label>
                        <input placeholder="Bidding Method" type="text"
                               class="form-control{{ $errors->has('bid_method') ? ' is-invalid' : '' }}"
                               name="bid_method" value="{{ old('bid_method') }}" required minlength="4" maxlength="255" autofocus>
                    </div>
                    <div class="col-lg-4 mb-3">
                        <label>Bid Date</label>
                        {{--<input placeholder="Bid Date" type="date"--}}
                               {{--class="form-control{{ $errors->has('bid_date') ? ' is-invalid' : '' }}"--}}
                               {{--name="bid_date" value="{{ old('bid_date') }}" required autofocus>--}}

                            <div class='input-group' >
                                <input date-format="yyyy-mm-dd hh:ii" placeholder="Bid Date" type='text' class="form-control datetimepicker"
                                       name="bid_date" value="{{ old('bid_date') }}" required autofocus/>

                            </div>

                    </div>

                    <div class="col-lg-4 mb-3" id="locationField">
                        <label>Bid Location</label>
                        <input id="autocomplete1" onFocus="geolocate()" placeholder="Bid Location" type="text"
                               class="form-control{{ $errors->has('bidlocation') ? ' is-invalid' : '' }}"
                               name="bidlocation" value="{{ old('bidlocation') }}" required minlength="4" maxlength="255" autofocus>
                    </div>

                    <div class="col-lg-4 mb-3">
                        <label>Status</label>
                        <select class="getAwardedInfo form-control{{ $errors->has('bid_phase') ? ' is-invalid' : '' }}"
                                name="bid_phase" required>
                            <option data-modal="" value="Open Solicitation">Open Solicitation</option>
                            <option data-modal="awarded" value="Awarded Solicitation">Bid Results</option>
                            <option data-modal="closed" value="Closed Solicitation">Award</option>
                        </select>
                    </div>

                    <div class="col-lg-4 mb-3">

                        <label>CSI Division</label>

                        <div class="form-group">
                            <select class="form-control select2" name="csi_division[]" multiple="multiple"
                                    data-placeholder="Select CSI Division" style="width: 100%;" required autofocus>
                                <?php foreach($divisions as $division){ ?>
                                    <option value="{{$division->id}}">{{$division->division_name}}</option>

                                <?php } ?>
                            </select>
                        </div>


                    </div>


                    <div class="col-lg-4 mb-3">
                        <label>Completion Time</label>
                        <input placeholder="Completion Time" type="text"
                               class="form-control{{ $errors->has('completion_time') ? ' is-invalid' : '' }}"
                               name="completion_time" value="{{ old('completion_time') }}" required minlength="4" maxlength="255" autofocus>
                    </div>


                    <div class="col-lg-4 mb-3">
                        <label>Liquidated Damages</label>
                        <input placeholder="Liquidated Damages Per CCD's" type="number"
                               class="form-control{{ $errors->has('liquidated_damages') ? ' is-invalid' : '' }}"
                               name="liquidated_damages" value="{{ old('liquidated_damages') }}" required autofocus>
                    </div>

                    <div class="col-lg-4 mb-3">
                        <label>Bid Documents Availability</label>
                        <input placeholder="Bid Documents Availability" type="text"
                               class="form-control"
                               name="document_avail" value="{{ old('document_avail') }}" required autofocus>
                    </div>
                    <div class="col-lg-4 mb-3">
                        <label>Bid Range</label>
                        <input placeholder="Bid Range" type="text"
                               class="form-control"
                               name="bidrange" value="{{ old('bidrange') }}" required autofocus>
                    </div>

                </div>
                <hr>
                <div class="row">

                    <div class="col-lg-7 mb-3">
                        <label>Bonds</label>

                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-lg-4 form-group">
                                    <input placeholder="Bid Bond %" type="number"
                                           class="form-control{{ $errors->has('bbond') ? ' is-invalid' : '' }}"
                                           name="bbond" value="{{ old('bbond') }}" required autofocus>
                                </div>

                                <div class="col-lg-4 form-group">
                                    <input placeholder="Perfomance Bond %" type="number"
                                           class="form-control{{ $errors->has('pbond') ? ' is-invalid' : '' }}"
                                           name="pbond" value="{{ old('pbond') }}" required autofocus>
                                </div>

                                <div class="col-lg-4 form-group">
                                    <input placeholder="Payment Bond %" type="number"
                                           class="form-control{{ $errors->has('paybond') ? ' is-invalid' : '' }}"
                                           name="paybond" value="{{ old('paybond') }}" required autofocus>
                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="col-lg-6 mb-3" id="locationField">
                        <label>Pre-Bid Meeting</label>

                        <div class= "container-fluid">
                            <div class="row">
                                <div class="col-lg-6 form-group">
                                    <input class="datetimepicker form-control{{ $errors->has('pre_bid_date') ? ' is-invalid' : '' }}" placeholder="Pre Bid Date" type='text'
                                           name="pre_bid_date" value="{{ old('pre_bid_date') }}" required minlength="4" maxlength="255" required autofocus/>


                                    {{--<input placeholder="Date" type="date"--}}
                                           {{--class="form-control{{ $errors->has('pre_bid_date') ? ' is-invalid' : '' }}"--}}
                                           {{--name="pre_bid_date" value="{{ old('pre_bid_date') }}" required minlength="4" maxlength="255" autofocus>--}}

                                </div>

                                <div class="col-lg-6 form-group">

                                    <input id="autocomplete2" onFocus="geolocate()" placeholder="Location" type="text"
                                           class="form-control{{ $errors->has('pre_bid_location') ? ' is-invalid' : '' }}"
                                           name="pre_bid_location" value="{{ old('pre_bid_location') }}" required minlength="4" maxlength="255"
                                           autofocus>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <hr>
                <div class="row">

                    <div class="col-lg-4 mb-3 hide">
                        <label>Project category</label>
                        <select class="form-control" name="project_cat" value="{{ old('project_cat') }}">
                            @foreach($cats as $cat)
                                <option value="{{$cat->id}}">{{$cat->name}}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="col-lg-4 mb-3">
                        <label>Project Status</label>
                        <select class="form-control" name="status" required minlength="3" maxlength="255" autofocus>
                            <option value="Active">Active</option>
                            <option value="Inactive">Inactive</option>
                        </select>

                    </div>

                    <div class="col-lg-4 mb-3">
                        <label>Assign QTO</label>
                        <select class="form-control" name="assign_qto" required autofocus>

                            <option value="0">--Select QTO--</option>

                            @foreach($users as $cat)
                                @foreach($cat as $cattwo)
                                    <option value="{{$cattwo->id}}">{{$cattwo->name}}</option>
                                @endforeach
                            @endforeach
                        </select>

                    </div>

                    <div class="col-lg-4 mb-3">
                        <label>Assign Owner</label>

                        <select class="form-control" name="ownerid" required autofocus>

                            <option value="0">--Select Owner--</option>


                            @foreach($owneriddall as $strowneriddall)
                                <option value="{{$strowneriddall->id}}">{{$strowneriddall->owner_name}}</option>
                            @endforeach

                        </select>

                    </div>

                    <div class="col-lg-4 mb-3">
                        <label>Assign Plan Holders</label>

                        <div class="form-group">
                            <select class="form-control select2" name="pholder[]" multiple="multiple"
                                    data-placeholder="Select Plan Holders" style="width: 100%;" required autofocus>

                                @foreach($planholderall as $strplanholderall)
                                    <option value="{{$strplanholderall->id}}">{{$strplanholderall->company}}</option>

                                @endforeach

                            </select>
                        </div>

                    </div>

                    <div class="col-lg-4 mb-3">
                        <label>Notes</label>
                        <input placeholder="Notes" type="text"
                               class="form-control{{ $errors->has('notes') ? ' is-invalid' : '' }}"
                               name="notes" value="{{ old('notes') }}" autofocus>
                    </div>

                    <div class="col-lg-12 mb-3">
                        <label>Description</label>
                        <textarea name="description" class="form-control{{ $errors->has('description') ? ' is-invalid' : '' }}" id="" cols="30" rows="10">{{ old('description') }}</textarea>
                    </div>

                </div>
                <hr>
                <div class="row">

                    <div class="col-lg-6 mb-3">
                        <input type="hidden" name="specification_docs" value="" class="specification_docs_hidden"/>
                        <label>Specification Documents</label>

                        <div class="dzStyling well dz-clickable clearfix" id="dzSpecsDocs">
                            {{--<input name="file" type="file" multiple />--}}
                            <div class="dz-default dz-message">
                                <span>
                                    <i class="upload-icon ace-icon fa fa-cloud-upload blue fa-3x"></i>
                                    <br>
                                    <span class="bigger-150 bolder">
                                        <i class="ace-icon fa fa-caret-right red"></i>
                                            Drop files
                                    </span>
                                        to upload
                                    <span class="smaller-80 grey">(or click)</span>
                                    <br>
                                    Only "PDF" files allowed.
                                </span>
                            </div>
                        </div>

                        {{--<div class="dropzone well dz-clickable clearfix" id="dropzoneSpecsDocs">
                            <div id="docs-ph" class="dz-default dz-message">
                                <span>
                                    <i class="upload-icon ace-icon fa fa-cloud-upload blue fa-3x"></i>
                                    <br>
                                    <span class="bigger-150 bolder">
                                        <i class="ace-icon fa fa-caret-right red"></i>
                                            Drop files
                                    </span>
                                        to upload
                                    <span class="smaller-80 grey">(or click)</span>
                                    <br>
                                    Only "PDF" files allowed.
                                </span>
                            </div>
                        </div>--}}

                        <div class="dz-default dz-message">


                        </div>

                    </div>

                    <div class="col-lg-6 mb-3">
                        <input type="hidden" name="plan_doc" value="" class="design_upload_hidden"/>
                        <label>Plan Documents</label>
                        <div class="dzStyling well dz-clickable clearfix" id="dropzonePlanDocs">
                            {{--<input name="file" type="file" multiple />--}}
                            <div class="dz-default dz-message">
                                <span>
                                    <i class="upload-icon ace-icon fa fa-cloud-upload blue fa-3x"></i>
                                    <br>
                                    <span class="bigger-150 bolder">
                                        <i class="ace-icon fa fa-caret-right red"></i>
                                            Drop files
                                    </span>
                                        to upload
                                    <span class="smaller-80 grey">(or click)</span>
                                    <br>
                                    Only "PDF" files allowed.
                                </span>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-6 mb-3 hide">
                        <input type="hidden" name="grab_doc" value="" class="grab_upload_hidden"/>
                        <label>Grab All Documents</label>
                        <div class="dzStyling well dz-clickable clearfix" id="dropzoneGrabDocs">
                            {{--<input name="file" type="file" multiple />--}}
                            <div class="dz-default dz-message">
                                <span>
                                    <i class="upload-icon ace-icon fa fa-cloud-upload blue fa-3x"></i>
                                    <br>
                                    <span class="bigger-150 bolder">
                                        <i class="ace-icon fa fa-caret-right red"></i>
                                            Drop files
                                    </span>
                                        to upload
                                    <span class="smaller-80 grey">(or click)</span>
                                    <br>
                                    Only "EXCEL" files allowed.
                                </span>
                            </div>
                        </div>
                    </div>


                </div>

            </div>

            <div class="col-lg-12 " style="background-color: rgba(0,0,0,.03); padding: 2%">
                <center>
                    <button type="submit" class="btn btn-primary btn-info  " style="padding-left:6% ;padding-right:6% ">Create
                    </button>

                </center>
            </div>

        </form>
    </div>

    <div id="preview-template" class="hide">
        <div class="dz-preview dz-file-preview">
            <div class="dz-image">
                <img data-dz-thumbnail="">
            </div>

            <div class="dz-details">
                <div class="dz-size">
                    <span data-dz-size=""></span>
                </div>

                <div class="dz-filename">
                    <span data-dz-name=""></span>
                </div>
            </div>

            <div class="dz-progress">
                <span class="dz-upload" data-dz-uploadprogress=""></span>
            </div>

            <div class="dz-error-message">
                <span data-dz-errormessage=""></span>
            </div>

            <div class="dz-success-mark">
                <span class="fa-stack fa-lg bigger-150">
                    <i class="fa fa-circle fa-stack-2x white"></i>

                    <i class="fa fa-check fa-stack-1x fa-inverse green"></i>
                </span>
            </div>

            <div class="dz-error-mark">
                <span class="fa-stack fa-lg bigger-150">
                    <i class="fa fa-circle fa-stack-2x white"></i>

                    <i class="fa fa-remove fa-stack-1x fa-inverse red"></i>
                </span>
            </div>
        </div>
    </div>
    <div class="plan_documents" data-url="{{route('save_plan')}}"></div>
</section>



<!-- /.content -->
<script>
    var placeSearch, autocomplete;

    function initAutocomplete() {
        autocomplete = new google.maps.places.Autocomplete(
                document.getElementById('autocomplete'), {types: ['geocode']});
        autocomplete = new google.maps.places.Autocomplete(
                document.getElementById('autocomplete1'), {types: ['geocode']});
        autocomplete = new google.maps.places.Autocomplete(
                document.getElementById('autocomplete2'), {types: ['geocode']});
//   autocomplete.setFields('address_components');
//   autocomplete.addListener('place_changed', fillInAddress);
    }

    function geolocate() {
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(function (position) {
                var geolocation = {
                    lat: position.coords.latitude,
                    lng: position.coords.longitude
                };
                var circle = new google.maps.Circle({
                    center: geolocation,
                    radius: position.coords.accuracy
                });
                autocomplete.setBounds(circle.getBounds());
            });
        }
    }
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD5KPKoO7ZP-grfU1aOx2GD1ra1pQMBdAQ&libraries=places&callback=initAutocomplete"
        async defer></script>


{{--@include('modal.solicitation');--}}

@endsection

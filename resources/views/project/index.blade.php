<?php //setlocale(LC_MONETARY,"en_US"); ?>
@extends('layouts.app')

@section('content')
<!-- Content Header (Page header) -->

<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">

            <div class="col-sm-12">
                <div id="MyElement" class="alert alert-success alert-dismissible fade hide " role="alert">
                    <h3 style="color:#fff"><strong><i class="fa fa-check" aria-hidden="true"></i></strong> Alert!</h3>
                    <p style="color:#fff">Project <?php if(!empty($_GET['msg'])){echo $_GET['msg']; $gomsg= 1;}?> Successfully!</p>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div id="MyElement2" class="alert alert-success alert-dismissible fade hide " role="alert">
                    <h3 style="color:#fff"><strong><i class="fa fa-check" aria-hidden="true"></i></strong> Alert!</h3>
                    <p style="color:#fff">{{Session::get('alertone')}}</p>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="float-sm-right">

                </div>
            </div>
        </div>
    </div>
</section>

<!-- Main content -->
<section class="content">
    <div class="card card-info card-outline">
        <div class="container-fluid mt-2">
            <div class="info-box bg-info">
                <span class="info-box-icon"><i class="fa fa-clipboard"></i></span>
                <div class="info-box-content">
                    <div class="col-md-12 col-sm-12 col-xs-6">
                        <h1>Project Management</h1>
                    </div>
                </div>
                <span class="info-box-icon float-right">
                    <a class="add-resource" href="{{route('pro_add')}}" title="Add New Group"> <i class="fa fa-plus"></i></a>
                </span>
            </div>
        </div>
        <div class="card-body">

            <div class="table-responsive">
                <table id="user_table" class="table table-bordered table-hover">
                    <thead>
                    <tr>
                        <th>Title</th>
                        <th>Amount ($)</th>
                        <th>Project Location</th>
                        <th style=" width: 35px;">View</th>
                        <th>Status</th>
                        <th>Bid Phase</th>
                        <th>Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($projects as $pro)
                        <?php
                        $bidPhase = \App\Helper\TBD::GetSolicitationLabel($pro->bid_phase);
                        ?>
                        <tr>
                            <td>{{$pro->title}}</td>
                            <td>USD {{ number_format($pro->bid_amount) }}</td>
                            <td>{{$pro->location}}</td>
                            <td>
                                <center>
                                    <a href="{{route('pdetail', $pro->id)}}">
                                        <i class="fa fa-eye" aria-hidden="true"></i>
                                    </a>
                                </center>
                            </td>
                            <td>
                                <div class="btn-group">
                                    <button type="button" class="btn btn-default">{{$pro->status}}</button>
                                    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown"
                                            aria-expanded="false">
                                        <span class="caret"></span>
                                        <span class="sr-only">Toggle Dropdown</span>
                                    </button>
                                    <div class="dropdown-menu" role="menu" x-placement="top-start" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(67px, -165px, 0px);">
                                        <a class="dropdown-item" href="{{route('pro_status_active', $pro->id)}}">Active</a>
                                        <a class="dropdown-item" href="{{route('pro_status_inactive', $pro->id)}}">Inactive</a>
                                        <a class="dropdown-item" href="{{route('pro_status_delete', $pro->id)}}">Delete</a>
                                    </div>
                                </div>
                            </td>
                            <td>
                                {{ $bidPhase }}
                            </td>
                            <td>
                                <div class="btn-group">
                                    <button type="button" class="btn btn-default">Action</button>
                                    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown"
                                            aria-expanded="false">
                                        <span class="caret"></span>
                                        <span class="sr-only">Toggle Dropdown</span>
                                    </button>
                                    <div class="dropdown-menu" role="menu" x-placement="top-start" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(67px, -165px, 0px);">
                                        <a class="dropdown-item" href="{{route('pro_edit', $pro->id)}}">Edit</a>

                                    </div>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <!-- /.card-body -->
    </div>
    <!-- /.card -->
<script>
    
    var existtwo =  '{{ !empty($gomsg) }}' ;
    
    if(existtwo){

document.getElementById('MyElement').classList.add('show');
document.getElementById('MyElement').classList.remove('hide');

    }
  </script>
  
  <script>
    
    var existtwo2 = '{{Session::has('alertone')}}';
    
    if(existtwo2){

document.getElementById('MyElement2').classList.add('show');
document.getElementById('MyElement2').classList.remove('hide');

    }
  </script>
</section>
<!-- /.content -->
@endsection

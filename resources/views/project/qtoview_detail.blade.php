@extends('layouts.app')

@section('content')

    <link rel="stylesheet" href="{{asset('/plugins/dropzone/css/dropzone.min.css')}}">
    <script src="{{asset('/plugins/dropzone/js/dropzone.js')}}"></script>


    <style type="text/css">
        .dropzone {

            width: 100%;
        }
    </style>
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">

                <div class="col-sm-12">
                    <div id="MyElement2" class="alert alert-success alert-dismissible fade hide " role="alert">
                        <h3 style="color:#fff"><strong><i class="fa fa-check" aria-hidden="true"></i></strong> Alert!</h3>
                        <p style="color:#fff">{{Session::get('alertone')}}</p>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="float-sm-right">

                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="card card-info card-outline">
            <div class="container-fluid mt-2">
                <div class="info-box bg-info">
                    <span class="info-box-icon"><i class="fa fa-clipboard"></i></span>
                    <div class="info-box-content">
                        <div class="col-md-5 col-sm-6 col-xs-6">
                            <h1>View Projects</h1>
                        </div>
                    </div>

                </div>
            </div>

            <div class="card-body">

                <div class="container-fluid" >
                    <div class="row">
                        <div class="col-md-12">
                            <div class="table-resposive">
                                <table class="table table-striped">
                                    <thead>
                                    <tr>
                                        <th colspan="2">Project Details</th>
                                        {{--<th>Value</th>--}}
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td>Title</td>
                                        <td>{{$id->title}} </td>
                                    </tr>
                                    <tr>
                                        <td>Solicitation</td>
                                        <td>{{$id->solicitation}}</td>
                                    </tr>
                                    <tr>
                                        <td>Bid Amount</td>
                                        <td> {{$id->bid_amount}} </td>
                                    </tr>
                                    <tr>
                                        <td>Location</td>
                                        <td>{{$id->location}}</td>
                                    </tr>
                                    <tr>
                                        <td>Bidding Method</td>
                                        <td>{{$id->bid_method}}</td>
                                    </tr>
                                    <tr>
                                        <td>Bid Date</td>
                                        <td>{{ \App\Helper\TBD::formatDate($id->bid_date) }}</td>
                                    </tr>
                                    <tr>
                                        <td>Bid Phase</td>
                                        <td>{{$id->bid_phase}}</td>
                                    </tr>
                                    <tr>
                                        <td>Pre-Bid Meeting</td>
                                        <td>{{ \App\Helper\TBD::formatDate($id->pre_bid_date)}} {{$id->pre_bid_location}}</td>
                                    </tr>
                                    <tr>
                                        <td>Completion Time</td>
                                        <td>{{$id->completion_time}}</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- /.card-body -->
        </div>

        <?php if(!empty($id->qto_upload)){ ?>
        <div class="card card-info card-outline">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-12">
                        <h4>Files already uploaded</h4>
                        <div class="table-resposive">
                            <table class="table table-striped">
                                <thead>
                                <tr>
                                    <th>Sr. #</th>
                                    <th>Download Link</th>
                                    <th>Delete</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                $downloadqto = json_decode($id->qto_upload);
                                foreach($downloadqto as $key => $abcd){ ?>
                                <tr>
                                    <td>
                                        File {{ $key+1 }}
                                    </td>
                                    <td>
                                        <a title="Download" href="{{route('qto_download',['id' => $id->id,'key' => $key])}}">
                                            {{ explode('.',$abcd)[1] }}
                                        </a>
                                    </td>
                                    <td>
                                        <a href="{{route('qto_delete',['id' => $id->id,'key' => $key])}}">
                                            <i class="fa fa-trash fa-2x"></i>
                                        </a>
                                    </td>
                                </tr><?php
                                }
                                ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php } ?>

        <div class="card card-info card-outline">
            <div class="card-body">

                <div class="row">
                    <div class="col-md-4">
                        <form method="post" action="{{ route('qto_video_url') }}">
                            @csrf
                            <input type="hidden" name="project_id" value="{{ $id->id }}">
                            <div class="form-group">
                                <label>Embeded Youtube Video</label>
                                <textarea rows="4" cols="50" name="qto_video" class="form-control" id="videoCode" placeholder="https://www.youtube.com/watch?v=uo_yV06qx54">{{$id->qto_video}}</textarea>
                                <br>
                                <button type="submit" class="btn btn-primary btn-info pull-right  " style="padding-left:6% ;padding-right:6% ">Update</button>
                            </div>
                        </form>
                    </div>
                    <div class="col-md-8 text-center <?php if(is_null($id->qto_video)) echo ' hide'?>">
                        <h4>Video Preview</h4>

                        <?php
                        echo \App\Helper\TBD::GetQTOVideoIframe($id->qto_video);
                        ?>
                        <div class="video-plays hide">
                            <iframe width="100%" height="315" src="https://www.youtube.com/watch?v=lV9X2K8uEYE" frameborder="0"
                                    allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen>
                            </iframe>
                        </div>
                        {{--<iframe width="560" height="315" src="https://www.youtube.com/embed/L3xet24jS8I" frameborder="0"--}}
                        {{--allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen>--}}
                        {{--</iframe>--}}
                    </div>
                </div>

            </div>
        </div>

        <div class="card card-info card-outline">
            <div class="card-body">
                @php
                $url = "/public/uploads/" ;
                @endphp

                <div class="row">
                    <h4>
                        Upload Quantity Takeoffs
                        <span style="font-size:12px">(max 20 files can be upload)</span>
                    </h4>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <form id="dropzone" method="post" action="{{route('qto_upload',$id->id)}}" enctype="multipart/form-data">
                            @csrf
                            <input type="hidden" name="qto_docs" value="" class="qto_docs_hidden"/>
                            <div class="dzStyling well dz-clickable clearfix" id="dropzoneQTODocs">

                                <div class="dz-default dz-message">
                                <span>
                                    <span class="bigger-150 bolder">
                                        <i class="ace-icon fa fa-caret-right red"></i>
                                            Drop files
                                    </span>
                                        to upload
                                    <span class="smaller-80 grey">(or click)</span>
                                    <br>
                                    <i class="upload-icon ace-icon fa fa-cloud-upload blue fa-3x"></i>
                                </span>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div id="preview-template" class="hide">
            <div class="dz-preview dz-file-preview">
                <div class="dz-image">
                    <img data-dz-thumbnail="">
                </div>

                <div class="dz-details">
                    <div class="dz-size">
                        <span data-dz-size=""></span>
                    </div>

                    <div class="dz-filename">
                        <span data-dz-name=""></span>
                    </div>
                </div>

                <div class="dz-progress">
                    <span class="dz-upload" data-dz-uploadprogress=""></span>
                </div>

                <div class="dz-error-message">
                    <span data-dz-errormessage=""></span>
                </div>

                <div class="dz-success-mark">
                <span class="fa-stack fa-lg bigger-150">
                    <i class="fa fa-circle fa-stack-2x white"></i>

                    <i class="fa fa-check fa-stack-1x fa-inverse green"></i>
                </span>
                </div>

                <div class="dz-error-mark">
                <span class="fa-stack fa-lg bigger-150">
                    <i class="fa fa-circle fa-stack-2x white"></i>

                    <i class="fa fa-remove fa-stack-1x fa-inverse red"></i>
                </span>
                </div>
            </div>
        </div>
        <div class="qto_documents" data-url="{{route('qto_upload',$id->id)}}"></div>
        <!-- /.card -->
    </section>
    <!-- /.content -->
    <script>

        var existtwo2 = '{{Session::has('alertone')}}';

        if(existtwo2){

            document.getElementById('MyElement2').classList.add('show');
            document.getElementById('MyElement2').classList.remove('hide');

        }
    </script>
@endsection
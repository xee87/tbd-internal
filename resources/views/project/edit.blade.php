<?php

$distit = (Auth::user()->hasRole('DEO')) ? 'hidden' : 'number';
$planDocuments = $specificationDocs = $grabDocs = '';
$alertOne = Session::get('alertone');

?>
@extends('layouts.app')

@section('content')

    <link rel="stylesheet" href="{{asset('/plugins/select2/select2.min.css')}}">
    <link rel="stylesheet" href="{{asset('/plugins/dropzone/css/dropzone.min.css')}}">
    <script src="{{asset('/plugins/dropzone/js/dropzone.js')}}"></script>
    <script src="{{asset('/plugins/jquery/jquery.min.js')}}"></script>
    <script src="{{asset('/plugins/sweetalert/sweetalert.min.js')}}"></script>

    <style type="text/css">
        .dropzone {

            width: 100%;
        }

        .select2-container--default .select2-selection--multiple .select2-selection__choice {

            color: black;
        }
    </style>

    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">

                <div class="col-sm-12">
                    <div id="MyElement" class="alert alert-success alert-dismissible fade hide " role="alert">
                        <h3 style="color:#fff"><strong><i class="fa fa-check" aria-hidden="true"></i></strong> Alert!
                        </h3>

                        <p style="color:#fff">Project <?php if (!empty($_GET['msg'])) {
                                echo $_GET['msg'];
                                $gomsg = 1;
                            }?> Successfully!</p>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div id="MyElement2"
                         class="alert alert-success alert-dismissible <?php if (!isset($alertOne)) echo " hide" ?>"
                         role="alert">
                        <h3 style="color:#fff"><strong><i class="fa fa-check" aria-hidden="true"></i></strong> Alert!
                        </h3>

                        <p style="color:#fff">{{ $alertOne }}</p>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="float-sm-right">

                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="card card-info card-outline">
            <div class="container-fluid mt-2">
                <div class="info-box bg-info">
                    <span class="info-box-icon">
                        <a class="add-resource" href="{{route('pro_view')}}" title="Add New Group">
                            <i class="fa fa-arrow-left"></i>
                        </a>
                    </span>

                    <div class="info-box-content">
                        <div class="col-md-5 col-sm-6 col-xs-6">
                            <h1>Edit Project</h1>
                        </div>
                    </div>
                <span class="info-box-icon float-right">
                    <i class="fa fa-list"></i>
                </span>
                </div>
            </div>
            <form action="{{ route('pro_update', $project->id) }}" method="post" role="form"
                  enctype="multipart/form-data" class="dropzone" id="mydz" style="border:0px;padding:0px 0px;">
                @csrf
                @if ($errors->any())
                    @foreach ($errors->all() as $error)
                        <div class="alert alert-danger mb-2" role="alert">
                            {{ $error }}
                        </div>
                    @endforeach
                @endif

                <div class="row " style="padding: 40px;">

                    <div class="col-lg-4 mb-3">
                        <label>Title</label>
                        <input placeholder="Title" type="text"
                               class="form-control{{ $errors->has('title') ? ' is-invalid' : '' }}"
                               name="title" value="{{$project->title}}" required autofocus>
                    </div>

                    <div class="col-lg-4 mb-3">
                        <label>Solicitation</label>
                        <input placeholder="Solicitation" type="text"
                               class="form-control{{ $errors->has('solicitation') ? ' is-invalid' : '' }}"
                               name="solicitation" value="{{ $project->solicitation }}" required autofocus>
                    </div>

                    <div class="col-lg-4 mb-3">
                        <label>Bid Amount</label>
                        <input placeholder="Bid Amount" type="number"
                               class="form-control{{ $errors->has('bid_amount') ? ' is-invalid' : '' }}"
                               name="bid_amount" value="{{ $project->bid_amount }}" required autofocus>
                    </div>

                    <div class="col-lg-4 mb-3" id="locationField">
                        <label>Project Location</label>
                        <input id="autocomplete" onFocus="geolocate()" placeholder="Location" type="text"
                               class="form-control{{ $errors->has('location') ? ' is-invalid' : '' }}"
                               name="location" value="{{ $project->location }}" required autofocus>
                    </div>

                    <div class="col-lg-4 mb-3">
                        <label>Bidding Method</label>
                        <input placeholder="Bidding Method" type="text"
                               class="form-control{{ $errors->has('bid_method') ? ' is-invalid' : '' }}"
                               name="bid_method" value="{{ $project->bid_method}}" required autofocus>
                    </div>

                    <div class="col-lg-4 mb-3">
                        <label>Bid Date</label>

                        @php
                        $strbiddate = strtotime( $project->bid_date);
                        $strbiddatenow = date("Y/m/d H:i", $strbiddate);
                        @endphp

                        {{--<input type="date" class="form-control{{ $errors->has('bid_date') ? ' is-invalid' : '' }}"--}}
                        {{--name="bid_date" value="{{$strbiddatenow}}" required>--}}

                        <input date-format="yyyy-mm-dd hh:ii" placeholder="Bid Date" type='text'
                               class="form-control datetimepicker"
                               name="bid_date" value="{{$strbiddatenow}}" required autofocus/>
                    </div>

                    <div class="col-lg-4 mb-3" id="locationField">
                        <label>Bid Location</label>
                        <input id="autocomplete" onFocus="geolocate()" placeholder="Bid Location" type="text"
                               class="form-control{{ $errors->has('bidlocation') ? ' is-invalid' : '' }}"
                               name="bidlocation" value="{{ $project-> bid_location}}" required autofocus>
                    </div>

                    <div class="col-lg-4 mb-3">
                        <label>Status</label>
                        <select class="getAwardedInfo form-control{{ $errors->has('bid_phase') ? ' is-invalid' : '' }}"
                                name="bid_phase">
                            <option value="Open Solicitation" {{($project->bid_phase=='Open Solicitation' ? 'selected': '')}}>
                                Open Solicitation
                            </option>
                            <option data-modal="awarded"
                                    value="Awarded Solicitation" {{($project->bid_phase=='Awarded Solicitation' ? 'selected': '')}}>
                                Bid Results
                            </option>
                            <option data-modal="closed"
                                    value="Closed Solicitation" {{($project->bid_phase=='Closed Solicitation' ? 'selected': '')}}>
                                Award
                            </option>
                        </select>
                    </div>

                    <div class="col-lg-4 mb-3">
                        <label>CSI Division</label>

                        <div class="form-group">
                            <select class="form-control select2" id="csi_division" name="csi_division[]"
                                    multiple="multiple" data-placeholder="Select CSI Division" style="width: 100%;"
                                    required autofocus>
                                <?php
                                $selected = explode(',', $project->csi_division);
                                $select = '';
                                foreach($divisions as $division){
                                (in_array($division->id, $selected) ? $select = 'selected' : $select = ''); ?>

                                <option value="{{$division->id}}" {{ $select }}>{{$division->division_name}}</option>

                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-lg-4 mb-3">
                        <label>Completion Time</label>
                        <input placeholder="Completion Time" type="text"
                               class="form-control{{ $errors->has('completion_time') ? ' is-invalid' : '' }}"
                               name="completion_time" value="{{ $project->completion_time }}" required autofocus>
                    </div>

                    <div class="col-lg-4 mb-3">
                        <label>Liquidated Damages</label>
                        <input placeholder="Liquidated Damages Per CCD's" type="number"
                               class="form-control{{ $errors->has('liquidated_damages') ? ' is-invalid' : '' }}"
                               name="liquidated_damages" value="{{ $project->liquidated_damages }}" required autofocus>
                    </div>

                    <div class="col-lg-4 mb-3">
                        <label>Bid Documents Availability</label>
                        <input placeholder="Bid Documents Availability" type="text"
                               class="form-control{{ $errors->has('document_avail') ? ' is-invalid' : '' }}"
                               name="document_avail" value="{{ $project->document_avail }}" required autofocus>
                    </div>

                    <div class="col-lg-4 mb-3">
                        <label>Bid Range</label>
                        <input placeholder="Bid Range" type="text"
                               class="form-control{{ $errors->has('bidrange') ? ' is-invalid' : '' }}"
                               name="bidrange" value="{{ $project->bidrange }}" required autofocus>
                    </div>

                    <div class="col-lg-6 mb-3">
                        <label>Bonds</label>

                        <div class="row">
                            <div class="col-lg-4 form-group">
                                <input placeholder="Bid Bond %" type="number"
                                       class="form-control{{ $errors->has('b_bond') ? ' is-invalid' : '' }}"
                                       name="bbond" value="{{$project->b_bond }}" required autofocus>
                            </div>

                            <div class="col-lg-4 form-group">
                                <input placeholder="Perfomance Bond %" type="number"
                                       class="form-control{{ $errors->has('pbond') ? ' is-invalid' : '' }}"
                                       name="pbond" value="{{ $project->p_bond }}" required autofocus>
                            </div>

                            <div class="col-lg-4 form-group">
                                <input placeholder="Payment Bond %" type="number"
                                       class="form-control{{ $errors->has('paybond') ? ' is-invalid' : '' }}"
                                       name="paybond" value="{{ $project->pay_bond }}" required autofocus>
                            </div>
                        </div>

                    </div>

                    <div class="col-lg-6 mb-3" id="locationField">
                        <label>Pre-Bid Meeting</label>


                        <div class="row">
                            <div class="col-lg-6 form-group">
                                @php

                                $strbiddatetwo = strtotime( $project->pre_bid_date);
                                $strbiddatetwonow = date("Y/m/d H:i", $strbiddatetwo);


                                @endphp
                                {{--<input placeholder="Date" type="date"--}}
                                {{--class="form-control{{ $errors->has('pre_bid_date') ? ' is-invalid' : '' }}"--}}
                                {{--name="pre_bid_date" value="{{ $strbiddatetwonow }}" required autofocus>--}}

                                <input class="datetimepicker form-control{{ $errors->has('pre_bid_date') ? ' is-invalid' : '' }}"
                                       placeholder="Pre Bid Date" type='text'
                                       name="pre_bid_date" value="{{ $strbiddatetwonow }}" required minlength="4"
                                       maxlength="255" required autofocus/>
                            </div>

                            <div class="col-lg-6 form-group">
                                <input id="autocomplete1" onFocus="geolocate()" placeholder="Location" type="text"
                                       class="form-control{{ $errors->has('pre_bid_location') ? ' is-invalid' : '' }}"
                                       name="pre_bid_location" value="{{ $project->pre_bid_location }}" required
                                       autofocus>
                            </div>
                        </div>

                    </div>

                    <div class="col-lg-4 mb-3 hide">
                        <label>Project category</label>
                        <select class="form-control" name="project_cat" value="{{ $project->category_id }}">
                            @foreach($cats as $cat)
                                <option value="{{$cat->id}}">{{$cat->name}}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="col-lg-4 mb-3">
                        <label>Project Status</label>
                        <select class="form-control" name="status" required autofocus>
                            <option value="{{ $project->status }}">{{ $project->status }}</option>

                            <?php if($project->status == "Inactive" ){?>
                            <option value="Active">Active</option>
                            <?php }else{ ?>
                            <option value="Inactive">Inactive</option>
                            <?php } ?>
                        </select>
                    </div>

                    <div class="col-lg-4 mb-3">
                        <label>Assign QTO</label>
                        <select class="form-control" name="assign_qto" required autofocus>
                            <?php if($project->assign_qto == 0){?>

                            <option value="0">--Select QTO--</option>
                            <?php }else{?>

                            <option value="{{$idd->id}}">{{$idd->name}}</option>
                            <?php } ?>

                            <?php if (!empty($idd->id)) {
                                $qtoid = $idd->id;
                            } else {
                                $qtoid = 0;
                            }   ?>

                            @foreach($users as $cat)
                                @foreach($cat as $cattwo)
                                    <?php if($cattwo->id != $qtoid){ ?>
                                    <option value="{{$cattwo->id}}">{{$cattwo->name}}</option>
                                    <?php }?>
                                @endforeach
                            @endforeach
                        </select>

                    </div>

                    <div class="col-lg-4 mb-3">
                        <label>Assign Owner</label>

                        <select class="form-control" name="ownerid" required autofocus>

                            <?php if($project->owner_id == 0){?>

                            <option value="0">--Select Owners--</option>
                            <?php }else{?>

                            <option value="{{$owneridd->id}}">{{$owneridd->owner_name}}</option>
                            <?php } ?>

                            <?php if (!empty($owneridd->id)) {
                                $qtoid = $owneridd->id;
                            } else {
                                $qtoid = 0;
                            }   ?>

                            @foreach($owneriddall as $strowneriddall)
                                <?php if($strowneriddall->id != $qtoid){ ?>

                                <option value="{{$strowneriddall->id}}">{{$strowneriddall->owner_name}}</option>

                                <?php }?>
                            @endforeach

                        </select>

                    </div>

                    <div class="col-lg-4 mb-3">
                        <label>Assign Plan Holders</label>

                        <div class="form-group">
                            <select class="form-control select2" id="pholder" name="pholder[]" multiple="multiple"
                                    data-placeholder="Select Plan Holders" style="width: 100%;" required autofocus>
                                @foreach($planholderall as $strplanholderall)
                                    <option value="{{$strplanholderall->id}}">{{$strplanholderall->company}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-lg-4 mb-3">
                        <label>Notes</label>
                        <input placeholder="Notes" type="text"
                               class="form-control{{ $errors->has('notes') ? ' is-invalid' : '' }}"
                               name="notes" value="{{ $project->notes }}" autofocus>
                    </div>
                    <div class="col-lg-12 mb-3">
                        <label>Description</label>
                        <textarea name="description"
                                  class="form-control{{ $errors->has('description') ? ' is-invalid' : '' }}" id=""
                                  cols="30" rows="10">{{ $project->description }}</textarea>
                    </div>

                    <?php if(!empty($project->qto_upload) && $project->qto_upload != '[]' ){ ?>

                    <div class="col-lg-12 mb-3">

                        <label>QTO Uploaded Files: </label><br>

                        <div class="row">
                            <div class="table-responsive">
                                <table class="table table-striped">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Image</th>
                                        <th>File Name</th>
                                        <?php if($distit == 'number'): ?>
                                        <th> Price</th>
                                        <?php endif; ?>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    $downloadqtofile = json_decode($project->qto_upload);

                                    $downloadqtoprice = json_decode($project->qto_price);

                                    $counting = count($downloadqtofile);
                                    $countit = 1;
                                    $countpriceit = 0;

                                    foreach($downloadqtofile as $k => $abcde){
                                    if (!empty($project->qto_price) && $project->qto_price != '[]') {

                                        if (!empty($downloadqtoprice[$countpriceit])) {
                                            $dqp = $downloadqtoprice[$countpriceit];
                                        } else {
                                            $dqp = "1";
                                        }
                                    } else {
                                        $dqp = "1";
                                    }
                                    ?>
                                    <tr>
                                        <td>
                                            <b>File <?php echo $k + 1;?> </b>
                                        </td>
                                        <td>
                                            <img src='{{asset('images/excel.png')}}' style="width: 30px;">
                                        </td>
                                        <td>
                                            <p>{{explode('.',$abcde)[1]}}</p>
                                        </td>
                                        <?php if($distit == 'number'): ?>
                                        <td>
                                            <input class="input-price" type="{{ $distit }}" name="qtoprice{{$countit}}"
                                                   value="{{$dqp}}" required>
                                        </td>
                                        <?php endif; ?>
                                    </tr>
                                    <?php
                                    $countpriceit++;
                                    $downloadqtoprice++;
                                    $countit++;
                                    }
                                    ?>
                                    </tbody>
                                </table>
                                <input type="hidden" name="qtototalnumber" value="{{$counting}}">
                            </div>
                        </div>

                        <hr>

                    </div>

                    <?php }   ?>

                    <div class="col-lg-6 mb-3">
                        <input type="hidden" name="specification_docs" value="<?php echo $specificationDocs; ?>"
                               class="specification_docs_hidden"/>
                        <label>Specification Documents</label>

                        <div class="dzStyling well dz-clickable clearfix" id="dzSpecsDocs">
                            {{--<input name="file" type="file" multiple />--}}
                            <div class="dz-default dz-message">
                            <span>
                                <i class="upload-icon ace-icon fa fa-cloud-upload blue fa-3x"></i>
                                <br>
                                <span class="bigger-150 bolder">
                                    <i class="ace-icon fa fa-caret-right red"></i>
                                        Drop files
                                </span>
                                    to upload
                                <span class="smaller-80 grey">(or click)</span>
                                <br>
                                Only "PDF" files allowed.
                            </span>
                            </div>
                        </div>

                        <div class="previouslyUploadedFiles clearfix">

                            <?php
                                $url = "/public/bid_uploads/";
                                if(!empty($project->bid_doc_url) && $project->bid_doc_url != '[]'){ ?>

                                    <h4>Uploaded Files:</h4> <?php

                                    $downloadqto = explode(',', $project->bid_doc_url);

                                    foreach($downloadqto as $abcd){
                                        $parts = explode('_', $abcd);
                                        //$parts = isset($parts[1]) ? $parts[1] : $parts[0];
                                        if(!isset($parts[1])) continue;
                                        $parts = $parts[1];
                                        $parts = strlen($parts) > 20 ? substr($parts,0,18). '...' : $parts; ?>

                                        <div class="file-container">
                                            <center>
                                                {{--<a href="{{config('app.url')}}{{$url }}{{$abcd}}">--}}
                                                <a href="{{url('bid_uploads/' . $abcd)}}">

                                                    <img src='{{asset('images/pdf.png')}}' style="width: 25%;">

                                                    <p>{{ $parts }}</p>

                                                </a>
                                                <a class="deleteFileBtn" onclick="deleteit('{{$project->id}}','{{$abcd}}','1')">
                                                    <i class="fa fa-trash" aria-hidden="true" style="color:red"></i>
                                                </a>
                                            </center>
                                        </div> <?php
                                    }

                                }
                            ?>

                        </div>
                    </div>

                    <div class="col-lg-6 mb-3">
                        <input type="hidden" name="plan_doc" value="<?php echo $planDocuments; ?>"
                               class="design_upload_hidden"/>
                        <label>Plan Documents</label>

                        <div class="dzStyling well dz-clickable clearfix" id="dropzonePlanDocs">
                            {{--<input name="file" type="file" multiple />--}}
                            <div class="dz-default dz-message">
                            <span>
                                <i class="upload-icon ace-icon fa fa-cloud-upload blue fa-3x"></i>
                                <br>
                                <span class="bigger-150 bolder">
                                    <i class="ace-icon fa fa-caret-right red"></i>
                                        Drop files
                                </span>
                                    to upload
                                <span class="smaller-80 grey">(or click)</span>
                                <br>
                                Only "PDF" files allowed.
                            </span>
                            </div>
                        </div>
                        <div class="previouslyUploadedFiles clearfix">
                            <?php
                                $url = "/public/bid_uploads/";
                                if(!empty($project->plan_doc) && $project->plan_doc != '[]'){ ?>
                                    <h4>Uploaded Files:</h4> <?php

                                    $downloadqto = explode(',', $project->plan_doc);

                                    if(count($downloadqto) > 0){
                                        foreach($downloadqto as $abcd){

                                        $fileName = explode('_', $abcd);
                                        if (!isset($fileName[1])) continue;
                                        //$fileName = (isset($fileName[1])) ? $fileName[1] : $fileName[0];
                                            $fileName = $fileName[1];
                                            $fileName = (strlen($fileName) > 20) ? substr($fileName, 0, 18) . '...' : $fileName; ?>
                                            <div class="file-container">
                                                <center>
                                                    <a href="{{ url('bid_uploads/'. $abcd) }}">
                                                        <img src='{{asset('images/pdf.png')}}' style="width: 25%;">
                                                        <p>{{ $fileName}}</p>
                                                    </a>

                                                    <a class="deleteFileBtn" onclick="deleteit('{{$project->id}}','{{$abcd}}','2')">
                                                        <i class="fa fa-trash" aria-hidden="true" style="color:red"></i>
                                                    </a>
                                                </center>
                                            </div><?php
                                        }
                                    }
                                }
                            ?>
                        </div>
                    </div>

                    <div class="col-lg-6 mb-3 hide">
                        <input type="hidden" name="grab_doc" value="<?php echo $grabDocs; ?>"
                               class="grab_upload_hidden"/>
                        <label>Grab All Documents</label>

                        <div class="dzStyling well dz-clickable clearfix" id="dropzoneGrabDocs">
                            {{--<input name="file" type="file" multiple />--}}
                            <div class="dz-default dz-message">
                            <span>
                                <i class="upload-icon ace-icon fa fa-cloud-upload blue fa-3x"></i>
                                <br>
                                <span class="bigger-150 bolder">
                                    <i class="ace-icon fa fa-caret-right red"></i>
                                        Drop files
                                </span>
                                    to upload
                                <span class="smaller-80 grey">(or click)</span>
                                <br>
                                Only "EXCEL" files allowed.
                            </span>
                            </div>
                        </div>
                        <div class="previouslyUploadedFiles clearfix">
                            <?php
                                $url = "/public/bid_uploads/";
                                if(!empty($project->grab_docs) && $project->grab_docs != '[]'){ ?>

                                    <h4>Uploaded Files:</h4> <?php

                                    $downloadqto = explode(',', $project->grab_docs);

                                    foreach($downloadqto as $abcd){
                                        $fileNameParts = explode('_',$abcd);
                                        if(!isset($fileNameParts[1])) continue;
                                        $fileName = $fileNameParts[1];
                                        $fileName = (strlen($fileName) > 20) ? substr($fileName,0,18) . '...' : $fileName; ?>
                                        <div class="file-container">
                                    <center>
                                        <a href="{{config('app.url')}}{{$url }}{{$abcd}}">

                                            <img src='{{asset('images/excel.png')}}' style="width: 25%;">
                                            <p> {{ $fileName }}</p>

                                            {{--<p>{{ explode('_',$abcd)[1]. '...' }}</p>--}}

                                        </a>

                                        <a class="deleteFileBtn" onclick="deleteit('{{$project->id}}','{{$abcd}}','4')">
                                            <i class="fa fa-trash" aria-hidden="true" style="color:red"></i>
                                        </a>
                                    </center>
                                </div><?php
                                    }
                                }
                            ?>
                        </div>
                    </div>


                </div>


                {{-- </div>--}}


                <div class="col-lg-12 " style="background-color: rgba(0,0,0,.03); padding: 2%">
                    <center>
                        <button type="submit" class="btn btn-info  " style="padding-left:6% ;padding-right:6% ">
                            Update
                        </button>

                    </center>
                </div>

            </form>
        </div>
    </section>

    <div id="preview-template" class="hide">
        <div class="dz-preview dz-file-preview">
            <div class="dz-image">
                <img data-dz-thumbnail="">
            </div>

            <div class="dz-details">
                <div class="dz-size">
                    <span data-dz-size=""></span>
                </div>

                <div class="dz-filename">
                    <span data-dz-name=""></span>
                </div>
            </div>

            <div class="dz-progress">
                <span class="dz-upload" data-dz-uploadprogress=""></span>
            </div>

            <div class="dz-error-message">
                <span data-dz-errormessage=""></span>
            </div>

            <div class="dz-success-mark">
                <span class="fa-stack fa-lg bigger-150">
                    <i class="fa fa-circle fa-stack-2x white"></i>

                    <i class="fa fa-check fa-stack-1x fa-inverse green"></i>
                </span>
            </div>

            <div class="dz-error-mark">
                <span class="fa-stack fa-lg bigger-150">
                    <i class="fa fa-circle fa-stack-2x white"></i>

                    <i class="fa fa-remove fa-stack-1x fa-inverse red"></i>
                </span>
            </div>
        </div>
    </div>
    <div class="plan_documents" data-url="{{route('save_plan')}}"></div>


    <!-- /.content -->
    <?php
    //  $csijson = explode(',',$project->csi_division);
    $strplanholder_id = $project->planholder_id;

    ?>


    <script>


        /*var selectedValues = new Array();

         selectedValues = JSON.parse('<?php //echo json_encode($csijson); ?>');


         $("#csi_division").val(selectedValues);
         */

        var selectedph = new Array();


        selectedph = JSON.parse('<?php echo $strplanholder_id; ?>');


        $("#pholder").val(selectedph);


        function deleteit(id, proname, type) {

            swal("Are you sure you want to delete this document?").then(function (value) {

                if (typeof value !== 'undefined') {
                    var url = '{{ route("pro_delete", [":proname",":id", ":type" ]) }}';
                    url = url.replace(':proname', proname);
                    url = url.replace(':id', id);
                    url = url.replace(':type', type);
                    document.location.href = url;
                } else {
                    console.log(proname);
                }
            });
        }

        var placeSearch, autocomplete;

        function initAutocomplete() {
            autocomplete = new google.maps.places.Autocomplete(
                    document.getElementById('autocomplete'), {types: ['geocode']});
            autocomplete = new google.maps.places.Autocomplete(
                    document.getElementById('autocomplete1'), {types: ['geocode']});
//   autocomplete.setFields('address_components');
//   autocomplete.addListener('place_changed', fillInAddress);
        }

        function geolocate() {
            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(function (position) {
                    var geolocation = {
                        lat: position.coords.latitude,
                        lng: position.coords.longitude
                    };
                    var circle = new google.maps.Circle(
                            {center: geolocation, radius: position.coords.accuracy});
                    autocomplete.setBounds(circle.getBounds());
                });
            }
        }

    </script>

    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD5KPKoO7ZP-grfU1aOx2GD1ra1pQMBdAQ&libraries=places&callback=initAutocomplete"
            async defer></script>
    @include('modal.solicitation', ['selectedPlanholder' => $selectedPlanholder, 'projectID' => $project->id, 'awardedSolicitation' => $awardedSolicitation])
@endsection

<?php setlocale(LC_MONETARY,"en_US"); ?>
@extends('layouts.app')

@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-12">
                <div id="MyElement2" class="alert alert-success alert-dismissible fade hide " role="alert">
                    <h3 style="color:#fff"><strong><i class="fa fa-check" aria-hidden="true"></i></strong> Alert!</h3>
                    <p style="color:#fff">{{Session::get('alertone')}}</p>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="float-sm-right">

                </div>
            </div>
        </div>
    </div>
</section>

<!-- Main content -->
<section class="content">
    <div class="card card-info card-outline">
        <div class="container-fluid mt-2">
            <div class="info-box bg-info">
                <span class="info-box-icon"><i class="fa fa-clipboard"></i></span>
                <div class="info-box-content">
                        <h1>View Projects</h1>
                </div>
            </div>
        </div>
        <div class="card-body">
            <table id="user_table" class="table table-bordered table-hover">
                <thead>
                    <tr>
                        <th>Title</th>
                        <th>Amount</th>
                        <th>Location</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($projects as $pro)
                    <tr>
                        <td>{{$pro->title}}</td>
                        <td>{{  $pro->bid_amount }}</td>
                        <td>{{$pro->location}}</td>
                        <!--<td>{{$pro->assign_qto}}</td>-->
                     
                        <td>
                            <div class="btn-group">
                                <button type="button" class="btn btn-default">Action</button>
                                <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown"
                                    aria-expanded="false">
                                    <span class="caret"></span>
                                    <span class="sr-only">Toggle Dropdown</span>
                                </button>
                                <div class="dropdown-menu" role="menu" x-placement="top-start" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(67px, -165px, 0px);">
                                    <a class="dropdown-item" href="{{route('qto_detail_view', $pro->id)}}">Detail</a>
                                    
                                </div>
                            </div>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        <!-- /.card-body -->
    </div>
    <!-- /.card -->
<script>
    
    var existtwo2 = '{{Session::has('alertone')}}';
    
    if(existtwo2){

document.getElementById('MyElement2').classList.add('show');
document.getElementById('MyElement2').classList.remove('hide');

    }
  </script>
</section>
<!-- /.content -->
@endsection
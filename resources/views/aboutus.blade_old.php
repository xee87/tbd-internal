@extends('layout')

@section('content')

    <style type="text/css">

        .bg-blue {
            background-color: #f0f6f9;

        }

        .card {
            font-size: 1em;
            overflow: hidden;
            padding: 0;
            border: none;
            border-radius: .28571429rem;
            box-shadow: 0 1px 3px 0 #d4d4d5, 0 0 0 1px #d4d4d5;
        }

        .card-block {
            font-size: 1em;
            position: relative;
            margin: 0;
            padding: 1em;
            border: none;

            box-shadow: none;
            margin-top: 10%;
        }

        .card-img-top {
            display: block;
            width: 100%;
            height: auto;
        }

        .card-img-top {
            display: block;
            transition: transform .4s; /* smoother zoom */
        }

        .card-img-top:hover {
            transform: scale(1.3);
            transform-origin: 50% 50%;
        }

    </style>
    <section class="about-section">
        <div class="container-fluid" style="padding-right: 0px;padding-left:0px; ">


            <div id="myCarousel" class="carousel slide" data-ride="carousel">

                <div class="carousel-inner" style="background-color: #f3eeea;">

                    <div class="item active">
                        <div class="mobiledivv" style="position: absolute; margin-left: 10%; margin-top: 5%;">

                            <h1><b>Why Us</b></h1>

                            <h3><b>YES, We are master!</b></h3>

                            <h3><b>and We are ready to help you.</b></h3>

                            <p><strong>It’s the quality of our people that makes us different.</strong></p>
                            <br>


                        </div>

                        <div style="float:right; width: 50%">

                            <img class="mobileresp" src="images/banner-img.png" alt="Los Angeles" style="width:100%;">

                        </div>


                    </div>

                </div>


            </div>
        </div>
{{--<<<<<<< Updated upstream
        <img src="images/ju.jpg" style="width: 100%;">
    </div>
=======--}}
{{-->>>>>>> Stashed changes--}}

        <div style="background-color: #084887;">
            <div class="container">
                <div class="row" style=" padding: 2% 0%;">


                    <div class="col-lg-6">


                        <div class="footer-left salman">
                            <h4 style="color: #fff;"><b style="border-bottom: 3px solid #fff;padding-bottom: 1%;">Why
                                    Us </b></h4>

                        </div>


                    </div>

                    <div class="col-lg-6 text-right">

                        <ul class="social-network social-circle" style="text-align: center;">

                            <li><a href="#" class="icoRss" style="border: 0px;"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="#" class="icoRss" style="border: 0px;"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="#" class="icoRss" style="border: 0px;"><i class="fa fa-instagram"></i></a></li>
                            <li><a href="#" class="icoRss" style="border: 0px;"><i class="fa fa-google-plus"></i></a>
                            </li>

                        </ul>
                    </div>


                </div>
            </div>
        </div>

        <div class="container-fluid hide" style="background-color:#084887; ">
            <div class="row" style=" padding: 2%;">


                <div class="col-lg-6">

                    <center>
                        <div class="footer-left" style="margin-left: 10%;">
                            <h4 style="color: #fff;"><b style="border-bottom: 3px solid #fff;padding-bottom: 1%;">About
                                    Us </b></h4>

                        </div>
                    </center>


                </div>

                <div class="col-lg-6">
                    <center>
                        <ul class="social-network social-circle" style="text-align: center;">

                            <li><a href="#" class="icoRss" style="border: 0px;"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="#" class="icoRss" style="border: 0px;"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="#" class="icoRss" style="border: 0px;"><i class="fa fa-instagram"></i></a></li>
                            <li><a href="#" class="icoRss" style="border: 0px;"><i class="fa fa-google-plus"></i></a>
                            </li>

                        </ul>


                    </center>
                </div>


            </div>
        </div>


        <div class="container">
            <div class="row" style="padding: 6% 0%;">
                <div class="col-md-12">
                    <h2 class="h1">Bid Like a Winner with True Bid Data</h2><br>
                </div>
                <div class="col-md-8">


                    <p>We believe that there is no better way to Bid, other than True Bid Data. As we create articulate Bid
                        Data out of scattered information.</p><br>

                    <p>A more valuable, less invasive way, where customers are earned rather than bought. It’s our mission
                        to make every important aspect of our customer experience better with unmatched absoluteness,
                        courteousness and consideration.
                    </p><br>

                    <p>We provide all the necessary definite data of bids with complete Specifications, Plans, Addenda and
                        most importantly Quantity Take-off. We are compulsive about our clients, to get their best result on
                        Bidding, with a strong feeling of achievement. </p><br>

                    <p>Our centre of interest is to make sure that our client gets all the Bid Data in just one click
                        without any ambiguity and errors. We ensure that our Quantity Take-offs are precise and immaculate.
                    </p><br>
                </div>
                <div class="col-md-4">
                    <img src="images/ju.jpg" style="width: 100%;">
                </div>
                <div class="col-md-12">


                    <p>Agility, reliability and ability are our main strengths, which are utilized in a balanced way to
                        fulfil our customers’ requirements, taking it to a whole new level of certainty within the time
                        frame.
                    </p><br>

                    <p>We have technically sound, highly experienced, motivated and professional staff, using their
                        expertise to get the work done in time, with high standard of accuracy. Our team is capable to
                        examine the bids with delicacy, keeping in mind the interest of client.
                    </p><br>

                    <p>Upon registration, we provide all the details on any desired bid. However, for bid documents and
                        Quantity Take-off, subscription is required. One new feature is added, if any customer wants
                        Quantity Take-off of any particular division, they can buy it alone rather than buying the whole
                        Quantity Take-off. (One-Off Feature)
                    </p><br>

                    <p>Take-off on Demand (TOD) provides Quantity Take-off of the projects which are not listed on our
                        website. This allows customer to gain every angle coverage of the Project quantities.
                    </p><br>

                    <p>Surely, we are pioneers in bidding solutions. That’s why, we are True Bid Data.</p><br>
                </div>


            </div>
        </div>


        <div class="section full-width bg-blue hide ">
            <div class="container">
                <div class="row">


                    <center>


                        <h2 class="section-title video-block__title h1">What does it take to build Britain&#39;s future
                            heritage?</h2>

                        <div class="video-block__container js-responsive-video">
                            <iframe width="560" height="315"
                                    title="What does it take to build Britain&#39;s future heritage?"
                                    data-src="https://www.youtube.com/embed/3PoqRtblMW8?rel=0"
                                    data-cookieconsent="marketing" allowfullscreen></iframe>
                        </div>

                    </center>

                </div>
                <hr>
                <div class="row">


                    <div class="col-md-4" style="padding: 2%;">
                        <div class="card">
                            <div>
                                <img class="card-img-top" src="https://picsum.photos/200/150/?random">
                            </div>

                            <p class="card-block">

                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quam velit quisquam veniam
                                excepturi temporibus inventore corporis dicta sit culpa veritatis placeat earum, dolorum
                                asperiores, delectus dolore voluptatibus, at magnam nobis!
                            </p>
                        </div>
                    </div>
                    <div class="col-md-4" style="padding: 2%;">
                        <div class="card">
                            <img class="card-img-top" src="https://picsum.photos/200/150/?random">

                            <p class="card-block">
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quam velit quisquam veniam
                                excepturi temporibus inventore corporis dicta sit culpa veritatis placeat earum, dolorum
                                asperiores, delectus dolore voluptatibus, at magnam nobis!
                            </p>
                        </div>
                    </div>
                    <div class="col-md-4" style="padding: 2%;">
                        <div class="card">
                            <img class="card-img-top" src="https://picsum.photos/200/150/?random">

                            <p class="card-block">
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quam velit quisquam veniam
                                excepturi temporibus inventore corporis dicta sit culpa veritatis placeat earum, dolorum
                                asperiores, delectus dolore voluptatibus, at magnam nobis!
                            </p>
                        </div>
                    </div>


                    <div class="col-md-4" style="padding: 2%;">
                        <div class="card">
                            <img class="card-img-top" src="https://picsum.photos/200/150/?random">

                            <p class="card-block">
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quam velit quisquam veniam
                                excepturi temporibus inventore corporis dicta sit culpa veritatis placeat earum, dolorum
                                asperiores, delectus dolore voluptatibus, at magnam nobis!
                            </p>
                        </div>
                    </div>
                    <div class="col-md-4" style="padding: 2%;">
                        <div class="card">
                            <img class="card-img-top" src="https://picsum.photos/200/150/?random">

                            <p class="card-block">
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quam velit quisquam veniam
                                excepturi temporibus inventore corporis dicta sit culpa veritatis placeat earum, dolorum
                                asperiores, delectus dolore voluptatibus, at magnam nobis!
                            </p>
                        </div>
                    </div>
                    <div class="col-md-4" style="padding: 2%;">
                        <div class="card">
                            <img class="card-img-top" src="https://picsum.photos/200/150/?random">

                            <p class="card-block">
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quam velit quisquam veniam
                                excepturi temporibus inventore corporis dicta sit culpa veritatis placeat earum, dolorum
                                asperiores, delectus dolore voluptatibus, at magnam nobis!
                            </p>
                        </div>
                    </div>


                </div>

            </div>
            <br><br>
        </div>


    </section>


@endsection
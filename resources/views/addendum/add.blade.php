<?php
use \Illuminate\Support\Facades\View;
?>
@extends('layouts.app')

@section('content')
        <!-- Select2 -->
<link rel="stylesheet" href="{{asset('/plugins/select2/select2.min.css')}}">
<link rel="stylesheet" href="{{asset('/plugins/dropzone/css/dropzone.min.css')}}">
<script src="{{asset('/plugins/dropzone/js/dropzone.js')}}"></script>
<script src="{{asset('/plugins/sweetalert/sweetalert.min.js')}}"></script>

<style type="text/css">
    .select2-container--default .select2-selection--multiple .select2-selection__choice {

        color: black;
    }
</style>
<!-- Content Header (Page header) -->

<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
            </div>
            <div class="col-sm-6">
                <div class="float-sm-right">

                </div>
            </div>
        </div>
    </div>
</section>

<!-- Main content -->
<section class="content">
    <div class="card card-info card-outline">
        <div class="container-fluid mt-2">
            <div class="info-box bg-info">
                <span class="info-box-icon"><a class="add-resource" href="{{route('addendum_view')}}" title="Add New Group">
                        <i class="fa fa-arrow-left"></i></a></span>

                <div class="info-box-content">
                    <div class="col-md-5 col-sm-6 col-xs-6">
                        <h1>Add Addendum</h1>
                    </div>
                </div>
                <span class="info-box-icon float-right">
                    <i class="fa fa-list"></i>
                </span>
            </div>
        </div>

        <form action="{{ route('addendum_save') }}" method="post" role="form" enctype="multipart/form-data"
              class="" id="" style="border:0px;padding:0px 0px;">
            @csrf
            @if ($errors->any())
                <div class="alert alert-danger mb-2" role="alert">
                    @foreach ($errors->all() as $error)
                        <p>{{ $error }}</p>
                    @endforeach
                </div>
            @endif
            <div style="padding: 40px">
                <div class="row " style="">

                <div class="row">

                    {{--<div class="col-lg-6 mb-3">--}}
                        {{--<input type="hidden" name="specification_docs" value="" class="specification_docs_hidden"/>--}}
                        {{--<label>Specification Documents</label>--}}

                        {{--<div class="dzStyling well dz-clickable clearfix" id="dzSpecsDocs">--}}
                            {{--<input name="file" type="file" multiple />--}}
                            {{--<div class="dz-default dz-message">--}}
                                {{--<span>--}}
                                    {{--<i class="upload-icon ace-icon fa fa-cloud-upload blue fa-3x"></i>--}}
                                    {{--<br>--}}
                                    {{--<span class="bigger-150 bolder">--}}
                                        {{--<i class="ace-icon fa fa-caret-right red"></i>--}}
                                            {{--Drop files--}}
                                    {{--</span>--}}
                                        {{--to upload--}}
                                    {{--<span class="smaller-80 grey">(or click)</span>--}}
                                    {{--<br>--}}
                                    {{--Only "PDF" files allowed.--}}
                                {{--</span>--}}
                            {{--</div>--}}
                        {{--</div>--}}

                        {{--<div class="dropzone well dz-clickable clearfix" id="dropzoneSpecsDocs">
                            <div id="docs-ph" class="dz-default dz-message">
                                <span>
                                    <i class="upload-icon ace-icon fa fa-cloud-upload blue fa-3x"></i>
                                    <br>
                                    <span class="bigger-150 bolder">
                                        <i class="ace-icon fa fa-caret-right red"></i>
                                            Drop files
                                    </span>
                                        to upload
                                    <span class="smaller-80 grey">(or click)</span>
                                    <br>
                                    Only "PDF" files allowed.
                                </span>
                            </div>
                        </div>--}}

                        <div class="dz-default dz-message">


                        </div>

                    <div class="col-lg-12 mb-3">

                        <label>Select Project</label>

                        <div class="form-group">
                            <select class="form-control" name="project_id"
                                    data-placeholder="Select Project" style="width: 100%;" required autofocus>
                                <?php foreach($addendum as $project){ ?>
                                <option value="{{$project->id}}">{{$project->title}}</option>

                                <?php } ?>
                            </select>
                        </div>

                    </div>

                    <div class="col-lg-12 mb-3">
                        <input type="hidden" name="addendum_doc" value="" class="design_upload_hidden"/>
                        <label>addendum Documents</label>
                        <div class="dzStyling well dz-clickable clearfix" id="dropzoneAddendumDocs">
                            {{--<input name="file" type="file" multiple />--}}
                            <div class="dz-default dz-message">
                                <span>
                                    <i class="upload-icon ace-icon fa fa-cloud-upload blue fa-3x"></i>
                                    <br>
                                    <span class="bigger-150 bolder">
                                        <i class="ace-icon fa fa-caret-right red"></i>
                                            Drop files
                                    </span>
                                        to upload
                                    <span class="smaller-80 grey">(or click)</span>
                                    <br>
                                    Only "PDF" files allowed.
                                </span>
                            </div>
                        </div>

                    </div>

                </div>
            </div>

        </div>
            <div class="col-lg-12 " style="background-color: rgba(0,0,0,.03); padding: 2%">
                <center>
                    <button type="submit" class="btn btn-primary btn-info  " style="padding-left:6% ;padding-right:6% ">Create
                    </button>

                </center>
            </div>
        </form>
    </div>

    <div id="preview-template" class="hide">
        <div class="dz-preview dz-file-preview">
            <div class="dz-image">
                <img data-dz-thumbnail="">
            </div>

            <div class="dz-details">
                <div class="dz-size">
                    <span data-dz-size=""></span>
                </div>

                <div class="dz-filename">
                    <span data-dz-name=""></span>
                </div>
            </div>

            <div class="dz-progress">
                <span class="dz-upload" data-dz-uploadprogress=""></span>
            </div>

            <div class="dz-error-message">
                <span data-dz-errormessage=""></span>
            </div>

            <div class="dz-success-mark">
                <span class="fa-stack fa-lg bigger-150">
                    <i class="fa fa-circle fa-stack-2x white"></i>

                    <i class="fa fa-check fa-stack-1x fa-inverse green"></i>
                </span>
            </div>

            <div class="dz-error-mark">
                <span class="fa-stack fa-lg bigger-150">
                    <i class="fa fa-circle fa-stack-2x white"></i>

                    <i class="fa fa-remove fa-stack-1x fa-inverse red"></i>
                </span>
            </div>
        </div>
    </div>
    <div class="plan_documents" data-url="{{route('save_plan')}}"></div>
</section>



<!-- /.content -->
{{--<script>--}}
    {{--var placeSearch, autocomplete;--}}

    {{--function initAutocomplete() {--}}
        {{--autocomplete = new google.maps.places.Autocomplete(--}}
                {{--document.getElementById('autocomplete'), {types: ['geocode']});--}}
        {{--autocomplete = new google.maps.places.Autocomplete(--}}
                {{--document.getElementById('autocomplete1'), {types: ['geocode']});--}}
        {{--autocomplete = new google.maps.places.Autocomplete(--}}
                {{--document.getElementById('autocomplete2'), {types: ['geocode']});--}}
{{--//   autocomplete.setFields('address_components');--}}
{{--//   autocomplete.addListener('place_changed', fillInAddress);--}}
    {{--}--}}

    {{--function geolocate() {--}}
        {{--if (navigator.geolocation) {--}}
            {{--navigator.geolocation.getCurrentPosition(function (position) {--}}
                {{--var geolocation = {--}}
                    {{--lat: position.coords.latitude,--}}
                    {{--lng: position.coords.longitude--}}
                {{--};--}}
                {{--var circle = new google.maps.Circle({--}}
                    {{--center: geolocation,--}}
                    {{--radius: position.coords.accuracy--}}
                {{--});--}}
                {{--autocomplete.setBounds(circle.getBounds());--}}
            {{--});--}}
        {{--}--}}
    {{--}--}}
{{--</script>--}}
{{--<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD5KPKoO7ZP-grfU1aOx2GD1ra1pQMBdAQ&libraries=places&callback=initAutocomplete"--}}
        {{--async defer></script>--}}


{{--@include('modal.solicitation');--}}

@endsection

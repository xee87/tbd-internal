<?php

$distit =  (Auth::user()->hasRole('DEO')) ? 'hidden' : 'number';
$planDocuments = $specificationDocs = '';
$alertOne = Session::get('alertone');

?>
@extends('layouts.app')

@section('content')

    <link rel="stylesheet" href="{{asset('/plugins/select2/select2.min.css')}}">
    <link rel="stylesheet" href="{{asset('/plugins/dropzone/css/dropzone.min.css')}}">
    <script src="{{asset('/plugins/dropzone/js/dropzone.js')}}"></script>
    <script src="{{asset('/plugins/jquery/jquery.min.js')}}"></script>
    <script src="{{asset('/plugins/sweetalert/sweetalert.min.js')}}"></script>

    <style type="text/css">
        .dropzone {

            width: 100%;
        }

        .select2-container--default .select2-selection--multiple .select2-selection__choice {

            color: black;
        }
    </style>

    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">

                <div class="col-sm-12">
                    <div id="MyElement" class="alert alert-success alert-dismissible fade hide " role="alert">
                        <h3 style="color:#fff"><strong><i class="fa fa-check" aria-hidden="true"></i></strong> Alert!</h3>
                        <p style="color:#fff">Project <?php if(!empty($_GET['msg'])){echo $_GET['msg']; $gomsg= 1;}?> Successfully!</p>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div id="MyElement2" class="alert alert-success alert-dismissible <?php if(!isset($alertOne)) echo " hide" ?>" role="alert">
                        <h3 style="color:#fff"><strong><i class="fa fa-check" aria-hidden="true"></i></strong> Alert!</h3>
                        <p style="color:#fff">{{ $alertOne }}</p>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="float-sm-right">

                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="card card-info card-outline">
            <div class="container-fluid mt-2">
                <div class="info-box bg-info">
                    <span class="info-box-icon">
                        <a class="add-resource" href="{{route('pro_view')}}" title="Add New Group">
                            <i class="fa fa-arrow-left"></i>
                        </a>
                    </span>

                    <div class="info-box-content">
                        <div class="col-md-5 col-sm-6 col-xs-6">
                            <h1>Edit Addendum</h1>
                        </div>
                    </div>
                <span class="info-box-icon float-right">
                    <i class="fa fa-list"></i>
                </span>
                </div>
            </div>



            <form action="{{ route('addendum_update', $project->id) }}" method="post" role="form"
                  enctype="multipart/form-data" class="dropzone" id="mydz" style="border:0px;padding:0px 0px;">
                @csrf
                @if ($errors->any())
                    @foreach ($errors->all() as $error)
                        <div class="alert alert-danger mb-2" role="alert">
                            {{ $error }}
                        </div>
                    @endforeach
                @endif

                <div class="row " style="padding: 40px;">



                    <?php //var_dump($planDocuments); exit; ?>


                    <div class="col-md-6 mb-3">
                        <input type="hidden" name="plan_doc" value="<?php echo $planDocuments; ?>" class="design_upload_hidden"/>
                        <label>Addendum Documents</label>
                        <div class="dzStyling well dz-clickable clearfix" id="dropzonePlanDocs">
                            {{--<input name="file" type="file" multiple />--}}
                            <div class="dz-default dz-message">
                            <span>
                                <i class="upload-icon ace-icon fa fa-cloud-upload blue fa-3x"></i>
                                <br>
                                <span class="bigger-150 bolder">
                                    <i class="ace-icon fa fa-caret-right red"></i>
                                        Drop files
                                </span>
                                    to upload
                                <span class="smaller-80 grey">(or click)</span>
                                <br>
                                Only "PDF" files allowed.
                            </span>
                            </div>
                        </div>
                        <div class="previouslyUploadedFiles clearfix">
                            <?php
                            $url = "/public/bid_uploads/" ;
                            if(isset($docs)){
                                if(!empty($docs)){
                                ?>
                                <h4>Uploaded Files:</h4>
                                <?php
                                //$downloadqto = explode(',',$project->plan_doc);
                                foreach($docs as $abcd){
                                    $fileNameParts = explode('_', $abcd);
                                    $fileName = isset($fileNameParts[1]) ? $fileNameParts[1] : $fileNameParts[0];

                                ?>

                                <div class="file-container">
                                    <center>
                                        <a title="{{$abcd}}" href="{{ url('bid_uploads/' . $abcd) }}">

                                            <img src='{{asset('images/pdf.png')}}' style="width: 25%;">

                                            <p>{{ $fileName . '...' }}</p>
                                        </a>

                                        <a class="deleteFileBtn" onclick="deleteit('{{$project->id}}','{{$abcd}}','0')">
                                            <i class="fa fa-trash" aria-hidden="true" style="color:red"></i>
                                        </a>
                                    </center>
                                </div>
                            <?php }  } } ?>
                        </div>
                    </div>

                </div>
                {{-- </div>--}}



                <div class="col-lg-12 " style="background-color: rgba(0,0,0,.03); padding: 2%">
                    <center>
                        <button type="submit" class="btn btn-info  " style="padding-left:6% ;padding-right:6% ">
                            Update
                        </button>

                    </center>
                </div>

            </form>
        </div>
    </section>

    <div id="preview-template" class="hide">
        <div class="dz-preview dz-file-preview">
            <div class="dz-image">
                <img data-dz-thumbnail="">
            </div>

            <div class="dz-details">
                <div class="dz-size">
                    <span data-dz-size=""></span>
                </div>

                <div class="dz-filename">
                    <span data-dz-name=""></span>
                </div>
            </div>

            <div class="dz-progress">
                <span class="dz-upload" data-dz-uploadprogress=""></span>
            </div>

            <div class="dz-error-message">
                <span data-dz-errormessage=""></span>
            </div>

            <div class="dz-success-mark">
                <span class="fa-stack fa-lg bigger-150">
                    <i class="fa fa-circle fa-stack-2x white"></i>

                    <i class="fa fa-check fa-stack-1x fa-inverse green"></i>
                </span>
            </div>

            <div class="dz-error-mark">
                <span class="fa-stack fa-lg bigger-150">
                    <i class="fa fa-circle fa-stack-2x white"></i>

                    <i class="fa fa-remove fa-stack-1x fa-inverse red"></i>
                </span>
            </div>
        </div>
    </div>
    <div class="plan_documents" data-url="{{route('save_plan')}}"></div>


    <!-- /.content -->
    <?php
    //  $csijson = explode(',',$project->csi_division);
    $strplanholder_id = $project->planholder_id;

    ?>


    <script>


        /*var selectedValues = new Array();

         selectedValues = JSON.parse('<?php //echo json_encode($csijson); ?>');


         $("#csi_division").val(selectedValues);
         */

        var selectedph = new Array();


        selectedph = JSON.parse('<?php echo $strplanholder_id; ?>');


        $("#pholder").val(selectedph);


        function deleteit(id, proname, type ) {

            swal("Are you sure you want to delete this document?").then(function(value) {

                if(typeof value !== 'undefined'){
                    var url = '{{ route("pro_delete", [":proname",":id", ":type" ]) }}';
                    url = url.replace(':proname', proname);
                    url = url.replace(':id', id);
                    url = url.replace(':type', type);
                    document.location.href = url;
                }else{
                    console.log(proname);
                }
            });
        }


    </script>

@endsection
{{--C:\Users\Xee\AppData\Roaming\Composer\vendor\bin--}}
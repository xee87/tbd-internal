@extends('layout')

@section('content')


    <style>


        body {
            background-image: url('/images/O7KlZIGA.png');
            background-position-y: bottom;
            background-size: cover;
            background-repeat: no-repeat;
        }

        @media (max-width: 992px) {

            body {
                background-image: url('/images/O7KlZIGA.png');
                background-position-y: center;
                background-size: cover;
                background-repeat: no-repeat;
            }
        }
    </style>




    <div class="container" id="mr" style="margin-top:5%;    min-height: 700px;">


        <div id="MyElement" class="alert alert-danger alert-dismissible hide"
             style="background-color:#dc3545; border-color: #dc3545">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>

            <h3 style="color:#fff;margin-top:0px;"><strong><i class="fa fa-close" aria-hidden="true"></i></strong>
                Alert!</h3>

            <p style="color:#fff">{{Session::get('alertone')}}</p>

        </div>

        <div class="row form-group m-b-0">
            <div class="col-xs-12">
                <ul class="nav nav-pills nav-justified thumbnail setup-panel">
                    <li class="active"><a href="#step-1">
                            <h4 class="list-group-item-heading">Step 1</h4>
                            <p class="list-group-item-text">Personal Information</p>
                        </a></li>
                    <li class="disabled"><a href="#step-2">
                            <h4 class="list-group-item-heading">Step 2</h4>

                            <p class="list-group-item-text">Credit Card Details</p>
                        </a></li>
                    <!-- <li class="disabled"><a href="#step-3">
                        <h4 class="list-group-item-heading">Step 3</h4>
                        <p class="list-group-item-text">Confirmation</p>
                    </a></li> -->
                </ul>
            </div>
        </div>
        <form action="{{ route('r_user_store') }}" method="post">
            <div class="row setup-content" id="step-1">
                <div class="col-xs-12">
                    <div class="col-md-12 well">

                        <div class="row " style="padding: 30px;">
                            <div class="col-lg-12">
                                <h3 class="text-white pt-5" style="color:#084887">Personal Information</h3>
                            </div>
                            @csrf

                            @if ($errors->any())
                                <div class="col-lg-12">
                                @foreach ($errors->all() as $error)
                                    <div class="alert alert-danger mb-2" role="alert">
                                        {{ $error }}
                                    </div>
                                @endforeach
                                </div>
                            @endif


                            <div class="col-lg-6 mb-3" style="margin-top:2%">
                                <label for="name">Name</label>
                                <input id="name" placeholder="Name" type="text"
                                       class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}"
                                       name="name" required autofocus onchange="checkvalues()">
                            </div>
                            <div class="col-lg-6 mb-3" style="margin-top:2%">
                                <label for="email">Email</label>
                                <input id="email" placeholder="Email" type="email"
                                       class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}"
                                       name="email" required onchange="checkvalues()">
                            </div>
                            <div class="col-lg-6 mb-3" style="margin-top:2%">
                                <label for="password">Password</label>
                                <input id="password" placeholder="Password" type="password"
                                       class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}"
                                       name="password" required onchange="checkvalues()">
                            </div>
                            <div class="col-lg-6 mb-3" style="margin-top:2%">
                                <label for="name">Confirm password</label>
                                <input placeholder="Retype password" id="password-confirm" type="password"
                                       class="form-control{{ $errors->has('password_confirmation') ? ' is-invalid' : '' }}"
                                       name="password_confirmation" required onchange="checkvalues()">
                            </div>

                            <div class="col-lg-6 mb-3" style="margin-top:2%">
                                <label for="name">Company Name</label>
                                <input type="text" placeholder="Company Name" class="form-control" name="company_name" required/>
                            </div>

                            <div class="col-lg-6 mb-3" style="margin-top:2%">
                                <label for="name">Contact</label>
                                <input type="tel" placeholder="Contact" class="form-control" name="contact" required/>
                            </div>

                            <div class="col-lg-6 mb-3" style="margin-top:2%">
                                <label for="name">Address</label>
                                <input type="text" class="form-control address" name="user_address" id="autocomplete" onFocus="geolocate()"/>
                            </div>



                            <div class="col-lg-12" style="margin-top:2%">
                                <div id="register-link" class="text-left">
                                    <a href="/user_login" class="text-info">Login?</a>
                                </div>
                                <br>

                                <p style="color: #084887;">ALL FIELDS ARE REQUIRED.</p>

                                <div class="next-btn-wrapper m-y-lg">
                                    <button id="activate-step-2" style="background-color: #084887;"  class="btn btn-3d btn-reveal btn-orange blue-button-bg">
                                        <i class="fa fa-arrow-right"></i>
                                        <span>Next</span>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row setup-content" id="step-2">
                <div class="col-xs-12">
                    <div class="col-md-12 well">

                        <div class="row " style="padding: 30px;">
                            <div class="col-md-12">
                                <h3 class="text-white pt-5" style="color:#084887">Credit Card Details</h3>
                                {{--<p>You are required to pay one time registration fee USD {{$strplan['cost']}}</p>--}}
                                <p> Your card details will be stored on Braintree, we don't store your credit card
                                    information.</p>
                            </div>

                            <div class="col-md-12">
                                <div id="dropin-container"></div>
                            </div>

                            <hr/>

                            <div class="col-md-12">

                                <button type="submit" id="payment-button" style="background-color: #084887;" class="btn btn-3d btn-reveal btn-orange blue-button-bg">
                                    <i class="fa fa-money"></i>
                                    <span> Pay & Submit </span>
                                </button>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>

    <script src="https://js.braintreegateway.com/js/braintree-2.32.1.min.js"></script>
    <script>
        jQuery.ajax({
                    url: "{{ route('token') }}",
                })
                .done(function (res) {
                    braintree.setup(res.data.token, 'dropin', {
                        container: 'dropin-container',
                        onReady: function () {
                            jQuery('#payment-button').removeAttr('disabled');
                        }

                    });
                });


        function checkvalues(){

            var strcname =  document.getElementById('name').value;
            var strcemail = document.getElementById('email').value;
            var strcpassword = document.getElementById('password').value;
            var strcpasswordconfirm = document.getElementById('password-confirm').value;

            if( strcname != "" && strcemail != "" && strcpassword != "" && strcpasswordconfirm != "" ){
                document.getElementById('activate-step-2').disabled = false;
            }else{

                document.getElementById('activate-step-2').disabled = true;
            }

        }

    </script>



    <script type="text/javascript">

        $(document).ready(function () {

            var navListItems = $('ul.setup-panel li a'),
                    allWells = $('.setup-content');

            allWells.hide();

            navListItems.click(function (e) {
                e.preventDefault();
                var $target = $($(this).attr('href')),
                        $item = $(this).closest('li');

                if (!$item.hasClass('disabled')) {
                    navListItems.closest('li').removeClass('active');
                    $item.addClass('active');
                    allWells.hide();
                    $target.show();
                }
            });

            $('ul.setup-panel li.active a').trigger('click');

            // DEMO ONLY //
            $('#activate-step-2').on('click', function (e) {
                $('ul.setup-panel li:eq(1)').removeClass('disabled');
                $('ul.setup-panel li a[href="#step-2"]').trigger('click');
                //$(this).remove();
            })


            $('#activate-step-3').on('click', function (e) {
                $('ul.setup-panel li:eq(2)').removeClass('disabled');
                $('ul.setup-panel li a[href="#step-3"]').trigger('click');
                $(this).remove();
            })
        });

        var existtwo = '{{Session::has('alertone')}}';

        if (existtwo) {

            document.getElementById('MyElement').classList.add('show');
            document.getElementById('MyElement').classList.remove('hide');

        }



        var placeSearch, autocomplete;

        function initAutocomplete() {
            autocomplete = new google.maps.places.Autocomplete(
                    document.getElementById('autocomplete'), {types: ['geocode']});
            /*autocomplete = new google.maps.places.Autocomplete(
                    document.getElementById('autocomplete1'), {types: ['geocode']});
            autocomplete = new google.maps.places.Autocomplete(
                    document.getElementById('autocomplete2'), {types: ['geocode']});*/
//   autocomplete.setFields('address_components');
//   autocomplete.addListener('place_changed', fillInAddress);
        }

        function geolocate() {
            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(function (position) {
                    var geolocation = {
                        lat: position.coords.latitude,
                        lng: position.coords.longitude
                    };
                    var circle = new google.maps.Circle({
                        center: geolocation,
                        radius: position.coords.accuracy
                    });
                    autocomplete.setBounds(circle.getBounds());
                });
            }
        }
    </script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD5KPKoO7ZP-grfU1aOx2GD1ra1pQMBdAQ&libraries=places&callback=initAutocomplete"
            async defer></script>



@endsection
@extends('layout')

@section('content')

    <div class="container">
        <div class="my-margin-top">
            <div class="row">


                <div class="col-md-6">
                    <div class="my-image-firsrt">
                        <img class="image-width" src="images/decide-graphic.png" style="width: 40%;">

                    </div>

                </div>
                <br>
                <div class="col-md-6">
                    <div class="default">
                        <h1 class="margin"> Subcription Plans</h1>

                        <p class="plans-desc">The Quantity Takeoff Documents Are Paid, Please Buy Suitable<br> Subcription Plan and get
                            access to the Documents. </p>
                    </div>
                </div>
                <br>
            </div>
        </div>
        <br>



            <div class="row">

                    <div class="col-md-4 col-sm-6">
                        <div class="for-border">
                            <div class="for-image">
                                <div class="doller"><span class="dollar-sign">$</span>

                                    <h1 class="full-size"><span>$</span>450</h1>

                                    <p>yearly</p></div>
                            </div>

                            <table>
                                <tr>
                                    <td><span style="color: #f0542d; margin-right: 11px;" class="fa fa-download "
                                              aria-hidden="true"></span>Download Limit
                                    </td>

                                    <th>30</th>
                                </tr>

                                <tr>
                                    <td><span style="color: #f0542d; margin-right: 11px;" class="fa fa-clock-o "></span>Duration
                                    </td>

                                    <th>1 year</th>
                                </tr>
                                <tr>
                                    <td><span style="color: #f0542d; margin-right: 11px;" class="fa fa-money "></span>Max
                                        Project Value
                                    </td>

                                    <th>10,000</th>
                                </tr>

                            </table>
                            <div class="my-button-firsrt-not">
                                <button class="button button2 btn">Explore More&nbsp&nbsp&nbsp<i
                                            class="fa fa-long-arrow-right"
                                            aria-hidden="true"></i>
                                </button>
                            </div>
                        </div>
                    </div>







                    <div class="col-md-4 col-sm-6">
                        <div class="for-border">
                            <div class="for-image">
                                <div class="doller"><span class="dollar-sign">$</span>

                                    <h1 class="full-size"><span>$</span>450</h1>

                                    <p>yearly</p></div>
                            </div>

                            <table>
                                <tr>
                                    <td><span style="color: #f0542d; margin-right: 11px;" class="fa fa-download "
                                              aria-hidden="true"></span>Download Limit
                                    </td>

                                    <th>30</th>
                                </tr>

                                <tr>
                                    <td><span style="color: #f0542d; margin-right: 11px;" class="fa fa-clock-o "></span>Duration
                                    </td>

                                    <th>1 year</th>
                                </tr>
                                <tr>
                                    <td><span style="color: #f0542d; margin-right: 11px;" class="fa fa-money "></span>Max
                                        Project Value
                                    </td>

                                    <th>10,000</th>
                                </tr>

                            </table>
                            <div class="my-button-firsrt-not">
                                <button class="button button2 btn">Explore More&nbsp&nbsp&nbsp<i
                                            class="fa fa-long-arrow-right"
                                            aria-hidden="true"></i>
                                </button>
                            </div>
                        </div>
                    </div>





                    <div class="col-md-4 col-sm-6">
                        <div class="for-border">
                            <div class="for-image">
                                <div class="doller"><span class="dollar-sign">$</span>

                                    <h1 class="full-size"><span>$</span>450</h1>

                                    <p>yearly</p></div>
                            </div>

                            <table>
                                <tr>
                                    <td><span style="color: #f0542d; margin-right: 11px;" class="fa fa-download "
                                              aria-hidden="true"></span>Download Limit
                                    </td>

                                    <th>30</th>
                                </tr>

                                <tr>
                                    <td><span style="color: #f0542d; margin-right: 11px;" class="fa fa-clock-o "></span>Duration
                                    </td>

                                    <th>1 year</th>
                                </tr>
                                <tr>
                                    <td><span style="color: #f0542d; margin-right: 11px;" class="fa fa-money "></span>Max
                                        Project Value
                                    </td>

                                    <th>10,000</th>
                                </tr>

                            </table>
                            <div class="my-button-firsrt-not">
                                <button class="button button2 btn">Explore More&nbsp&nbsp&nbsp<i
                                            class="fa fa-long-arrow-right"
                                            aria-hidden="true"></i>
                                </button>
                            </div>
                        </div>
                    </div>




        <br>



                    <div class="col-md-4 col-sm-6">
                        <div class="for-border">
                            <div class="for-image">
                                <div class="doller"><span class="dollar-sign">$</span>

                                    <h1 class="full-size"><span>$</span>450</h1>

                                    <p>yearly</p></div>
                            </div>

                            <table>
                                <tr>
                                    <td><span style="color: #f0542d; margin-right: 11px;" class="fa fa-download "
                                              aria-hidden="true"></span>Download Limit
                                    </td>

                                    <th>30</th>
                                </tr>

                                <tr>
                                    <td><span style="color: #f0542d; margin-right: 11px;" class="fa fa-clock-o "></span>Duration
                                    </td>

                                    <th>1 year</th>
                                </tr>
                                <tr>
                                    <td><span style="color: #f0542d; margin-right: 11px;" class="fa fa-money "></span>Max
                                        Project Value
                                    </td>

                                    <th>10,000</th>
                                </tr>

                            </table>
                            <div class="my-button-firsrt-not">
                                <button class="button button2 btn">Explore More&nbsp&nbsp&nbsp<i
                                            class="fa fa-long-arrow-right"
                                            aria-hidden="true"></i>
                                </button>
                            </div>
                        </div>
                    </div>







                    <div class="col-md-4 col-sm-6">
                        <div class="for-border">
                            <div class="for-image-two">
                                <div class="doller"><span class="dollar-sign">$</span>

                                    <h1 class="full-size"><span>$</span>1000</h1>

                                    <p>yearly</p></div>
                            </div>

                            <table>
                                <tr>
                                    <td><span style="color: #f0542d; margin-right: 11px;" class="fa fa-download "
                                              aria-hidden="true"></span>Download Limit
                                    </td>

                                    <th>30</th>
                                </tr>

                                <tr>
                                    <td><span style="color: #f0542d; margin-right: 11px;" class="fa fa-clock-o "></span>Duration
                                    </td>

                                    <th>1 year</th>
                                </tr>
                                <tr>
                                    <td><span style="color: #f0542d; margin-right: 11px;" class="fa fa-money "></span>Max
                                        Project Value
                                    </td>

                                    <th>10,000</th>
                                </tr>

                            </table>
                            <div class="my-button-firsrt-not">
                                <button class="button button2 btn">Explore More&nbsp&nbsp&nbsp<i
                                            class="fa fa-long-arrow-right"
                                            aria-hidden="true"></i>
                                </button>
                            </div>
                        </div>
                    </div>


            </div>
    </div>










@endsection



@extends('layouts.app')

@section('content')
        <!-- Content Header (Page header) -->


<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <!-- <h1>User Add</h1> -->
            </div>
            <div class="col-sm-6">
                <div class="float-sm-right">
                    <!-- <button type="button" class="btn btn-block btn-info" onclick="location.href='{{route('user_view')}}';">List</button> -->
                </div>
            </div>
        </div>
    </div>
</section>

<!-- Main content -->
<section class="content">
    <div class="card card-info card-outline">
        <div class="container-fluid mt-2">
            <div class="info-box bg-info">

                <span class="info-box-icon">
                   <a class="add-resource" href="{{route('user_view')}}" title="Add New Group">
                       <i class="fa fa-arrow-left"></i>
                   </a>
                </span>


                <div class="info-box-content">
                    <div class="col-md-5 col-sm-6 col-xs-6">
                        <h1>User Add</h1>
                    </div>
                </div>
                <span class="info-box-icon float-right">
                    <i class="fa fa-users"></i>
                </span>
            </div>
        </div>


        <form action="{{ route('user_create') }}" method="post">
            @csrf
            @if ($errors->any())
                @foreach ($errors->all() as $error)
                    <div class="alert alert-danger mb-2" role="alert">
                        {{ $error }}
                    </div>
                @endforeach
            @endif
            <div class="row " style="padding: 40px;">
                <div class="col-lg-6 mb-3">
                    <label for="name">Name</label>
                    <input id="name" placeholder="Name" type="text"
                           class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}"
                           name="name" value="{{ old('name') }}" required autofocus>
                </div>
                <div class="col-lg-6 mb-3">
                    <label for="email">Email</label>
                    <input id="email" placeholder="Email" type="email"
                           class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}"
                           name="email" value="{{ old('email') }}" required>
                </div>
                <div class="col-lg-6 mb-3">
                    <label for="password">Password</label>
                    <input id="password" placeholder="Password" type="password"
                           class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}"
                           name="password" required>
                </div>
                <div class="col-lg-6 mb-3">
                    <label for="name">Retype password</label>
                    <input placeholder="Retype password" id="password-confirm" type="password" class="form-control"
                           name="password_confirmation" required>
                </div>
                <div class="col-lg-6 mb-3">
                    <label>Role</label>
                    <select class="form-control" name="role" value="{{ old('role') }}" id="user_role"
                            onchange="checkmyuser()">
                        @foreach($roles as $role)
                            <?php if($role->name != 'User'){?>
                            <option>{{$role->name}}</option>
                            <?php } ?>
                        @endforeach
                    </select>
                </div>


                <div class="col-lg-6 mb-3" id="proj_user" style="display: none;">
                    <label>Assign Project</label>

                    <select class="form-control" name="user_assign_proj">

                        <option value="">--Select Project--</option>
                        @foreach($user_pro as $up)

                            <option value="{{$up->id}}">{{$up->title}}</option>


                        @endforeach
                    </select>


                </div>

            </div>

            <div class="col-lg-12 " style="background-color: rgba(0,0,0,.03); padding: 2%">
                <center>
                    <button type="submit" class="btn btn-info " style="padding-left:6% ;padding-right:6% ">Create
                    </button>
                </center>
            </div>

            <!-- /.col -->

        </form>

        <script type="text/javascript">
            checkmyuser();
            function checkmyuser() {

                var checkuser = document.getElementById('user_role').value;

                if (checkuser == "QTO") {

                    document.getElementById("proj_user").style.display = "block";
                } else {
                    document.getElementById("proj_user").style.display = "none";
                }


            }
        </script>

</section>
<!-- /.content -->
@endsection

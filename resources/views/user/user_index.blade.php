@extends('layouts.app')

@section('content')


<?php
    $danis = array('Admin', 'QTO', 'DEO');

?>

<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-md-12">
                <div id="MyElement" class="alert alert-success alert-dismissible fade hide " role="alert">
                    <h3 style="color:#fff"><strong><i class="fa fa-check" aria-hidden="true"></i></strong> Alert!</h3>
                    <p style="color:#fff">{{Session::get('alertone')}}</p>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- Main content -->
<section class="content">
    <div class="card card-info card-outline">
         <div class="container-fluid mt-2">
            <div class="info-box bg-info">
                <span class="info-box-icon"><i class="fa fa-users"></i></span>
                <div class="info-box-content">
                    <div class="col-md-12">

                        <?php

                            if($id == 1){
                                echo '<h1>User Management</h1>';
                            }else{
                                echo '<h1>Customer Management</h1>';
                            }
                        ?>



                    </div>
                </div>
                <span class="info-box-icon float-right">
                    <a class="add-resource" href="{{route('user_add')}}" title="Add New Group"> <i class="fa fa-plus"></i></a>
                </span>
            </div>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table id="user_table" class="table table-bordered table-hover">
                    <thead>
                    <tr>
                        <th>Name</th>

                        @if($id ==2)
                            <th>Company</th>
                            <th>Contact</th>
                        @endif

                        <th>Email</th>
                        <th>Date Added</th>
                        @if($id == 1)
                            <th>Role</th>
                        @endif
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($users as $user)
                        <?php
                        if($id == 1 && !in_array($user->getRoleNames()->first(), $danis)){
                            continue;
                        }
                        if( $id == 2 &&  in_array($user->getRoleNames()->first(), $danis)){
                            continue;
                        }

                        ?>
                        <tr>
                            <td>{{$user->name}}</td>

                            @if($id == 2)

                                <td>{{ $user->company_name }}</td>
                                <td>{{ $user->contact }}</td>

                            @endif

                            <td>{{$user->email}}</td>
                            <?php
                            $originalDate = $user->created_at;
                            $newDate = date("M-d-Y h:i:sa", strtotime($originalDate));
                            ?>
                            <td>{{$newDate}}</td>
                            <?php //var_dump($id) ?>
                            @if($id == 1)
                                <td>{{$user->getRoleNames()->first()}}</td>
                            @endif
                            <td>
                                <div class="btn-group">

                                    <button type="button" class="btn btn-default">{{($user->status == 1) ? 'Active': 'Inactive'}}</button>

                                    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown"
                                            aria-expanded="false">
                                        <span class="caret"></span>
                                        <span class="sr-only">Toggle Dropdown</span>
                                    </button>
                                    <div class="dropdown-menu" role="menu" x-placement="top-start" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(67px, -165px, 0px);">
                                        <a class="dropdown-item" href="{{route('user_status_active', $user->id)}}">Active</a>
                                        <a class="dropdown-item" href="{{route('user_status_inactive', $user->id)}}">Inactive</a>
                                        <a class="dropdown-item" href="{{route('user_status_delete', $user->id)}}">Delete</a>
                                    </div>
                                </div>
                            </td>
                            <td>
                                <div class="btn-group">
                                    <button type="button" class="btn btn-default">Action</button>
                                    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown"
                                            aria-expanded="false">
                                        <span class="caret"></span>
                                        <span class="sr-only">Toggle Dropdown</span>
                                    </button>
                                    <div class="dropdown-menu" role="menu" x-placement="top-start" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(67px, -165px, 0px);">
                                        <a class="dropdown-item" href="{{route('user_edit',$user->id)}}">Edit</a>
                                        <!--    <a class="dropdown-item" href="{{route('user_delete',$user->id)}}">Delete</a> -->
                                    </div>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <!-- /.card-body -->
    </div>
    <!-- /.card -->

<script>
    
    var existtwo = '{{Session::has('alertone')}}';
    
    if(existtwo){

document.getElementById('MyElement').classList.add('show');
document.getElementById('MyElement').classList.remove('hide');

    }
  </script>
</section>
<!-- /.content -->
@endsection

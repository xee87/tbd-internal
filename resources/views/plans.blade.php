@extends('layout')

@section('content')
<?php

use App\Helper\TBD;

?>

    <div class="container">
        <div class="my-margin-top">
            <div class="row">


                <div class="col-md-6">
                    <div class="my-image-firsrt">
                        <img class="image-width" src="images/decide-graphic.png" style="width: 40%;">

                    </div>

                </div>
                <br>
                <div class="col-md-6">
                    <div class="default">
                        <h1 class="margin"> Subscription Plans</h1>

                        <p class="plans-desc">Flexible plans that grow with you. Fastest and easiest way to grow your business is by hassle free bidding. We'll help you bid smarter from day one. </p>


                        <br>

                    </div>
                </div>
                <br>
            </div>
        </div>

        <div id="myCarousel" class="carousel slide hide" data-ride="carousel">

            <div class="carousel-inner" style="background-color: #f3eeea;">

                <div class="item active">
                    <div class="mobiledivv" style="position: absolute; margin-left: 10%; margin-top: 5%;">

                        <h1><b>Subscription Plans</b></h1>



                        <p style="">
                            The Quantity Tak Off documents are paid, please buy suitable subscription plan and get
                            access
                            to the documents.
                        </p>



                        <br>


                    </div>

                    <div style="float:right; width: 50%">

                        <img class="mobileresp" src="images/banner-img.png" alt="Los Angeles" style="width:100%;">

                    </div>


                </div>

            </div>


        </div>
    </div>
    <div style="background-color: #ff7829">
        <div class="container hide">

            <div class="row" style=" padding-top: 2%;padding-bottom: 2%;">

                <div class="col-lg-6 col-md-6">

                    {{--<center>--}}
                    <div class="footer-left" style="margin-left: 10%;">
                        <h4 style="color: #fff;">
                            <b style="border-bottom: 3px solid #fff;padding-bottom: 1%;">Subscription Plans</b>
                        </h4>
                    </div>
                    {{--</center>--}}

                </div>

                <div class="col-lg-6 col-md-6 text-right">

                    <ul class="social-network social-circle" style="text-align: center;">

                        <li><a href="#" class="icoRss" style="border: 0px;"><i class="fa fa-facebook"></i></a></li>
                        <li><a href="#" class="icoRss" style="border: 0px;"><i class="fa fa-twitter"></i></a></li>
                        <li><a href="#" class="icoRss" style="border: 0px;"><i class="fa fa-instagram"></i></a></li>
                        <li><a href="#" class="icoRss" style="border: 0px;"><i class="fa fa-google-plus"></i></a></li>

                    </ul>

                </div>
            </div>
        </div>
    </div>
    <div class="plans-sub ameoba-right-pattern">


        <?php

            $plansRange = array(
                'Plan 1' => array(
                        'start' => 0,
                        'end' => 1000000,
                ),
                'Plan 2' => array(
                        'start' => 1000000,
                        'end' => 4000000,
                ),
                'Plan 3' => array(
                        'start' => 4000000,
                        'end' => 10000000,
                )
            );

            $message = '';
            $hide = 'hide';
            $strcheckplan = 0;
            if( !empty(Session::get('checkplan')) ){

                $message = "Please upgrade your subscription to get access.";
                $hide = '';
                $strcheckplan = Session::get('checkplan');

            }elseif($downloadLimitOut == true){

                $message = "You have reached your limit, please resubscribe to download the
                document.";
                $hide = '';

            }

        ?>

        <div class="container pricing-plans" id="mr">



            <div style=" margin-bottom: 30px;">

                <div id="MyElement" class="alert alert-danger alert-dismissible {{ $hide }}"
                     style="background-color:#dc3545; border-color: #dc3545;">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>

                    <h3 style="color:#fff;margin-top:0px;"><strong><i class="fa fa-exclamation-triangle" aria-hidden="true"></i></strong>
                        {{ $message }}   </h3>

                    {{--<p style="color:#fff">{{Session::get('alertone')}}</p>--}}

                </div>



            </div>

            <div class="plan-tabs">


                <div class="accordion" id="accordionExample1">

                <?php
                    $count = 0;
                    foreach($plansRange as $key => $range){
                        $count += 1;
                    ?>


                        <div class="card">

                            <div class="card-header" id="heading-{{$count}}">
                                <h3 class="mb-0 plans-acc-head">
                                    <button style="width: 100%;text-align: left" type="button" class="btn-link" data-toggle="collapse" data-target="#collapse-{{$count}}"><i class="fa fa-plus"></i>
                                        {{ $key }}

                                        @if($range['end'] < 5000000)
                                            <span style="float: right;">{{  "Bid Range $" . TBD::number_format_short($range['start'],1) ." to $". TBD::number_format_short($range['end'],1)   }}</span>
                                        @else
                                            <span style="float: right;">{{  "Bid Range $" . TBD::number_format_short(5000000,1) ." and Above"   }}</span>
                                        @endif

                                    </button>
                                </h3>
                            </div>

                            <div id="collapse-{{$count}}" class="collapse" aria-labelledby="heading-{{$count}}" data-parent="#accordionExample">

                                <div class="card-body">
                                    <div class="row">
                                        <!-- Free Tier -->

                                        @foreach($plans as $plan)

                                            @if($plan->max_rate > $range['start'] && $plan->max_rate == $range['end'])

                                            <?php if (  $strcheckplan <= $plan->max_rate  ){

                                            $color = '#f0542d';
                                            $buttonS = false;
                                            if ($selectedPlan) {
                                                if ($plan->braintree_plan == $selectedPlan['braintree_plan']) {
                                                    $color = '#a67200';
                                                    $buttonS = true;
                                                }
                                            }
                                            ?>

                                            <div class="col-md-3 col-sm-6">
                                                <div class="for-border">
                                                    <div class="{{  ($buttonS) ? 'for-gold-image' : 'for-image' }}">
                                                        <div class="doller"><span class="" style="color: transparent">$</span>
                                                            <?php

                                                            if($plan->downloads == 20){
                                                                $unlimited = $plan->cost/$plan->downloads;
                                                            }
                                                            $perProject = ($plan->downloads != -1)?$plan->cost/$plan->downloads:0;

                                                            ?>

                                                            @if($perProject == 0)
                                                                <h1 class="">Unlimited</h1>
                                                            @else
                                                                <h1 class="full-size"><span class="dollar-sign">$ </span>{{ number_format($perProject) }}</h1>
                                                            @endif

                                                            <h4> {{  ($plan->downloads != -1)?'/ Project':'' }} </h4></div>
                                                    </div>

                                                    <table class="plans-table">
                                                        <tr>
                                                            <td>
                                        <span style="color: {{ $color?$color:'#f0542d' }}; margin-right: 11px;" class="fa fa-download "
                                              aria-hidden="true"></span>Download Limit
                                                            </td>

                                                            <th>{{ ($plan->downloads != -1)?$plan->downloads:"Unlimited" }}</th>
                                                        </tr>

                                                        <tr>
                                                            <td>
                                                                <span style="color:{{ $color?$color:'#f0542d' }}; margin-right: 11px;" class="fa fa-clock-o "></span>Duration
                                                            </td>

                                                            <th>{{ $plan->name }}</th>
                                                        </tr>
                                                        <tr>
                                                            <td><span style="color: {{ $color?$color:'#f0542d' }}; margin-right: 5px;" class="fa fa-money "></span>
                                                                Plan Cost
                                                            </td>

                                                            <th>$  <?php echo number_format($plan->cost  ,0)   ?></th>
                                                        </tr>

                                                    </table>
                                                    <div class="my-button-firsrt-not">
                                                        {{--@if($buttonS)--}}

                                                            {{--<button  class="button home-explore-btn btn orange-button-bg">Subscribed--}}
                                                                {{--<i class="fa fa-arrow-right button-arrow" aria-hidden="true"></i>--}}
                                                            {{--</button>--}}

                                                        {{--@else--}}
                                                            {{--<a data-toggle="modal" data-target="#myModal" onclick="showpayment('{{$plan->id}}', '{{$plan->name}}')" >--}}
                                                                {{--<button class="button home-explore-btn btn orange-button-bg">Subscribe--}}
                                                                    {{--<i class="fa fa-arrow-right button-arrow" aria-hidden="true"></i>--}}
                                                                {{--</button>--}}
                                                            {{--</a>--}}

                                                            <a class="plans-modal-link" data-toggle="modal" data-target="#myModaltwo"
                                                               data-price="<?php echo number_format($plan->cost  ,0) ?>"
                                                               data-project="<?php  echo ($plan->downloads != -1)? '$ ' . number_format($perProject) . ' / Project': 'Unlimited' ?>"

                                                            >
                                                                <button class="button home-explore-btn btn orange-button-bg">Subscribe
                                                                    <i class="fa fa-arrow-right button-arrow" aria-hidden="true"></i>
                                                                </button>
                                                            </a>
                                                        {{--@endif--}}
                                                    </div>
                                                </div>
                                            </div>

                                            {{--<div class="col-lg-4 col-md-4 hide" style="margin-top:4%">
                                                <div class="card mb-5 mb-lg-0" style="border-top: 5px solid {{ $color }} !important;">
                                                    <div class="card-body" style=" padding: 12%;">

                                                        <h6 class="card-price text-center">
                                                            <span --}}{{--class="orange-color"--}}{{-- style="color: {{ $color }}">
                                                                $ {{ number_format($plan->cost, 2)}} / </span>
                                                            {{ $plan->name }}
                                                        </h6>
                                                        <hr>
                                                        <ul class="fa-ul" style="margin-left:0px;">
                                                            <li class="p-y-sm">
                                                                <span style="color: {{ $color }}" class="fa fa-download --}}{{--orange-color--}}{{--"
                                                                      aria-hidden="true"></span>
                                                                &nbsp;&nbsp;Downloads Limit
                                                                <strong style="float:right;">{{ $plan->downloads }}</strong>
                                                            </li>
                                                            <li class="p-y-sm">
                                                                <span style="color: {{ $color }}" class="fa fa-usd" aria-hidden="true"></span>
                                                                &nbsp;&nbsp; Price <strong style="float:right;">${{ number_format($plan->cost, 2)
                                                                }}</strong>
                                                            </li>
                                                            <li class="p-y-sm"><span style="color: {{ $color }}" class="fa fa-clock-o "></span>&nbsp;&nbsp;
                                                                Duration
                                                                <strong style="float:right;">{{ $plan->name }}</strong>
                                                            </li>
                                                            <li class="p-y-sm">
                                                                <span style="color: {{ $color }}" class="fa fa-money "></span>&nbsp;&nbsp;
                                                                Max. Project Value
                                                                <strong style="float:right;">{{ number_format($plan->max_rate,2) }}</strong>
                                                            </li>

                                                        </ul>
                                                        <br><br>
                                                        @if($buttonS)
                                                            <a style="color: white; background-color: {{ $color }}"
                                                               class="btn btn-3d btn-reveal text-uppercase">
                                                                <i class="fa fa-usd"></i> Subscribed
                                                            </a>
                                                        @else
                                                            <a class="btn btn-3d btn-reveal btn-orange text-uppercase" data-toggle="modal"
                                                               data-target="#myModal" onclick="showpayment('{{$plan->id}}', '{{$plan->name}}')">
                                                                <i class="fa fa-usd"></i> Subscribe
                                                            </a>
                                                        @endif
                                                    </div>

                                                </div>
                                            </div>--}}

                                            <?php } ?>

                                            @endif

                                        @endforeach

                                    </div>
                                </div>
                            </div>
                        </div>


                    <?php
                    }
                //exit;
                ?>
                </div>



            </div>


            <br><br><br>


        </div>

        <script>

            function showpayment(idd, pname) {

                document.getElementById('ppname').innerHTML = pname;
                document.getElementById('pid').value = idd;
            }


        </script>


        @auth
        <div id="myModal" class="modal fade" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content" style="margin-top:20%;">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title" id="ppname">Payment Method:</h4>
                    </div>
                    <div class="modal-body">
                        <h4>Payment Method:</h4>

                        <form method="post" action="{{ route('subscription.create') }}">
                            @csrf
                            <div id="dropin-container"></div>
                            <hr/>
                            <input type="hidden" id="pid" name="plan"/>
                            <center>
                                <button type="submit" class="btn btn-success btn-lg " id="payment-button">Pay</button>
                            </center>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>

            </div>
        </div>
        @endauth

        @guest
        <div id="myModaltwo" class="modal fade" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content" style="margin-top:20%;">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title" id="ppname">Payment Method:</h4>
                    </div>
                    <div class="modal-body">
                        Please <a href="/user_login">log in</a> to continue for subscription.
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>

            </div>
        </div>
        @endguest
    </div>

    <div class="extra-info m-y-xl hide">
        <div class="container">
            <div class="row">
                <center><h2>Here's What You Get:</h2></center>
                <br>

                <div class="col-lg-4 col-md-4 text-center">
                    <img src="images/cloud-server.png" style="width: 35%;">

                    <h3><b>What is Lorem Ipsum?</b></h3>


                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been
                        the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley
                        of type and scrambled it to make a type specimen book. It has survived not only five centuries,
                        but also the leap into electronic typesetting, remaining essentially unchanged.</p>

                </div>
                <div class="col-lg-4 col-md-4 text-center">
                    <img src="images/cloud-server.png" style="width: 35%;">

                    <h3><b>What is Lorem Ipsum?</b></h3>


                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been
                        the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley
                        of type and scrambled it to make a type specimen book. It has survived not only five centuries,
                        but also the leap into electronic typesetting, remaining essentially unchanged.</p>

                </div>
                <div class="col-lg-4 col-md-4 text-center">
                    <img src="images/cloud-server.png" style="width: 35%;">

                    <h3><b>What is Lorem Ipsum?</b></h3>


                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been
                        the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley
                        of type and scrambled it to make a type specimen book. It has survived not only five centuries,
                        but also the leap into electronic typesetting, remaining essentially unchanged.</p>

                </div>
                <div class="col-lg-4 col-md-4 text-center">
                    <img src="images/cloud-server.png" style="width: 35%;">

                    <h3><b>What is Lorem Ipsum?</b></h3>


                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been
                        the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley
                        of type and scrambled it to make a type specimen book. It has survived not only five centuries,
                        but also the leap into electronic typesetting, remaining essentially unchanged.</p>

                </div>
                <div class="col-lg-4 col-md-4 text-center">
                    <img src="images/cloud-server.png" style="width: 35%;">

                    <h3><b>What is Lorem Ipsum?</b></h3>


                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been
                        the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley
                        of type and scrambled it to make a type specimen book. It has survived not only five centuries,
                        but also the leap into electronic typesetting, remaining essentially unchanged.</p>

                </div>
                <div class="col-lg-4 col-md-4 text-center">
                    <img src="images/cloud-server.png" style="width: 35%;">

                    <h3><b>What is Lorem Ipsum?</b></h3>


                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been
                        the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley
                        of type and scrambled it to make a type specimen book. It has survived not only five centuries,
                        but also the leap into electronic typesetting, remaining essentially unchanged.</p>

                </div>

            </div>
        </div>
    </div>


@auth
<div id="myModaltwo" class="modal myModalPlan fade singlePrice" role="dialog">
    <div class="modal-dialog" style="min-width: 70%;">

        <!-- Modal content-->
        <div class="modal-content" style="margin-top:5%;">
            {{--<div class="modal-header">--}}
            <button type="button" class="close" data-dismiss="modal">&times;</button>

            {{--</div>--}}

            <div class="row">

                <div  class="col-md-6 padding-r-0">
                    <div class="border-gray">
                        <div style="visibility: hidden;" class="after-searching"><p>Recommended</p></div>
                        <div class="book-image"><img src="{{asset('images/decide-graphic.png')}}" style="width: 49%;"></div>
                        <div style="visibility: hidden;" class="search-book"><h4>Check Out Our <span class="head-bold">Convenient </span>Subscription Plans</h4></div>
                        <div style="visibility: hidden;" class="button-searc">
                            <a href="/plans">
                                <button class="button button1 btn golden-button-bg">Subscription Plans
                                    <i class="fa fa-arrow-right button-arrow" aria-hidden="true"></i>
                                </button>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 padding-l-0">
                    <div class="border-blue">


                        <h2 class="pro-title">Bill Avenue Road Constructions</h2>
                        <p style="padding:0 60px; font-weight: bold;">File will be sent to you via your provided email once the payment has been made over PayPal. Please use the link below to pay</p>
                        <h4 style="visibility: hidden;">TakeOff: <span class="file-name">Expert 459M</span></h4>
                        <div class="btn-cont">
                            <a href="https://paypal.me/tbdata?locale.x=en_US" target="_blank">
                                <button class="button btn buy-btn button-orange">
                                    <span style="font-size: 22px;font-weight: 600;" ><i class="fa fa-paypal"></i> Continue to Paypal</span>
                                    <i class="fa fa-arrow-right button-arrow" aria-hidden="true"></i>
                                    <p style="font-size: 18px;">Buy for $<span class="price"></span></p>
                                </button>
                            </a>
                        </div>


                    </div>
                </div>

            </div>

        </div>

    </div>
</div>

@endauth

    <script src="https://js.braintreegateway.com/js/braintree-2.32.1.min.js"></script>
    <script>
        jQuery.ajax({
                    url: "{{ route('token') }}",
                })
                .done(function (res) {
                    braintree.setup(res.data.token, 'dropin', {
                        container: 'dropin-container',
                        onReady: function () {
                            jQuery('#payment-button').removeAttr('disabled');
                        }
                    });
                });
    </script>
@endsection
@extends('layout')

@section('content')

    <style>
        .pac-container {

            z-index: 99999999999;

        }
    </style>


    @php
    $openSolicitation = $solcount[1];
    $awardedSolicitation = $solcount[0];
    $closedSolicitation = $solcount[2];

    @endphp

    {{--<div class="container-fluid" id="cid" style="padding-left:0px; padding-right:0px;">--}}

    @auth
    <div class="save-search m-b-sm">
        <div class="container">
            <div class="row m-y-lg m-y-xs">
                <div class="col-lg-3"></div>
                <div class="col-lg-9">
                    <div class="row">
                        <div class="col-lg-9 col-sm-9">
                            <select class="form-control" id="sfield" onchange="search_field()">
                                <option disabled selected>--Select Search--</option>
                                @foreach($savesearch as $ss)
                                    <option value="{{$ss->keyword}}|{{ str_replace('--', ',', $ss->location)}}|{{$ss->open}}|{{$ss->awarded}}|{{$ss->closed}}|{{$ss->pricefrom}}|{{$ss->priceto}}|{{$ss->date_from}}|{{$ss->date_to}}">{{$ss->save_as}}</option>
                                @endforeach

                            </select>
                        </div>
                        <div class="col-lg-3 col-sm-3 secondElement">
                            <button class="btn btn-3d btn-reveal btn-orange" data-toggle="modal" data-target="#myModal">
                                <i class="fa fa-save"></i>
                                <span>New Search</span>
                            </button>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
    @endauth


    <div class="container search-page">

        @if(isset($alertone))
            <div class="alert alert-success" style="background-color:#28a745">
                {{--{{ $alertone }}--}}
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>

                <h3 style="color:#fff"><strong><i class="fa fa-check" aria-hidden="true"></i></strong> Alert!</h3>

                <p style="color:#fff">{{$alertone}}</p>

            </div>
        @endif

        <div id="MyElement" class="alert alert-success alert-dismissible hide" style="background-color:#28a745">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>

            <h3 style="color:#fff"><strong><i class="fa fa-check" aria-hidden="true"></i></strong> Alert!</h3>

            <p style="color:#fff">{{Session::get('alertone')}}</p>
        </div>

        <div class="row">
            <div class="col-lg-3 col-md-3 col-sm-12">

                <div class="sidebar1 projectSearchFrom">
                    <div class="left-navigation">
                        <h4>Advance Search</h4>

                        <form action="{{ route('s_filter') }}" method="post" role="form" enctype="multipart/form-data">
                            @csrf
                            <div class="row">

                                <div class="col-lg-12" style="margin-top: 2%;">
                                    <input type="text" id="title" class="form-control" name="title" value="{{ (isset($searched))?$searched->title:''  }}"
                                           placeholder="Keyword.." onkeyup='saveValue(this);'>
                                </div>
                                <?php //echo $searched->location; ?>
                                <div class="col-lg-12" style="margin-top: 2%;">
                                    <input id="autocomplete" onFocus="geolocate()" placeholder="Location.." type="text"
                                           class="form-control" value="{{ (isset($searched))?$searched->location:''  }}" name="location" onkeyup='saveValue(this);'>
                                </div>
                                <!--<div class="col-lg-4">-->
                                <!--    <input type="text"  class="form-control" placeholder="Geo Area..">-->
                                <!--</div>-->

                            </div>

                            <div class="row" style="margin-top: 4%;">

                                <div class="col-lg-12">
                                    <h4>Status</h4>
                                </div>

                                <div class="col-lg-12">
                                    <label class="container-checkbox">
                                        <?php
                                         $checked = '';
                                         if(isset($searched)){
                                             $checked = $searched->cbox;
                                         }

                                        ?>
                                        Open Solicitation <span class="projectsCount"> ({{$openSolicitation}})</span>
                                        <input type="radio" id="cbox1" name="cbox" class="checkboxit"
                                               value="Open Solicitation" {{ ( $checked == 'Open Solicitation' )? 'checked':''  }}>
                                        <span class="checkmark"></span>
                                    </label>
                                </div>

                                <div class="col-lg-12">
                                    <label class="container-checkbox">
                                        Bid Results <span class="projectsCount"> ({{$awardedSolicitation}})</span>
                                        <input type="radio" id="cbox2" name="cbox" class="checkboxit"
                                               value="Awarded Solicitation" {{ ( $checked == 'Awarded Solicitation' )? 'checked':''  }} >
                                        <span class="checkmark"></span>
                                    </label>

                                </div>

                                <div class="col-lg-12">
                                    <label class="container-checkbox">
                                        Awarded <span class="projectsCount"> ({{$closedSolicitation}})</span>
                                        <input type="radio" id="cbox3" name="cbox" class="checkboxit"
                                               value="Closed Solicitation" {{ ( $checked == 'Closed Solicitation' )? 'checked':''  }}>
                                        <span class="checkmark"></span>
                                    </label>

                                </div>


                            </div>

                            <div class="row" style="margin-top: 4%;">
                                <div class="col-lg-12">
                                    <h4> Bid Range</h4>
                                </div>



                                <div class="col-lg-12">
                                    From
                                    <input type="number" id="bfrom" name="bid_from" class="form-control" value="{{ (isset($searched))?$searched->bid_from:''  }}"
                                           placeholder="$" onkeyup='saveValue(this);'>
                                </div>

                                {{--<div class="col-lg-5">--}}
                                    {{--<input type="number" id="bfrom" name="bid_from" class="form-control" value="{{ (isset($searched))?$searched->bid_from:''  }}" placeholder="$" onkeyup='saveValue(this);'>--}}
                                {{--</div>--}}

                                {{--<div class="col-lg-1 no-padding">--}}
                                    {{--<h4 class="to-search-field">to</h4>--}}
                                {{--</div>--}}


   {{--<div class="col-lg-1 no-padding">--}}
                                    {{--<h4>to</h4>--}}
                                {{--</div>--}}

                                <div class="col-lg-12">
                                    To
                                    <input type="number" id="bto" name="bid_to" class="form-control" placeholder="$"
                                           onkeyup='saveValue(this);' value="{{ (isset($searched))?$searched->bid_to:''  }}">
                                </div>
                            </div>

                            <div class="row" style="margin-top: 4%;">
                                <div class="col-lg-12">
                                    <h4> Date Range:</h4>
                                </div>


                                <div class="col-lg-12">
                                    From
                                    <input type="date" id="dfrom" name="date_from" class="form-control"
                                           onkeyup='saveValue(this);' value="{{ (isset($searched) && strlen($searched->date_from)>0)?date('Y-m-d', strtotime($searched->date_from)):''  }}" >
                                </div>

                                {{--<div class="col-lg-1">
                                    <h4>to</h4>
                                </div>--}}

                                <div class="col-lg-12">
                                    To
                                    <input data-value="" type="date" id="dto" name="date_to" class="form-control"
                                           onkeyup='saveValue(this);' value="{{ (isset($searched) && strlen($searched->date_to)>0)?date('Y-m-d', strtotime($searched->date_to)):''  }}">
                                </div>
                            </div>

                            <div class="row" style="margin-top: 10%;  ">
                                <div class="col-lg-6">
                                    <button type="submit" class="btn btn-3d btn-reveal btn-orange btn-small">
                                        <i class="fa fa-rocket"></i>
                                        <span>
                                            Find Projects
                                        </span>
                                    </button>
                                </div>

                                <!--<div class="col-lg-6">-->
                                <!--    <span class="btn btn-3d btn-reveal btn-default btn-small" onclick="clearfeild()">-->
                                <!--        <i class="fa fa-times"></i>-->
                                <!--        <span>-->
                                <!--            Clear Feilds-->
                                <!--        </span>-->
                                <!--    </span>-->
                                <!--</div>-->
                            </div>
                        </form>

                    </div>
                </div>

            </div>
            <script type="text/javascript">
                var df = '';
                var dt = '';
                document.getElementById("title").value = getSavedValue("title");
                document.getElementById("autocomplete").value = getSavedValue("autocomplete");
                document.getElementById("bfrom").value = getSavedValue("bfrom");
                document.getElementById("bto").value = getSavedValue("bto");
                if(getSavedValue("dfrom") == ""){
                     df = getSavedValue("dfrom")
                }else{
                    <?php
                    $dateform = '';
                    if(isset($searched)){
                        if(!empty($searched->date_from)){
                            $dateform = date('Y-m-d', strtotime($searched->date_from));
                        }
                    }
                    ?>
                     df = <?php (!empty($dateform))? $dateform:"" ?>
                }
                document.getElementById("dfrom").value =  df;
                if(getSavedValue("dto") == ""){
                    dt = getSavedValue("dto")
                }else{
                    dt = <?php (isset($searched))? date('Y-m-d', strtotime($searched->date_from)):"" ?>
                }
                console.log(df);
                document.getElementById("dto").value = dt;

                function saveValue(e) {
                    var id = e.id;  // get the sender's id to save it .
                    var val = e.value; // get the value.
                    localStorage.setItem(id, val);// Every time user writing something, the localStorage's value will override .
                }


                function getSavedValue(v) {
                    if (!localStorage.getItem(v)) {
                        return "";
                    }
                    return localStorage.getItem(v);
                }

                function clearfeild() {

                    document.getElementById("title").value = "";
                    document.getElementById("autocomplete").value = "";
                    document.getElementById("bfrom").value = "";
                    document.getElementById("bto").value = "";
                    document.getElementById("dfrom").value = "";
                    document.getElementById("dto").value = "";

                }
            </script>
            <div class="col-lg-9 col-md-9 col-sm-12 conabc">
                <table class="table table-striped custab projects-list">
                    <?php
                    if(!empty($projects)){ ?>
                    @foreach($projects as $pro)
                        <?php $checkempty = $pro->title; ?>
                        <tr class="single-item">
                            <td>
                                <div class="row">
                                    <div class="col-md-9 col-sm-6">
                                        <h4 class="project-title">
                                            <a href="{{route('pdetail', $pro->id)}}">{{$pro->title}}</a>
                                        </h4>

                                        <div class="project-description m-y-lg">
                                            @if(isset($pro->description) && strlen($pro->description) > 0)
                                                {{ (strlen($pro->description) > 50) ? substr($pro->description,0, 150) . '...' : $pro->description}}
                                            @endif
                                        </div>

                                        <div class="location">
                                            <label><i class="fa fa-map-marker"></i> Location</label>
                                            <span class="text">{{$pro->location}}</span>
                                        </div>


                                    </div>
                                    <div class="col-md-3 col-sm-6 m-t-sm secondElement">
                                        @php
                                        $date = new DateTime($pro->bid_date);
                                        $result = $date->format('M d, Y');
                                        @endphp

                                        <div class="bid-date">
                                            <label>
                                                <i class="fa fa-calendar"></i>
                                                Bid Date
                                            </label>
                                            <span class="text">{{$result}}</span>
                                        </div>


                                        <div class="bid-amount m-y-md">
                                            <label>
                                                <i class="fa fa-money"></i>
                                                Bid Amount
                                            </label>



                                                <span class="text">
                                                    <span class="currency">$</span>
                                                    <?php  echo $checkamount = number_format_short($pro->bid_amount, 1);  ?>
                                                </span>
                                        </div>

                                        <div class="project-details">
                                            <a href="{{route('pdetail', $pro->id)}}"
                                               class="btn btn-3d btn-reveal btn-orange">
                                                <i class="fa fa-line-chart white-text"></i>
                                                    <span>
                                                        Explore More
                                                    </span>
                                            </a>
                                        </div>

                                    </div>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                    <?php }

                    if (empty($checkempty)) {
                        echo "<br><h4>No Result Found!</h3>";
                    }

                    function number_format_short($n, $precision)
                    {
                        if ($n < 900) {
                            // 0 - 900
                            $n_format = number_format($n, $precision);
                            $suffix = '';
                        } else if ($n < 900000) {
                            // 0.9k-850k
                            $n_format = number_format($n / 1000, $precision);
                            $suffix = 'K';
                        } else if ($n < 900000000) {
                            // 0.9m-850m
                            $n_format = number_format($n / 1000000, $precision);
                            $suffix = 'M';
                        } else if ($n < 900000000000) {
                            // 0.9b-850b
                            $n_format = number_format($n / 1000000000, $precision);
                            $suffix = 'B';
                        } else {
                            // 0.9t+
                            $n_format = number_format($n / 1000000000000, $precision);
                            $suffix = 'T';
                        }
                        // Remove unecessary zeroes after decimal. "1.0" -> "1"; "1.00" -> "1"
                        // Intentionally does not affect partials, eg "1.50" -> "1.50"
                        if ($precision > 0) {
                            $dotzero = '.' . str_repeat('0', $precision);
                            $n_format = str_replace($dotzero, '', $n_format);
                        }
                        return $n_format . $suffix;
                    }

                    ?>
                </table>
            </div>
        </div>

    </div>



    <div id="myModal" class="modal fade" role="dialog">

        <div class="modal-dialog">

            <form method="post" action="{{ route('s_save_filter') }}">

            <!-- Modal content-->
            <div class="modal-content" style="margin-top:20%;margin-bottom: 20%">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title" id="ppname">Save Search</h4>
                </div>

                <div class="modal-body">

                    <div class="row">
                        @csrf

                        <div class="col-lg-12" style=" margin-top: 5%;">
                            <label>Save Search as</label>
                            <input type="text" class="form-control" name="save_search" placeholder="Save Search as"
                                   required>
                        </div>

                        <div class="col-lg-6" style=" margin-top: 5%;">
                            <label>Keyword</label>
                            <input type="text" class="form-control" name="title"
                                   placeholder="Keyword..">
                        </div>

                        <div class="col-lg-6" style=" margin-top: 5%;">
                            <label>Location</label>
                            <input id="autocomplete1" onFocus="geolocate()" placeholder="Location.." type="text"
                                   class="form-control" name="location">
                        </div>


                        <div class="col-lg-4" style=" margin-top: 5%;">

                            <input type="radio" name="cbox" value="1"> Open Solicitation
                        </div>

                        <div class="col-lg-4" style=" margin-top: 5%;">
                            <input type="radio" name="cbox" value="2"> Bid Results
                        </div>

                        <div class="col-lg-4" style=" margin-top: 5%;">
                            <input type="radio" name="cbox" value="3"> Awarded
                        </div>


                        <div class="col-lg-6" style=" margin-top: 5%;">
                            <label>Price Range From</label>
                            <input type="number" class="form-control" name="bid_from" placeholder="$">
                        </div>

                        <div class="col-lg-6" style=" margin-top: 5%;">
                            <label>Price Range to</label>
                            <input type="number" class="form-control" name="bid_to" placeholder="$">
                        </div>


                        <div class="col-lg-6" style=" margin-top: 5%;">
                            <label>Date Range From</label>
                            <input type="date" class="form-control" name="date_from">
                        </div>

                        <div class="col-lg-6" style=" margin-top: 5%;">
                            <label>Date Range to</label>
                            <input type="date" class="form-control" name="date_to">
                        </div>

                    </div>

                </div>
                <div class="modal-footer">
                    <button class="btn btn-3d btn-reveal btn-orange">
                        <i class="fa fa-save"></i>
                        <span> Save Search </span>
                    </button>

                    <button type="button" class="btn btn-3d btn-reveal btn-danger" data-dismiss="modal">
                        <i class="fa fa-close"></i>
                        <span>Close</span>
                    </button>
                </div>
            </div>
            </form>
        </div>
    </div>

    {{--</div>--}}

    <script type="">

        var existtwo = '{{Session::has('alertone')}}';

        if (existtwo) {

            document.getElementById('MyElement').classList.add('show');
            document.getElementById('MyElement').classList.remove('hide');

        }

        function search_field() {

            var keywords = document.getElementById('sfield').value;

            var res = keywords.split("|");
            document.getElementById('title').value = res[0];

            var totalWords = res[1];
            //var firstWord = totalWords.replace(/ .*/,'');

            document.getElementById('autocomplete').value = totalWords;

            if (res[2] != 0 && res[2] == 1) {
                document.getElementById("cbox1").checked = true;
            }

            if (res[3] != 0 && res[3] == 1) {
                document.getElementById("cbox2").checked = true;
            }

            if (res[4] != 0 && res[4] == 1) {
                document.getElementById("cbox3").checked = true;
            }


            document.getElementById('bfrom').value = res[5];

            document.getElementById('bto').value = res[6];

            document.getElementById('dfrom').value = res[7];

            document.getElementById('dto').value = res[8];
        }

        (function ($) {
            var pagify = {
                items: {},
                container: null,
                totalPages: 1,
                perPage: 3,
                currentPage: 0,
                createNavigation: function () {
                    this.totalPages = Math.ceil(this.items.length / this.perPage);

                    $('.pagination', this.container.parent()).remove();
                    var pagination = $('<div class="pagination clearfix"></div>').append('<a class="nav prev disabled" data-next="false"><</a>');

                    for (var i = 0; i < this.totalPages; i++) {
                        var pageElClass = "page";
                        if (!i)
                            pageElClass = "page current";
                        var pageEl = '<a class="' + pageElClass + '" data-page="' + (
                                i + 1) + '">' + (
                                i + 1) + "</a>";
                        pagination.append(pageEl);
                    }
                    pagination.append('<a class="nav next" data-next="true">></a>');

                    this.container.after(pagination);

                    var that = this;
                    $("body").off("click", ".nav");
                    this.navigator = $("body").on("click", ".nav", function () {
                        var el = $(this);
                        that.navigate(el.data("next"));
                    });

                    $("body").off("click", ".page");
                    this.pageNavigator = $("body").on("click", ".page", function () {
                        var el = $(this);
                        that.goToPage(el.data("page"));
                    });
                },
                navigate: function (next) {
                    // default perPage to 5
                    if (isNaN(next) || next === undefined) {
                        next = true;
                    }
                    $(".pagination .nav").removeClass("disabled");
                    if (next) {
                        this.currentPage++;
                        if (this.currentPage > (this.totalPages - 1))
                            this.currentPage = (this.totalPages - 1);
                        if (this.currentPage == (this.totalPages - 1))
                            $(".pagination .nav.next").addClass("disabled");
                    }
                    else {
                        this.currentPage--;
                        if (this.currentPage < 0)
                            this.currentPage = 0;
                        if (this.currentPage == 0)
                            $(".pagination .nav.prev").addClass("disabled");
                    }

                    this.showItems();
                },
                updateNavigation: function () {

                    var pages = $(".pagination .page");
                    pages.removeClass("current");
                    $('.pagination .page[data-page="' + (
                            this.currentPage + 1) + '"]').addClass("current");
                },
                goToPage: function (page) {

                    this.currentPage = page - 1;

                    $(".pagination .nav").removeClass("disabled");
                    if (this.currentPage == (this.totalPages - 1))
                        $(".pagination .nav.next").addClass("disabled");

                    if (this.currentPage == 0)
                        $(".pagination .nav.prev").addClass("disabled");
                    this.showItems();
                },
                showItems: function () {
                    this.items.hide();
                    var base = this.perPage * this.currentPage;
                    this.items.slice(base, base + this.perPage).show();

                    this.updateNavigation();
                },
                init: function (container, items, perPage) {
                    this.container = container;
                    this.currentPage = 0;
                    this.totalPages = 1;
                    this.perPage = perPage;
                    this.items = items;
                    this.createNavigation();
                    this.showItems();
                }
            };

            // stuff it all into a jQuery method!
            $.fn.pagify = function (perPage, itemSelector) {
                var el = $(this);
                var items = $(itemSelector, el);

                // default perPage to 5
                if (isNaN(perPage) || perPage === undefined) {
                    perPage = 3;
                }

                // don't fire if fewer items than perPage
                if (items.length <= perPage) {
                    return true;
                }

                pagify.init(el, items, perPage);
            };
        })(jQuery);

        $(".conabc").pagify(10, ".single-item");

        var placeSearch, autocomplete;

        function initAutocomplete() {
            autocomplete = new google.maps.places.Autocomplete(
                    document.getElementById('autocomplete'), {types: ['geocode']});

            autocomplete = new google.maps.places.Autocomplete(
                    document.getElementById('autocomplete1'), {types: ['geocode']});
//   autocomplete.setFields('address_components');
//   autocomplete.addListener('place_changed', fillInAddress);
        }

        function geolocate() {
            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(function (position) {
                    var geolocation = {
                        lat: position.coords.latitude,
                        lng: position.coords.longitude
                    };
                    var circle = new google.maps.Circle(
                            {center: geolocation, radius: position.coords.accuracy});
                    autocomplete.setBounds(circle.getBounds());
                });
            }
        }

    </script>

    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD5KPKoO7ZP-grfU1aOx2GD1ra1pQMBdAQ&libraries=places&callback=initAutocomplete"
            async defer></script>

@endsection
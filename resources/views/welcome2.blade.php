@extends('layout')

@section('content')

    <div class="container">
        <div class="row bottom-top-padding">
            <div class="col-md-6">

                <div class="my-image-firsrt-landing">
                    <img class="img-responsive" src="images/Banner-Document-graphic.png">
                </div>
            </div>

            <div class="col-md-6">
                <h2 class="margin"> Fastest And Easiet <span class="my-size">Way to Bid</span></h2>

                <p><i class="fa fa-check" aria-hidden="true"></i>
                    Project Divisions Takeoff</p>

                <p><i class="fa fa-check" aria-hidden="true"></i>
                    Full Prtoject Takeoff</p>

                <p><i class="fa fa-check" aria-hidden="true"></i>
                    Takeoff on Demand</p>

                <p><i class="fa fa-check" aria-hidden="true"></i>
                    Takeoff subcription Plans</p>

                <p><i class="fa fa-check" aria-hidden="true"></i>
                    Prtoject Leads plans and Speces</p>

                <div class="my-button-firsrt">
                    <button class="button button2 btn">Explore More&nbsp&nbsp&nbsp<i class="fa fa-long-arrow-right"
                                                                                     aria-hidden="true"></i>
                    </button>
                </div>
            </div>
        </div>
    </div>
    <div class="for-color">
        <div class="container">
            <div class="row">
                <div class="line-side"><img src="images/stamp.png" style=" with:100%; height: 106px;"></div>
                <br>

                <div class="line-down"><h2>What Do We </h2><span class="green-border">Offer</span></div>
                <br>

                <div class="col-md-12">

                    <div class="col-xs-4">
                        <div class="line-content"><img src="images/clipboard.png"
                                                       style=" with:100%;margin-bottom: 3px; /*height: 106px;*/">

                            <div class="break"><p>Project Leads </p></div>
                        </div>
                    </div>


                    <div class="col-xs-4">
                        <div class="line-content"><img src="images/home.png"
                                                       style=" with:100%; margin-bottom: 3px;/*height: 106px;*/">

                            <div class="break"><p>Project Leads </p></div>
                        </div>
                    </div>
                    <div class="col-xs-4">
                        <div class="line-content"><img src="images/result.png"
                                                       style=" with:100%; margin-bottom: 3px;/*height: 106px;*/">

                            <div class="break"><p>Project Leads </p></div>
                        </div>
                    </div>

                </div>
                <br>

                <div class="col-md-12">

                    <div class="col-xs-4">
                        <div class="line-content"><img src="images/analysis (1).png"
                                                       style=" with:100%; margin-bottom: 3px;/*height: 106px;*/">

                            <div class="break"><p>Project Leads </p></div>
                        </div>
                    </div>
                    <div class="col-xs-4">
                        <div class="line-content"><img src="images/win.png"
                                                       style=" with:100%; margin-bottom: 3px;/*height: 106px;*/">

                            <div class="break"><p>Project Leads </p></div>
                        </div>
                    </div>
                    <div class="col-xs-4">
                        <div class="line-content"><img src="images/bar-chart.png"
                                                       style=" with:100%; margin-bottom: 3px;/*height: 106px;*/">

                            <div class="break"><p>Project Leads </p></div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <br>

    <div class="container-fluid">
        <div class="tw0-box">
            <div class="line-down-two"><span class="green-border"><h2>Get Quantity </h2> Takeoffs</span></div>
            <div class="container">
            <div class="row">

                <div class="col-md-6">
                    <div class="border-first">
                        <div class="searc-book"><img src="images/search-graphic.png"></div>
                        <div class="searching"><p>Search Project From Directory</p></div>
                        <div class="button-search">
                            <button class="button button2 btn">Search&nbsp&nbsp&nbsp<i class="fa fa-long-arrow-right"
                                                                                       aria-hidden="true"></i>
                            </button>
                        </div>

                    </div>
                </div>


                <div class="col-md-6 ">
                    <div class="border-gray">
                        <div class="after-searching"><p>Recommended</p></div>
                        <div class="book-image"><img src="images/decide-graphic.png" style="width: 49%;"></div>
                        <div class="search-book"><p>Check Out Our <span>Convenient </span>Subcription Plans</p></div>
                        <div class="button-searc">
                            <button class="button button1 btn">Subcription Plans &nbsp&nbsp&nbsp<i
                                        class="fa fa-long-arrow-right" aria-hidden="true"></i>
                            </button>
                        </div>


                    </div>
                </div>

            </div>
                </div>
        </div>
    </div>
    <br>
    <div class="container">

        <div class="three-box">
            <div class="line-down-three"><span class="green-border"><h2>List Yourself</h2> With US</span></div>
            <div class="row">

                <div class="col-md-4">
                    <div class="blue-background">
                    <div class=""><h2>General<br> Contractor</h2></div>
                    <div class=""><p>Listing</p></div>
                    <div class="button-cool">
                        <button class="button button3 btn">Enlist&nbsp&nbsp&nbsp<i class="fa fa-long-arrow-right"
                                                                                   aria-hidden="true"></i>
                        </button>

                </div>
                </div>
                </div>

                <div class="col-md-4">
                    <div class="blue-one">
                    <div class=""><h2>SubContractor</h2></div>
                    <div class=""><p>Listing</p></div>
                    <div class="button-mid">
                        <button class="button button3 btn">Enlist&nbsp&nbsp&nbsp<i class="fa fa-long-arrow-right"
                                                                                   aria-hidden="true"></i>
                        </button>

                    </div>
                </div>
                    </div>

                <div class="col-md-4">
                    <div class="blue">
                    <div class=""><h2>Trade<br> Contractor</h2></div>
                    <div class=""><p>Listing</p></div>
                    <div class="button-cool">
                        <button class="button button3 btn">Enlist&nbsp&nbsp&nbsp<i class="fa fa-long-arrow-right"
                                                                                   aria-hidden="true"></i>
                        </button>

                    </div>
                </div>
                    </div>


            </div>

        </div>
        <br>




</div>


@endsection



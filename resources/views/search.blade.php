@extends('layout')

@section('content')

    <style>
        .pac-container {

            z-index: 99999999999;

        }
    </style>



    @php
    $openSolicitation = $solcount[1];
    $awardedSolicitation = $solcount[0];
    $closedSolicitation = $solcount[2];



    @endphp

    {{--<div class="container-fluid" id="cid" style="padding-left:0px; padding-right:0px;">--}}

    @auth
    <div class="save-search">
        <div class="container">
            <div class="row m-y-lg m-y-xs">
                <div class="col-lg-2"></div>
                <div class="col-lg-10">
                    <div class="row">
                        <div class="col-lg-8 col-sm-8">
                            <select style="width: 100% !important;" class="form-control header-search-input search-select form-control" id="sfield" onchange="search_field()">
                                <option disabled selected>--Saved Searches--</option>
                                @foreach($savesearch as $ss)
                                    <option value="{{$ss->keyword}}|{{ str_replace('--', ',', $ss->location)}}|{{$ss->open}}|{{$ss->awarded}}|{{$ss->closed}}|{{$ss->pricefrom}}|{{$ss->priceto}}|{{$ss->date_from}}|{{$ss->date_to}}">{{$ss->save_as}}</option>
                                @endforeach

                            </select>
                        </div>
                        <div class="col-lg-4 col-sm-4 saved-new-search-btn secondElement">
                            <button  class="btn new-search  btn-orange orange-button-bg " data-toggle="modal" data-target="#searchModal">
                                <span>New Search</span>
                            </button>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
    @endauth

            <!-----NEW Search Filter Blue Color BG--->
    <div class="blue-bg-search-filter search-filter-c">
        <div class="container change-cont">
            <form class="searchForm" action="{{ route('s_filter') }}" data-ajax="{{ route('ajax_search') }}" method="post" role="form" style="padding-bottom: 15px;"
                  enctype="multipart/form-data">
                @csrf
            <div class="row">

                <div class="col-md-9 no-padding">

                    <div class="col-md-4">

                        <div class="heading-white"><h4 class="bold"><i style="padding-right: 10px;" class="glyphicon glyphicon-search"></i>Keyword</h4></div>
                        <div class="heading-white">
                            <div class="input-group" >
                                <div class="input-group-btn ">
                                    <input style="border-radius: 5px;" type="text" id="title"  class="form-control ui-autocomplete-input main-search" placeholder="Windows, Roof etc." onkeyup='saveValue(this);' name="p_title" value="{{ (isset($searched))?$searched->p_title:''  }}"  autocomplete="off">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="heading-white"><h4 class="bold"><i style="padding-right: 10px;" class="fa fa-map-marker"></i>Location</h4></div>

                        <div class="heading-white">

                            <div class="input-group loc-wid">

                                <div class="input-group-btn">
                                    <input style="border-radius: 5px;" type="text" id="autocomplete" value="{{ (isset($searched))?$searched->location:''  }}"
                                           onFocus="geolocate()" class="form-control ui-autocomplete-input main-search" placeholder="New York" name="location" autocomplete="off">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="input-group bg-sel"  style="width: 100%;">
                            <div><h4 class="bold"><i style="padding-right: 10px;" class="fa fa-sort-amount-desc"></i>Sort</h4></div>
                            <?php
                            $selected = '';
                            if (isset($searched)) {
                                $selected = $searched->sorting;
                            }
                            ?>

                            <select name="sorting" class="header-search-input search-select form-control">
                                <option value="bid_date-DESC" {{ ($selected == 'bid_date-DESC')?"selected":"" }} >Bid Date (Newest to Oldest)</option>
                                <option value="bid_date-ASC" {{ ($selected == 'bid_date-ASC')?"selected":"" }} >Bid Date (Oldest to Newest)</option>
                                <option value="bid_amount-ASC" {{ ($selected == 'bid_amount-ASC')?"selected":"" }} >Bid Amount (Low to High)</option>
                                <option value="bid_amount-DESC" {{ ($selected == 'bid_amount-DESC')?"selected":"" }} >Bid Amount (High to Low)</option>
                            </select>
                            <input type="text" style="border-right: none;border-radius: 5px 0 0 5px !important;" class="header-search-input form-control hide" placeholder="All" name="title" id="tags">

                            <div class="input-group-btn hide ">
                                <button class="header-search-input search-btn" style="border-left: none;border-radius: 0px 5px 5px 0px !important;" type="submit">
                                    <i class="fa fa-chevron-down" style="font-size:24px; "></i>
                                </button>
                            </div>
                        </div>
                    </div>

                </div>

                <div class="col-md-3 no-padding search-tablet-resolution-fix">
                    <div class="col-md-6 col-sm-6 col-xs-6">
                        <div><h4 class="bold">&nbsp;</h4></div>
                        <div class="button-search-second">
                            <button id="submit_filters" type="submit" style="" class="button button2 btn orange-button-bg">Search
                                <i class="fa fa-arrow-right button-arrow" aria-hidden="true"></i>
                            </button>
                        </div>
                    </div>

                    <div class="col-md-6 col-sm-6 col-xs-6 search-drop-toggle">
                        <div><h4 class="bold">&nbsp;</h4></div>
                        <div class="button-search-second-space">
                            <div class="input-group-btn ">
                                <button type="button" class="search-toggle collapsed" data-toggle="collapse" data-target="#demo">
                                    <i class="fa fa-filter" style="font-size:24px;padding-right: 20px;"></i>&nbsp&nbsp&nbsp&nbsp&nbsp<i
                                            class="drop-col-icon fa fa-chevron-down" style="font-size:24px;"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>




            </div>

            <?php
                  $collapsed = '';
                  if(isset($searched) &&
                    (   !empty($searched->date_from) ||
                        !empty($searched->date_to) ||
                        !empty($searched->bid_from) ||
                        !empty($searched->bid_to) ||
                        !empty($searched->cbox)
                    ) ){
                    $collapsed = 'in';
                  }

            ?>

            <div class="row collapse {{ $collapsed }}" id="demo" >

                <div class="bg-for-margin-top">

                    <div class="col-md-9">

                        <div class="row">

                        <div class="col-md-6 col-sm-12 date-range-s">

                            <div class="for-bg-blue">

                                <div class=""><h4 class="bold">Date Range</h4></div>

                                <div class="rounded">


                                    <h4 class="box-margin">From</h4>
                                    <div class="">
                                        <input type="date"  id="dfrom" name="date_from"  onfocus="(this.type='date')" class="search-wid header-search-input ui-autocomplete-input"
                                               onkeyup='saveValue(this);' placeholder="dd/mmm/yyy"
                                               value="{{ (isset($searched) && strlen($searched->date_from)>0)?date('Y-m-d', strtotime($searched->date_from)):''  }}">
                                    </div>

                                    <h4 class="box-margin-one">To</h4>
                                    <div class="">
                                        <input data-value="" type="date" id="dto" name="date_to"  class="search-wid header-search-input ui-autocomplete-input"
                                               onkeyup='saveValue(this);' placeholder="dd/mmm/yyy"
                                               value="{{ (isset($searched) && strlen($searched->date_to)>0)?date('Y-m-d', strtotime($searched->date_to)):''  }}">
                                    </div>
                                </div>

                            </div>
                        </div>

                        <div class="col-md-6 col-sm-12 col-sm-12 bid-range-s">

                            <div class="for-bg-blue">
                                <div ><h4 class="bold">Bid Range ($)</h4></div>
                                <div class="rounded">
                                    <h4 class="box-margin">From</h4>

                                    <div class="">

                                        <input type="text" id="bfrom" name="bid_from" class="search-wid header-search-input ui-autocomplete-input"
                                               value="{{ (isset($searched))?$searched->bid_from:''  }}"
                                               placeholder="Any" onkeyup='saveValue(this);'>
                                    </div>



                                    <h4 class="box-margin-one">To</h4>
                                    <div class="">
                                        <input type="text" id="bto" name="bid_to" class="search-wid header-search-input ui-autocomplete-input" placeholder="Any"
                                               onkeyup='saveValue(this);'
                                               value="{{ (isset($searched))?$searched->bid_to:''  }}">
                                    </div>
                                </div>
                            </div>

                        </div>
                            </div>
                    </div>


                    <div class="col-md-3">

                            <div class="input-group bg-sel" style="width: 100%;">
                                <div><h4 style="padding-top: 5px;" class="bold">Status</h4></div>
                                <?php
                                    $checked = '';
                                    if (isset($searched)) {
                                        $checked = $searched->cbox;
                                    }
                                ?>

                                <select name="cbox" class="header-search-input search-select form-control">
                                    <option value="">All</option>
                                    <option value="Open Solicitation" {{ ( $checked == 'Open Solicitation' )? 'selected':''  }}>Open Solicitation</option>
                                    <option value="Awarded Solicitation" {{ ( $checked == 'Awarded Solicitation' )? 'selected':''  }}>Bid Results</option>
                                    <option  value="Closed Solicitation" {{ ( $checked == 'Closed Solicitation' )? 'selected':''  }}>Awarded</option>
                                </select>
                                <input type="text" style="border-right: none;border-radius: 5px 0 0 5px !important;" class="header-search-input form-control hide" placeholder="All" name="title" id="tags">

                                <div class="input-group-btn hide ">
                                    <button class="header-search-input search-btn" style="border-left: none;border-radius: 0px 5px 5px 0px !important;" type="submit">
                                        <i class="fa fa-chevron-down" style="font-size:24px; "></i>
                                    </button>
                                </div>
                            </div>

                    </div>
            </div>

</div>

            </form>
        </div>

    </div>


    <div class="container search-page">

        @if(isset($alertone))
            <div class="alert alert-success" style="background-color:#28a745">
                {{--{{ $alertone }}--}}
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>

                <h3 style="color:#fff"><strong><i class="fa fa-check" aria-hidden="true"></i></strong> Alert!</h3>

                <p style="color:#fff">{{$alertone}}</p>

            </div>
        @endif

        <div id="MyElement" class="alert alert-success alert-dismissible hide" style="background-color:#28a745">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>

            <h3 style="color:#fff"><strong><i class="fa fa-check" aria-hidden="true"></i></strong> Alert!</h3>

            <p style="color:#fff">{{Session::get('alertone')}}</p>
        </div>

        <div class="row">

            <!--   LEft Search Bar OLD---->
            <div class="col-lg-3 col-md-3 hide col-sm-12">

                <div class="sidebar1 projectSearchFrom">
                    <div class="left-navigation">
                        <h4>Advance Search</h4>

                        <form action="{{ route('s_filter') }}" method="post" role="form"
                              enctype="multipart/form-data">
                            @csrf
                            <div class="row">

                                <div class="col-lg-12" style="margin-top: 2%;">
                                    <input type="text" id="title" class="form-control" name="p_title"
                                           value="{{ (isset($searched))?$searched->title:''  }}"
                                           placeholder="Keyword.." onkeyup='saveValue(this);'>
                                </div>
                                <?php //echo $searched->location; ?>
                                <div class="col-lg-12" style="margin-top: 2%;">
                                    <input id="autocomplete" onFocus="geolocate()" placeholder="Location.."
                                           type="text"
                                           class="form-control"
                                           value="{{ (isset($searched))?$searched->location:''  }}" name="location"
                                           onkeyup='saveValue(this);'>
                                </div>
                                <!--<div class="col-lg-4">-->
                                <!--    <input type="text"  class="form-control" placeholder="Geo Area..">-->
                                <!--</div>-->

                            </div>

                            <div class="row" style="margin-top: 4%;">

                                <div class="col-lg-12">
                                    <h4>Status</h4>
                                </div>

                                <div class="col-lg-12">
                                    <label class="container-checkbox">
                                        <?php
                                        $checked = '';
                                        if (isset($searched)) {
                                            $checked = $searched->cbox;
                                        }

                                        ?>
                                        Open Solicitation <span class="projectsCount"> ({{$openSolicitation}}
                                            )</span>
                                        <input type="radio" id="cbox1" name="cbox" class="checkboxit"
                                               value="Open Solicitation" {{ ( $checked == 'Open Solicitation' )? 'checked':''  }}>
                                        <span class="checkmark"></span>
                                    </label>
                                </div>

                                <div class="col-lg-12">
                                    <label class="container-checkbox">
                                        Bid Results <span class="projectsCount"> ({{$awardedSolicitation}})</span>
                                        <input type="radio" id="cbox2" name="cbox" class="checkboxit"
                                               value="Awarded Solicitation" {{ ( $checked == 'Awarded Solicitation' )? 'checked':''  }} >
                                        <span class="checkmark"></span>
                                    </label>

                                </div>

                                <div class="col-lg-12">
                                    <label class="container-checkbox">
                                        Awarded <span class="projectsCount"> ({{$closedSolicitation}})</span>
                                        <input type="radio" id="cbox3" name="cbox" class="checkboxit"
                                               value="Closed Solicitation" {{ ( $checked == 'Closed Solicitation' )? 'checked':''  }}>
                                        <span class="checkmark"></span>
                                    </label>

                                </div>


                            </div>

                            <div class="row" style="margin-top: 4%;">
                                <div class="col-lg-12">
                                    <h4> Bid Range</h4>
                                </div>


                                <div class="col-lg-12">
                                    From
                                    <input type="number" id="bfrom" name="bid_from" class="form-control"
                                           value="{{ (isset($searched))?$searched->bid_from:''  }}"
                                           placeholder="$" onkeyup='saveValue(this);'>
                                </div>

                                {{--<div class="col-lg-5">--}}
                                {{--<input type="number" id="bfrom" name="bid_from" class="form-control" value="{{ (isset($searched))?$searched->bid_from:''  }}" placeholder="$" onkeyup='saveValue(this);'>--}}
                                {{--</div>--}}

                                {{--<div class="col-lg-1 no-padding">--}}
                                {{--<h4 class="to-search-field">to</h4>--}}
                                {{--</div>--}}


                                {{--<div class="col-lg-1 no-padding">--}}
                                {{--<h4>to</h4>--}}
                                {{--</div>--}}

                                <div class="col-lg-12">
                                    To
                                    <input type="number" id="bto" name="bid_to" class="form-control" placeholder="$"
                                           onkeyup='saveValue(this);'
                                           value="{{ (isset($searched))?$searched->bid_to:''  }}">
                                </div>
                            </div>

                            <div class="row" style="margin-top: 4%;">
                                <div class="col-lg-12">
                                    <h4> Date Range:</h4>
                                </div>


                                <div class="col-lg-12">
                                    From
                                    <input type="date" id="dfrom" name="date_from" class="form-control"
                                           onkeyup='saveValue(this);'
                                           value="{{ (isset($searched) && strlen($searched->date_from)>0)?date('Y-m-d', strtotime($searched->date_from)):''  }}">
                                </div>

                                {{--<div class="col-lg-1">
                                    <h4>to</h4>
                                </div>--}}

                                <div class="col-lg-12">
                                    To
                                    <input data-value="" type="date" id="dto" name="date_to" class="form-control"
                                           onkeyup='saveValue(this);'
                                           value="{{ (isset($searched) && strlen($searched->date_to)>0)?date('Y-m-d', strtotime($searched->date_to)):''  }}">
                                </div>
                            </div>

                            <div class="row" style="margin-top: 10%;  ">
                                <div class="col-lg-6">
                                    <button type="submit" class="btn btn-3d btn-reveal btn-orange btn-small">
                                        <i class="fa fa-rocket"></i>
                                        <span>
                                            Find Projects
                                        </span>
                                    </button>
                                </div>

                                <!--<div class="col-lg-6">-->
                                <!--    <span class="btn btn-3d btn-reveal btn-default btn-small" onclick="clearfeild()">-->
                                <!--        <i class="fa fa-times"></i>-->
                                <!--        <span>-->
                                <!--            Clear Feilds-->
                                <!--        </span>-->
                                <!--    </span>-->
                                <!--</div>-->
                            </div>
                        </form>

                    </div>
                </div>

            </div>
            <!--   LEft Search Bar OLD---->
            <script type="text/javascript">
                var df = '';
                var dt = '';
                document.getElementById("title").value = getSavedValue("title");
                document.getElementById("autocomplete").value = getSavedValue("autocomplete");
                document.getElementById("bfrom").value = getSavedValue("bfrom");
                document.getElementById("bto").value = getSavedValue("bto");
                if (getSavedValue("dfrom") == "") {
                    df = getSavedValue("dfrom");
                } else {
                    <?php
                    $dateform = '';
                    if(isset($searched)){
                        if(!empty($searched->date_from)){
                            $dateform = date('Y-m-d', strtotime($searched->date_from));
                        }
                    }
                    ?>
                     df = <?php (!empty($dateform))? $dateform : '' ?>;





                }
                document.getElementById("dfrom").value = df;
                if (getSavedValue("dto") == "") {
                    dt = getSavedValue("dto")
                } else {
                    dt = <?php (isset($searched))? date("Y-m-d", strtotime($searched->date_from)): '' ?>;

                }
               // console.log(df);
                document.getElementById("dto").value = dt;

                function saveValue(e) {
                    var id = e.id;  // get the sender's id to save it .
                    var val = e.value; // get the value.
                    localStorage.setItem(id, val);// Every time user writing something, the localStorage's value will override .
                }


                function getSavedValue(v) {
                    if (!localStorage.getItem(v)) {
                        return "";
                    }
                    return localStorage.getItem(v);
                }

                function clearfeild() {

                    document.getElementById("title").value = "";
                    document.getElementById("autocomplete").value = "";
                    document.getElementById("bfrom").value = "";
                    document.getElementById("bto").value = "";
                    document.getElementById("dfrom").value = "";
                    document.getElementById("dto").value = "";

                }
            </script>




            <div class="col-lg-12 col-md-12 col-sm-12 conabc">
                <div class="ajax-loader hide">
                    <div class="animate-flicker"><img style="width: 100px;" src="{{ asset('images/TBD-T.png') }}"></div>
                </div>
                <table class="table table-striped custab projects-list">
                    <?php
                    if(!empty($projects)){
                     $count = 0;

                    ?>
                    @foreach($projects as $pro)
                        <?php $checkempty = $pro->title; ?>
                        <tr class="single-item on-load">
                            <td class="" style="border: none;">
                                <?php $count = $count + 1; ?>
                                <div class="{{ ($count  % 2 == 0)?'grey-list-back':'white-list-back' }}">
                                <div class="row ">

                                    <div class="col-md-9 col-sm-6">
                                        <div class="">
                                            <h3 class="project-title ">
                                                <a href="{{route('pdetail', $pro->id)}}">{{$pro->title}}</a>
                                            </h3>

                                            <div class="project-description m-y-lg">
                                                @if(isset($pro->description) && strlen($pro->description) > 0)
                                                    {{--{{ (strlen($pro->description) > 50) ? substr($pro->description,0, 150) . '...' : $pro->description}}--}}
                                                {{ $pro->description  }}
                                                    {{--{{$pro->description}}--}}
                                                @endif
                                            </div>

                                            <div class="location">
                                                <i style="font-size: 15px; color: #39c1a8;" class="fa fa-map-marker loc-icon"></i>
                                                <span class="text">{{$pro->location}}</span>
                                            </div>
                                        </div>


                                    </div>
                                    <div class="col-md-3 col-sm-6 m-t-sm secondElement">
                                        @php
                                        $date = new DateTime($pro->bid_date);
                                        $result = $date->format('M d, Y');
                                        @endphp

                                        <div class="bid-amount">
                                            <label class="amount-label">
                                                Bid Amount: $
                                            </label>

                                            <span class="amount-itself">
                                                <?php  echo $checkamount = number_format_short($pro->bid_amount, 1);  ?>
                                            </span>
                                        </div>

                                        <div class="bid-date">
                                            <label>
                                                Bid Date:
                                            </label>
                                            <span class="text">{{$result}}</span>
                                        </div>

                                        <div class="project-details">
                                            <a href="{{route('pdetail', $pro->id)}}"
                                               class="downloads-button btn blue-button-bg">
                                                <i class="hide fa fa-line-chart white-text"></i>
                                                        See Details
                                                <i class="fa fa-arrow-right button-arrow" style="padding-top: 4px;color: white;" aria-hidden="true"></i>
                                            </a>
                                        </div>

                                    </div>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                    <?php }

                    if (empty($checkempty)) {
                        echo "<br><h4>No Result Found!</h3>";
                    }

                    function number_format_short($n, $precision)
                    {
                        if ($n < 900) {
                            // 0 - 900
                            $n_format = number_format($n, $precision);
                            $suffix = '';
                        } else if ($n < 900000) {
                            // 0.9k-850k
                            $n_format = number_format($n / 1000, $precision);
                            $suffix = 'K';
                        } else if ($n < 900000000) {
                            // 0.9m-850m
                            $n_format = number_format($n / 1000000, $precision);
                            $suffix = 'M';
                        } else if ($n < 900000000000) {
                            // 0.9b-850b
                            $n_format = number_format($n / 1000000000, $precision);
                            $suffix = 'B';
                        } else {
                            // 0.9t+
                            $n_format = number_format($n / 1000000000000, $precision);
                            $suffix = 'T';
                        }
                        // Remove unecessary zeroes after decimal. "1.0" -> "1"; "1.00" -> "1"
                        // Intentionally does not affect partials, eg "1.50" -> "1.50"
                        if ($precision > 0) {
                            $dotzero = '.' . str_repeat('0', $precision);
                            $n_format = str_replace($dotzero, '', $n_format);
                        }
                        return $n_format . $suffix;
                    }

                    ?>

                </table>

            </div>
        </div>

    </div>


    <div id="searchModal" class="modal fade" role="dialog">

        <div class="modal-dialog">

            <form method="post" action="{{ route('s_save_filter') }}">

                <!-- Modal content-->
                <div class="modal-content" style="margin-top:20%;margin-bottom: 20%">

                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title" id="ppname">Save Search</h4>
                    </div>

                    <div class="modal-body">

                        <div class="row">
                            @csrf

                            <div class="col-lg-12" style=" margin-top: 5%;">
                                <label>Save Search as</label>
                                <input type="text" class="form-control" name="save_search"
                                       placeholder="Save Search as"
                                       required>
                            </div>

                            <div class="col-lg-6" style=" margin-top: 5%;">
                                <label>Keyword</label>
                                <input type="text" class="form-control" name="p_title"
                                       placeholder="Keyword..">
                            </div>

                            <div class="col-lg-6" style=" margin-top: 5%;">
                                <label>Location</label>
                                <input id="autocomplete1" onFocus="geolocate()" placeholder="Location.." type="text"
                                       class="form-control" name="location">
                            </div>


                            <div class="col-lg-4" style=" margin-top: 5%;">

                                <input type="radio" name="cbox" value="1"> Open Solicitation
                            </div>

                            <div class="col-lg-4" style=" margin-top: 5%;">
                                <input type="radio" name="cbox" value="2"> Bid Results
                            </div>

                            <div class="col-lg-4" style=" margin-top: 5%;">
                                <input type="radio" name="cbox" value="3"> Awarded
                            </div>


                            <div class="col-lg-6" style=" margin-top: 5%;">
                                <label>Price Range From</label>
                                <input type="text" class="form-control bfrom"  name="bid_from" placeholder="$">
                            </div>

                            <div class="col-lg-6" style=" margin-top: 5%;">
                                <label>Price Range to</label>
                                <input type="text" class="form-control bto" name="bid_to" placeholder="$">
                            </div>


                            <div class="col-lg-6" style=" margin-top: 5%;">
                                <label>Date Range From</label>
                                <input type="date" class="form-control" name="date_from">
                            </div>

                            <div class="col-lg-6" style=" margin-top: 5%;">
                                <label>Date Range to</label>
                                <input type="date" class="form-control" name="date_to">
                            </div>

                        </div>


                    </div>
                    <div class="modal-footer">
                        <button style="" class="btn blue-button-bg subcontactor-listing-btn">

                           Save Search
                        </button>

                        <button type="button" class="btn subcontactor-listing-btn orange-button-bg" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <table class="clone-table hide">
        <tbody>
        <tr class="single-item ajax-clone hide">
            <td class="" style="border: none;">
                <div class="side-bg">
                    <div class="row ">

                        <div class="col-md-9 col-sm-6">
                            <div class="">
                                <h3 class="project-title">
                                    <a href=""></a>
                                </h3>

                                <div class="project-description m-y-lg">

                                </div>

                                <div class="location">
                                    <i style="font-size: 15px; color: #39c1a8;" class="fa fa-map-marker loc-icon"></i>
                                    <span class="text"></span>
                                </div>
                            </div>

                        </div>
                        <div class="col-md-3 col-sm-6 m-t-sm secondElement">


                            <div class="bid-amount">
                                <label class="amount-label">
                                    Bid Amount: $
                                </label>

                                            <span class="amount-itself">

                                            </span>
                            </div>

                            <div class="bid-date">
                                <label>
                                    Bid Date:
                                </label>
                                <span class="text"></span>
                            </div>

                            <div class="project-details">
                                <a href=""
                                   class="downloads-button btn blue-button-bg">
                                    <i class="hide fa fa-line-chart white-text"></i>
                                    See Details
                                    <i class="fa fa-arrow-right button-arrow" style="padding-top: 4px;color: white;" aria-hidden="true"></i>
                                </a>
                            </div>

                        </div>
                    </div>
                </div>
            </td>
        </tr>
        </tbody>
    </table>

    {{--</div>--}}

    <script type="">

        var existtwo = '{{Session::has('alertone')}}';

        if (existtwo) {

            document.getElementById('MyElement').classList.add('show');
            document.getElementById('MyElement').classList.remove('hide');

        }

        function search_field() {

            var keywords = document.getElementById('sfield').value;

            var res = keywords.split("|");
            document.getElementById('title').value = res[0];

            var totalWords = res[1];
            //var firstWord = totalWords.replace(/ .*/,'');

            document.getElementById('autocomplete').value = totalWords;

            if (res[2] != 0 && res[2] == 1) {
                document.getElementById("cbox1").checked = true;
            }

            if (res[3] != 0 && res[3] == 1) {
                document.getElementById("cbox2").checked = true;
            }

            if (res[4] != 0 && res[4] == 1) {
                document.getElementById("cbox3").checked = true;
            }


            document.getElementById('bfrom').value = res[5];

            document.getElementById('bto').value = res[6];

            document.getElementById('dfrom').value = res[7];

            document.getElementById('dto').value = res[8];

            $('#submit_filters').trigger('click');



        }





        var placeSearch, autocomplete;

        function initAutocomplete() {
            autocomplete = new google.maps.places.Autocomplete(
                    document.getElementById('autocomplete'), {types: ['geocode']});

            autocomplete = new google.maps.places.Autocomplete(
                    document.getElementById('autocomplete1'), {types: ['geocode']});
//   autocomplete.setFields('address_components');
//   autocomplete.addListener('place_changed', fillInAddress);
        }

        function geolocate() {
            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(function (position) {
                    var geolocation = {
                        lat: position.coords.latitude,
                        lng: position.coords.longitude
                    };
                    var circle = new google.maps.Circle(
                            {center: geolocation, radius: position.coords.accuracy});
                    autocomplete.setBounds(circle.getBounds());
                });
            }
        }

    </script>

    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD5KPKoO7ZP-grfU1aOx2GD1ra1pQMBdAQ&libraries=places&callback=initAutocomplete"
            async defer></script>

@endsection
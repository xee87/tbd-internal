<?php

$complete = $awardedSolicitation;

?>

<div class="modal fade" id="awarded" tabindex="-1" role="dialog" aria-labelledby="awardedLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content card card-info">
            <div class="modal-header card-header">
                <h4 class="modal-title">Bit Results</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <?php //echo '<pre>'; var_dump($awardedSolicitation)?>
                <form class="division-form" action="{{ route('solicitation_add') }}" method="post" role="form" >
                    @csrf
                    <?php
                        $ph_id = [];
                        if(is_array($awardedSolicitation)){

                            foreach($awardedSolicitation as $k){
                                $ph_id[] = $k->planholder_id;
                            }
                        }
                    ?>

                    <div class="form-group">
                        <label>Plan Holders</label>
                        <input type="hidden" value="Awarded Solicitation" name="sol_type">
                        <select class="form-control selectedPH" name="pholder[]" data-placeholder="Select Plan Holders" style="width: 100%;" required autofocus>
                            <option value=""> -- Select Planholder -- </option>
                            @if(is_object($selectedPlanholder) || is_array($selectedPlanholder))
                                @foreach($selectedPlanholder as $strplanholderall)

                                    <option value="{{$strplanholderall->id}}" {{ (in_array($strplanholderall->id, array_unique($ph_id))) ? 'disabled': '' }}>{{$strplanholderall->company}}</option>

                                @endforeach
                            @endif
                        </select>
                    </div>

                    @if($awardedSolicitation)

                        @foreach($awardedSolicitation as $k => $awarded)

                            @if($awarded->project_id == $projectID)

                                <div class="callout callout-success relative toBeDeleted">
                                    <button type="button" class="delete_bid close" data-url="{{ route('solicitation_delete') }}" data-projectID="{{$projectID}}" data-phID="{{$awarded->planholder_id}}"  aria-label="Close"><span aria-hidden="true">×</span></button>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <h5>{{ $awarded->company }} :<br/> <span class="ph_name text-muted"></span></h5>

                                            <input class="ph_id" type="hidden" name="planHolder[id][]" value="{{ $awarded->planholder_id }}">
                                        </div>
                                        <div class="col-md-6">
                                            <p>
                                                <label for="price"> Price</label><br>
                                                <input class="greyborder priceField" type="text" name="planHolder[price][]" value="{{ $awarded->price  }}">
                                            </p>
                                        </div>
                                    </div>
                                    <div class="delete-planHolder hide">
                                        <i class="fa fa-trash"></i>
                                    </div>
                                </div>

                            @endif

                        @endforeach

                    @endif

                    <div class="ph_records">

                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-info awardedSolicitation">Save</button>
                    </div>

                    <input type="hidden" name="project_id" value="{{ $projectID }}">
                </form>
            </div>

            <div class="callout callout-success hide htmlLayout relative">
                <div class="row">
                    <div class="col-md-6">
                        <h5>Name :<br/> <span class="ph_name text-muted"></span></h5>
                        <input class="ph_id" type="hidden" name="planHolder[id][]" value="">
                    </div>
                    <div class="col-md-6">
                        <p>
                            <label for="price"> Price</label><br>
                            <input class="greyborder priceField" type="text" name="planHolder[price][]" value="">
                        </p>
                    </div>
                </div>
                <div class="delete-planHolder hide">
                    <i class="fa fa-trash"></i>
                </div>
            </div>

        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>

<div class="modal fade" id="closed" tabindex="-1" role="dialog" aria-labelledby="closedLabel" data-awarded="<?php //echo count($awardedSolicitation) > 0 ? true : false?>">
    <div class="modal-dialog" role="document">
        <div class="modal-content card card-info">
            <div class="modal-header card-header">
                <h4 class="modal-title">Award</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <form class="division-form" action="{{ route('solicitation_award') }}" method="post" role="form" >
                <div class="modal-body">
                    <?php // echo '<pre>'; var_dump($complete)?>
                        @csrf
                        <input type="hidden" name="project_id" value="{{ $projectID }}">
                        <input type="hidden" value="Closed Solicitation" name="sol_type">
                        @if(is_array($complete))
                            <?php foreach($complete as $k => $award){ ?>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">

                                  <div class="icheck-info icheck-inline">
                                      <input id="select<?php echo $k ?>" type="radio" value="{{ $award->planholder_id }}" <?php if((int)$award->awarded == 1) echo "checked" ?> name="awarded" >
                                      <label for="select<?php echo $k ?>">
                                          {{ $award->company  }}
                                      </label>
                                  </div>

                                </div>
                            </div>
                            <div class="col-md-6">
                              <h5> USD <span class="text-muted"> {{ $award->price  }} </span></h5>
                          </div>
                         </div>
                    <?php } ?>
                        @endif
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-info awardedSolicitation">Award</button>
                </div>
            </form>
        </div><!-- /.modal-content -->
</div><!-- /.modal-dialog -->
</div>
@extends('layouts.app')

@section('content')
<!-- Content Header (Page header) -->
<div id="MyElement" class="alert alert-success alert-dismissible fade hide " role="alert">
  <h3 style="color:#fff"><strong><i class="fa fa-check" aria-hidden="true"></i></strong> Alert!</h3> 
  <p style="color:#fff">{{Session::get('alertone')}}</p>
  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
  </button>
</div>

<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
            </div>
            <div class="col-sm-6">
                <div class="float-sm-right">

                </div>
            </div>
        </div>
    </div>
</section>

<!-- Main content -->
<section class="content">

    <div class="card card-info card-outline">
        <div class="container-fluid mt-2">
            <div class="info-box bg-info">
                <span class="info-box-icon"><i class="fa fa-download"></i></span>
                <div class="info-box-content">
                   {{-- <div class="col-md-5 col-sm-6 col-xs-6">--}}
                        <h1>Subscription Management</h1>
                    {{--</div>--}}
                </div>
                <span class="info-box-icon float-right">
                    <a class="add-resource" href="{{route('sub_add')}}" title="Add New Group"> <i class="fa fa-plus"></i></a>
                </span>
            </div>
        </div>
        <div class="card-body">
            <div class="table-responsive">
              <table id="user_table" class="table table-bordered table-hover">
                <thead>
                    <tr>
                        <th>Sr#</th>
                        <th>Package Name</th>
                        <th>Package Slug Name</th>
                        <th>Braintree id</th>
                        <th>Description</th>
                        <th>Cost</th>
                        <th>Downloads</th>
                        <th>Max Price</th>
                        <th>Date</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $countit = 1; ?>
                    
                    @foreach($results as $subplans)
                    
                    
                    <tr>
                        <td>{{$countit}}</td>
                        
                        <td>{{$subplans->name}}</td>

                        <td>{{$subplans->slug}}</td>

                        <td>{{$subplans->braintree_plan}}</td>

                        <td>{{$subplans->description}}</td>
                        
                        <td>{{$subplans->cost}}</td>
                        
                        <td>{{$subplans->downloads}}</td>

                       
                        <td>{{$subplans->max_rate}}</td>
                         <?php 
                                                      $originalDate = $subplans->created_at;
                                                      $newDate = date("M-d-Y h:i:sa", strtotime($originalDate));
                          ?>
                        <td>{{$newDate}}</td>
                        
                        <td>
                            <div class="btn-group">
                                <button type="button" class="btn btn-default">Action</button>
                                <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown"
                                    aria-expanded="false">
                                    <span class="caret"></span>
                                    <span class="sr-only">Toggle Dropdown</span>
                                </button>
                                <div class="dropdown-menu" role="menu" x-placement="top-start" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(67px, -165px, 0px);">
                                    <a class="dropdown-item" href="{{route('sub_downloads_edit', $subplans->id)}}">Edit</a>
                                    
                                </div>
                            </div>
                        </td>
                    </tr>
                    <?php $countit++; ?>
                    
                 @endforeach
                </tbody>
            </table>
            </div>
        </div>
        <!-- /.card-body -->
    </div>
    <!-- /.card -->
<script>
    
    var existtwo = '{{Session::has('alertone')}}';
    
    if(existtwo){

document.getElementById('MyElement').classList.add('show');
document.getElementById('MyElement').classList.remove('hide');

    }
  </script>
</section>
<!-- /.content -->
@endsection

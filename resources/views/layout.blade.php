<?php

    use App\Helper\TBD;
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <title>True Bid Data</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        {{--<link rel="stylesheet" href="{{asset('css/bootstrap-theme.css')}}">--}}
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
        <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
        <link rel="stylesheet" href="{{asset('css/tbd-styles.css')}}">
        <link rel="stylesheet" href="{{asset('css/tbd-styles-salman.css')}}">
        <link rel="stylesheet" media="print, screen" href="{{asset('css/print.css')}}">
        <link rel="stylesheet" href="{{asset('plugins/owl-carousel/owl.carousel.css')}}">
        <link rel="stylesheet" href="{{asset('plugins/owl-carousel/owl.theme.css')}}">
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i" rel="stylesheet">
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <link rel="stylesheet" href="{{asset('/plugins/select2/select2.min.css')}}">
        <link rel="stylesheet" href="{{asset('plugins/dropzone/css/dropzone.min.css')}}">
        <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
        <link rel="shortcut icon" type="image/png" href="{{asset('/images/TBD-T.png')}}"/>


        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        {{--<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>--}}
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
        <script src="{{asset('plugins/owl-carousel/owl.carousel.min.js')}}"></script>
        <script src="{{asset('plugins/pagify/pagify.js')}}"></script>
        <script src="{{asset('plugins/dropzone/js/dropzone.js')}}"></script>
        <script src="{{asset('plugins/jQueryMaskPlugin/dist/jquery.mask.js')}}"></script>
        <script src="{{asset('js/tbd-frontend-scripts.js')}}"></script>
        <script src="{{asset('plugins/sweetalert/sweetalert.min.js')}}"></script>
        <script src="{{asset('/plugins/select2/select2.full.min.js')}}"></script>
        <script>
            $(function () {
               // $("#user_table").DataTable();
                //Initialize Select2 Elements
                $('.select2').select2();
            })
        </script>
        {{--<link href='https://fonts.googleapis.com/css?family=Roboto' rel='stylesheet'>--}}
    </head>
    <style type="text/css">
        .ui-menu{
            z-index: 9999;
        }
        /*#myNavbar{
            height: 750px;
        }*/
    </style>

    <script type="text/javascript">
        var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
        (function(){
            var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
            s1.async=true;
            s1.src='https://embed.tawk.to/5db1b395df22d91339a0dee7/default';
            s1.charset='UTF-8';
            s1.setAttribute('crossorigin','*');
            s0.parentNode.insertBefore(s1,s0);
        })();
    </script>

    <body class="tbd-frontend overflow-hide">

        <div class="gooey">

            <div class="animate-flicker"><img style="width: 100px;" src="{{ asset('images/TBD-T.png') }}"></div>

            {{--<span class="dot hide"></span>--}}
            {{--<div class="dots hide">--}}
                {{--<span></span>--}}
                {{--<span></span>--}}
                {{--<span></span>--}}
            {{--</div>--}}
        </div>
        <div class="page-content-tbd hide">

            <?php if(Auth::guest()){ ?>
            <div class="top-header">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12 text-right no-padding">
                            <div class="linksContainer">
                                <a class="headerLinks" href="tel:234 2345 678">
                                    <i class="fa fa-phone"></i>
                                    1-718-717-2601
                                </a>
                                <a class="headerLinks" href="mailto:sales@truebiddata.com">
                                    <i class="fa fa-envelope"></i>
                                </a>
                                <a class="headerLinks" href="/user_login">
                                    <i class="fa fa-lock"></i>
                                    Login
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <?php    } ?>

            <header class="tbd-header">
            <nav class="navbar navbar-inverse">
                <div class="container-fluid">
                    <div class="navbar-header m-20 visible-xs">
                        <a style="padding:0px" class="visible-xs navbar-brand" href="/">
                            <img src="{{asset('images/spaced-tbd-logo.png')}}" class="mobileimg" style="width: 130px;">
                        </a>

                        <button id="ChangeToggle" type="button" class="navbar-toggle" data-toggle="slide-collapse" data-target="#myNavbar">
                            <div id="navbar-hamburger">
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </div>
                            <div id="navbar-close" class="hidden">
                                <span class="glyphicon glyphicon-remove"></span>
                            </div>
                        </button>
                    </div>

                    <div class="collapse navbar-collapse mtb-15" id="myNavbar">

                        <div class="row">
                            <div class="col-md-2 col-sm-2">
                                <a style="padding:0px;" class="navbar-brand" href="/" style="padding: 10px 5px;">
                                    <img src="{{asset('images/spaced-tbd-logo.png')}}" class="mobileimg pmt-20" style="width: 130px;">
                                </a>
                                <span class="visible-xs close-button-sidbar">
                                    <i class="fa fa-remove"></i>
                                </span>

                            </div>
                            <div class="col-sm-10 col-md-10 m-t-sm">
                                <div class="row">
                                    <div class="col-md-5 col-sm-12 pmt-20 search-bar">

                                        <form class="navbar-form" action="{{ route('s_filter') }}" method="post" role="form"
                                              enctype="multipart/form-data">
                                            @csrf
                                            <div class="input-group" style="width: 100%;">

                                                <input type="text" class="form-control" placeholder="Click to start project search" name="p_title" id="tags">

                                                <div class="input-group-btn ">
                                                    <button class="btn btn-default search-btn" type="submit" style="height:40px;"><i class="glyphicon glyphicon-search"></i>
                                                    </button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                    <div class="col-md-7 col-sm-12 tbd-menu">
                                        @guest
                                        <div class="nav-rigter m-y-sm">
                                            <div>

                                                <a class="btn btn-blue blue-button-bg" href="/user-register">
                                                    <span>Register</span>
                                                    {{--<i class="fa fa-lock arrow-button"></i>--}}
                                                </a>
                                            </div>
                                        </div>
                                        @endguest
                                        <ul class="nav navbar-nav  main-menu">
                                            {{--<li><a href="/search">Advanced Search</a></li>--}}
                                           {{-- <li><a href="/">Home</a></li>--}}
                                            <li>
                                                <a class="{{ Request::segment(1) === 'whyus' ? 'current' : null }}" href="/whyus">
                                                    Why Us
                                                </a>
                                                <div class="menu-green-border"></div>
                                            </li>
                                            <li>
                                                <a class="{{ Request::segment(1) === 'plans' ? 'current' : null }}" href="/plans">
                                                    Pricing
                                                </a>
                                                <div class="menu-green-border"></div>
                                            </li>
                                            <li>
                                                <a class="{{ Request::segment(1) === 'coveragearea' ? 'current' : null }}" href="/coveragearea">
                                                    Coverage Area
                                                </a>
                                                <div class="menu-green-border"></div>
                                            </li>
                                            <li>
                                                <a class="{{ Request::segment(1) === 'contactus' ? 'current' : null }}" href="/contactus">
                                                    Contact Us
                                                </a>
                                                <div class="menu-green-border"></div>
                                            </li>
                                            <li>
                                                <a class="{{ Request::segment(1) === 'sample-project' ? 'current' : null }}" href="/sample-project">
                                                    Sample Project
                                                </a>
                                                <div class="menu-green-border"></div>
                                            </li>
                                            {{--<li class="dropdown">--}}
                                                {{--<a data-toggle="dropdown" class="dropdown-toggle" href="#">Others <b class="caret"></b></a>--}}
                                                {{--<ul class="dropdown-menu">--}}
                                                    {{--<li><a class="{{ Request::segment(1) === '#' ? 'current' : null }}" href="#">Screen Prints</a></li>--}}
                                                    {{--<li><a class="{{ Request::segment(1) === '#' ? 'current' : null }}" href="#">Sample Project</a></li>--}}

                                                {{--</ul>--}}
                                            {{--</li>--}}
                                            <li class="user-controls hide">

                                                <div class="dropdown others" >
                                                    <a class="title-menu-other">
                                                        Others<span class="caret"></span>
                                                    </a>
                                                    <div class="dropdown-content">
                                                        <a class="dropdown-item {{ Request::segment(1) === '#' ? 'current' : null }}" href="">
                                                            Screen Prints
                                                        </a>
                                                        <a class="dropdown-item {{ Request::segment(1) === '#' ? 'current' : null }}" href="">
                                                            Sample Project
                                                        </a>

                                                    </div>
                                                </div>

                                            </li>

                                            @auth

                                            <li class="user-controls" >
                                                <div class="dropdown">
                                                    <a class="dropbtn text-uppercase">
                                                        {{ Auth::user()->name }}<span class="caret"></span>
                                                    </a>
                                                    <div class="dropdown-content">
                                                        <?php if(TBD::allowAccess()) : ?>
                                                            <a class="dropdown-item" href="{{ route('user_record') }}">
                                                                <i class="fa fa-download"></i>
                                                                Download History
                                                            </a>
                                                            <a class="dropdown-item" href="{{ route('s_filter') }}">
                                                                <i class="glyphicon glyphicon-search"></i>
                                                                Saved Search
                                                            </a>
                                                        <?php endif; ?>
                                                        <a class="dropdown-item" href="{{ route('logout') }}"
                                                           onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                                            <i class="fa fa-power-off"></i> {{ __('Logout') }} </a>

                                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                                            @csrf
                                                        </form>

                                                    </div>
                                                </div>
                                            </li>

                                            @endauth


                                        </ul>


                                    </div>
                                </div>

                            </div>
                        </div>
                        <?php

                            $searchresult = array();
                            $strresults = DB::select( DB::raw("SELECT * FROM projects WHERE status = 'Active' ") );

                            foreach ($strresults as $strans) {
                               array_push($searchresult, $strans->title);
                            }

                            $finalresult = json_encode( $searchresult);

                            echo "<script type='text/javascript'> var availableTags = ". $finalresult . ";</script>";
                        ?>

                         <script>

//                            $( "#tags" ).autocomplete({
//                              source: availableTags
//                            });

//                            $(function() {
//                                $('#ChangeToggle').click(function() {
//
//                                });
//                            });

//                            var mywindow = $('#window');
//                            $('#navbar').on('show.bs.collapse', function(x) {
//                                mywindow.css({visibility: 'hidden'});;
//                                $('body').attr("scroll","no").attr("style", "overflow: hidden");
//                            });
//                            $('#navbar').on('hide.bs.collapse', function(x) {
//                                mywindow.css({visibility: 'visible'});
//                                $('body').attr("scroll","yes").attr("style", "");
//                            });
//                            $(document).ready(function() {
//                                $('.navbar').on('show.bs.collapse', function() {
//                                    $(this).addClass('open');
//                                });
//                                $('.navbar').on('hidden.bs.collapse', function() {
//                                    $(this).removeClass('open');
//                                });
//                            });

                            $(document).ready(function () {
                                /*$(".navbar-toggle").on("click", function () {
                                    $(this).toggleClass("active");
                                });
                                $(document).on('click',function(event){
                                    if(!$(event.target).closest('.navbar-toggle').length) {
                                        if ($('.navbar-toggle').hasClass('active') ) {
                                            $('.collapse').collapse('hide');
                                            $(".navbar-toggle").toggleClass("active");
                                        }
                                    }
                                });*/

                            });


                         </script>
                    </div>
                </div>
            </nav>
        </header>

            <div class="page-content-area-tbd">
                @yield('content')
            </div>

            @yield('scripts')

            <footer class="footer hide">
            <div class="container-fluid">
            <div class="row" style="padding: 2% 0% 0% 0%;">

                <div class="col-lg-6 col-md-6 col-sm-12">

                        <div class="footer-logo">
                            <a style="padding:0px; padding-top: 15px;" href="/">
                                <img src="{{asset('images/logo-grey.png')}}" class="pmt-20" style="width: 130px;">
                            </a>
                        </div>

                        <div class="col-md-12 hidden-xs hidden-sm no-padding">
                            <h5 class="footer-detail">
                                {{--&copy; Copyright {{  date('Y') }} True Bid Data. <br><br>--}}
                                {{--All rights reserved. Version 1.0.1.1--}}

                                <p><i class="fa fa-map-marker" style="font-size: 20px;margin-right: 1%"></i>
                                    99 Wall Street, Suite 556
                                    <br/>
                                    <span style="padding-left: 22px">New York, NY, 10005</span>
                                </p>

                                <p><i class="fa fa-mobile" style="font-size: 24px;margin-right: 1%"></i>  1-718-717-2601</p>

                                <p>
                                    <i class="fa fa-envelope-o" style="font-size: 18px;margin-right: 1%"></i>
                                    <a style="color:#b5b5b5 !important;" class="white-text" href="mailto:sales@truebiddata.com">info@truebiddata.com</a>
                                </p>
                            </h5>
                        </div>

                </div>


                <div class="col-lg-6 col-md-6 col-sm-12">

                    <div class="footer-search">
                        <form action="{{ route('s_filter') }}" method="post" role="form"

                              enctype="multipart/form-data">
                            @csrf
                            <div class="content-center">
                                <label class="search-label">Search</label>
                                <div class="input-group" style=" width: 100%;">

                                    <input style="color:white;" type="text" class="footer-search-input form-control" placeholder="Click to start project search" name="p_title" id="tags">

                                    <div class="input-group-btn ">
                                        <button class="footer-search-btn btn btn-default search-btn" type="submit" >
                                            <i class="glyphicon glyphicon-search f-s-icon"></i>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </form>

                        <div class="footer-icons">
                            <ul class="social-network social-circle " style="text-align: center;">

                                <li>
                                    <a href="https://www.facebook.com/truebid.data.1" class="icoRss">
                                        <i class="fa fa-facebook"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="https://www.instagram.com/truebiddata/" class="icoRss">
                                        <i class="fa fa-instagram"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="https://twitter.com/TrueBidData" class="icoRss">
                                        <i class="fa fa-twitter"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="https://www.youtube.com/channel/UCFGJFwjrbzqdtvY-N7neDAw?view_as=subscriber" class="icoRss">
                                        <i class="fa fa-youtube-play"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="www.linkedin.com/in/true-bid-a47a37196" class="icoRss">
                                        <i class="fa fa-linkedin"></i>
                                    </a>
                                </li>

                            </ul>
                        </div>

                    </div>

                    <div class="footer-right hide" style="">
                        <p><i class="fa fa-map-marker" style="font-size: 20px;margin-right: 1%"></i>
                            99 Wall Street, Suite 556
                            <br/>
                            New York, NY, 10005
                        </p>

                        <p><i class="fa fa-mobile" style="font-size: 24px;margin-right: 1%"></i> (123) 456-789-123</p>

                        <p>
                            <i class="fa fa-envelope-o" style="font-size: 18px;margin-right: 1%"></i>
                            <a class="white-text" href="mailto:info@truebiddata.com">info@truebiddata.com</a>
                        </p>

                    </div>


                </div>

            </div>
                <div class="col-sm-12 col-xs-12 hidden-md hidden-lg">
                    <h5 class="footer-detail">
                        {{--&copy; Copyright {{  date('Y') }} True Bid Data. <br><br>--}}
                        {{--All rights reserved. Version 1.0.1.1--}}
                        <p><i class="fa fa-map-marker" style="font-size: 20px;margin-right: 1%"></i>
                            99 Wall Street, Suite 556
                            <br/>
                            <span style="padding-left: 22px">New York, NY, 10005</span>
                        </p>

                        <p><i class="fa fa-mobile" style="font-size: 24px;margin-right: 1%"></i>  1-718-717-2601</p>

                        <p>
                            <i class="fa fa-envelope-o" style="font-size: 18px;margin-right: 1%"></i>
                            <a style="color:#b5b5b5" class="white-text" href="mailto:sales@truebiddata.com">info@truebiddata.com</a>
                        </p>
                    </h5>
                </div>
                <hr style="border-color: #616161;">
                <div class="col-md-12">
                    <h5 style="color:#b5b5b5" class="copyright-info">
                        &copy; Copyright {{  date('Y') }} True Bid Data.
                        All rights reserved. Version 1.0.1.1
                    </h5>
                </div>
            </div>
        </footer>

        </div>

        <div class="menu-overlay"></div>
    </body>


</html>
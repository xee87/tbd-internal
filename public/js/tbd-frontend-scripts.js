/**
 * Created by smart comp on 4/26/2019.
 */

jQuery(document).ready(function(){
    initializeOwlCarousel();
    // searchSideBarScroll();
    stickyHeader();
    searchPaginationScrollTop();
    changeButtonNameAfterDownloadFile();
    tabClicksHandle();
    setContractorType();
   // activeMenu();
    changeIconOnCollapse();
    slidingMenu();
    changeContainer();
    ajaxCallOnSearch();

    jsPagination();
    projectPlanDocs();
    projectSpecificationDocs();

    accordian();

   // $('.tbd-header').removeClass('hide');
    $('#bfrom').mask('000,000,000,000', {reverse: true});
    $('#bto').mask('000,000,000,000', {reverse: true});



    $("#searchModal").on('shown.bs.modal', function(){
        $('.bfrom').mask('000,000,000,000', {reverse: true});
        $('.bto').mask('000,000,000,000', {reverse: true});
    });

    $(function () {
        $('[data-toggle="tooltip"]').tooltip()
    })

// Initialize popover component
    $(function () {
        $('[data-toggle="popover"]').popover()
    })




});




function jsPagination(){

    $(".conabc").pagify(10, ".single-item");

}

function changeContainer(){


    //$('input[type="date"]').datepicker();

    if ( $(window).width() > 1800 ) {
        $('.change-cont').removeClass('container').addClass('container-fluid');
    }else{
        $('.change-cont').removeClass('container-fluid').addClass('container');
    }

    $(window).on('resize', function(){
        if ( $(window).width() > 1800 ) {
            $('.change-cont').removeClass('container').addClass('container-fluid');
        }else{
            $('.change-cont').removeClass('container-fluid').addClass('container');
        }
    });
}

function accordian(){

    $(".collapse.show").each(function(){
        $(this).prev(".card-header").find(".fa").addClass("fa-minus").removeClass("fa-plus");
    });

    $(".collapse").on('show.bs.collapse', function(){
        $(this).prev(".card-header").find(".fa").removeClass("fa-plus").addClass("fa-minus");
    }).on('hide.bs.collapse', function(){
        $(this).prev(".card-header").find(".fa").removeClass("fa-minus").addClass("fa-plus");
    });

}


function slidingMenu(){
    $('[data-toggle="slide-collapse"]').on('click', function() {

        //$('#navbar-hamburger').toggleClass('hidden');
        //$('#navbar-close').toggleClass('hidden');
        //$(".menu-overlay").toggleClass('hidden');
        jQuery('body.tbd-frontend').addClass('overflow-hide');
        jQuery('.navbar').addClass('mobileNavFixes');



        $navMenuCont = $($(this).data('target'));
        $navMenuCont.animate({
            'width': 'toggle'
        }, 350);
        $(".menu-overlay").fadeIn(500);

    });

    $(".menu-overlay, .close-button-sidbar").click(function(event) {

        console.log('hi');
        $('.close-button-sidbar').css('display','none');
        $(".navbar-toggle").trigger("click");
        $(".menu-overlay").fadeOut(500);

        $(document).find('.tbd-frontend').removeClass('overflow-hide');
        $(document).find('.navbar').removeClass('mobileNavFixes');
    });

}

function stickyHeader(){

    $(document).scroll(function() {
        if($(document).scrollTop() >= 10){
            $('.navbar').addClass('affix');
        }else{
            $('.navbar').removeClass('affix');
        }

        if( $('.search-filter-c').is('*') ){

            if ( $(window).width() > 1200 ){

                if($(document).scrollTop() >= 10){
                    $('.search-filter-c').addClass('affix');
                }else{
                    $('.search-filter-c').removeClass('affix');
                }
            }
        }

    });

}

function activeMenu(){
    $('a').on('click', function(){
        $('a').removeClass("active");
        $(this).addClass("active");
    });
}

function setContractorType(){

        $('#general_list').click(function() {
           $('#con_type').val('1');
           $('.title-list').html("General Contractor Listing");
        });

        $('#sub_list').click(function() {
           $('#con_type').val('2');
            $('.title-list').html("Subcontractor Listing");
        });

        $('#trade_list').click(function() {
           $('#con_type').val('3');
            $('.title-list').html("Trade Contractor Listing");
        });
}

function callSweetAlert(){
    swal("Cancelled", "Successfully! :)", "error");
}

function changeButtonNameAfterDownloadFile(){

    $('.first-download').on('click', function(e){
        var link1 = $(this);
        var auth = $(this).data("auth");
        /*if(auth == false){
            return true;
        }*/
        e.preventDefault();

        var link = link1.attr("data-href");
        var price = link1.attr("data-price");
        var title = link1.attr("data-project");
        var file = link1.closest('tr').find('.file-name').text();

        //$('.border-blue .btn-cont a').attr('href', link);
        $('.border-blue .btn-cont a').attr('href', 'https://paypal.me/tbdata?locale.x=en_US');
        $('.border-blue .btn-cont .price').text(price);
        $('.border-blue .pro-title').text(title);
        $('.border-blue .file-name').text(file);

        $('.border-blue .btn-cont').on('click', function(){

            $(link1).html('Download File');
            $(link1).removeClass('first-download');
            $(link1).removeClass('active');
            //$(link1).addClass('btn-blue blue-button-bg');
            $('.close').trigger('click');

        });


        //swal({
        //    title: "Are you sure?",
        //    text: "Are you sure you want to buy all files of this project? Payment will be detected from your card!",
        //    icon: "warning",
        //    buttons: [
        //        'No, cancel it!',
        //        'Yes, I am sure!'
        //    ],
        //    dangerMode: true,
        //}).then(function(isConfirm) {
        //    if (isConfirm) {
        //        console.log('true');
        //        var link = link1.attr("href");
        //        window.location.href = link;
        //        $(link1).html('Download File');
        //        $(link1).removeClass('first-download');
        //        $(link1).removeClass('active btn-orange blue-orange-bg');
        //        $(link1).addClass('btn-blue blue-button-bg');
        //        $(link1).unbind('click');
        //
        //    } else {
        //        swal("Cancelled", "Successfully! :)", "error");
        //    }
        //});
    });


    $('.plans-modal-link').on('click', function(e) {
        var link1 = $(this);
       // var auth = $(this).data("auth");
        /*if(auth == false){
         return true;
         }*/
        e.preventDefault();

       // var link = link1.attr("data-href");
        var price = link1.attr("data-price");
        var title = link1.attr("data-project");
        var file = link1.closest('tr').find('.file-name').text();

        //$('.border-blue .btn-cont a').attr('href', link);
        $('.border-blue .btn-cont a').attr('href', 'https://paypal.me/tbdata?locale.x=en_US');
        $('.border-blue .btn-cont .price').text(price);
        $('.border-blue .pro-title').text(title);
        $('.border-blue .file-name').text(file);

    });


}

function fadeResults(){

    $('.conabc table.projects-list tbody tr').fadeOut(50, function () {
        $('.conabc table.projects-list tbody').html('');
    });


}

function showLoader(){
    $('.ajax-loader').removeClass('hide');
}

function hideLoader(){
    $('.ajax-loader').addClass('hide');
}

function  projectSpecificationDocs(){
    var args = {
        selector: '#dzSpecsDocs',
        uploadDocType : 'bid_doc',
        //acceptedFiles : '.pdf,.xls,.xlsx',
        acceptedFiles : '.pdf',
        url: $('.plan_documents').data('url'),
        hiddenField: $('.specification_docs_hidden')
    };

    if($(args.selector).is('*')) initializeDropZone(args);
}

function projectPlanDocs(){
    var args = {
        selector: '#dropzonePlanDocs',
        uploadDocType : 'plan_docs',
        //acceptedFiles : '.pdf,.xls,.xlsx',
        acceptedFiles : '.pdf',
        url: $('.plan_documents').data('url'),
        hiddenField: $('.design_upload_hidden')
    };

    if($(args.selector).is('*')) initializeDropZone(args);
}

function initializeDropZone(args){

    //if(!$(args.selector).is('*')) return false;

    try {
          console.log(args.url);

       // $('input[type="submit"]').hide();

        Dropzone.autoDiscover = false;
        var uploadDesign = args.hiddenField;
        var myDropzone = new Dropzone(args.selector, {
            url: args.url,
            init:function(){
                this.on('success', function(file, responseText){
                    var response = JSON.parse(responseText);
                    var fileuploded = file.previewElement.querySelector("[data-dz-name]");
                    fileuploded.innerHTML = response.message;
                    /*console.log('server');
                     console.log(fileuploded.innerHTML);*/
                    //file.name =
                });
            },
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },

            //paramName: "plan_doc",
            paramName: args.uploadDocType,
            //acceptedFiles: ".pdf",
            acceptedFiles: args.acceptedFiles,
            previewTemplate: $('#preview-template').html(),
            thumbnailHeight: 120,
            thumbnailWidth: 120,
            maxFiles: (args.selector == '#dropzoneGrabDocs') ? 1 : 500,
            maxFilesize: 20,
            addRemoveLinks: true,
            dictRemoveFile: 'Remove',
            dictDefaultMessage: '<span class="bigger-150 bolder"><i class="ace-icon fa fa-caret-right red"></i> Drop files</span> to upload \ <span class="smaller-80 grey">(or click)</span> <br /> \ <i class="upload-icon ace-icon fa fa-cloud-upload blue fa-3x"></i>',
            thumbnail: function (file, dataUrl) {
                // console.log(file);
                if (file.previewElement) {
                    $(file.previewElement).removeClass("dz-file-preview");
                    var images = $(file.previewElement).find("[data-dz-thumbnail]").each(function () {
                        var thumbnailElement = this;
                        thumbnailElement.alt = file.id;
                        thumbnailElement.src = dataUrl;
                    });
                    setTimeout(function () {
                        $(file.previewElement).addClass("dz-image-preview");
                    }, 1);
                }
            }

        });

        myDropzone.on("queuecomplete", function (multiple, i) {
            $('input[type="submit"]').show();
            var designUpload = [];
            jQuery(myDropzone.files).each(function (e, i) {
                var updatedName = i.previewElement.querySelector("[data-dz-name]").innerHTML;
                //i.previewElement.id = i.name;
                i.previewElement.id = updatedName;
                //if (i.accepted) designUpload.push(i.name);
                if (i.accepted) designUpload.push(updatedName);
            });
            uploadDesign.val(designUpload);

            if(args.reload) window.location.reload();
        });

        myDropzone.on('removedfile', function (file) {
            var valArr = uploadDesign.val().split(',');
            var index = valArr.indexOf(file.previewElement.id);
            if (index > -1) valArr.splice(index, 1);
            uploadDesign.val(valArr);
        });

        //  console.log(myDropzone);

    } catch (e) {
        console.log(e);
        //alert('Dropzone.js does not support older browsers!');
    }
}

function ajaxCallOnSearch(){

    $('.bg-sel .search-select').change(function(){

        fadeResults();
        showLoader();

        var form = $(".searchForm");
        var url = form.attr('data-ajax');
        removeJsPagination();
        $.ajax({
            type: "POST",
            url: url,
            data: form.serialize(),
            success: function(data)
            {

                //console.log(data.length);
                if(data.length == 0){
                    $('.no-data').removeClass('hide');
                    $('.conabc table.projects-list tbody').html('<h4 class="no-data">No Result Found!</h4>');
                    //return;
                }else{

                    $('.no-data').remove();
                    $.each(data, function(i, v){

                        var cloneHtml = $('.clone-table tr').clone().removeClass('hide');
                        var sidebg = ((i  % 2 == 0)?'white-list-back':'grey-list-back');
                        cloneHtml.find('.side-bg').removeAttr('class').addClass(sidebg);
                        cloneHtml.find('.project-title a').text(v.title);
                        cloneHtml.find('.project-title a').attr('href', 'productdetail/' +  v.id);
                        cloneHtml.find('.project-details a').attr('href', 'productdetail/' +  v.id);
                        cloneHtml.find('.location span').text(v.location);
                        cloneHtml.find('.bid-date span').text(v.bid_date);
                        cloneHtml.find('.project-description').text(v.description);
                        cloneHtml.find('.amount-itself').text(v.bid_amount);

                        $('.conabc table.projects-list tbody').append(cloneHtml);

                    });

                }


                hideLoader();
                jsPagination();
            }
        });




    });

}


function removeJsPagination(){
    $('.pagination').remove();
}


$(function(){
    $('div.dropdown').click(toggleSubItems);
});


//jQuery(document).ready(function(){
//    $('div.dropdown').trigger('click');
//});
function toggleSubItems(){


    $('div.dropdown-content').not($(this).children('div.dropdown-content')).hide();
    $(this).children('div.dropdown-content').toggle(400);
}

function initializeOwlCarousel(){
    $('.owl-carousel').owlCarousel({
        "singleItem": false,
        "autoPlay": true,
        "items":7,
        "itemsDesktop":[1199,7],
        "itemsDesktopSmall":[980,5],
        "itemsTablet":[768,4],
        "itemsTabletSmall":false,
        "itemsMobile":[479,2],
        "pagination": false
    });
}

function tabClicksHandle(){

    var link1 = window.location.href;
    var link = link1.split('#');
   // console.log(link[1]);
    if(typeof link[1] != 'undefined') {

        $(".tablinks").each(function (i, e) {

            var href = $(this).attr('href');
            if ('#' + link[1] === href) {

             //   console.log(document.cookie);

              //  document.cookie = "selectedtab=" + link1;

                e.click();
                $(this).addClass('active');
            }
        });

    }else{
        if( $('#defaultOpen').length ){
            document.getElementById("defaultOpen").click();
        }
    }

}

function changeIconOnCollapse(){
    $('#demo').on('shown.bs.collapse', function() {
        $(".drop-col-icon").addClass('fa-chevron-up').removeClass('fa-chevron-down');
    });

    $('#demo').on('hidden.bs.collapse', function() {
        $(".drop-col-icon").addClass('fa-chevron-down').removeClass('fa-chevron-up');
    });
}

function searchSideBarScroll(){
    jQuery(document).scroll(function() {

        if($(document).scrollTop() >= 10){
            $('.navbar').addClass('affix');
        }else{
            $('.navbar').removeClass('affix');
        }

        if($('.search-page').is('*') && $(window).width() > 700) {

            var $projectFormElem = $('.projectSearchFrom');
            var $footerElem = $('.footer');

            if ($($projectFormElem).offset().top + $($projectFormElem).height() >= $($footerElem).offset().top - 10) {
                $($projectFormElem).removeClass('affix');
            } else {

            }

            //$($projectFormElem).css('top', 'unset');
            if ($(document).scrollTop() + window.innerHeight < $($footerElem).offset().top) {
                $($projectFormElem).addClass('affix'); // restore when you scroll up
                $($projectFormElem).css('top', '80px');
            } else {
                $($projectFormElem).css('top', '-79px');
            }

            if($(document).scrollTop() <= 10){
                $('.projectSearchFrom').removeClass('affix');
            }
        }
    });
}

function searchPaginationScrollTop(){
    $(document).on('click','.search-page .pagination a', function(){
        $("html, body").animate({ scrollTop: 0 }, "slow");
    })
}

jQuery(window).on('load', function() {
    jQuery('.gooey').fadeOut('slow');
    //jQuery('.tbd-header').removeClass('hide');
    jQuery('.page-content-tbd').fadeIn('500', function(){
        $(this).removeClass('hide');
        jQuery('.footer').fadeIn('500').removeClass('hide');
        jQuery('body.tbd-frontend').removeClass('overflow-hide');
    });

});
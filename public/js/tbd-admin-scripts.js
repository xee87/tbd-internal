/**
 * Created by smart comp on 5/30/2019.
 */
if (typeof jQuery !== 'undefined')(function( window, document, $, undefined ){

    "use strict";

    var
        self = '',
        selectedId = [],

        TBD = {

            init: function(){

                if(window.innerWidth < 992){
                    $('body').addClass('sidebar-collapse');
                }
                self = this;
                self.selectedId = [];
                //self.initializeDropZone();
                self.projectPlanDocs();
                self.projectSpecificationDocs();
                self.projectQTODocs();
                self.divisionsEditForm();
                self.projectStatusModal();
                self.projectAddendumDocs();
                self.dateTimePicker();
                self.deleteSelectedPH();
                self.saveSolicitation();
                self.inputMasking();
                self.deleteBidRecord();
                self.viewUploadVideo();
                self.loadIntroVideoModal();
                self.projectGrabDocs();


            },

            inputMasking : function(){
                $('.usPhoneFax').inputmask({
                    "mask" : "(999) 999-9999"
                })
            },

            divisionsEditForm: function(){
                $('.editDivision').on('click', function(){
                    var id = $(this).data('id');
                    var name = $(this).data('name');
                     $('.division-form .divisionName').val(name);
                     $('.division-form .divisionId').val(id);
                    $('.modal-title').text('Edit Division');
                    $('.division-form .modal-footer .btn-primary').text('Update');
                });
            },

            dateTimePicker: function () {
                $.datetimepicker.setLocale('en');
                $('.datetimepicker').datetimepicker({
                    format:'Y/m/d H:i',
                    step: 15
                });
            },

            setPlanHolderPrice: function(){
                $('.selectedPH').unbind('change').on('change', function(){

                    var phID = $(this).val();

                    if(!self.selectedId.includes(phID) && !phID == ''){
                        self.selectedId.push(phID);

                        var phName = $('.selectedPH option:selected').text();
                        var html = $(this).closest('.modal-content').find('.htmlLayout').clone();

                        html.removeClass('hide');
                        html.removeClass('htmlLayout');
                        html.find('.ph_name').html(phName);
                        html.find('.ph_id').val(phID);
                        html.find('.priceField').val(1);
                        $('.ph_records').append(html);
                    }
                });

            },

            saveSolicitation: function(){
                $('.awardedSolicitation').on('click', function(e){
                    e.preventDefault();
                    var formId = $(this).closest('.modal').attr('id');
                    $.ajax({
                        url: $(this).closest('form').attr('action'),
                        type: "POST",
                        data:$(this).closest('form').serialize(),
                        success: function (data) {
                            $('#' + formId).modal('toggle');
                            var response = JSON.parse(data);
                            $('#MyElement2').find('p').text(response.message + '. The page will refresh automatically. ');
                            $('#MyElement2').removeClass('hide');
                            $('#MyElement2').removeClass('fade');
                            $('html, body').animate({scrollTop:0}, 'slow');

                            setInterval(function(){
                                window.location.reload();
                            }, 3000);


                        },
                        error: function () {

                        }
                    });
                })
            },

            deleteSelectedPH : function(){
                $(document).find('.delete-planHolder').on('click', function(){
                    $(this).closest('.callout').remove();
                });
            },

            deleteBidRecord: function(){
                $(document).find('.delete_bid').on('click', function(){

                    var a = $(this);

                    var project_id = $(this).attr('data-projectID');
                    var ph_id = $(this).attr('data-phID');
                    var url = $(this).attr('data-url');
                  //  console.log(url);

                    $.ajax({
                        url: url,
                        type: "POST",
                        data:{projectID:project_id,ph_id:ph_id},
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        success: function (data) {
                           // console.log(data);
                            $('html, body').animate({scrollTop:0}, 'slow');
                            $(a).closest('.toBeDeleted').remove();
                            $('#MyElement2').find('p').text('Plan holder deleted successfully!');
                            $('#MyElement2').removeClass('hide');
                            $('#MyElement2').removeClass('fade');
                            //$('#awarded').modal('toggle');
                            //window.setTimeout(function(){location.reload()},3000)
                        },
                        error: function () {
                            response.log('failed');
                        }
                    });
                });
            },

            loadIntroVideoModal: function(){
               /* $('.video-embed-tbd').on('click', function(){
                    $('#intro-video-modal').modal('show');
                });*/
            },

            projectStatusModal: function(){
                $('.getAwardedInfo').on('change', function(){
                    //console.log($(this).data('modal'));
                    if($(this).val() == 'Awarded Solicitation'){

                        //if($('#closed').data('awarded')){
                        //    $('#closed').modal('toggle');
                        //}else{

                        $('#awarded').modal('toggle');


                    }
                    if($(this).val() == "Closed Solicitation"){
                        $('#closed').modal('toggle');
                    }
                    //$(this).toggleClass( "modal" );
                });

                self.setPlanHolderPrice();
            },

            projectSpecificationDocs: function(){
                var args = {
                    selector: '#dzSpecsDocs',
                    uploadDocType : 'bid_doc',
                    //acceptedFiles : '.pdf,.xls,.xlsx',
                    acceptedFiles : '.pdf',
                    url: $('.plan_documents').data('url'),
                    hiddenField: $('.specification_docs_hidden')
                };

                if($(args.selector).is('*')) self.initializeDropZone(args);
            },

            projectPlanDocs: function(){
                var args = {
                    selector: '#dropzonePlanDocs',
                    uploadDocType : 'plan_docs',
                    //acceptedFiles : '.pdf,.xls,.xlsx',
                    acceptedFiles : '.pdf',
                    url: $('.plan_documents').data('url'),
                    hiddenField: $('.design_upload_hidden')
                };

                if($(args.selector).is('*')) self.initializeDropZone(args);
            },

            projectGrabDocs: function(){
                var args = {
                    selector: '#dropzoneGrabDocs',
                    uploadDocType : 'plan_docs',
                    //acceptedFiles : '.pdf,.xls,.xlsx',
                    acceptedFiles : '.xls,.xlsx',
                    maxFiles: 1,
                    url: $('.plan_documents').data('url'),
                    hiddenField: $('.grab_upload_hidden')
                };

                if($(args.selector).is('*')) self.initializeDropZone(args);
            },
            
            viewUploadVideo: function(){

                var videolink = $('#videoCode').val();
                $(".video-play").html(videolink);


                $('#videoCode').keyup(function(event) {
                    var videolink1 = $('textarea#videoCode').val();
                    $(".video-play").html(videolink1);
                });
               // $(".video-play").append(videolink);
            },

            projectQTODocs: function(){
                var args = {
                    selector: '#dropzoneQTODocs',
                    uploadDocType : 'file',
                    acceptedFiles : '.xls,.xlsx',
                    url: $('.qto_documents').data('url'),
                    hiddenField: $('.qto_docs_hidden'),
                    reload: true
                };

                if($(args.selector).is('*')) self.initializeDropZone(args);
            },

            projectAddendumDocs: function(){
                var args = {
                    selector: '#dropzoneAddendumDocs',
                    uploadDocType : 'plan_docs',
                    acceptedFiles : '.pdf',
                    url: $('.plan_documents').data('url'),

                    hiddenField: $('.design_upload_hidden'),
                    reload: false
                };
              //  console.log(args);

                if($(args.selector).is('*')) self.initializeDropZone(args);
            },

            initializeDropZone: function(args){

                //if(!$(args.selector).is('*')) return false;

                try {
                  //  console.log(args.selector);
                    
                    $('input[type="submit"]').hide();
                    
                    Dropzone.autoDiscover = false;
                    var uploadDesign = args.hiddenField;
                    var myDropzone = new Dropzone(args.selector, {
                        url: args.url,
                        init:function(){
                            this.on('success', function(file, responseText){
                                var response = JSON.parse(responseText);
                                var fileuploded = file.previewElement.querySelector("[data-dz-name]");
                                fileuploded.innerHTML = response.message;
                                /*console.log('server');
                                console.log(fileuploded.innerHTML);*/
                                //file.name =
                            });
                        },
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        
                        //paramName: "plan_doc",
                        paramName: args.uploadDocType,
                        //acceptedFiles: ".pdf",
                        acceptedFiles: args.acceptedFiles,
                        previewTemplate: $('#preview-template').html(),
                        thumbnailHeight: 120,
                        thumbnailWidth: 120,
                        maxFiles: (args.selector == '#dropzoneGrabDocs') ? 1 : 500,
                        maxFilesize: 20,
                        addRemoveLinks: true,
                        dictRemoveFile: 'Remove',
                        dictDefaultMessage: '<span class="bigger-150 bolder"><i class="ace-icon fa fa-caret-right red"></i> Drop files</span> to upload \ <span class="smaller-80 grey">(or click)</span> <br /> \ <i class="upload-icon ace-icon fa fa-cloud-upload blue fa-3x"></i>',
                        thumbnail: function (file, dataUrl) {
                           // console.log(file);
                            if (file.previewElement) {
                                $(file.previewElement).removeClass("dz-file-preview");
                                var images = $(file.previewElement).find("[data-dz-thumbnail]").each(function () {
                                    var thumbnailElement = this;
                                    thumbnailElement.alt = file.id;
                                    thumbnailElement.src = dataUrl;
                                });
                                setTimeout(function () {
                                    $(file.previewElement).addClass("dz-image-preview");
                                }, 1);
                            }
                        }

                    });

                    myDropzone.on("queuecomplete", function (multiple, i) {
                        $('input[type="submit"]').show();
                        var designUpload = [];
                        jQuery(myDropzone.files).each(function (e, i) {
                            var updatedName = i.previewElement.querySelector("[data-dz-name]").innerHTML;
                            //i.previewElement.id = i.name;
                            i.previewElement.id = updatedName;
                            //if (i.accepted) designUpload.push(i.name);
                            if (i.accepted) designUpload.push(updatedName);
                        });
                        uploadDesign.val(designUpload);

                        if(args.reload) window.location.reload();
                    });
                    
                    myDropzone.on('removedfile', function (file) {
                        var valArr = uploadDesign.val().split(',');
                        var index = valArr.indexOf(file.previewElement.id);
                        if (index > -1) valArr.splice(index, 1);
                        uploadDesign.val(valArr);
                    });
                    
                  //  console.log(myDropzone);
                    
                } catch (e) {
                    console.log(e);
                    //alert('Dropzone.js does not support older browsers!');
                }
            },

            randString: function(length) {
                var result           = '';
                var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
                var charactersLength = characters.length;
                for ( var i = 0; i < length; i++ ) {
                    result += characters.charAt(Math.floor(Math.random() * charactersLength));
                }
                return result;
            },

            submitData: function(args){

                var url = $(args).attr('action');
                var method = 'POST';

                $.ajax({
                    url: url,
                    type: method,
                    data:$(args).serialize(),
                    success: function (data) {
                    },
                    error: function () {

                    }
                });
            }
        };

    $(document).ready(function(){
        TBD.init();
    });

})( window, document, jQuery );

$(document).ready(function(){

   if($('.chart-container').is('*')){

       $(function () {
           'use strict'

           var ticksStyle = {
               fontColor: '#495057',
               fontStyle: 'bold'
           }

           var mode = 'index';
           var intersect = true;


           var $visitorsChart = $('#visitors-chart');
           var visitorsChart = new Chart($visitorsChart, {
               data: {
                   labels: [oned, twod, threed, fourd, fived, sixd, sevend],
                   datasets: [{
                       type: 'line',
                       data: [one, two, three, four, five, six, seven],
                       backgroundColor: 'transparent',
                       borderColor: '#007bff',
                       pointBorderColor: '#007bff',
                       pointBackgroundColor: '#007bff',
                       fill: false
                       // pointHoverBackgroundColor: '#007bff',
                       // pointHoverBorderColor    : '#007bff'
                   }]
               },
               options: {
                   maintainAspectRatio: false,
                   tooltips: {
                       mode: mode,
                       intersect: intersect
                   },
                   hover: {
                       mode: mode,
                       intersect: intersect
                   },
                   legend: {
                       display: false
                   },
                   scales: {
                       yAxes: [{
                           // display: false,
                           gridLines: {
                               display: true,
                               lineWidth: '4px',
                               color: 'rgba(0, 0, 0, .2)',
                               zeroLineColor: 'transparent'
                           },
                           ticks: $.extend({
                               beginAtZero: true,
                               suggestedMax: 200
                           }, ticksStyle)
                       }],
                       xAxes: [{
                           display: true,
                           gridLines: {
                               display: false
                           },
                           ticks: ticksStyle
                       }]
                   }
               }
           })
       });
   }
});


<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Mail;
use App\Mail\SendEmailMailable;

class SendEmailJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */

    public $info;
    public $subject;
    public $view;

    public function __construct()
    {
        //

    }

    /**
     * Execute the job.
     *
     * @return void
     */


    public function setValues($info, $view , $subject){
        $this->info = $info;
        $this->subject = $subject;
        $this->view = $view;
    }


    public function handle()
    {
        $info = $this->info;
        foreach($info as $user){

            $data['name'] = $user['name'];
            $data['id'] = $user['id'];

            if($user['email']){
                $email = new SendEmailMailable();
                $email->setMail($this->view, $data, $this->subject) ;
                Mail::to($user['email'])->queue($email);

                if(count( Mail::failures() ) > 0) {
                    $admin = new SendEmailMailable();
                    $admin->setMail('emails.notification_admin', $data, 'JOB FAILED NOTIFICATION');
                    Mail::to('admin@admin.com')->queue($admin);
                }

            }else{
                $admin = new SendEmailMailable();
                $admin->setMail('emails.notification_admin', $data, 'JOB FAILED NOTIFICATION');
                Mail::to('admin@admin.com')->queue($admin);
            }
        }
    }

    public function failed(\Exception $e = null)
    {
        //handle error
    }
}

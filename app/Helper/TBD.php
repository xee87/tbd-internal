<?php
/**
 * Created by PhpStorm.
 * User: smart comp
 * Date: 6/1/2019
 * Time: 2:56 PM
 */
namespace App\Helper;

use Illuminate\Support\Facades\DB;

class TBD{

    public static function getFileSize(string $string){

        if(is_file(public_path($string))){

            return self::formatSizeUnits(filesize($string));

        }else{
            return '--';
        }
        //return $string;
    }

    private static function formatSizeUnits($bytes)
    {
        if ($bytes >= 1073741824)
        {
            $bytes = number_format($bytes / 1073741824, 2) . ' GB';
        }
        elseif ($bytes >= 1048576)
        {
            $bytes = number_format($bytes / 1048576, 2) . ' MB';
        }
        elseif ($bytes >= 1024)
        {
            $bytes = number_format($bytes / 1024, 2) . ' KB';
        }
        elseif ($bytes > 1)
        {
            $bytes = $bytes . ' bytes';
        }
        elseif ($bytes == 1)
        {
            $bytes = $bytes . ' byte';
        }
        else
        {
            $bytes = '0 bytes';
        }

        return $bytes;
    }

    public static function number_format_short($n, $precision)
    {
        if ($n < 900) {
            // 0 - 900
            $n_format = number_format($n, $precision);
            $suffix = '';
        } else if ($n < 900000) {
            // 0.9k-850k
            $n_format = number_format($n / 1000, $precision);
            $suffix = 'K';
        } else if ($n < 900000000) {
            // 0.9m-850m
            $n_format = number_format($n / 1000000, $precision);
            $suffix = 'M';
        } else if ($n < 900000000000) {
            // 0.9b-850b
            $n_format = number_format($n / 1000000000, $precision);
            $suffix = 'B';
        } else {
            // 0.9t+
            $n_format = number_format($n / 1000000000000, $precision);
            $suffix = 'T';
        }
        // Remove unecessary zeroes after decimal. "1.0" -> "1"; "1.00" -> "1"
        // Intentionally does not affect partials, eg "1.50" -> "1.50"
        if ($precision > 0) {
            $dotzero = '.' . str_repeat('0', $precision);
            $n_format = str_replace($dotzero, '', $n_format);
        }
        return $n_format . $suffix;
    }


    public static function short_description($des){

        if(isset($des) && strlen($des) > 0){
            return (strlen($des) > 50) ? substr($des,0, 150) . '...' : $des;
        }else{
            return '';;
        }

    }

    public static function bytesToHuman($bytes){
        $units = ['B', 'KB', 'MB', 'GB', 'TB', 'PB'];

        for ($i = 0; $bytes > 1024; $i++) {
            $bytes /= 1024;
        }

        return round($bytes, 2) . ' ' . $units[$i];
    }


    public static function LoadDataFromDB($tableName, $columns, $keys = null, $where = []){
//        DB::enableQueryLog();
        $rawData = DB::table($tableName)
            ->select($columns)
            ->where($where)
            ->get()
            ->toArray();

        $returnData = array();
        if($columns[0] == '*' & $keys ==  null) return $rawData;

        foreach($rawData as $singleData){
            if(count($columns) == 2){
                $returnData[$singleData->{$columns[0]}] = $singleData->{$columns[1]};
            } else {
                if($keys == null){
                    $returnData[$singleData->{$columns[0]}] = $singleData;
                } else {
                    if(is_array($keys)){
                        $key = '';
                        foreach($keys as $keyIndex) {
                            if(strlen($key) > 0) $key .= "|";
                            $key .= $singleData->{$columns[$keyIndex]};
                        }
                        $returnData[$key] = $singleData;
                    } else {
                        $returnData[$singleData->{$columns[$keys]}] = $singleData;
                    }
                }
            }
        }

        return $returnData;
    }

    public static function isZipFileDownloaded($project_id){
        $alreadyDownloaded = self::LoadDataFromDB(
            'otp_files_download',
            ['*'],
            null,
            [
                'user_id' => auth()->user()->id,
                'file_id' => '-1',
                'project_id' => $project_id,
                'type' => 2
            ]
        );

        return (count($alreadyDownloaded) == 0) ? false : true;
    }

    public static function isSingleFileDownloaded($projectId, $fileName){

        if(!auth()->user()) return false;

        $alreadyDownloaded = TBD::LoadDataFromDB(
            'otp_files_download',
            ['*'],
            null,
            [
                'user_id' => auth()->user()->id,
                'file_id' => $fileName,
                'project_id' => $projectId,
                'type' => 1
            ]
        );

        return (count($alreadyDownloaded) == 0) ? false : true;
    }

    public static function formatDate($dateString){

        return date("M d, Y h:i a", strtotime($dateString));
    }

    public static function formatOnlyDate($dateString){

        return date("M d, Y", strtotime($dateString));
    }

    public static function allowAccess(){

        $isDEO = is_object(auth()->user()) ? auth()->user()->hasRole('DEO') : false;
        $isQTO = is_object(auth()->user()) ? auth()->user()->hasRole('QTO') : false;

        $showContent = ($isQTO || $isDEO) ?  false : true;
        return $showContent;
        //if(!$showContent) return redirect()->route('landing');

        /*var_dump($isQTO);
        var_dump($isDEO);*/

        /*var_dump($showContent);
        exit;*/

    }

    public static function GetQTOVideoIframe($args){

        if(is_null($args)) return 'No Video Configured';

        $url = $args;
        preg_match(
            '/[\?\&]v=([^\?\&]+)/',
            $url,
            $matches
        );

        if(isset($matches[1])){
            $ids = $matches[1];

            //set a custom width and height
            $width = '100%';
            $height = '360';

            //echo the embed code. You can even wrap it in a class
            return '<div class="youtube-article"><iframe class="dt-youtube" width="' .$width. '" height="'.$height.'" src="//www.youtube.com/embed/'.$ids.'" frameborder="0" allowfullscreen></iframe></div>';
        }else{
            return 'Invalid video url';
        }
    }

    public static function GetSolLabelView($args){
        $bidPhase = '';
        if($args == 'Project Solicitation'){
            $bidPhase = 'Open Solicitation';
        }else{
            $bidPhase = $args;
        }

        return $bidPhase;
    }

    public static function GetSolicitationLabel($args){

        $bidPhase = '';

        if($args == 'Open Solicitation'){
            $bidPhase = 'Open Solicitation';
        }elseif($args == 'Awarded Solicitation'){
            $bidPhase = 'Bid Results';
        }elseif($args == 'Closed Solicitation'){
            $bidPhase = 'Award';
        }elseif($args == 'Project Solicitation'){
            $bidPhase = 'Open Solicitation';
        }else{
            $bidPhase = $args;
        }

        return $bidPhase;
    }
}
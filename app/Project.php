<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    protected $fillable = [
        "title", "solicitation", "bid_amount", "location", "bid_method", "bid_date",
        "bid_phase", "csi_division", "completion_time", "bid_doc_url",'plan_doc', "notes",
        "pre_bid_date", "pre_bid_location", "category_id", "status","assign_qto","qto_upload",
        "liquidated_damages","b_bond","p_bond","pay_bond","bid_location","owner_id",
        "planholder_id","qto_price",'bidrange', "document_avail",'description', 'qto_video', 'addendum', 'grab_docs'
    ];

    protected $table ="projects";

//    public function category()
//    {
//        return $this->belongsTo('App\Category');
//    }
}

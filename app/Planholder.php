<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Planholder extends Model
{
    protected $fillable = [
        "company", "planholder_address", "planholder_contact",
        "planholder_fax", "planholder_email", "planholder_phone"
    ];
    
    protected $table ="planholder";
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Owner extends Model
{
    protected $fillable = [
        "owner_name", "owner_address", "owner_contact","owner_fax", "contact_name", "owner_email", "owner_phone"
    ];
    
    protected $table ="project_owner";
}

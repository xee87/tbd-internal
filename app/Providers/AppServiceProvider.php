<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Braintree_Configuration;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Braintree_Configuration::environment(env('BRAINTREE_ENV'));
        Braintree_Configuration::merchantId(env('BRAINTREE_MERCHANT_ID'));
        Braintree_Configuration::publicKey(env('BRAINTREE_PUBLIC_KEY'));
        Braintree_Configuration::privateKey(env('BRAINTREE_PRIVATE_KEY'));
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {

    }
}

//BRAINTREE_ENV=sandbox
//BRAINTREE_MERCHANT_ID=7twbdbk5bdb2wtbs
//BRAINTREE_PUBLIC_KEY=9h7tpnr73b83vftw
//BRAINTREE_PRIVATE_KEY=5dc0f4db9ec2475f206f905b0c2e540c
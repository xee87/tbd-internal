<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class User_downloads extends Model
{
    protected $fillable = [
        "user_id", "downloads", "downloads_used","max_amount",
    ];
    
  protected $table ="user_project";
}

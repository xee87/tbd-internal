<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class User_project_record extends Model
{
    protected $fillable = [
        "user_id", "project_id", "project_title", "file_name", 
    ];
    
  protected $table ="user_project_record";
}

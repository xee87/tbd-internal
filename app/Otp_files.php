<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Otp_files extends Model
{
    protected $fillable = [
        "project_id", "file_id", "user_id", "type"
    ];

    protected $table ="otp_files_download";
}

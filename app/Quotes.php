<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Quotes extends Model
{
    protected $fillable = [
        "name", "company","contact","email","address","no_drawings","targets","specification_docs","plan_doc",
    ];

    protected $table ="quotes";
}

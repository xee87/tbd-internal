<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Set_downloads extends Model
{
    protected $fillable = [
        "package", "downloads","max_rate",
    ];
    
  protected $table ="set_downloads";
}

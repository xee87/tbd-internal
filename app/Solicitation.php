<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Solicitation extends Model
{
	protected $fillable = [
		"project_id", "planholder_id", "price", "status", 'awarded'
	];

	protected $table = "project_solicitation";

}
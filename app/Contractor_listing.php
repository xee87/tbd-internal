<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contractor_listing extends Model
{
    protected $fillable = [
        "company_name", "address", "contact_name","email", "contact", "fax", "website", "divisions", "type"
    ];

    protected $table ="contractor_listing";
}

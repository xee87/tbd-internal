<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Save_search extends Model
{
    protected $fillable = [
        "user_id", "save_as","keyword","location","open","awarded","closed","pricefrom","priceto","date_from","date_to",
    ];
    
  protected $table ="save_search";
}

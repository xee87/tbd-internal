<?php

namespace App\Http\Controllers;
use App\Plan;
use App\Subscription;
use Illuminate\Http\Request;
use App\User;
use App\User_downloads;
use App\Set_downloads;
use DB;
use Mail;

class SettingsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
  


    public function setting_downloads()
    {
       $strprices = DB::select('SELECT cost FROM plans WHERE braintree_plan = "registration" ');

        // $results = DB::select('select plans.id, set_downloads.max_rate, set_downloads.downloads,plans.created_at ,plans.braintree_plan,plans.description , plans.slug ,plans.name , plans.cost FROM set_downloads LEFT JOIN plans ON set_downloads.package = plans.id;');

       $otp_rate = DB::select('SELECT * FROM  otp_rate ');
       
        return view('setting.index',compact('strprices','otp_rate'));
    }

    public function rp(Request $request)
    {

        DB::update('update plans set cost = '. $request->reg_price .' where braintree_plan = "registration"');
        
        return redirect()->route('setting_downloads')->with('alertone', 'Registration Price Updated Successfully!');
    }
    
    public function otp_sett(Request $request)
    {


        DB::update('update otp_rate set price_range = '. $request->pricerange .', otp_project = '. $request->otpproject .', otp_file = '. $request->otpfile .' where id ='. $request->otpid );
        
        return redirect()->route('setting_downloads')->with('alertone', 'One Time Payment Updated Successfully!');
    }

    
}

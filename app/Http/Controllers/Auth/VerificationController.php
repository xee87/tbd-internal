<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\VerifiesEmails;
use App\User;
use Symfony\Component\HttpFoundation\Request;
use Illuminate\Auth\Events\Verified;

class VerificationController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Email Verification Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling email verification for any
    | user that recently registered with the application. Emails may also
    | be re-sent if the user didn't receive the original email message.
    |
    */

    use VerifiesEmails;

    /**
     * Where to redirect users after verification.
     *
     * @var string
     */
    //protected $redirectTo = '/';
    protected $redirectTo = '/dashboard';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
        $this->middleware('signed')->only('verify');
        $this->middleware('throttle:6,1')->only('verify', 'resend');
    }

    public function verify(Request $request)
    {
        $user = User::find($request->id);

        if ($request->route('id') != $user->getKey()) {
            throw new AuthorizationException;
        }

        if ($user->markEmailAsVerified()) {
            $user->status =1;
            $user->update();

            //event(new Verified($user));
        }

        //return redirect($this->redirectPath())->with('verified', true);
        return redirect()
            ->route('user_l')
            ->with(
                'alertone',
                'Verification successful, please login to proceed further.'
            );
    }


    /*public function verify(Request $request)
    {
        $userId = $request->route('id');
        var_dump($userId);
        $user = User::findOrFail($userId);
        var_dump($user);

        exit;

        if ($user->markEmailAsVerified()) {

            $user->status =1;
            $user->update();

            event(new Verified($user));
        }


        if ($request->user()->markEmailAsVerified()) {
            event(new Verified($request->user()));
        }

        return redirect($this->redirectPath())->with('verified', true);
    }*/
}

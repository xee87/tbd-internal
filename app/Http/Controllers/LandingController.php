<?php

namespace App\Http\Controllers;

use App\Helper\TBD;
use App\Otp_files;
use App\Project;
use App\Quotes;
use App\Solicitation;
use App\User;
use App\Owner;
use App\Category;
use App\Planholder;
use App\Division;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Response;
use App\User_downloads;
use App\User_project_record;
use App\Subscription;
use App\Save_search;
use App\User_otp;
use Mail;
use Visitor;
use Zipper;
use Session;
use Cookie;
use App\Mail\SendEmailMailable;
use DateTime;



class LandingController extends Controller
{
    public function index2(){

        return view('welcome2');
    }

    public function newsubscrption(){

        return view('new-subscriptions');
    }
    public function index()
    {
        Visitor::log();
        $divisions = Division::where('status', '!=', '0')->get();
        return view('welcome', compact('divisions'));
    }

    public function contactmail(Request $request){

        //var_dump(htmlspecialchars($request->c_message)); exit;

        $data['name'] = $request->c_name;
        $data['email'] = $request->c_email;
        $data['phone'] = $request->c_contact;
        $data['message1'] = $request->c_message;

        $sendMail = new SendEmailMailable();
        $sendMail->setMail('emails.contact_us', $data, $request->c_subject);
        Mail::to(env('MAIL_FROM_ADDRESS'))->send($sendMail);
        $successMsg = 'sent';
        return view('contactus', compact('successMsg'));
    }

    public function quotes(){
        return view('quotes');
    }

    public function save_quotes(Request $request){

        $q = Quotes::create($request->all());

        $data['name'] = $q->name;
        $data['company'] = $q->company;
        $data['email'] = $q->email;
        $data['contact'] = $q->contact;
        $data['id'] = $q->id;

        $sendMail = new SendEmailMailable();
        $sendMail->setMail('emails.quote', $data, 'RECEIVED QUOTE');
        Mail::to(env('MAIL_FROM_ADDRESS'))->send($sendMail);

        return redirect()->back()->with('msg', 'The Message');
    }

    public function all_quotes(){
        $quotes = Quotes::all();
        return view('quotes.index', compact('quotes'));
    }

    public function view_quotes(Quotes $id){

        $q = $id;
        return view('quotes.view', compact('q'));
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function search()
    {

        //var_dump(TBD::allowAccess());exit;
        if (!TBD::allowAccess()) {
            return redirect()->route('landing');
        }

        $savesearch = [];
        $solcount = [];

        if (!empty(auth()->user()->id)) {
            $savesearch = Save_search::where('user_id', auth()->user()->id)->get();
        }


        $solcount[0] = Project::where('status', '=', 'Active')->where('bid_phase', 'Awarded Solicitation')->count();

        $solcount[1] = Project::where('status', '=', 'Active')->where('bid_phase', 'Open Solicitation')->count();

        $solcount[2] = Project::where('status', '=', 'Active')->where('bid_phase', 'Closed Solicitation')->count();


        return view('search', compact('savesearch', 'solcount'));

    }

    public function search_query(Request $request){

        $builder = Project::query()->where('status', "Active");


        if (!empty($request->p_title)) {

            $builder->where('title', 'like', "%{$request->p_title}%");
        }

        if (!empty($request->location)) {

            $builder->where('location', 'like', "%{$request->location}%");
        }

        if (!empty($request->bid_from) && !empty($request->bid_to)) {
            $intone = (int)str_replace(',', '', $request->bid_from);
            $inttwo = (int)str_replace(',', '', $request->bid_to);
            $builder->whereBetween('bid_amount', array($intone, $inttwo));
        }

        if (!empty($request->date_from) && !empty($request->date_to)) {
            $intdateone = $request->date_from;
            $intdatetwo = $request->date_to;
            $builder->whereBetween('bid_date', array($intdateone, $intdatetwo));
        }

        if (!empty($request->cbox)) {
            $strcbox1 = $request->cbox;
            $builder->where('bid_phase', 'like', "%{$request->cbox}%");
        }

        if(!empty($request->sorting)){
            $sorting = explode('-',$request->sorting);
            $builder->orderBy($sorting[0], $sorting[1]);
        }else{
            $builder->orderBy('bid_date', 'desc');
        }

        return $projects = $builder->get();
    }


    public function ajax_search(Request $request){

        $projects = $this->search_query($request);

        foreach($projects as $key => $pro){

              $projects[$key]->bid_amount =  TBD::number_format_short($pro->bid_amount, 1);
              $projects[$key]->description =  TBD::short_description($pro->description);
              $date = new DateTime($pro->bid_date);
              $projects[$key]->bid_date = $date->format('M d, Y');
        }


        return response()->json($projects);

    }

    public function search_filter(Request $request, $saved = 0)
    {

        $searched = $request;
        $projects = $this->search_query($request);

        if (!TBD::allowAccess()) {
           // return redirect()->route('landing');
        }
      


        $savesearch = [];
        $solcount = [];
        if (!empty(auth()->user()->id)) {
            $savesearch = Save_search::where('user_id', auth()->user()->id)->get();
        }


        $solcount[0] = Project::where('status', '=', 'Active')->where('bid_phase', 'Awarded Solicitation')->count();

        $solcount[1] = Project::where('status', '=', 'Active')->where('bid_phase', 'Open Solicitation')->count();

        $solcount[2] = Project::where('status', '=', 'Active')->where('bid_phase', 'Closed Solicitation')->count();


        if($saved == 1){
            $alertone = 'Search Saved Successfully!';
            return view('search', compact('projects', 'savesearch', 'solcount', 'searched', 'alertone'));
        }else{
            return view('search', compact('projects', 'savesearch', 'solcount', 'searched'));
        }


    }

    public function s_save_filter(Request $request)
    {
        if (!TBD::allowAccess()) {
            return redirect()->route('landing');
        }

        $checkradio1 = 0;
        $checkradio2 = 0;
        $checkradio3 = 0;
        if (!empty($request->cbox)) {

            if ($request->cbox == '1') {
                $checkradio1 = 1;
                $request->cbox = 'Open Solicitation';
            } else if ($request->cbox == '2') {
                $checkradio2 = 1;
                $request->cbox = 'Awarded Solicitation';
            } else {
                $checkradio3 = 1;
                $request->cbox = 'Closed Solicitation';

            }

        }


        $ssl = $request->location;
        $bssl = str_replace(',', '--', $ssl);

        Save_search::create([
            'user_id' => auth()->user()->id,
            'save_as' => $request->save_search,
            'keyword' => $request->p_title,
            'location' => $bssl,

            'open' => $checkradio1,
            'awarded' => $checkradio2,
            'closed' => $checkradio3,

            'pricefrom' => $request->bid_from,
            'priceto' => $request->bid_to,
            'date_from' => $request->date_from,
            'date_to' => $request->date_to,
        ]);


//        Save_search::create([
//            'user_id' => auth()->user()->id,
//            'save_as' => $request->save_search,
//            'keyword' => $request->save_search_title,
//            'location' => $bssl,
//
//            'open' => $checkradio1,
//            'awarded' => $checkradio2,
//            'closed' => $checkradio3,
//
//            'pricefrom' => $request->save_search_form,
//            'priceto' => $request->save_search_to,
//            'date_from' => $request->save_search_date_form,
//            'date_to' => $request->save_search_date_to,
//        ]);


        return $this->search_filter($request, 1);

       // return view('search', compact('searched'));

        //return redirect()->route('search')->with('alertone', 'Search Saved Successfully!');
    }

    public function user_record()
    {

        if (!TBD::allowAccess()) {
            return redirect()->route('landing');
        }

        $user = User_downloads::where("user_id", auth()->user()->id)->select('downloads', 'downloads_used')->get();

        $user_project = User_project_record::where("user_id", auth()->user()->id)->select('project_id', 'file_name', 'project_title', 'created_at')->get();

        $non_subscribed = Otp_files::where('user_id', auth()->user()->id)->get();
       // var_dump($non_subscribed); exit;

        $singleDownloaded = array();
        if($non_subscribed){
            foreach($non_subscribed as $file){
               $p = Project::where('id', $file->project_id)->first();
               // var_dump($p); exit;
                $qtofile = '';
                if($p){
                    $qtofile = json_decode($p->qto_upload)[$file->file_id];
                    $singleDownloaded[] = (object)array('project_title' => $p->title, 'file_name' => explode('.',$qtofile)[1], 'created_at' => $file->download_date);
                }
            }
        }

        $d = $user->pop();

       // var_dump($singleDownlaoded); exit;

        return view('user_record', compact('d', 'user_project', 'singleDownloaded'));
    }

    public function project_detail(Project $id)
    {

        $otp_rate = DB::select('SELECT * FROM  otp_rate ');
        $userprojectrecord = [];

        if ($id->bid_amount <= $otp_rate[0]->price_range) {

            $checkprice = $otp_rate[0]->otp_project;

            $checkfileprice = $otp_rate[0]->otp_file;

        } else if ($id->bid_amount <= $otp_rate[1]->price_range) {

            $checkprice = $otp_rate[1]->otp_project;
            $checkfileprice = $otp_rate[1]->otp_file;

        } else if ($id->bid_amount <= $otp_rate[2]->price_range) {

            $checkprice = $otp_rate[2]->otp_project;
            $checkfileprice = $otp_rate[2]->otp_file;

        } else {
            $checkprice = 50;
            $checkfileprice = 10;
        }


        $project = $id;

        $arrayphinfo = array();

        $powner = Owner::where('id', '=', $project->owner_id)->first();


        $phi = json_decode($project->planholder_id);

        if (!empty($phi)) {

            foreach ($phi as $strphi) {

                $phinfo = Planholder::where('id', '=', $strphi)->first();

                array_push($arrayphinfo, $phinfo);

            }
        }
        $planHolderDetails = $solicitations = [];

        if($project->bid_phase <> 'Open Solicitation'){
            $solicitations = Solicitation::where('project_id', '=' , $project->id)->get();
        }

        $planHolders = [];
        foreach($solicitations as $k => $data){
            $planHolders[] = $data->planholder_id;
        }

        if(count($planHolders) > 0){
            $planHolderDetails = DB::select("SELECT * FROM planholder where id IN (". implode(',', $planHolders ).")");
        }

        $categoryproject = Category::where('id', $id->category_id)->first();

        $isZipDownloaded = false; // by default it should be false
        $isQTOFileDownloaded = false;
        if (!empty(auth()->user())) {
            $userprojectrecord = User_downloads::where("user_id", auth()->user()->id)->first();
            $isZipDownloaded = TBD::isZipFileDownloaded($id->id);
            $isQTOFileDownloaded =  Otp_files::where('user_id', auth()->user()->id)->where('project_id', $id->id)->exists();
        }


        //var_dump($QTOFileDownloaded); exit;

//        if(Cookie::get('redirect_cookie')){
//
//        }

//        if(!empty(Cookie::get('redirect_cookie'))){

            Cookie::queue('redirect_cookie', route('pdetail', ['id' => $id]), 15);
//        }



        return view('product_detail', compact('project', 'checkprice', 'checkfileprice', 'powner', 'arrayphinfo', 'categoryproject', 'userprojectrecord', 'isZipDownloaded', 'solicitations', 'planHolderDetails', 'isQTOFileDownloaded'));

    }

    public function project_download(Project $id, $key, $proamount)
    {

        if (!TBD::allowAccess()) {
            return redirect()->route('landing');
        }

        $userstwo = DB::table('users')->select('name', 'email')->where('id', auth()->user()->id)->first();

        $userotp = User_otp::where('user_id', auth()->user()->id)->where('project_id', $id->id)->where('status', 'all')->first();

        if ($userotp) {


            if ($userotp->project_download == 'all' && $userotp->status == 'all') {

                $downloadqto = json_decode($id->qto_upload);

                $dnew = explode("/", $downloadqto[$key]);


                User_project_record::create([
                    "user_id" => auth()->user()->id,
                    "project_id" => $id->id,
                    "project_title" => $id->title,
                    "file_name" => $dnew[1],

                ]);


                return Storage::download($downloadqto[$key]);

            } else if ($userotp->project_download == 'single' && $userotp->status == 'all') {


                $downloadqto = json_decode($id->qto_upload);

                $dnew = explode("/", $downloadqto[$key]);


                User_project_record::create([
                    "user_id" => auth()->user()->id,
                    "project_id" => $id->id,
                    "project_title" => $id->title,
                    "file_name" => $dnew[1],

                ]);

                User_otp::where('id', $userotp->id)->update([
                    "status" => 'cancel',]);


                return Storage::download($downloadqto[$key]);


            } else {


            }

        }

        $user = User_downloads::where("user_id", auth()->user()->id)->select('downloads', 'downloads_used', 'max_amount')->get();


        $d = $user->pop();


        if (auth()->user()->subscribed('main')) {


            if (!empty($d)) {


                if ($proamount <= $d->max_amount) {


                    if ($d->downloads > $d->downloads_used || $d->downloads == -1) {


                        $downloadqto = json_decode($id->qto_upload);

                        $dnew = explode("/", $downloadqto[$key]);


                        // $userdownloadfile = User_project_record::where('file_name', '=', $dnew[1])->where('user_id', '=', auth()->user()->id)->first();

                        $strupr = User_project_record::where('user_id', '=', auth()->user()->id)->where('project_id', '=', $id->id)->first();

                        if ($strupr === null) {
                            User_downloads::where("user_id", auth()->user()->id)->increment('downloads_used');
                        }


                        User_project_record::create([
                            "user_id" => auth()->user()->id,
                            "project_id" => $id->id,
                            "project_title" => $id->title,
                            "file_name" => $dnew[1],

                        ]);


                        return Storage::download($downloadqto[$key]);
                    } else {

                        if (env('MAIL_SEND')) {

                            $contactName = $userstwo->name;
                            $contactEmail = $userstwo->email;

                            $data = array('name' => $userstwo->name);
                            Mail::send('emails.dowloadmail', $data, function ($message) use ($contactEmail, $contactName) {
                                $message->to($contactEmail, $contactName)->subject
                                ('Download Limit');

                            });
                        }
                        return redirect()->route('plans');
                    }

                } else {

                    //exit;
                    return redirect()->route('plans')->with('checkplan', $proamount);
                }

            } else {
                if (env('MAIL_SEND')) {
                    $contactName = $userstwo->name;
                    $contactEmail = $userstwo->email;


                    $data = array('name' => $userstwo->name);
                    Mail::send('emails.dowloadmail', $data, function ($message) use ($contactEmail, $contactName) {
                        $message->to($contactEmail, $contactName)->subject
                        ('Download Limit');

                    });
                }
                return redirect()->route('plans');
            }

        } else {

            return redirect()->route('plans');
        }
    }

    public function aboutus()
    {

        return view('aboutus');

    }


    public function sampleproject(){

        return view('sample-project');

    }

    public function coveragearea()
    {

        return view('coveragearea');


    }

    public function contactus()
    {

        return view('contactus');


    }

    public function grab_all_files(Project $id, $docs){
        $alldocs = explode(',',$id->$docs);

        $headers = ["Content-Type" => "application/zip"];
        $fileName = "file" . uniqid() . ".zip";

        foreach ($alldocs as $strdownloadqto){
            if(is_file(public_path('bid_uploads/' . $strdownloadqto))) {
                $mypath = $strdownloadqto;
                Zipper::make(public_path('/documents/' . $fileName))
                    ->add(public_path('bid_uploads/' . $mypath))->close();
            }
        }

        if(is_file(public_path('/documents/' . $fileName))){
            return response()->download(public_path('/documents/' . $fileName), $fileName, $headers)->deleteFileAfterSend(true);
        }

    }

    public function otp_set(Project $id)
    {


        $otp_rate = DB::select('SELECT * FROM  otp_rate ');

        $user = User::where('id', '=', auth()->user()->id)->first();

        /*$alreadyDownloaded = TBD::LoadDataFromDB(
                'otp_files_download',
                ['*'],
                null,
                [
                    'user_id' => auth()->user()->id,
                    'file_id' => '-1',
                    'project_id' => $id->id,
                    'type' => 2
                ]
        );*/

        $alreadyDownloaded = TBD::isZipFileDownloaded($id->id);

        if (!$alreadyDownloaded) {


            if ($id->bid_amount <= $otp_rate[0]->price_range) {
                $checkprice = $user->charge($otp_rate[0]->otp_project);

            } else if ($id->bid_amount <= $otp_rate[1]->price_range) {
                $checkprice = $user->charge($otp_rate[1]->otp_project);
            } else if ($id->bid_amount <= $otp_rate[2]->price_range) {

                $checkprice = $user->charge($otp_rate[2]->otp_project);

            } else {
                $checkprice = $user->charge(50);
            }


            if ($checkprice) {
                User_otp::create([
                    "user_id" => auth()->user()->id,
                    "project_id" => $id->id,
                    "project_download" => 'all',
                    "status" => 'cancel',

                ]);
            }

            Otp_files::create([
                "user_id" => auth()->user()->id,
                "project_id" => $id->id,
                "file_id" => '-1',
                'type' => 2
            ]); // to track the single downloads of user

        }

        $headers = ["Content-Type" => "application/zip"];
        $fileName = "file" . time() . ".zip";


        $downloadqto = json_decode($id->qto_upload);

        foreach ($downloadqto as $strdownloadqto) {


            $mypath = $strdownloadqto;


            $files = glob(public_path('js/*'));

            Zipper::make(public_path('/documents/' . $fileName))
                ->add(storage_path('/app/' . $mypath))->close();


            $dnew = explode("/", $strdownloadqto);


            $strupr = User_project_record::where('user_id', '=', auth()->user()->id)->where('project_id', '=', $id->id)->first();

            if ($strupr === null) {
                User_downloads::where("user_id", auth()->user()->id)->increment('downloads_used');
            }


            User_project_record::create([
                "user_id" => auth()->user()->id,
                "project_id" => $id->id,
                "project_title" => $id->title,
                "file_name" => $dnew[1],

            ]);


        }


        return response()->download(public_path('/documents/' . $fileName), $fileName, $headers);


        // return Storage::download($id->qto_upload);


        // return redirect()->route('pdetail', $id->id)->with('alertone', 'Payment successfully done! Now you can download all files of this project');

    }

    public function otp_set_single(Project $id, $key, $qtoprice)
    {
        //$otp_rate = DB::select('SELECT * FROM  otp_rate ');

        if(!auth()->user()){
            return view('user_login');
        }

        $user = User::where('id', '=', auth()->user()->id)->first();

        // if ($id->bid_amount <= $otp_rate[0]->price_range) {
        // 	$checkprice = $user->charge($otp_rate[0]->otp_file);
        // } else if ($id->bid_amount <= $otp_rate[1]->price_range) {
        // 	$checkprice = $user->charge($otp_rate[1]->otp_file);
        // } else if ($id->bid_amount <= $otp_rate[2]->price_range) {
        // 	$checkprice = $user->charge($otp_rate[2]->otp_file);
        // } else {
        // 	$checkprice = $user->charge(10);
        // }
        $downloadqto = json_decode($id->qto_upload);

        /*$alreadyDownloaded = TBD::LoadDataFromDB(
                'otp_files_download',
                ['*'],
                null,
                [
                    'user_id' => auth()->user()->id,
                    'file_id' => $downloadqto[$key],
                    'project_id' => $id->id,
                    'type' => 1
                ]
        );*/

        $alreadyDownloaded = TBD::isSingleFileDownloaded($id->id, $key);

        if (!$alreadyDownloaded) {

            $checkprice = $user->charge($qtoprice);


            if ($checkprice) {

                $strupr = User_project_record::where('user_id', '=', auth()->user()->id)
                    ->where('project_id', '=', $id->id)
                    ->first();

                if ($strupr === null) {
                    User_downloads::where("user_id", auth()->user()->id)->increment('downloads_used');
                }

                User_otp::create([
                    "user_id" => auth()->user()->id,
                    "project_id" => $id->id,
                    "project_download" => 'single',
                    "status" => 'cancel',
                ]);

                Otp_files::create([

                    "user_id" => auth()->user()->id,
                    "project_id" => $id->id,
                    "file_id" => $key,
                    'type' => 1
                ]); // to track the single downloads of user

            } else {
                return redirect()->route('pdetail', $id->id)->with('alertone', 'Payment Unsuccessfully!');
            }

        }

        return Storage::download($downloadqto[$key]);
       /// Session::flash('file_download', $downloadqto[$key]);

       // var_dump($storage);

        // return redirect()->route('pdetail', $id->id);


    }


}

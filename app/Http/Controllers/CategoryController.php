<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cats = Category::all();
        return view('category.index', compact('cats'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $cats = Category::all();
        return view('category.add', compact('cats'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|min:4|max:255',
            'parent' =>'required'
        ]);
        $p = $request->parent;
        
        if($p != "0"){
            Category::create([
                'name' => $request['name'],
                'parent_id' => $p,
                'cat_description' => $request->cat_description,
            ]);
        }else{
            $cat  = Category::create([
                'name' => $request['name'],
                'cat_description' => $request->cat_description,
            ]);
            $cat->update([
                'parent_id' => $cat->id,
            ]);
        }
        return redirect()->route('cat_view')->with('alertone', 'Category Created Successfully!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function show(Category $category)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function edit(Category $id)
    {
        $id = $id;
        $cats = Category::all();
        return view('category.edit',compact('cats','id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Category $id)
    {
        $this->validate($request, [
            'name' => 'required|min:4|max:255',
            'parent' =>'required'
        ]);
        $p = $request->parent;
        if($p != "0"){
            $id->update([
                'name' => $request['name'],
                'parent_id' => $p,
                'cat_description' => $request->cat_description,
            ]);
        }else{
            $id->update([
                'name' => $request['name'],
                'parent_id' => $id->id,
                'cat_description' => $request->cat_description,
            ]);
        }
        return redirect()->route('cat_view')->with('alertone', 'Category Updated Successfully!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function destroy(Category $id)
    {
        Category::where('parent_id', $id->id)->delete();
        $id->delete();
        return redirect()->route('cat_view');
    }
}

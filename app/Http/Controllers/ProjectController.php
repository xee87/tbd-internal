<?php

namespace App\Http\Controllers;

use App\Project;
use App\Solicitation;
use App\Category;
use App\User;
use App\Owner;
use App\Planholder;
use App\Helper\TBD;
use Braintree\Plan;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Mail;

class ProjectController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $projects = Project::where('status', '!=', 'delete')->get();
        return view('project.index', compact('projects'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function addendum(){
        $addendum = Project::where('addendum', '!=', '')->where('status', '!=', 'delete')->get();
        return view('addendum.index', compact('addendum'));
    }

    public function add_addendum(){
        $addendum = Project::where('status', 'Active')->where(function($q){
            $q->where('addendum', '')->orWhere('addendum', null);
        })->get();
        return view('addendum.add', compact('addendum'));
    }

    public function update_addendum(Request $request, Project $id){
        $new_doc = $request->plan_doc;
        $doc = '';
        if(strlen($id->addendum) > 0 && strlen($new_doc) > 0){
                $doc = $id->addendum . ',' . $new_doc;
        }elseif(strlen($id->addendum) > 0 && strlen($new_doc) == 0){
            $doc = $id->addendum;
        }elseif(strlen($id->addendum) == 0 && strlen($new_doc) > 0){
            $doc = $new_doc;
        }

        Project::where('id', $id->id)->update(['addendum'=> $doc]);
        return redirect()->route('addendum_view')->with('alertone', 'Addendum Updated Successfully!');
    }

    public function save_addendum(Request $request){
        Project::where('id', $request->project_id)->update(['addendum'=> $request->addendum_doc]);
        return redirect()->route('addendum_view')->with('alertone', 'Addendum Created Successfully!');
    }

    public function edit_addendum(Project $id){
        $docs = (strlen($id->addendum)>0) ? explode(',', $id->addendum): '';
        $project = $id;
        return view('addendum.edit', compact('docs', 'project'));
    }

    function delete_addendum(Project $id){
        Project::where('id', $id->id)->update(['addendum'=> null]);
        return redirect()->route('addendum_view')->with('alertone', 'Addendum Deleted Successfully!');

//        $id->update(['addendum', '']);
    }

    public function create()
    {
        $users = array();
        $usersrole = DB::table('model_has_roles')->select('model_id')->where('role_id', '3')->get();


        foreach ($usersrole as $checkuser) {

            $userstwo = DB::table('users')->select('name', 'id')->where(['id' => $checkuser->model_id, 'status' => 1])->get();
            array_push($users, $userstwo);
        }

        $cats = Category::all();

        $owneriddall = Owner::all();

        $planholderall = Planholder::all();
        $divisions = TBD::LoadDataFromDB('csi_division', ['*'], null, ['status' => 1]);

        return view('project.add', compact('cats', 'users', 'owneriddall', 'planholderall', 'divisions'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //var_dump($request);
        // dd($request->all());
        $this->validate($request, [
            "title" => 'required|min:4|max:255',
            "solicitation" => 'required|min:4|max:255',
            "bid_amount" => 'required|max:255',
            "location" => 'required|min:4|max:255',
            "bid_method" => 'required|min:4|max:255',
            "bid_date" => 'required',
            "bid_phase" => 'required|max:255',
            "csi_division" => 'required',
            "liquidated_damages" => 'required',
            "bbond" => 'required',
            "pbond" => 'required',
            "paybond" => 'required',
            "completion_time" => 'required|min:4|max:255',
            "notes" => 'max:255',
            "pre_bid_date" => 'required|min:4|max:255',
            "pre_bid_location" => 'required|min:4|max:255',
            "project_cat" => 'required',
            "status" => 'required|min:3|max:255',
            "document_avail" => 'required',
            "bidrange" => 'required',
            "assign_qto" => 'required',
            "bidlocation" => 'required',
            "ownerid" => 'required',
            "pholder" => 'required',
        ]);


        //$bidit = array();

        //$image = $request->file('bid_doc');

        // $bidittwo=array();

        // $imagetwo = $request->file('bid_doc_plan');

        // $strcsi_division = json_encode($request->csi_division);

        $strpholder = json_encode($request->pholder);

        //var_dump($image);
        //exit;
        /* if (!empty($image)) {*/

        /*foreach ($image as $perimage) {
                $today = date("Ymdhis");
                $iname = $perimage->getClientOriginalName();

                $imageName = $today . "_" . $iname;

                $perimage->move(public_path('bid_uploads'), $imageName);

                array_push($bidit, $imageName);
            }

            $myJSON = json_encode($bidit);

            var_dump($myJSON);
            exit;*/

        // $path = $request->file('bid_doc')->store('BidDocs');

        /*var_dump($request->specification_docs);
        var_dump($request->plan_doc);*/
        $csi_divisions = implode(",", $request->csi_division);

//        Project::create([
//            "title" => $request->title,
//            "solicitation" => $request->solicitation,
//            "bid_amount" => $request->bid_amount,
//            "location" => $request->location,
//            "bid_method" => $request->bid_method,
//            "bid_date" => $request->bid_date,
//            "bid_phase" => $request->bid_phase,
//            "csi_division" => $csi_divisions,
//            "completion_time" => $request->completion_time,
//            "bid_doc_url" => $request->specification_docs,
//            "plan_doc" => $request->plan_doc,
//            "notes" => $request->notes,
//            "pre_bid_date" => $request->pre_bid_date,
//            "pre_bid_location" => $request->pre_bid_location,
//            "category_id" => $request->project_cat,
//            "status" => $request->status,
//            "assign_qto" => $request->assign_qto,
//            "liquidated_damages" => $request->liquidated_damages,
//            "b_bond" => $request->bbond,
//            "p_bond" => $request->pbond,
//            "pay_bond" => $request->paybond,
//            "bid_location" => $request->bidlocation,
//            "owner_id" => $request->ownerid,
//            "planholder_id" => $strpholder,
//            "bidrange" => $request->bidrange,
//            "document_avail" => $request->document_avail,
//            "description" => $request->description,
//        ]);
        $test_details = array(
            "title" => $request->title,
            "solicitation" => $request->solicitation,
            "bid_amount" => $request->bid_amount,
            "location" => $request->location,
            "bid_method" => $request->bid_method,
            "bid_date" => $request->bid_date,
            "bid_phase" => $request->bid_phase,
            "csi_division" => $csi_divisions,
            "completion_time" => $request->completion_time,
            "bid_doc_url" => $request->specification_docs,
            "plan_doc" => $request->plan_doc,
            "notes" => $request->notes,
            "pre_bid_date" => $request->pre_bid_date,
            "pre_bid_location" => $request->pre_bid_location,
            "category_id" => $request->project_cat,
            "status" => $request->status,
            "assign_qto" => $request->assign_qto,
            "liquidated_damages" => $request->liquidated_damages,
            "b_bond" => $request->bbond,
            "p_bond" => $request->pbond,
            "document_avail" => $request->document_avail,
            "bidrange" => $request->bidrange,
            "pay_bond" => $request->paybond,
            "bid_location" => $request->bidlocation,
            "owner_id" => $request->ownerid,
            "planholder_id" => $strpholder,
            "grab_docs" => $request->grab_doc,
            "description" => $request->description
        );
        $test_detailsId = DB::table('projects')->insert($test_details);
//        var_dump($test_detailsId);
//        var_dump($request->document_avail);
//        var_dump($request->bidrange);
//        exit;

        /*} else {

            Project::create([
                "title" => $request->title,
                "solicitation" => $request->solicitation,
                "bid_amount" => $request->bid_amount,
                "location" => $request->location,
                "bid_method" => $request->bid_method,
                "bid_date" => $request->bid_date,
                "bid_phase" => $request->bid_phase,
                'plan_doc' => $request->plan_doc,
                "csi_division" => $strcsi_division,
                "completion_time" => $request->completion_time,
                "notes" => $request->notes,
                "pre_bid_date" => $request->pre_bid_date,
                "pre_bid_location" => $request->pre_bid_location,
                "category_id" => $request->project_cat,
                "status" => $request->status,
                "assign_qto" => $request->assign_qto,
                "liquidated_damages" => $request->liquidated_damages,
                "b_bond" => $request->bbond,
                "p_bond" => $request->pbond,
                "pay_bond" => $request->paybond,
                "bid_location" => $request->bidlocation,
                "owner_id" => $request->ownerid,
                "planholder_id" => $strpholder,
            ]);


        }*/

        if (!empty($request->assign_qto) && $request->assign_qto != 0) {

            $userstwo = DB::table('users')
                ->select('name', 'email')
                ->where('id', $request->assign_qto)
                ->first();

            $contactName = $userstwo->name;
            $contactEmail = $userstwo->email;

            $data = array('name' => $contactName);

            // todo: new job queue here


            if (env('MAIL_SEND')) {
                Mail::send('emails.qtomail', $data, function ($message) use ($contactEmail, $contactName) {
                    $message->to($contactEmail, $contactName)->subject('New Project Assigned');

                });
            }
        }

        return redirect()->route('pro_view')->with('alertone', 'Project Created Successfully!');
    }

    public function saveplan(Request $request)
    {

        // we'll save both plan and specification documents here.

        $plan_docs = $request->file('plan_docs');

        $bid_docs = $request->file('bid_doc');

        $addendum_docs = $request->file('addendum_doc');

        //$perimage = $plan_docs;
        if (is_object($plan_docs) || is_object($bid_docs)) {

            $perimage = !is_null($plan_docs) ? $plan_docs : $bid_docs;

            $today = date("Ymdhis");

            $iname = $perimage->getClientOriginalName();

            $imageName = $today . "_" . $iname;

            $perimage->move(public_path('bid_uploads'), $imageName);

            //var_dump($imageName);

            echo json_encode(['response' => true, 'message' => $imageName]);
            exit;
        }



        echo json_encode(['response' => false, 'Can\'t upload file']);
        exit;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Project $project
     * @return \Illuminate\Http\Response
     */
    public function show(Project $project)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Project $project
     * @return \Illuminate\Http\Response
     */
    public function edit(Project $id)
    {
        $users = array();
        $usersrole = DB::table('model_has_roles')->select('model_id')->where('role_id', '3')->get();


        foreach ($usersrole as $checkuser) {
            $userstwo = DB::table('users')->select('name', 'id')->where(['id'=> $checkuser->model_id, 'status' => 1])->get();
            array_push($users, $userstwo);
        }


        $project = $id;


        $ed = $project->assign_qto;

        $idd = User::find($ed);

        $edd = $project->owner_id;

        $owneridd = Owner::find($edd);

        $owneriddall = Owner::get();

        $cats = Category::all();

        $planholderall = Planholder::all();

        $ph_id = json_decode($project->planholder_id,true);
       // var_dump($ph_id); exit;
        $selectedPlanholder = Planholder::find($ph_id);


        // DB::enableQueryLog(); //
        $awardedSolicitation = DB::table('project_solicitation')
            ->select('project_solicitation.awarded', 'planholder.id' , 'project_solicitation.price','planholder.company', 'project_solicitation.planholder_id','project_solicitation.project_id')
            ->join('planholder','planholder.id','=','project_solicitation.planholder_id')
            ->where(['project_solicitation.project_id' => $project->id])
            ->get();




      //  echo '<pre>';
       //var_dump(count($selectedPlanholder)); exit;
        // dd(DB::getQueryLog());
        $divisions = TBD::LoadDataFromDB('csi_division', ['*'], null, ['status' => 1]);

        return view(
            'project.edit',
            compact('project', 'cats', 'users', 'idd', 'owneridd', 'owneriddall', 'planholderall', 'divisions', 'selectedPlanholder', 'awardedSolicitation')
        );
    }

    public function status_active(Project $id)
    {

        $id->update([
            "status" => "Active"

        ]);
        return redirect()->route('pro_view')->with('alertone', 'Project Status Active!');
    }

    public function delete_solicitation(Request $id){


        DB::table('project_solicitation')->where('project_id', $id->projectID)->where('planholder_id',$id->ph_id)->delete();
        echo 'true'; exit;


    }

    public function status_inactive(Project $id)
    {

        $id->update([
            "status" => "Inactive"

        ]);
        return redirect()->route('pro_view')->with('alertone', 'Project Status Active!');
    }

    public function status_delete(Project $id)
    {

        $id->update([
            "status" => "delete"

        ]);
        return redirect()->route('pro_view')->with('alertone', 'Project Deleted!');;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Project $project
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Project $id)
    {
        // dd((int)$request->ownerid);

        /* $this->validate($request, [
             "title" => 'required|min:4|max:255',
             "solicitation" => 'required|min:4|max:255',
             "bid_amount" => 'required|max:255',
             "location" => 'required|min:4|max:255',
             "bid_method" => 'required|min:4|max:255',
             "bid_date" => 'required|min:4|max:255',
             "bid_phase" => 'required|max:255',
             "csi_division" => 'required',
             "completion_time" => 'required|min:4|max:255',
             "liquidated_damages" => 'required',
             "bbond" => 'required',
             "pbond" => 'required',
             "paybond" => 'required',
             // "bid_doc" => 'required|mimes:pdf,xls,xlsx|max:8000',
             "notes" => 'max:255',
             "pre_bid_date" => 'required|min:4|max:255',
             "pre_bid_location" => 'required|min:4|max:255',
             "project_cat" => 'required',
             //"status" => 'required|min:3|max:255',
             "assign_qto" => 'required',
             "bidlocation" => 'required',
             "ownerid" => 'required',
             "pholder" => 'required',
             "bidrange" => 'max:255',
             "document_avail" => 'max:255',
             "description" => 'max:255'
         ]);*/


        $prodoc = DB::table('projects')->select('bid_doc_url', 'plan_doc', 'grab_docs')->where('id', $id->id)->first();
        /*$superArr['bid_doc_url']*/
        $specificationDocuments = $qto_price_array = $superArr = [];

        if(strlen($request->specification_docs) > 0 )$specificationDocuments = explode(',', $request->specification_docs);

        $planDocuments = $request->plan_doc;
        $grabDocuments = $request->grab_doc;


        if (is_array($specificationDocuments) && count($specificationDocuments) > 0) {
            $superArr['bid_doc_url'] = $specificationDocuments;
        }

        if (isset($superArr['bid_doc_url']) && !empty($prodoc->bid_doc_url)) {

            $oldData = explode(',',$prodoc->bid_doc_url);
            $superArr['bid_doc_url'] = array_merge($superArr['bid_doc_url'], $oldData);
        }



        if(isset($superArr['bid_doc_url']) && count($superArr['bid_doc_url']) > 0) $superArr['bid_doc_url'] = implode(',',$superArr['bid_doc_url']);


        if (!empty($prodoc->plan_doc) && strlen($planDocuments) > 0) {
            if (strlen($planDocuments) > 0) $planDocuments .= ',';
            $planDocuments .= $prodoc->plan_doc;
        }

        if (strlen($planDocuments) > 0) {
            $superArr['plan_doc'] = $planDocuments;
        }
        //var_dump($prodoc->grab_docs); exit;

        if (!empty($prodoc->grab_docs) && strlen($grabDocuments) > 0) {
            if (strlen($grabDocuments) > 0) $grabDocuments .= ',';
            $grabDocuments .= $prodoc->grab_docs;
        }

        if(!empty($prodoc->grab_docs)){
            $superArr['grab_docs'] = $prodoc->grab_docs;
        }

        if (strlen($grabDocuments) > 0) {
            $superArr['grab_docs'] = $grabDocuments;
        }

        /*array_push($bidit, $specificationDocuments);*/

        //$strcsi_division = json_encode($request->csi_division);

        $csi_division = implode(",",$request->csi_division);

        $strpholder = json_encode($request->pholder);

        /*QTO FILES PRICE*/
        $qtoPrice = ["qto_price"  => null];
        if(isset($request->qtototalnumber)) {


            $qtn = $request->qtototalnumber;

            for ($i = 1; $i <= $qtn; $i++) {

                $strstrqp = "qtoprice" . $i;
                array_push($qto_price_array, $request->$strstrqp);

            }
            $strqtopricearray = json_encode($qto_price_array);

            $qtoPrice["qto_price"] = $strqtopricearray;
        }
        /*QTO FILES PRICE*/


        //$image = $request->file('bid_doc');

        // if (!empty($image)) {

        /*foreach ($image as $perimage) {

            $today = date("Ymdhis");

            $iname = $perimage->getClientOriginalName();

            $imageName = $today . "_" . $iname;

            $perimage->move(public_path('bid_uploads'), $imageName);

        }*/

//        var_dump($request->document_avail);
//        var_dump($request->bidrange);
//      //  exit;

//        if($request->bid_phase == 'Open Solicitation'){
//            $request->solicitation = 'Open Solicitation';
//        }

        $postedData = [
            "title" => $request->title,
            "solicitation" => $request->solicitation,
            "bid_amount" => $request->bid_amount,
            "location" => $request->location,
            "bid_method" => $request->bid_method,
            "bid_date" => $request->bid_date,
            "bid_phase" => $request->bid_phase,
            "csi_division" => $csi_division,
            "completion_time" => $request->completion_time,
            //"bid_doc_url" => $bidit,
            "notes" => $request->notes,
            "pre_bid_date" => $request->pre_bid_date,
            "pre_bid_location" => $request->pre_bid_location,
            //"plan_doc" => $planDocuments,
            "category_id" => $request->project_cat,
            "status" => $request->status,
            "assign_qto" => $request->assign_qto,
            "liquidated_damages" => $request->liquidated_damages,
            "b_bond" => $request->bbond,
            "p_bond" => $request->pbond,
            "pay_bond" => $request->paybond,
            "bid_location" => $request->bidlocation,
            "owner_id" => (int)$request->ownerid,
            "planholder_id" => $strpholder,
            //"qto_price" => $strqtopricearray,
            "grab_docs" => $request->grab_doc,
            "bidrange" => $request->bidrange,
            "document_avail" => $request->document_avail,
            "description" => $request->description,
        ];
        $postedData = array_merge($postedData, $superArr);
        $postedData = array_merge($postedData, $qtoPrice);
//        echo '<pre>';
//        var_dump($postedData);
//        exit;

//        $id->update($postedData);
        DB::table('projects')
            ->where('id', $id->id)
            ->update($postedData);

        /*} else {


            $id->update([
                "title" => $request->title,
                "solicitation" => $request->solicitation,
                "bid_amount" => $request->bid_amount,
                "location" => $request->location,
                "bid_method" => $request->bid_method,
                "bid_date" => $request->bid_date,
                "bid_phase" => $request->bid_phase,
                "csi_division" => $strcsi_division,
                "completion_time" => $request->completion_time,
                "notes" => $request->notes,
                "pre_bid_date" => $request->pre_bid_date,
                "pre_bid_location" => $request->pre_bid_location,
                "category_id" => $request->project_cat,
                "status" => $request->status,
                "assign_qto" => $request->assign_qto,
                "liquidated_damages" => $request->liquidated_damages,
                "b_bond" => $request->bbond,
                "p_bond" => $request->pbond,
                "pay_bond" => $request->paybond,
                "bid_location" => $request->bidlocation,
                "owner_id" => (int)$request->ownerid,
                "planholder_id" => $strpholder,
                "qto_price" => $strqtopricearray,
            ]);


        }*/
        if (!empty($request->assign_qto) && $request->assign_qto != 0) {

            $userstwo = DB::table('users')->select('name', 'email')->where('id', $request->assign_qto)->first();


            $contactName = $userstwo->name;
            $contactEmail = $userstwo->email;


            $data = array('name' => $contactName);



            if (env('MAIL_SEND')) {
                Mail::send('emails.qtomail', $data, function ($message) use ($contactEmail, $contactName) {
                    $message->to($contactEmail, $contactName)->subject
                    ('New Project Assigned');

                });
            }
        }
        return redirect()->route('pro_view')->with('alertone', 'Project Updated Successfully!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Project $project
     * @return \Illuminate\Http\Response
     * @type = null
     */
    public function destroy($name, Project $id, $type)

    {

        if($type == 0){
            $columnName = 'addendum';
        }elseif($type == 4){
            $columnName = 'grab_docs';
        }else
        {
            $columnName = ($type == '2') ? 'plan_doc' : 'bid_doc_url';

        }

        if (!empty($name) && !empty($id)) {

            $prodoc = DB::table('projects')->select($columnName)->where('id', $id->id)->first();

            //var_dump($prodoc); exit;

            if (!empty($prodoc->{$columnName})) {

                $bidit = explode(',', $prodoc->{$columnName});

                $arraycount = 0;

                foreach ($bidit as $perbidit) {

                    if ($perbidit === $name) {
                        unset($bidit[$arraycount]);
                    }
                    $arraycount++;
                }

                $myJSON = implode(',', $bidit);
                //echo $id->id; exit;

                Project::where('id', $id->id)->update([$columnName => $myJSON]);
//                $id->update(
//                    [$columnName => $myJSON,]
//                );
            }
        }
        // $id->delete();
        if($type ==0){

            return redirect()->route('addendum_edit', $id->id);
        }else{

            return redirect()->route('pro_edit', $id->id);
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Project $project
     * @return \Illuminate\Http\Response
     */

    public function qtoview()
    {
        $id = auth()->user()->id;

        $projects = Project::where("assign_qto", $id)->where("status", "Active")->get();
        return view('project.qtoview', compact('projects'));
    }

    public function upload_qto_video(Request $request){

        if(!empty($request->project_id)){
            Project::where('id', '=', $request->project_id)->update(['qto_video' => $request->qto_video]);
            return redirect()->route('qto_detail_view', ['id' => $request->project_id])->with('alertone', 'Video updated successfully.');
        }

        return redirect()->route('qto_detail_view', ['id' => $request->project_id])->with('alertone', 'Please provide video url.');

        // echo 'die'; exit;
    }

    public function qtoview_detail(Project $id)
    {
        $idd = auth()->user()->id;

        abort_if($id->assign_qto != $idd, 403);

        return view('project.qtoview_detail', compact('id'));
    }

    public function fileStore(Request $request, Project $id)
    {

        $prodoc = DB::table('projects')->select('qto_upload')->where('id', $id->id)->first();
        $qto_assign = !empty($prodoc->qto_upload) ? \GuzzleHttp\json_decode($prodoc->qto_upload) : [];
        //$qto_assign = array();

        $image = $request->file('file');


        //foreach ($image as $perimage) {

        $imageName = $image->getClientOriginalName();
        $filename = uniqid() . '.' . $imageName;

        $incomingImage = $imageName = $image->storeAs('qto', $filename);

        array_push($qto_assign, $imageName);
        //}

        $myJSON = json_encode($qto_assign);


        $id->update(["qto_upload" => $myJSON,]);

        echo json_encode(['response' => true, 'message' => $incomingImage]);
        exit;
        //return response()->json(['success' => $imageName]);
    }

    public function fileDestroy(Request $request)
    {
        $filename = $request->get('filename');
        ImageUpload::where('filename', $filename)->delete();
        $path = public_path() . '/uploads/' . $filename;
        if (file_exists($path)) {
            unlink($path);
        }
        return $filename;
    }

    public function downloadqto(Project $id, $key)
    {

        $downloadqto = json_decode($id->qto_upload);
        return Storage::download($downloadqto[$key]);

    }

    public function deleteqto(Project $id, $key)
    {

        $downloadqto = json_decode($id->qto_upload);
        $qtoPrice = json_decode($id->qto_price);

        $arr = json_decode($id->qto_upload);
        $priceArr = $new_arr = array();
        foreach ($arr as $k => $value) {
            if ($value == $downloadqto[$key]) {
                unset($qtoPrice[$key]);
                continue;
            } else {
                $new_arr[] = $value;
                if(isset($qtoPrice[$k])) $priceArr[] = $qtoPrice[$k];
            }
        }

        $myJSON = json_encode($new_arr);
        $priceJSON = json_encode($priceArr);

        $id->update(["qto_upload" => $myJSON,]);
        $id->update(["qto_price" => $priceJSON,]);

        // return Storage::delete($downloadqto[$key]);
        return redirect()->route('qto_view')->with('alertone', 'QTO file Deleted Successfully!');

    }

    public function owner_list()
    {


        $owners = Owner::all();
        return view('owner.index', compact('owners'));

    }

    public function owner_list_add()
    {


        $user_pro = Project::where('owner_id', 0)->orWhereNull('owner_id')->where('status', 'Active')->get();

        return view('owner.add', compact('user_pro'));

    }

    public function owner_create(Request $request)
    {
        // dd($request->all());
        $this->validate($request, [
            "ownername" => 'required',
            "owneraddress" => 'required',
            "contact_name" => 'required',

        ]);


        $ownerid = Owner::create([
            "owner_name" => $request->ownername,
            "owner_address" => $request->owneraddress,
            "contact_name" => $request->contact_name,
            "owner_fax" => $request->owner_fax,
            "owner_email" => $request->owner_email,
            "owner_phone" => $request->owner_phone,

        ]);

        if (!empty($request->owner_proj)) {

            Project::where('id', '=', $request->owner_proj)->update(['owner_id' => $ownerid->id]);

        }


        return redirect()->route('owner_list')->with('alertone', 'Owner Created Successfully!');
    }

    public function owner_edit(Owner $id)
    {

        $ownerinfo = $id;

        $user_pro = Project::where('owner_id', 0)->orWhereNull('owner_id')->where('status', 'Active')->get();

        $user_pro_owner = Project::where('owner_id', $ownerinfo->id)->where('status', 'Active')->get();

        return view('owner.edit', compact('ownerinfo', 'user_pro', 'user_pro_owner'));

    }

    public function owner_del($id)
    {

        $ownerid = $id;

        Project::where('id', '=', $ownerid)->update(['owner_id' => 0]);

        return redirect()->route('owner_list')->with('alertone', 'Ownership Removed Successfully!');
    }

    public function owner_update(Request $request, $id)
    {
        // dd($request->all());
        $this->validate($request, [
            "ownername" => 'required',
            "owneraddress" => 'required',
            "contact_name" => 'required'

        ]);

        //var_dump($request->contact_name);exit;


        Owner::where('id', '=', $id)->update([
            "owner_name" => $request->ownername,
            "owner_address" => $request->owneraddress,
            "contact_name" => $request->contact_name,
            "owner_fax" => $request->owner_fax,
            "owner_email" => $request->owner_email,
            "owner_phone" => $request->owner_phone,
            "contact_name" => $request->contact_name,
        ]);

        if (!empty($request->owner_proj)) {

            Project::where('id', '=', $request->owner_proj)->update(['owner_id' => $id]);

        }


        return redirect()->route('owner_list')->with('alertone', 'Owner Updated Successfully!');
    }

    public function planholder_list()
    {


        $planholders = Planholder::all();
        return view('planholder.index', compact('planholders'));

    }

    public function planholder_add()
    {


        $user_pro = Project::where('status', 'Active')->get();

        return view('planholder.add', compact('user_pro'));

    }

    public function planholder_create(Request $request)
    {
        // dd($request->all());
        $this->validate($request, [
            "company" => 'required',
            "planholderaddress" => 'required',
            "planholdercontact" => 'required',

        ]);

        $planholderid = Planholder::create([
            "company" => $request->company,
            "planholder_address" => $request->planholderaddress,
            "planholder_contact" => $request->planholdercontact,
            "planholder_fax" => $request->ph_fax,
            "planholder_email" => $request->ph_email,
            "planholder_phone" => $request->ph_phone,

        ]);


        $array_planholder = array();

        if (!empty($request->owner_proj)) {

            $phid = Project::select('planholder_id')->where('id', '=', $request->owner_proj)->first();


            if (!empty($phid->planholder_id) && $phid->planholder_id != NULL) {

                $strphid = json_decode($phid->planholder_id);

                array_push($strphid, $planholderid->id);

                $array_planholder = $strphid;

            } else {


                array_push($array_planholder, $planholderid->id);
            }

            $arr_planholder = json_encode($array_planholder);


            Project::where('id', '=', $request->owner_proj)->update(['planholder_id' => $arr_planholder]);

        }


        return redirect()->route('planholder_list')->with('alertone', 'Plan Holder Created Successfully!');
    }

    public function planholder_edit(Planholder $id)
    {


        $planholderinfo = $id;


        $user_pro = Project::where('status', 'Active')->get();

        // $user_pro_owner = Project::where('owner_id', $ownerinfo->id)->where('status','Active')->get();

        return view('planholder.edit', compact('planholderinfo', 'user_pro'));
    }

    public function planholder_del($id)
    {


        $ownerid = $id;

        Project::where('id', '=', $ownerid)->update(['owner_id' => 0]);

        return redirect()->route('owner_list')->with('alertone', 'Ownership Removed Successfully!');
    }

    public function planholder_update(Request $request, $id)
    {
        // dd($request->all());
        $this->validate($request, [
            "company" => 'required',
            "planholderaddress" => 'required',
            "planholdercontact" => 'required',

        ]);


        Planholder::where('id', '=', $id)->update([
            "company" => $request->company,
            "planholder_address" => $request->planholderaddress,
            "planholder_contact" => $request->planholdercontact,
            "planholder_fax" => $request->ph_fax,
            "planholder_email" => $request->ph_email,
            "planholder_phone" => $request->ph_phone,

        ]);

        $array_planholder = array();

        if (!empty($request->owner_proj)) {

            $phid = Project::select('planholder_id')->where('id', '=', $request->owner_proj)->first();


            if (!empty($phid->planholder_id) && $phid->planholder_id != NULL) {

                $strphid = json_decode($phid->planholder_id);
                array_push($strphid, (int)$id);
                $array_planholder = $strphid;

            } else {

                //@TODO: Might Cause Bug.
                array_push($array_planholder, $planholderid->id);
            }

            $arr_planholder = json_encode($array_planholder);


            Project::where('id', '=', $request->owner_proj)->update(['planholder_id' => $arr_planholder]);

        }


        return redirect()->route('planholder_list')->with('alertone', 'Plan Holder Updated Successfully!');
    }

    public function sol_add(Request $request){

        /* var_dump($_REQUEST);
         exit;*/
        //exit;
        $getData = $request->planHolder;

        $project_id = $request->project_id;

        $sol_type = $request->sol_type;

        Project::where('id', '=', $project_id)->update([/*'solicitation' => $sol_type, */'bid_phase' => $sol_type]);

        $db = [];

        if(!is_null($getData)) {

            foreach ($getData['id'] as $k => $data) {
                Solicitation::updateOrCreate(
                    ['project_id' => $project_id, 'planholder_id' => $data],
                    ['price' => $getData['price'][$k]]
                );
                //$db[] = array('project_id' => $project_id, 'planholder_id' => $data, 'price' => $getData['price'][$k]);
            }


          //  Solicitation::updateOrCreate($db);
            $userNotice = "Bid Results saved!";
            $type = 'alertone';
        }else{
            $userNotice = "Invalid Paramters";
            $type = 'alert2';
        }

        //return redirect()->route('pro_edit', $project_id)->with($type, $userNotice);
        echo json_encode(['status' => true, "message" => $userNotice]);
        exit;

    }

    public function sol_award(Request $request){
        /*$planholder_id = $request->awarded;*/

        /*var_dump($_REQUEST['project_id']);
        var_dump($_REQUEST['awarded']);
        var_dump($request->awarded);
        exit;*/
        $project_id = $request->project_id;
        $sol_type = $request->sol_type;
        Project::where('id', '=', $project_id)->update([/*'solicitation' => $sol_type,*/ 'bid_phase' => $sol_type]);


        Solicitation::where(['project_id' => $request->project_id])->update(['awarded' => 0]);

        Solicitation::where([
            'project_id' => $request->project_id,
            'planholder_id' => $request->awarded,
        ])->update(['awarded' => 1]);

        Solicitation::where(['project_id' => $request->project_id])->update(['status' => 2]);

        //return redirect()->route('pro_edit', $project_id)->with('alertone', 'Project Solicitation Close!');;
        echo json_encode(['status' => true, "message" => "Project Awarded!"]);
        exit;

    }

}

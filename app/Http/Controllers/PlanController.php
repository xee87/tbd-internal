<?php

namespace App\Http\Controllers;

use App\User_downloads;
use Illuminate\Http\Request;
use App\Helper\TBD;
use App\Plan;
use DB;
use App\User;
use App\User_project_record;
use Laravel\Cashier\Subscription;

class PlanController extends Controller
{
    
    public function __construct()
    {


    }

    public function index()
    {
        if(!TBD::allowAccess()){
            return	redirect()->route('landing');
        }

        $user = null;
        $selectedPlan = '';
        $downloadLimitOut = false;

        if(auth()->user()){
            $selectedPlan = Subscription::select('braintree_plan')->where('user_id', '=', auth()->user()->id)->first();
            $UD = User_downloads::where('user_id', auth()->user()->id)->first();
           // var_dump($UD->downloads_used); exit;
            if(is_object($UD)){
                if($UD->downloads_used >= $UD->downloads || $UD->downloads == -1 ){
                    $downloadLimitOut = true;
                }
            }
        }

        if(auth()->user()){
            $user = User_project_record::where('user_id', '=', auth()->user()->id )->first();
        }
        //var_dump($user); exit;
    
        $plans = DB::select('select plans.id, set_downloads.max_rate, set_downloads.downloads, plans.braintree_plan,
                                plans.name , plans.cost
                                FROM set_downloads
                                LEFT JOIN plans ON set_downloads.package = plans.id;'
        );

        return view('plans', compact('plans','user', 'selectedPlan', 'downloadLimitOut'));
    }

    public function planshow($plan)
    {
        
        $plan = Plan::find($plan);
        return view('show', compact('plan'));
    }

}

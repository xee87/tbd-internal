<?php

namespace App\Http\Controllers;

use App\Plan;
use App\Subscription;
use Illuminate\Http\Request;
use App\User;
use App\User_downloads;
use App\Set_downloads;
use DB;
use Mail;

class SubscriptionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {


        $plan = Plan::findOrFail($request->get('plan'));


        if (auth()->user()->subscribed('main')) {

            auth()->user()->subscription('main')->swap($plan->braintree_plan);


        } else {

            $request->user()->newSubscription('main', $plan->braintree_plan)->create($request->payment_method_nonce);
        }


        // if (auth()->user()->subscribed('main')) {

        $downloadsnumb = 0;
        $max_amount = 0;

        $b = Set_downloads::where('package', $plan->id)->get();


        $downloadsnumb = $b[0]->downloads;
        $max_amount = $b[0]->max_rate;

        $user = User_downloads::updateOrCreate(['user_id' => auth()->user()->id], ['downloads' => $downloadsnumb, 'downloads_used' => 0, 'max_amount' => $max_amount]);

        //                 }else{
        //     dd('subscription error');
        // }

        // $userstwo =DB::table('users')->select('name','email')->where('id', auth()->user()->id)->first();


        $contactName = auth()->user()->name;
        $contactEmail = auth()->user()->email;

        $packagecost = $plan->cost;
        $packagename = $plan->name;
        $packagedownloads = $downloadsnumb;

        $data = array(
            'name' => $contactName,
            'pcost' => $packagecost,
            'pname' => $packagename,
            'pdownload' => $packagedownloads
        );

        if (env('MAIL_SEND')) {
            Mail::send('emails.paymentmail', $data, function ($message) use ($contactEmail, $contactName) {
                $message->to($contactEmail, $contactName)->subject
                ('Payment Done');

            });
        }


        return redirect()->route('user_record')->with('alertone', 'Your plan subscribed successfully');
    }

    public function create2(Request $request, Plan $plan)
    {
        $plan = Plan::findOrFail($request->get('plan'));

        if ( auth()->user()->subscribed('main') ) {

            auth()->user()->subscription('main')->swap($plan->braintree_plan);

        }else{

            $request->user()->newSubscription('main', $plan->braintree_plan)->create($request->payment_method_nonce);
        }


        // if (auth()->user()->subscribed('main')) {

        $downloadsnumb=0;
        $max_amount = 0;

        $b= Set_downloads::where('package', $plan->id )->get();


        $downloadsnumb = $b[0]->downloads;
        $max_amount = $b[0]->max_rate;

        $user = User_downloads::updateOrCreate(
            ['user_id' => auth()->user()->id ],
            [ 'downloads' => $downloadsnumb,'downloads_used' => 0 ,'max_amount' => $max_amount]
        );

        //                 }else{
        //     dd('subscription error');
        // }

        // $userstwo =DB::table('users')->select('name','email')->where('id', auth()->user()->id)->first();



        //   $contactName = auth()->user()->name;
        //   $contactEmail = auth()->user()->email;


        //   $data = array('name'=> $contactName);
        // Mail::send('emails.paymentmail', $data, function($message) use ($contactEmail, $contactName) {
        // $message->to( $contactEmail ,  $contactName)->subject
        //       ('Payment Done');

        // });

        return redirect()->route('user_record')->with('alertone', 'Your plan subscribed successfully');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    public function rp(Request $request)
    {

        DB::update('update register_charges set price = ' . $request->reg_price . ' where id = 1');

        return redirect()->route('sub_downloads')->with('alertone', 'Registration Price Updated Successfully!');
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */


    public function sub_downloads()
    {
        $strprices = DB::select('SELECT * FROM register_charges WHERE id = 1');

        $results = DB::select('select plans.id, set_downloads.max_rate, set_downloads.downloads,plans.created_at ,plans.braintree_plan,plans.description , plans.slug ,plans.name , plans.cost FROM set_downloads LEFT JOIN plans ON set_downloads.package = plans.id;');


        return view('subscription.index', compact('results', 'strprices'));
    }


    public function sub_downloads_edit($id)
    {


        $results = DB::select('select plans.id, set_downloads.max_rate,set_downloads.downloads,plans.created_at ,plans.braintree_plan,plans.description , plans.slug ,plans.name , plans.cost FROM set_downloads LEFT JOIN plans ON set_downloads.package = plans.id WHERE set_downloads.package =' . $id . ' ;');


        return view('subscription.edit', compact('results'));
    }

    public function  sub_downloads_update(Request $request, $id)
    {

        $this->validate($request, [
            "pname" => 'required',
            // "psname" => 'required',
            "braintree_id" => 'required',
            // "description" => 'required',
            "cost" => 'required',
            "download" => 'required',
            "max_price" => 'required',
        ]);


        Plan::where('id', $id)->update(['name' => $request->pname, 'slug' => $request->psname, 'braintree_plan' => $request->braintree_id, 'cost' => $request->cost, 'description' => $request->description]);


        DB::update('update set_downloads set downloads = ' . $request->download . ', max_rate = ' . $request->max_price . ' where package = ' . $id);


        return redirect()->route('sub_downloads')->with('alertone', 'Subscription Updated Successfully!');
    }


    public function  sub_add()
    {

        return view('subscription.add');
    }

    public function  sub_store(Request $request)
    {

        $this->validate($request, [
            "pname" => 'required',
            // "psname" => 'required',
            "braintree_id" => 'required',
            // "description" => 'required',
            "cost" => 'required',
            "download" => 'required',
            "max_price" => 'required',
        ]);

        $data = Plan::create([
            "name" => $request->pname,
            "slug" => $request->psname,
            "braintree_plan" => $request->braintree_id,
            "description" => $request->description,
            "cost" => $request->cost,

        ]);

        Set_downloads::create([

            "package" => $data->id,
            "downloads" => $request->download,
            "max_rate" => $request->max_price,
        ]);


        return redirect()->route('sub_downloads')->with('alertone', 'Subscription Package Created Successfully!');
    }


    /**
     * Display the specified resource.
     *
     * @param  \App\Subscription $subscription
     * @return \Illuminate\Http\Response
     */
    public function show(Subscription $subscription)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Subscription $subscription
     * @return \Illuminate\Http\Response
     */
    public function edit(Subscription $subscription)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Subscription $subscription
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Subscription $subscription)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Subscription $subscription
     * @return \Illuminate\Http\Response
     */
    public function destroy(Subscription $subscription)
    {
        //
    }
}

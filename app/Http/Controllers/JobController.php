<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Jobs\SendEmailJob;
use Carbon\Carbon;
use App\Project;
use App\Solicitation;
use App\Category;
use App\User;
use App\Owner;
use App\Planholder;

class JobController extends Controller
{
    //
    public function processQueue()
    {

        $p = Project::select('planholder_id')->where( 'id', '=', 1 )->first();
        $ids  = json_decode($p->planholder_id);
        $info = array();

        foreach($ids as $id){
            $contact = Planholder::where('id', '=', $id)->first();
            $info[] = array('id' => $id,  'name' => $contact->company, 'email' => $contact->planholder_email);
        }

        $subject = 'Plan Holders Testing Queue';
        $view = 'emails.qtomail';

        $emailJob = new SendEmailJob();
        $emailJob->setValues($info, $view, $subject);
        $emailJob->delay(Carbon::now()->addSeconds(10));
        dispatch($emailJob);

    }
}

<?php

namespace App\Http\Controllers;


use App\Project;
use App\Category;
use App\User;
use App\Owner;
use App\Planholder;
use App\Contractor_listing;
use Illuminate\Http\Request;
use App\Division;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Mail;

class ContractorController extends Controller
{

    public function store(Request $request){

        $csi_divisions = !empty($request->csi_division) ? implode(",", $request->csi_division) : '';
        Contractor_listing::create([
            "company_name" => $request->company_name,
            "address" => $request->address,
            "contact_name" => $request->contact_name,
            "email" => $request->email,
            "contact" => $request->contact,
            "fax" => $request->fax,
            "website" => $request->fax,
            "divisions" => $csi_divisions,
            "type" => $request->type,
        ]);
        $successMsg = 'added';
        return view('welcome', compact('successMsg'));

       // var_dump($request); exit;
    }

    public function view($type){
        $contractors = Contractor_listing::where('type', $type)->get();

        if(count($contractors) == 0){
            $contractors = $type;
        }

        return view('contractor.index', compact('contractors'));
    }

}
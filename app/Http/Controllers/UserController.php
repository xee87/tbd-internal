<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Models\Role;
use Illuminate\Http\Request;
use App\Project;
use Mail;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
        //echo $id;
        $users = User::where('status', '!=', 0)->get();
        return view('user.user_index', compact('users', 'id'));
    }

    public function clients(){
        return $this->index(2);
    }

    public function users(){
        return $this->index(1);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */




    public function create()
    {
        $roles = Role::all();
        $user_pro = Project::where('assign_qto', 0)->where('status', 'Active')->get();
        return view('user.user_add', compact('roles', 'user_pro'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|min:4|max:255',
            'email' => 'required',
            'password' => 'required|string|min:6|confirmed',
            'role' => 'required'
        ]);

        $user = User::create([
            'name' => $request['name'],
            'email' => $request['email'],
            'password' => Hash::make($request['password']),
        ]);

        if (!empty($request['user_assign_proj']) && $request['user_assign_proj'] != '') {


            Project::where('id', $request['user_assign_proj'])->update([
                'assign_qto' => $user->id,

            ]);


            //   $data = array('name'=> $user->name);
            // Mail::send('emails.qtomail', $data, function($message) {
            // $message->to($user->email, $user->name)->subject
            //       ('Laravel HTML Testing Mail');

            // });


        }
        $user->assignRole($request['role']);
        return redirect()->route('user_view')->with('alertone', 'User Created Successfully!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    public function status_active(User $id)
    {

        $id->update([
            "status" => 1

        ]);
        return redirect()->route('user_view')->with('alertone', 'User Status Active!');
    }


    public function status_inactive(User $id)
    {

        $id->update([
            "status" => 2

        ]);
        return redirect()->route('user_view')->with('alertone', 'User Status Active!');
    }

    public function status_delete(User $id)
    {

        $id->update([
            "status" => 0

        ]);
        return redirect()->route('user_view')->with('alertone', 'User Deleted!');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit(User $id)
    {
        $user = $id;
        $roles = Role::all();
        $user_pro = Project::where('assign_qto', 0)->where('status', 'Active')->get();
        return view('user.user_edit', compact('user', 'roles', 'user_pro'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $id)
    {
        $this->validate($request, [
            'name' => 'required|min:4|max:255',
            'email' => 'required',
            'password' => 'required|string|min:6|confirmed',
            'role' => 'required'

        ]);


        $id->update([
            'name' => $request['name'],
            'email' => $request['email'],
            'password' => Hash::make($request['password']),
        ]);

        if (!empty($request['user_assign_proj']) && $request['user_assign_proj'] != '') {


            Project::where('id', $request['user_assign_proj'])->update([
                'assign_qto' => $id->id,

            ]);

        }


        $id->syncRoles($request['role']);
        return redirect()->route('user_view')->with('alertone', 'User Updated Successfully!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $id)
    {
        $id->delete();
        return redirect()->route('user_view');
    }
}

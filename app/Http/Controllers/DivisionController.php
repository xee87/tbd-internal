<?php

namespace App\Http\Controllers;


use App\Project;
use App\Category;
use App\User;
use App\Owner;
use App\Planholder;
use Illuminate\Http\Request;
use App\Division;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Mail;

class DivisionController extends Controller
{
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		$divisions = Division::where('status', '!=', '0')->get();

		return view('division.index', compact('divisions'));
	}

	public function division_list(Request $request){


		return view('division/index');
	}

	public function create_division(Request $request)
	{

		$this->validate($request, [
			"division_name" => 'required|min:4|max:255',
		]);

		if($request->id){
			$updateDivision = array('division_name' => $request->division_name);
			DB::table('csi_division')
					->where('id', $request->id)
					->update($updateDivision);
			return redirect()->route('division_list')->with('alertone', 'Division Updated Successfully!');


		}else{
			Division::create([
					"division_name" => $request->division_name,
			]);
			return redirect()->route('division_list')->with('alertone', 'Division Created Successfully!');
		}





//		$data = array(
//				"division_name" => $request->division_name
//		);
//
//		$test_detailsId = DB::table('csi_division')->insert($data);

	}

	public function status_active(Division $id)
	{

		$id->update([
				"status" => 1

		]);
		return redirect()->route('division_list')->with('alertone', 'Division Status Active!');
	}


	public function status_inactive(Division $id)
	{

		$id->update([
				"status" => 2

		]);
		return redirect()->route('division_list')->with('alertone', 'Division Status Active!');
	}

	public function status_delete(Division $id)
	{

		$id->update([
				"status" => 0

		]);
		return redirect()->route('division_list')->with('alertone', 'Division Deleted!');;
	}

}
<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Auth\RegisterController;
use App\Planholder;
use Illuminate\Support\Facades\Auth;
use App\User;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Models\Role;
use Illuminate\Http\Request;
use Mail;
use Braintree_Transaction;
use DB;
use App\Subscription;
use App\Plan;
use Cookie;

class RegistrationController extends RegisterController
{
//    public function registerUser()

    private $apiToken;

    public function __construct()
    {
        // Unique Token
        $this->apiToken = uniqid(base64_encode(str_random(60)));
    }

    public function registerUser()
    {
        //  self::parent();
        //echo 'hello'; exit;
        $strplan = Plan::where('braintree_plan', '=', 'registration')->first();

        return view('user_reg', compact('strplan'));
    }


    public function store(Request $request)
    {


        $this->validate($request, [
            'name' => 'required|min:2|max:255',
            'email' => 'required',
            'password' => 'required|string|min:4|confirmed',
            'password_confirmation' => 'required_with:password|same:password|min:4'
        ]);


        $user = User::where('email', '=', $request['email'])->first();

        if ($user === null) {

            $user = User::create([
                'name' => $request['name'],
                'email' => $request['email'],
                'password' => Hash::make($request['password']),
                'user_address' => $request['user_address'],
                'company_name' => $request['company_name'],
                'contact' => $request['contact'],
                'api_token' => $this->apiToken
            ]);

            Planholder::create([
                'company' =>  $request['company_name'],
                'planholder_address' => $request['user_address'],
                'planholder_email' => $request['email'],
                'planholder_contact' => $request['name'],
                'planholder_phone' => $request['contact'],
            ]);

            $user->assignRole('User');


            // if($data){


            // }else{abort(403, 'Unauthorized action.');}


             $checkuser = $user->createAsBraintreeCustomer($request->payment_method_nonce)/*->create( $request->payment_method_nonce , array())*/;

//
            //var_dump($checkuser); exit;
           // $checkuser =  Braintree_Transaction::sale([ 'amount' => '00' , 'paymentMethodNonce' => $request->payment_method_nonce]);
//
//
//             $user->createAsBraintreeCustomer($request->payment_method_nonce);
//
//             $strprices = DB::select('SELECT price FROM register_charges WHERE id = 1');


            //$user->newSubscription('reg', 'registration')->create($request->payment_method_nonce);
            // $user->charge($strprices[0]->price);


            if(env('MAIL_SEND')) {

                $user->sendEmailVerificationNotification();
                /*$contactName = $user->name;
                $contactEmail = $user->email;


                $data = array('name' => $contactName);
                Mail::send('emails.mail', $data, function ($message) use ($contactEmail, $contactName) {
                     $message->to($contactEmail, $contactName)->subject
                     ('User Registered');

                 });*/
            }

            if($request->is('api/*')){
                return response()->json([
                    'message' => 'User Created Successfully, please check your email and activate your account.',
                    'access_token' => $this->apiToken]);
            }
            return redirect()->route('user_l')->with('alertone', 'User Created Successfully, please check your email and activate your account.');

        } else {
            if($request->is('api/*')){
                return response()->json([
                    'message' => 'User Already Exist!']);
            }
            return redirect()->route('r_user')->with('alertone', 'User Already Exist!');
        }

    }

    public function user_login(){

        return view('user_login');
    }


    public function user_store(Request $request){

        $this->validate($request, [
            'email' => 'required',
            'password' => 'required|string',
            //'status'=> 1
        ]);

        $credentials = $request->only('email', 'password', 'status');

        if (Auth::attempt($credentials)) {

//            $postArray = ['api_token' => $this->apiToken];
//            $login = User::where('email',$request->email)->update($postArray);
            $user = User::where('email',$request->email)->first();
            //var_dump(Request::server('HTTP_REFERER')); exit;
            $redirect_link = Cookie::get('redirect_cookie');

            if(!empty($redirect_link)){
                return redirect()->intended($redirect_link . '#qtytoff');
            }else{
//                if($request->is('api/*')){
//                    return response()->json([
//                        'name' => $user->name,
//                        'email' => $user->email,
//                        'access_token' => $this->apiToken]);
//                }
                return redirect()->intended('dashboard');
            }

        } else {
            if($request->is('api/*')){
                return ['message' => 'Invalid Email Or Password'];
            }
            return redirect()->route('user_l')->with('alerttwo', 'Invalid Email Or Password');
        }
    }

}
<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Spatie\Permission\Models\Role;
use App\Subscription;
use App\Plan;
use Visitor;
use Carbon\Carbon; 

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {   
        



        if(!empty($request->monthcheck)){ 


            $time = strtotime($request->monthcheck);
             
            $currentMonth  = date('m', $time);

        }else{
            $currentMonth = date('m');
        }
         
          $totalmonthlysale = 0;

          $btpid =  Subscription::select('braintree_plan','created_at')->whereRaw('MONTH(created_at) = ?', $currentMonth)
            ->get();

         
          
          foreach ($btpid as $strbtpid) {


            $btpcost =  Plan::select('cost')->where('braintree_plan','=', $strbtpid->braintree_plan)->first();

            $totalmonthlysale = $totalmonthlysale + (int)$btpcost['cost'];

              
          }



        $btpidd =  Subscription::select('braintree_plan')->get();

         $totalsale = 0;
              
          
          foreach ($btpidd as $strbtpidd) {


            $btpcostt =  Plan::select('cost')->where('braintree_plan','=', $strbtpidd->braintree_plan)->first();



            $totalsale = is_object($btpcostt) ? $totalsale + (int)$btpcostt->cost : $totalsale;
              
          }

          






           $downcount = DB::table('user_project_record')->select(DB::raw('count(project_id) as pd'),'project_title','project_id')->groupBy('project_title','project_id')->orderBy('pd', 'desc')->get();

           $downcounttwo = DB::table('user_project_record')->select(DB::raw('count(*) as tpd'))->get();

            $strdc = $downcounttwo->pop();

            $totalvisitors = Visitor::count(); 

                        $visiternow = array();
                        $visiterdate = array();

                        for($i= 0; $i < 7 ; $i++){

                        $date = Carbon::now();
                            $ii = 0-$i;

                            $ii= $ii.' day';
                            
                        $date->modify( $ii );

                        $dateone = $date->format('Y-m-d');

                        $visiterdate[$i] = $date->format('Y-m-d');
                        
                        
                        $visiternow[$i] = DB::table('visitor_registry')->select('ip','created_at')->whereRaw('Date(created_at) = ?', $dateone)->count();
                        
                        }





                
            // $t = Carbon::now();
            

            //  $t->format('l');

          
            
                $role = auth()->user()->roles; 

                
                $checkrole = $role[0]->name;


            if(!Auth::guest() && $checkrole == "DEO"){

                return redirect()->route('pro_view');
               }
            else if(!Auth::guest() && $checkrole == "QTO"){

               return redirect()->route('qto_view');

            }else{
            

        return view('home',compact('downcount','strdc','totalsale','totalmonthlysale','totalvisitors','visiternow','visiterdate'));

        }
    }
}

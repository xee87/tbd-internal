<?php

namespace App\Http\Middleware;

use Auth;
use Closure;
use Spatie\Permission\Models\Role;
use App\User;

class If_User
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $id = Auth::id();
        $user = User::find($id);
        $role = $user->getRoleNames();
        if($role->pop() === 'User'){
            return redirect()->route('landing');
        };
        return $next($request);
    }
}
